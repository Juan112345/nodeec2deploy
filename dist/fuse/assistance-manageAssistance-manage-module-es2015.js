(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["assistance-manageAssistance-manage-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.html":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.html ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"mat-elevation-z8 width-card\">\n    <mat-card-content [formGroup]=\"formGroup\">\n        <div  id=\"assistance-search-manage\" >\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"titulo\">\n                <h1>Registrar Asitencias</h1>\n            </div>\n\n            <div id=\"agregar\">\n                <button mat-button class=\"button-color\" mat-raised-button  aria-label=\"AGREGAR\">\n                    Regresar\n                </button>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"empleado\" style=\"width: 100%;\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Empleado</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">account_circle</mat-icon>\n                    <mat-select name=\"empleado\" formControlName=\"empleado\" required>\n                        <mat-option value=\"0\">-Seleccione-</mat-option>\n                        <mat-option value=\"Clinsman Campos\">Clinsman Campos</mat-option>\n                        <mat-option value=\"Juan Balboa\">Juan Balboa</mat-option>\n                        <mat-option value=\"Cris Martinez\">Cris Martinez</mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n\n            <h1>Horas Acumuladas</h1>\n\n            <div class=\"third-row-assistance\">\n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas a favor</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"100\" name=\"horaAFavor\" value=\"56\" formControlName=\"horaFavor\" matInput>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas en contra</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"100\" name=\"horaEnContra\" value=\"0\" formControlName=\"horaContra\" matInput>\n                    </mat-form-field>\n            </div>\n\n            <div class=\"first-row-assistance\">\n                <mat-form-field appearance=\"outline\" id=\"campos\">\n                    <mat-label>Tipo de Registro</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">account_circle</mat-icon>\n                    <!--<input name=\"name\" formControlName=\"empleado\" matInput required>-->\n                    <mat-select name=\"empleado\" formControlName=\"empleado\" required (selectionChange)=\"tipoRegistro($event.value)\">\n                        <mat-option value=\"0\">-Seleccione-</mat-option>\n                        <mat-option value=\"1\">Individual</mat-option>\n                        <mat-option value=\"2\">Multiple</mat-option>\n                    </mat-select>\n                </mat-form-field>\n\n                <mat-form-field appearance=\"outline\" id=\"campos\">\n                    <mat-label>Día Asistencia</mat-label>\n                    <input matInput [matDatepicker]=\"assistanceDatePicker\" formControlName=\"diaAsistencia\"\n                        name=\"diaDeAsistencia\">\n                    <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePicker\"></mat-datepicker-toggle>\n                    <mat-datepicker #assistanceDatePicker></mat-datepicker>\n                </mat-form-field>\n            </div>\n\n            <div class=\"second-row-assistance\">\n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Hora de Entrada</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"24\" name=\"horaDeEntrada\" formControlName=\"horaEntrada\" matInput>\n                        <span matSuffix>am</span>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Hora de Salida</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"24\" name=\"horaDeSalida\" formControlName=\"horaSalida\" matInput>\n                        <span matSuffix>pm</span>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas Trabajadas</mat-label>\n                        <mat-icon matSuffix class=\"secondary-text\">home</mat-icon>\n                        <input type=\"number\" min=\"1\" max=\"24\" name=\"horasTrabajadas\" formControlName=\"horasTrabajadas\" matInput>\n                    </mat-form-field>\n            </div>\n\n            <div>\n                <mat-form-field appearance=\"outline\" id=\"text-area-width\">\n                    <mat-label>Comentario</mat-label>\n                    <textarea matInput cdkTextareaAutosize\n                    #autosize=\"cdkTextareaAutosize\"\n                    cdkAutosizeMinRows=\"1\"\n                    cdkAutosizeMaxRows=\"6\" name=\"comentario\" formControlName=\"comentario\" type=\"text\"></textarea>\n                </mat-form-field>\n            </div>\n            <div class=\"button-padding\" *ngIf=\"registroMultiple == true\">\n                <button mat-button class=\"button-color\"  aria-label=\"REGRESAR\" (click)=\"onSubmit()\">\n                    Agregar\n                </button>\n            </div>\n\n            <div class=\"example-container mat-elevation-z8\" *ngIf=\"registroMultiple == true\">\n                <table mat-table [dataSource]=\"dataSource\">\n              \n                  <!-- Position Column -->\n                  <ng-container matColumnDef=\"position\">\n                    <th mat-header-cell *matHeaderCellDef> Dia </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\n                  </ng-container>\n\n                  <!-- Empleado Column -->\n                  <ng-container matColumnDef=\"empleado\">\n                    <th mat-header-cell *matHeaderCellDef> Empleado </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.empleado}} </td>\n                  </ng-container>\n              \n                  <!-- Name Column -->\n                  <ng-container matColumnDef=\"name\">\n                    <th mat-header-cell *matHeaderCellDef> Hora Ingreso </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\n                  </ng-container>\n              \n                  <!-- Weight Column -->\n                  <ng-container matColumnDef=\"weight\">\n                    <th mat-header-cell *matHeaderCellDef> Hora Salida </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\n                  </ng-container>\n              \n                  <!-- Symbol Column -->\n                  <ng-container matColumnDef=\"symbol\">\n                    <th mat-header-cell *matHeaderCellDef> Horas Trabajadas </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\n                  </ng-container>\n\n                  <!-- Coment Column -->\n                  <ng-container matColumnDef=\"coment\">\n                    <th mat-header-cell *matHeaderCellDef> Comentario </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.coment}} </td>\n                  </ng-container>\n              \n                  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\n                  <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                </table>\n            </div>\n\n            <div class=\"button-manage\">\n                    <button mat-button class=\"button-color\" mat-raised-button aria-label=\"GUARDAR\">\n                        Guardar\n                    </button>\n    \n                    <button mat-button class=\"button-color\" mat-raised-button aria-label=\"LIMPIAR\">\n                        Limpiar\n                    </button>\n            </div>\n          </div>\n    </mat-card-content>\n</mat-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/manage.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/manage.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"mat-elevation-z8 width-card\">\n    <mat-card-content [formGroup]=\"formGroup\">\n        <div  id=\"assistance-search-manage\" >\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"titulo\">\n                <h1>Registrar Mis Asitencias</h1>\n            </div>\n\n            <div id=\"agregar\">\n                <button mat-button class=\"button-color\" mat-raised-button color=\"primary\" aria-label=\"AGREGAR\">\n                    Regresar\n                </button>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"empleado\" style=\"width: 100%;\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Empleado</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">account_circle</mat-icon>\n                    <mat-select name=\"empleado\" formControlName=\"empleado\" required>\n                        <mat-option value=\"0\">-Seleccione-</mat-option>\n                        <mat-option value=\"Clinsman Campos\">Clinsman Campos</mat-option>\n                        <mat-option value=\"Juan Balboa\">Juan Balboa</mat-option>\n                        <mat-option value=\"Cris Martinez\">Cris Martinez</mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n\n            <div class=\"first-row-assistance\">\n                <mat-form-field appearance=\"outline\" id=\"campos\">\n                    <mat-label>Tipo de Registro</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">account_circle</mat-icon>\n                    <!--<input name=\"name\" formControlName=\"empleado\" matInput required>-->\n                    <mat-select name=\"empleado\" formControlName=\"empleado\" required (selectionChange)=\"tipoRegistro($event.value)\">\n                        <mat-option value=\"0\">-Seleccione-</mat-option>\n                        <mat-option value=\"1\">Individual</mat-option>\n                        <mat-option value=\"2\">Multiple</mat-option>\n                    </mat-select>\n                </mat-form-field>\n\n                <mat-form-field appearance=\"outline\" id=\"campos\">\n                    <mat-label>Día Asistencia</mat-label>\n                    <input matInput [matDatepicker]=\"assistanceDatePicker\" formControlName=\"diaAsistencia\"\n                        name=\"diaDeAsistencia\">\n                    <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePicker\"></mat-datepicker-toggle>\n                    <mat-datepicker #assistanceDatePicker></mat-datepicker>\n                </mat-form-field>\n            </div>\n\n            <div class=\"second-row-assistance\">\n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Hora de Entrada</mat-label>\n                        <input  name=\"horaEntrada\" formControlName=\"horaEntrada\" matInput>\n                        <span matSuffix>am</span>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Hora de Salida</mat-label>\n                        <input name=\"horaSalida\" formControlName=\"horaSalida\" matInput>\n                        <span matSuffix>pm</span>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas Trabajadas</mat-label>\n                        <mat-icon matSuffix class=\"secondary-text\">home</mat-icon>\n                        <input name=\"horasTrabajadas\" formControlName=\"horasTrabajadas\" matInput>\n                    </mat-form-field>\n            </div>\n\n            <div>\n                <mat-form-field appearance=\"outline\" id=\"text-area-width\">\n                    <mat-label>Comentario</mat-label>\n                    <textarea matInput cdkTextareaAutosize\n                    #autosize=\"cdkTextareaAutosize\"\n                    cdkAutosizeMinRows=\"1\"\n                    cdkAutosizeMaxRows=\"6\" name=\"comentario\" formControlName=\"comentario\" type=\"text\"></textarea>\n                </mat-form-field>\n            </div>\n            <div class=\"button-padding\" *ngIf=\"registroMultiple == true\">\n                <button mat-button class=\"button-color\" mat-raised-button color=\"primary\" aria-label=\"REGRESAR\" (click)=\"onSubmit()\">\n                    Agregar\n                </button>\n            </div>\n            \n\n            <!--<h1>Horas Acumuladas</h1>\n\n            <div class=\"third-row-assistance\">\n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas a favor</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"100\" name=\"horaAFavor\" value=\"56\" formControlName=\"horaFavor\" matInput>\n                    </mat-form-field>\n    \n                    <mat-form-field appearance=\"outline\" id=\"campos\">\n                        <mat-label>Horas en contra</mat-label>\n                        <input type=\"number\" min=\"1\" max=\"100\" name=\"horaEnContra\" value=\"0\" formControlName=\"horaContra\" matInput>\n                    </mat-form-field>\n            </div>-->\n\n            <div class=\"example-container mat-elevation-z8\" *ngIf=\"registroMultiple == true\">\n                <table mat-table [dataSource]=\"dataSource\">\n              \n                  <!-- Position Column -->\n                  <ng-container matColumnDef=\"position\">\n                    <th mat-header-cell *matHeaderCellDef> Dia </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\n                  </ng-container>\n              \n                  <!-- Name Column -->\n                  <ng-container matColumnDef=\"name\">\n                    <th mat-header-cell *matHeaderCellDef> Hora Ingreso </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\n                  </ng-container>\n              \n                  <!-- Weight Column -->\n                  <ng-container matColumnDef=\"weight\">\n                    <th mat-header-cell *matHeaderCellDef> Hora Salida </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\n                  </ng-container>\n              \n                  <!-- Symbol Column -->\n                  <ng-container matColumnDef=\"symbol\">\n                    <th mat-header-cell *matHeaderCellDef> Horas Trabajadas </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\n                  </ng-container>\n\n                  <!-- Coment Column -->\n                  <ng-container matColumnDef=\"coment\">\n                    <th mat-header-cell *matHeaderCellDef> Comentario </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.coment}} </td>\n                  </ng-container>\n              \n                  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\n                  <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                </table>\n            </div>\n\n            <div class=\"button-manage\">\n                    <button mat-button class=\"button-color\" mat-raised-button color=\"primary\" aria-label=\"GUARDAR\" (click)='guardar()'>\n                        Guardar\n                    </button>\n    \n                    <button mat-button class=\"button-color\" mat-raised-button color=\"primary\" aria-label=\"LIMPIAR\">\n                        Limpiar\n                    </button>\n            </div>\n          </div>\n        <!--====================================================\n            <mat-card class=\"mat-elevation-z8 width-card\">\n    <div id=\"assistance-manage\" class=\"page-layout blank\">\n\n        <form [formGroup]=\"formGroup\">\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"asistencia\">\n                <h1>Asistencia</h1>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"empleado\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Empleado</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">account_circle</mat-icon>\n                    <!--<input name=\"name\" formControlName=\"empleado\" matInput required>\n                    <mat-select name=\"empleado\" formControlName=\"empleado\" required>\n                        <mat-option value=\"0\">-Seleccione-</mat-option>\n                        <mat-option value=\"1\">Clinsman Campos</mat-option>\n                        <mat-option value=\"2\">Juan Balboa</mat-option>\n                        <mat-option value=\"3\">Cris Martinez</mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"diaAsistencia\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Día Asistencia</mat-label>\n                    <input matInput [matDatepicker]=\"assistanceDatePicker\" formControlName=\"diaAsistencia\"\n                        name=\"diaDeAsistencia\">\n                    <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePicker\"></mat-datepicker-toggle>\n                    <mat-datepicker #assistanceDatePicker></mat-datepicker>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horaDeEntrada\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Hora de Entrada</mat-label>\n                    <input type=\"number\" min=\"1\" max=\"24\" name=\"horaDeEntrada\" formControlName=\"horaEntrada\" matInput>\n                    <span matSuffix>am</span>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horaDeSalida\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Hora de Salida</mat-label>\n                    <input type=\"number\" min=\"1\" max=\"24\" name=\"horaDeSalida\" formControlName=\"horaSalida\" matInput>\n                    <span matSuffix>pm</span>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horasTrabajadas\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Horas Trabajadas</mat-label>\n                    <mat-icon matSuffix class=\"secondary-text\">home</mat-icon>\n                    <input type=\"number\" min=\"1\" max=\"24\" name=\"horasTrabajadas\" formControlName=\"horasTrabajadas\" matInput>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" class=\"textarea-wrapper\" fxLayoutAlign=\"left start\" id=\"comentario\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Comentario</mat-label>\n                    <textarea name=\"comentario\" formControlName=\"comentario\" matInput type=\"text\" max-rows=\"20\"></textarea>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horasAcumuladas\">\n                <h1>Horas Acumuladas</h1>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horaAFavor\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Horas a favor</mat-label>\n                    <input type=\"number\" min=\"1\" max=\"100\" name=\"horaAFavor\" value=\"56\" formControlName=\"horaFavor\" matInput>\n                </mat-form-field>\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"left start\" id=\"horaEnContra\">\n                <mat-form-field appearance=\"outline\" fxFlex>\n                    <mat-label>Horas en contra</mat-label>\n                    <input type=\"number\" min=\"1\" max=\"100\" name=\"horaEnContra\" value=\"0\" formControlName=\"horaContra\" matInput>\n                </mat-form-field>\n            </div>\n\n            <div id=\"guardar\">\n                <button mat-button class=\"save-button\" mat-raised-button color=\"primary\" aria-label=\"GUARDAR\">\n                    Guardar\n                </button>\n            </div>\n\n            <div id=\"limpiar\">\n                <button mat-button class=\"delete-button\" mat-raised-button color=\"primary\" aria-label=\"LIMPIAR\">\n                    Limpiar\n                </button>\n            </div>\n\n            <div id=\"regresar\">\n                <button mat-button class=\"return-button\" mat-raised-button color=\"primary\" aria-label=\"REGRESAR\">\n                    Regresar\n                </button>\n            </div>\n\n        </form>\n\n    </div>\n</mat-card>\n        -->\n    </mat-card-content>\n</mat-card>");

/***/ }),

/***/ "./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.scss":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.scss ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".width-card {\n  margin: 20px;\n  width: 100%;\n}\n\n#assistance-search-manage {\n  padding-left: 15px;\n  padding-right: 15px;\n}\n\n#assistance-search-manage #titulo {\n  width: 80%;\n  float: left;\n  padding-right: 20%;\n}\n\n#assistance-search-manage #agregar {\n  float: right;\n  padding-top: 15px;\n}\n\n#assistance-search-manage #text-area-width {\n  width: 100%;\n}\n\ntextarea {\n  resize: none;\n}\n\n.button-manage {\n  padding-top: 20px;\n  width: 100%;\n  text-align: center;\n}\n\n.button-manage button {\n  margin-right: 5%;\n}\n\n.first-row-assistance {\n  display: flex;\n}\n\n.first-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n.second-row-assistance {\n  display: flex;\n}\n\n.second-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n.third-row-assistance {\n  display: flex;\n}\n\n.third-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n#campos {\n  width: 300px;\n}\n\n.example-container {\n  height: auto;\n  max-height: 400px;\n  overflow: auto;\n}\n\ntable {\n  width: 100%;\n}\n\n.button-padding {\n  padding-bottom: 15px;\n}\n\n.button-color {\n  background-color: #00a2ce !important;\n  color: white !important;\n}\n\n/*\nANTIGUO PORSIACA\n=========================================================================================================\n@import \"src/@fuse/scss/fuse\";\n.width-card {\n    margin: 20px;\n    width: 100%;\n}\n#assistance-manage {\n\n    padding-left: 15px;\n    padding-right: 15px;\n\n    #asistencia{\n        width: 100%;\n        float: left;\n    }\n\n    #empleado{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #diaAsistencia{\n        width: 45%;\n        float: left;\n        padding-right: 13%;\n    }\n\n    #horaDeEntrada{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horaDeSalida{\n        width: 33%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horasTrabajadas{\n        width: 33%;\n        float: left;\n    }\n\n    #comentario{\n        width: 66%;\n        float: left;\n    }\n\n    textarea {\n        resize: none;\n        height: 120px;\n    }\n\n    #horasAcumuladas{\n        width: 100%;\n        float: left;\n    }\n\n    #horaAFavor{\n        width: 35%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horaEnContra{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #guardar{\n        width: 20%;\n        float: left;\n        padding-left: 26%;\n        padding-right: 20%;\n        padding-bottom: 2%;\n    }\n\n    #limpiar{\n        width: 20%;\n        float: left;\n        padding-right: 20%;\n        padding-bottom: 2%;\n    }\n\n    #regresar{\n        width: 20%;\n        float: left;\n        padding-bottom: 2%;\n    }\n\n}\n\n*/\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9hcHBzL2Fzc2lzdGFuY2UvbWFuYWdlQXNzaXN0YW5jZS9hZG1pbi1tYW5hZ2UtYXNzaXN0YW5jZS9DOlxcVXNlcnNcXG11ZXJ0XFxnaXRcXGF3cy1iaXRidWNrZXQtZnJvbnQtcGFuZG9yYS9zcmNcXGFwcFxcbWFpblxcYXBwc1xcYXNzaXN0YW5jZVxcbWFuYWdlQXNzaXN0YW5jZVxcYWRtaW4tbWFuYWdlLWFzc2lzdGFuY2VcXGFkbWluLW1hbmFnZS1hc3Npc3RhbmNlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tYWluL2FwcHMvYXNzaXN0YW5jZS9tYW5hZ2VBc3Npc3RhbmNlL2FkbWluLW1hbmFnZS1hc3Npc3RhbmNlL2FkbWluLW1hbmFnZS1hc3Npc3RhbmNlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNDSjs7QURDQTtFQUVJLGtCQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURDSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNDUjs7QURFSTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtBQ0FSOztBREdJO0VBQ0ksV0FBQTtBQ0RSOztBRE1BO0VBQ0ksWUFBQTtBQ0hKOztBRE1BO0VBQ0ksaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNISjs7QURJSTtFQUNJLGdCQUFBO0FDRlI7O0FETUE7RUFDSSxhQUFBO0FDSEo7O0FESUk7RUFDSSxpQkFBQTtBQ0ZSOztBRE1BO0VBQ0ksYUFBQTtBQ0hKOztBRElJO0VBQ0ksaUJBQUE7QUNGUjs7QURNQTtFQUNJLGFBQUE7QUNISjs7QURJSTtFQUNJLGlCQUFBO0FDRlI7O0FETUE7RUFDSSxZQUFBO0FDSEo7O0FET0E7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDSko7O0FET0U7RUFDRSxXQUFBO0FDSko7O0FETUU7RUFDRSxvQkFBQTtBQ0hKOztBRE9FO0VBQ0Usb0NBQUE7RUFDQSx1QkFBQTtBQ0pKOztBRE1BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBQUEiLCJmaWxlIjoic3JjL2FwcC9tYWluL2FwcHMvYXNzaXN0YW5jZS9tYW5hZ2VBc3Npc3RhbmNlL2FkbWluLW1hbmFnZS1hc3Npc3RhbmNlL2FkbWluLW1hbmFnZS1hc3Npc3RhbmNlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpZHRoLWNhcmQge1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuI2Fzc2lzdGFuY2Utc2VhcmNoLW1hbmFnZSB7XHJcblxyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuXHJcbiAgICAjdGl0dWxve1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMjAlO1xyXG4gICAgfVxyXG5cclxuICAgICNhZ3JlZ2Fye1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgIH1cclxuXHJcbiAgICAjdGV4dC1hcmVhLXdpZHRoIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbnRleHRhcmVhIHtcclxuICAgIHJlc2l6ZTogbm9uZTtcclxufVxyXG5cclxuLmJ1dHRvbi1tYW5hZ2Uge1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1JTtcclxuICAgIH1cclxufVxyXG5cclxuLmZpcnN0LXJvdy1hc3Npc3RhbmNlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogNSU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zZWNvbmQtcm93LWFzc2lzdGFuY2Uge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcclxuICAgIH1cclxufVxyXG5cclxuLnRoaXJkLXJvdy1hc3Npc3RhbmNlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogNSU7XHJcbiAgICB9XHJcbn1cclxuXHJcbiNjYW1wb3N7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbn1cclxuLy8gdGFibGFcclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICBtYXgtaGVpZ2h0OiA0MDBweDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLmJ1dHRvbi1wYWRkaW5ne1xyXG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XHJcbiAgfVxyXG5cclxuICBcclxuICAuYnV0dG9uLWNvbG9ye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDE2MiwgMjA2KSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4vKlxyXG5BTlRJR1VPIFBPUlNJQUNBXHJcbj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5AaW1wb3J0IFwic3JjL0BmdXNlL3Njc3MvZnVzZVwiO1xyXG4ud2lkdGgtY2FyZCB7XHJcbiAgICBtYXJnaW46IDIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG4jYXNzaXN0YW5jZS1tYW5hZ2Uge1xyXG5cclxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcblxyXG4gICAgI2FzaXN0ZW5jaWF7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgI2VtcGxlYWRve1xyXG4gICAgICAgIHdpZHRoOiAzNCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAjZGlhQXNpc3RlbmNpYXtcclxuICAgICAgICB3aWR0aDogNDUlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEzJTtcclxuICAgIH1cclxuXHJcbiAgICAjaG9yYURlRW50cmFkYXtcclxuICAgICAgICB3aWR0aDogMzQlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2hvcmFEZVNhbGlkYXtcclxuICAgICAgICB3aWR0aDogMzMlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2hvcmFzVHJhYmFqYWRhc3tcclxuICAgICAgICB3aWR0aDogMzMlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG5cclxuICAgICNjb21lbnRhcmlve1xyXG4gICAgICAgIHdpZHRoOiA2NiU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgdGV4dGFyZWEge1xyXG4gICAgICAgIHJlc2l6ZTogbm9uZTtcclxuICAgICAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNob3Jhc0FjdW11bGFkYXN7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgI2hvcmFBRmF2b3J7XHJcbiAgICAgICAgd2lkdGg6IDM1JTtcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNob3JhRW5Db250cmF7XHJcbiAgICAgICAgd2lkdGg6IDM0JTtcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNndWFyZGFye1xyXG4gICAgICAgIHdpZHRoOiAyMCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyNiU7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMjAlO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcclxuICAgIH1cclxuXHJcbiAgICAjbGltcGlhcntcclxuICAgICAgICB3aWR0aDogMjAlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMiU7XHJcbiAgICB9XHJcblxyXG4gICAgI3JlZ3Jlc2Fye1xyXG4gICAgICAgIHdpZHRoOiAyMCU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDIlO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuKi8iLCIud2lkdGgtY2FyZCB7XG4gIG1hcmdpbjogMjBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbiNhc3Npc3RhbmNlLXNlYXJjaC1tYW5hZ2Uge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG59XG4jYXNzaXN0YW5jZS1zZWFyY2gtbWFuYWdlICN0aXR1bG8ge1xuICB3aWR0aDogODAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1yaWdodDogMjAlO1xufVxuI2Fzc2lzdGFuY2Utc2VhcmNoLW1hbmFnZSAjYWdyZWdhciB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG59XG4jYXNzaXN0YW5jZS1zZWFyY2gtbWFuYWdlICN0ZXh0LWFyZWEtd2lkdGgge1xuICB3aWR0aDogMTAwJTtcbn1cblxudGV4dGFyZWEge1xuICByZXNpemU6IG5vbmU7XG59XG5cbi5idXR0b24tbWFuYWdlIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYnV0dG9uLW1hbmFnZSBidXR0b24ge1xuICBtYXJnaW4tcmlnaHQ6IDUlO1xufVxuXG4uZmlyc3Qtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmZpcnN0LXJvdy1hc3Npc3RhbmNlIG1hdC1mb3JtLWZpZWxkIHtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG5cbi5zZWNvbmQtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnNlY29uZC1yb3ctYXNzaXN0YW5jZSBtYXQtZm9ybS1maWVsZCB7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuXG4udGhpcmQtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnRoaXJkLXJvdy1hc3Npc3RhbmNlIG1hdC1mb3JtLWZpZWxkIHtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG5cbiNjYW1wb3Mge1xuICB3aWR0aDogMzAwcHg7XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGhlaWdodDogYXV0bztcbiAgbWF4LWhlaWdodDogNDAwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYnV0dG9uLXBhZGRpbmcge1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLmJ1dHRvbi1jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMGEyY2UgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbi8qXG5BTlRJR1VPIFBPUlNJQUNBXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbkBpbXBvcnQgXCJzcmMvQGZ1c2Uvc2Nzcy9mdXNlXCI7XG4ud2lkdGgtY2FyZCB7XG4gICAgbWFyZ2luOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuI2Fzc2lzdGFuY2UtbWFuYWdlIHtcblxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuXG4gICAgI2FzaXN0ZW5jaWF7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjZW1wbGVhZG97XG4gICAgICAgIHdpZHRoOiAzNCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIH1cblxuICAgICNkaWFBc2lzdGVuY2lhe1xuICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTMlO1xuICAgIH1cblxuICAgICNob3JhRGVFbnRyYWRhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYURlU2FsaWRhe1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYXNUcmFiYWphZGFze1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjY29tZW50YXJpb3tcbiAgICAgICAgd2lkdGg6IDY2JTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgfVxuXG4gICAgdGV4dGFyZWEge1xuICAgICAgICByZXNpemU6IG5vbmU7XG4gICAgICAgIGhlaWdodDogMTIwcHg7XG4gICAgfVxuXG4gICAgI2hvcmFzQWN1bXVsYWRhc3tcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgIH1cblxuICAgICNob3JhQUZhdm9ye1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYUVuQ29udHJhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjZ3VhcmRhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMjYlO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbiAgICAjbGltcGlhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDIlO1xuICAgIH1cblxuICAgICNyZWdyZXNhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbn1cblxuKi8iXX0= */");

/***/ }),

/***/ "./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.ts ***!
  \********************************************************************************************************************/
/*! exports provided: AdminManageAssistanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminManageAssistanceComponent", function() { return AdminManageAssistanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");





const ELEMENT_DATA = [
    { position: '2020-07-10', name: '9am', empleado: 'Reynaldo', weight: '6pm', symbol: '8', coment: 'Puede mejorar.' },
    { position: '2020-07-10', name: '9am', empleado: 'Patty', weight: '6pm', symbol: '6', coment: 'Caso perdido.' },
];
let AdminManageAssistanceComponent = class AdminManageAssistanceComponent {
    constructor(_ngZone, formBuilder) {
        this._ngZone = _ngZone;
        this.formBuilder = formBuilder;
        this.displayedColumns = ['position', 'empleado', 'name', 'weight', 'symbol', 'coment'];
        this.dataSource = ELEMENT_DATA;
        this.registroMultiple = false;
    }
    triggerResize() {
        // Wait for changes to be applied, then trigger textarea resize.
        this._ngZone.onStable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1))
            .subscribe(() => this.autosize.resizeToFitContent(true));
    }
    ngOnInit() {
        this.buildForm();
    }
    buildForm() {
        const dateLength = 10;
        const today = new Date().toISOString().substring(0, dateLength);
        const empleado = '1';
        const horaEntrada = '9';
        const horaEntradaDia = 'am';
        const horaSalida = '18';
        const horaSalidaDia = 'pm';
        const horasTrabajadas = '8.30';
        const comentario = 'Asumio todas sus actividades con total normalidad.';
        /*const name = 'CLINSMAN CAMPOS';*/
        this.formGroup = this.formBuilder.group({
            /*empleado: name.toLowerCase()*/
            empleado: empleado,
            diaAsistencia: today,
            horaEntrada: horaEntrada,
            horaEntradaDia: horaEntradaDia,
            horaSalida: horaSalida,
            horaSalidaDia: horaSalidaDia,
            horasTrabajadas: horasTrabajadas,
            comentario: comentario
        });
    }
    onSubmit() {
        console.log(this.formGroup.value.comentario);
        const objeto = {
            position: `${this.formGroup.value.diaAsistencia}`,
            name: `${this.formGroup.value.horaEntrada}${this.formGroup.value.horaEntradaDia}`,
            empleado: `${this.formGroup.value.empleado}`,
            weight: `${this.formGroup.value.horaSalida}${this.formGroup.value.horaSalidaDia}`,
            symbol: `${this.formGroup.value.horasTrabajadas}`,
            coment: `${this.formGroup.value.comentario}`
        };
        ELEMENT_DATA.push(objeto);
        this.table.renderRows();
    }
    tipoRegistro(filterValue) {
        if (filterValue.trim().toLowerCase() == '2') {
            this.registroMultiple = true;
        }
        else {
            this.registroMultiple = false;
        }
    }
};
AdminManageAssistanceComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('autosize', { static: false })
], AdminManageAssistanceComponent.prototype, "autosize", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTable"], { static: false })
], AdminManageAssistanceComponent.prototype, "table", void 0);
AdminManageAssistanceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-admin-manage-assistance',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./admin-manage-assistance.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./admin-manage-assistance.component.scss */ "./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.scss")).default]
    })
], AdminManageAssistanceComponent);



/***/ }),

/***/ "./src/app/main/apps/assistance/manageAssistance/manage.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/main/apps/assistance/manageAssistance/manage.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".width-card {\n  margin: 20px;\n  width: 100%;\n}\n\n#assistance-search-manage {\n  padding-left: 15px;\n  padding-right: 15px;\n}\n\n#assistance-search-manage #titulo {\n  width: 80%;\n  float: left;\n  padding-right: 20%;\n}\n\n#assistance-search-manage #agregar {\n  float: right;\n  padding-top: 15px;\n}\n\n#assistance-search-manage #text-area-width {\n  width: 100%;\n}\n\ntextarea {\n  resize: none;\n}\n\n.button-manage {\n  padding-top: 20px;\n  width: 100%;\n  text-align: center;\n}\n\n.button-manage button {\n  margin-right: 5%;\n}\n\n.first-row-assistance {\n  display: flex;\n}\n\n.first-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n.second-row-assistance {\n  display: flex;\n}\n\n.second-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n.third-row-assistance {\n  display: flex;\n}\n\n.third-row-assistance mat-form-field {\n  padding-right: 5%;\n}\n\n#campos {\n  width: 300px;\n}\n\n.example-container {\n  height: auto;\n  max-height: 400px;\n  overflow: auto;\n}\n\ntable {\n  width: 100%;\n}\n\n.button-padding {\n  padding-bottom: 15px;\n}\n\n.button-color {\n  background-color: #00a2ce !important;\n  color: white !important;\n}\n\n/*\nANTIGUO PORSIACA\n=========================================================================================================\n@import \"src/@fuse/scss/fuse\";\n.width-card {\n    margin: 20px;\n    width: 100%;\n}\n#assistance-manage {\n\n    padding-left: 15px;\n    padding-right: 15px;\n\n    #asistencia{\n        width: 100%;\n        float: left;\n    }\n\n    #empleado{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #diaAsistencia{\n        width: 45%;\n        float: left;\n        padding-right: 13%;\n    }\n\n    #horaDeEntrada{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horaDeSalida{\n        width: 33%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horasTrabajadas{\n        width: 33%;\n        float: left;\n    }\n\n    #comentario{\n        width: 66%;\n        float: left;\n    }\n\n    textarea {\n        resize: none;\n        height: 120px;\n    }\n\n    #horasAcumuladas{\n        width: 100%;\n        float: left;\n    }\n\n    #horaAFavor{\n        width: 35%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #horaEnContra{\n        width: 34%;\n        float: left;\n        padding-right: 10px;\n    }\n\n    #guardar{\n        width: 20%;\n        float: left;\n        padding-left: 26%;\n        padding-right: 20%;\n        padding-bottom: 2%;\n    }\n\n    #limpiar{\n        width: 20%;\n        float: left;\n        padding-right: 20%;\n        padding-bottom: 2%;\n    }\n\n    #regresar{\n        width: 20%;\n        float: left;\n        padding-bottom: 2%;\n    }\n\n}\n\n*/\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9hcHBzL2Fzc2lzdGFuY2UvbWFuYWdlQXNzaXN0YW5jZS9DOlxcVXNlcnNcXG11ZXJ0XFxnaXRcXGF3cy1iaXRidWNrZXQtZnJvbnQtcGFuZG9yYS9zcmNcXGFwcFxcbWFpblxcYXBwc1xcYXNzaXN0YW5jZVxcbWFuYWdlQXNzaXN0YW5jZVxcbWFuYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tYWluL2FwcHMvYXNzaXN0YW5jZS9tYW5hZ2VBc3Npc3RhbmNlL21hbmFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FDQ0o7O0FEQ0E7RUFFSSxrQkFBQTtFQUNBLG1CQUFBO0FDQ0o7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDQ1I7O0FERUk7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUNBUjs7QURHSTtFQUNJLFdBQUE7QUNEUjs7QURNQTtFQUNJLFlBQUE7QUNISjs7QURNQTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDSEo7O0FESUk7RUFDSSxnQkFBQTtBQ0ZSOztBRE1BO0VBQ0ksYUFBQTtBQ0hKOztBRElJO0VBQ0ksaUJBQUE7QUNGUjs7QURNQTtFQUNJLGFBQUE7QUNISjs7QURJSTtFQUNJLGlCQUFBO0FDRlI7O0FETUE7RUFDSSxhQUFBO0FDSEo7O0FESUk7RUFDSSxpQkFBQTtBQ0ZSOztBRE1BO0VBQ0ksWUFBQTtBQ0hKOztBRE9BO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0pKOztBRE9FO0VBQ0UsV0FBQTtBQ0pKOztBRE1FO0VBQ0Usb0JBQUE7QUNISjs7QURNRTtFQUNFLG9DQUFBO0VBQ0EsdUJBQUE7QUNISjs7QURNQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQUFBIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9hcHBzL2Fzc2lzdGFuY2UvbWFuYWdlQXNzaXN0YW5jZS9tYW5hZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2lkdGgtY2FyZCB7XG4gICAgbWFyZ2luOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuI2Fzc2lzdGFuY2Utc2VhcmNoLW1hbmFnZSB7XG5cbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcblxuICAgICN0aXR1bG97XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XG4gICAgfVxuXG4gICAgI2FncmVnYXJ7XG4gICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgfVxuXG4gICAgI3RleHQtYXJlYS13aWR0aCB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cblxufVxuXG50ZXh0YXJlYSB7XG4gICAgcmVzaXplOiBub25lO1xufVxuXG4uYnV0dG9uLW1hbmFnZSB7XG4gICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJ1dHRvbiB7XG4gICAgICAgIG1hcmdpbi1yaWdodDogNSU7XG4gICAgfVxufVxuXG4uZmlyc3Qtcm93LWFzc2lzdGFuY2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICB9XG59XG5cbi5zZWNvbmQtcm93LWFzc2lzdGFuY2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWF0LWZvcm0tZmllbGQge1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICB9XG59XG5cbi50aGlyZC1yb3ctYXNzaXN0YW5jZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXQtZm9ybS1maWVsZCB7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIH1cbn1cblxuI2NhbXBvc3tcbiAgICB3aWR0aDogMzAwcHg7XG59XG4vLyB0YWJsYVxuXG4uZXhhbXBsZS1jb250YWluZXIge1xuICAgIGhlaWdodDogYXV0bztcbiAgICBtYXgtaGVpZ2h0OiA0MDBweDtcbiAgICBvdmVyZmxvdzogYXV0bztcbiAgfVxuICBcbiAgdGFibGUge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5idXR0b24tcGFkZGluZ3tcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgfVxuXG4gIC5idXR0b24tY29sb3J7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDE2MiwgMjA2KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICB9XG5cbi8qXG5BTlRJR1VPIFBPUlNJQUNBXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbkBpbXBvcnQgXCJzcmMvQGZ1c2Uvc2Nzcy9mdXNlXCI7XG4ud2lkdGgtY2FyZCB7XG4gICAgbWFyZ2luOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuI2Fzc2lzdGFuY2UtbWFuYWdlIHtcblxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuXG4gICAgI2FzaXN0ZW5jaWF7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjZW1wbGVhZG97XG4gICAgICAgIHdpZHRoOiAzNCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIH1cblxuICAgICNkaWFBc2lzdGVuY2lhe1xuICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTMlO1xuICAgIH1cblxuICAgICNob3JhRGVFbnRyYWRhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYURlU2FsaWRhe1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYXNUcmFiYWphZGFze1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjY29tZW50YXJpb3tcbiAgICAgICAgd2lkdGg6IDY2JTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgfVxuXG4gICAgdGV4dGFyZWEge1xuICAgICAgICByZXNpemU6IG5vbmU7XG4gICAgICAgIGhlaWdodDogMTIwcHg7XG4gICAgfVxuXG4gICAgI2hvcmFzQWN1bXVsYWRhc3tcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgIH1cblxuICAgICNob3JhQUZhdm9ye1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYUVuQ29udHJhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjZ3VhcmRhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMjYlO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbiAgICAjbGltcGlhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDIlO1xuICAgIH1cblxuICAgICNyZWdyZXNhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbn1cblxuKi8iLCIud2lkdGgtY2FyZCB7XG4gIG1hcmdpbjogMjBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbiNhc3Npc3RhbmNlLXNlYXJjaC1tYW5hZ2Uge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG59XG4jYXNzaXN0YW5jZS1zZWFyY2gtbWFuYWdlICN0aXR1bG8ge1xuICB3aWR0aDogODAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1yaWdodDogMjAlO1xufVxuI2Fzc2lzdGFuY2Utc2VhcmNoLW1hbmFnZSAjYWdyZWdhciB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG59XG4jYXNzaXN0YW5jZS1zZWFyY2gtbWFuYWdlICN0ZXh0LWFyZWEtd2lkdGgge1xuICB3aWR0aDogMTAwJTtcbn1cblxudGV4dGFyZWEge1xuICByZXNpemU6IG5vbmU7XG59XG5cbi5idXR0b24tbWFuYWdlIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYnV0dG9uLW1hbmFnZSBidXR0b24ge1xuICBtYXJnaW4tcmlnaHQ6IDUlO1xufVxuXG4uZmlyc3Qtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmZpcnN0LXJvdy1hc3Npc3RhbmNlIG1hdC1mb3JtLWZpZWxkIHtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG5cbi5zZWNvbmQtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnNlY29uZC1yb3ctYXNzaXN0YW5jZSBtYXQtZm9ybS1maWVsZCB7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuXG4udGhpcmQtcm93LWFzc2lzdGFuY2Uge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnRoaXJkLXJvdy1hc3Npc3RhbmNlIG1hdC1mb3JtLWZpZWxkIHtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG5cbiNjYW1wb3Mge1xuICB3aWR0aDogMzAwcHg7XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGhlaWdodDogYXV0bztcbiAgbWF4LWhlaWdodDogNDAwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYnV0dG9uLXBhZGRpbmcge1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLmJ1dHRvbi1jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMGEyY2UgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbi8qXG5BTlRJR1VPIFBPUlNJQUNBXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbkBpbXBvcnQgXCJzcmMvQGZ1c2Uvc2Nzcy9mdXNlXCI7XG4ud2lkdGgtY2FyZCB7XG4gICAgbWFyZ2luOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuI2Fzc2lzdGFuY2UtbWFuYWdlIHtcblxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuXG4gICAgI2FzaXN0ZW5jaWF7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjZW1wbGVhZG97XG4gICAgICAgIHdpZHRoOiAzNCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIH1cblxuICAgICNkaWFBc2lzdGVuY2lhe1xuICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTMlO1xuICAgIH1cblxuICAgICNob3JhRGVFbnRyYWRhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYURlU2FsaWRhe1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYXNUcmFiYWphZGFze1xuICAgICAgICB3aWR0aDogMzMlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICB9XG5cbiAgICAjY29tZW50YXJpb3tcbiAgICAgICAgd2lkdGg6IDY2JTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgfVxuXG4gICAgdGV4dGFyZWEge1xuICAgICAgICByZXNpemU6IG5vbmU7XG4gICAgICAgIGhlaWdodDogMTIwcHg7XG4gICAgfVxuXG4gICAgI2hvcmFzQWN1bXVsYWRhc3tcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgIH1cblxuICAgICNob3JhQUZhdm9ye1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjaG9yYUVuQ29udHJhe1xuICAgICAgICB3aWR0aDogMzQlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICB9XG5cbiAgICAjZ3VhcmRhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMjYlO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMCU7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbiAgICAjbGltcGlhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDIlO1xuICAgIH1cblxuICAgICNyZWdyZXNhcntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICB9XG5cbn1cblxuKi8iXX0= */");

/***/ }),

/***/ "./src/app/main/apps/assistance/manageAssistance/manage.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/main/apps/assistance/manageAssistance/manage.component.ts ***!
  \***************************************************************************/
/*! exports provided: ManageAssistanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageAssistanceComponent", function() { return ManageAssistanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");





const ELEMENT_DATA = [
    { position: '2020-07-10', name: '9am', weight: '6pm', symbol: '8', coment: 'Puede mejorar.' },
    { position: '2020-07-10', name: '9am', weight: '5pm', symbol: '7', coment: 'En proceso.' },
];
let ManageAssistanceComponent = class ManageAssistanceComponent {
    constructor(_ngZone, formBuilder) {
        this._ngZone = _ngZone;
        this.formBuilder = formBuilder;
        this.displayedColumns = ['position', 'name', 'weight', 'symbol', 'coment'];
        this.dataSource = ELEMENT_DATA;
        this.registroMultiple = false;
        this.infoEntrada = new Date();
        this.infoSalida = new Date();
    }
    triggerResize() {
        // Wait for changes to be applied, then trigger textarea resize.
        this._ngZone.onStable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1))
            .subscribe(() => this.autosize.resizeToFitContent(true));
    }
    /**
     * On init
     */
    ngOnInit() {
        this.infoEntrada = new Date();
        this.buildForm();
    }
    buildForm() {
        const dateLength = 10;
        const today = new Date().toISOString().substring(0, dateLength);
        const empleado = '1';
        const horaEntrada = this.infoEntrada.getHours() + ':' + this.infoEntrada.getMinutes();
        const horaEntradaDia = 'am';
        const horaSalida = this.infoSalida.getHours() + ':' + this.infoSalida.getMinutes();
        const horaSalidaDia = 'pm';
        const horasTrabajadas = this.horasTrabajadasFinal;
        const comentario = 'Asumio todas sus actividades con total normalidad.';
        /*const name = 'CLINSMAN CAMPOS';*/
        this.formGroup = this.formBuilder.group({
            /*empleado: name.toLowerCase()*/
            empleado: empleado,
            diaAsistencia: today,
            horaEntrada: horaEntrada,
            horaEntradaDia: horaEntradaDia,
            horaSalida: horaSalida,
            horaSalidaDia: horaSalidaDia,
            horasTrabajadas: horasTrabajadas,
            comentario: comentario
        });
    }
    guardar() {
        if (this.formGroup.value.horaEntrada != '') {
            this.infoSalida = new Date();
            this.diffMs = (this.infoSalida - this.infoEntrada); // milliseconds between now & Christmas
            this.diffHrs = Math.floor((this.diffMs % 86400000) / 3600000); // hours
            this.diffMins = Math.round(((this.diffMs % 86400000) % 3600000) / 60000); // minutes
            this.horasTrabajadasFinal = this.diffHrs + ':' + this.diffMins;
            this.buildForm();
        }
    }
    onSubmit() {
        console.log(this.formGroup.value.comentario);
        const objeto = {
            position: `${this.formGroup.value.diaAsistencia}`,
            name: `${this.formGroup.value.horaEntrada}${this.formGroup.value.horaEntradaDia}`,
            weight: `${this.formGroup.value.horaSalida}${this.formGroup.value.horaSalidaDia}`,
            symbol: `${this.formGroup.value.horasTrabajadas}`,
            coment: `${this.formGroup.value.comentario}`
        };
        ELEMENT_DATA.push(objeto);
        this.table.renderRows();
    }
    tipoRegistro(filterValue) {
        if (filterValue.trim().toLowerCase() == '2') {
            this.registroMultiple = true;
        }
        else {
            this.registroMultiple = false;
        }
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
    }
};
ManageAssistanceComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('autosize', { static: false })
], ManageAssistanceComponent.prototype, "autosize", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTable"], { static: false })
], ManageAssistanceComponent.prototype, "table", void 0);
ManageAssistanceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'manage-assistance',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./manage.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/assistance/manageAssistance/manage.component.html")).default,
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./manage.component.scss */ "./src/app/main/apps/assistance/manageAssistance/manage.component.scss")).default]
    })
], ManageAssistanceComponent);



/***/ }),

/***/ "./src/app/main/apps/assistance/manageAssistance/manage.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/main/apps/assistance/manageAssistance/manage.module.ts ***!
  \************************************************************************/
/*! exports provided: ManageAssistanceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageAssistanceModule", function() { return ManageAssistanceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm2015/menu.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm2015/select.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm2015/tabs.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm2015/ng2-charts.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _fuse_shared_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fuse/shared.module */ "./src/@fuse/shared.module.ts");
/* harmony import */ var _fuse_components_widget_widget_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fuse/components/widget/widget.module */ "./src/@fuse/components/widget/widget.module.ts");
/* harmony import */ var _manage_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./manage.component */ "./src/app/main/apps/assistance/manageAssistance/manage.component.ts");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm2015/datepicker.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm2015/input.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _admin_manage_assistance_admin_manage_assistance_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./admin-manage-assistance/admin-manage-assistance.component */ "./src/app/main/apps/assistance/manageAssistance/admin-manage-assistance/admin-manage-assistance.component.ts");





















const routes = [
    {
        path: 'manageAssistanceComponent',
        component: _manage_component__WEBPACK_IMPORTED_MODULE_14__["ManageAssistanceComponent"]
    },
    {
        path: 'adminManageAssistanceComponent',
        component: _admin_manage_assistance_admin_manage_assistance_component__WEBPACK_IMPORTED_MODULE_20__["AdminManageAssistanceComponent"]
    }
];
let ManageAssistanceModule = class ManageAssistanceModule {
};
ManageAssistanceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _manage_component__WEBPACK_IMPORTED_MODULE_14__["ManageAssistanceComponent"],
            _admin_manage_assistance_admin_manage_assistance_component__WEBPACK_IMPORTED_MODULE_20__["AdminManageAssistanceComponent"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes),
            _angular_material_button__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_15__["MatDatepickerModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_17__["MatInputModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"],
            _angular_material_select__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_18__["MatCardModule"],
            _angular_material_table__WEBPACK_IMPORTED_MODULE_19__["MatTableModule"],
            _agm_core__WEBPACK_IMPORTED_MODULE_9__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
            }),
            ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"],
            _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_11__["NgxChartsModule"],
            _fuse_shared_module__WEBPACK_IMPORTED_MODULE_12__["FuseSharedModule"],
            _fuse_components_widget_widget_module__WEBPACK_IMPORTED_MODULE_13__["FuseWidgetModule"]
        ]
    })
], ManageAssistanceModule);



/***/ })

}]);
//# sourceMappingURL=assistance-manageAssistance-manage-module-es2015.js.map