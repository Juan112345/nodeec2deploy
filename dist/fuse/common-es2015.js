(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/common/rest/services/ParameterController.ts":
/*!*************************************************************!*\
  !*** ./src/app/common/rest/services/ParameterController.ts ***!
  \*************************************************************/
/*! exports provided: ParameterController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParameterController", function() { return ParameterController; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../oauth.service */ "./src/app/common/oauth.service.ts");




let ParameterController = class ParameterController {
    constructor(httpClient, oauth) {
        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/parametersResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
    }
    getListMaster(arg0) {
        const metodo = this.url + '/getListMaster';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    getListParameters(arg0) {
        const metodo = this.url + '/getListParameters';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    createParameter(arg0) {
        const metodo = this.url + '/createParameter';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    updateParameter(arg0) {
        const metodo = this.url + '/updateParameter';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
};
ParameterController.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"] }
];
ParameterController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ParameterController);



/***/ }),

/***/ "./src/app/common/rest/services/PermitsController.ts":
/*!***********************************************************!*\
  !*** ./src/app/common/rest/services/PermitsController.ts ***!
  \***********************************************************/
/*! exports provided: PermitsController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermitsController", function() { return PermitsController; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../oauth.service */ "./src/app/common/oauth.service.ts");




let PermitsController = class PermitsController {
    constructor(httpClient, oauth) {
        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/permitsResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
    }
    getListPermits(arg0) {
        const metodo = this.url + '/getListPermits';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    createPermits(arg0) {
        const metodo = this.url + '/createPermits';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    updatePermits(arg0) {
        const metodo = this.url + '/updatePermits';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    getListPermitsWithPermitsPk(arg0) {
        const metodo = this.url + '/getListPermitsWithPermitsPk';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
};
PermitsController.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"] }
];
PermitsController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], PermitsController);



/***/ }),

/***/ "./src/app/common/rest/services/VacationController.ts":
/*!************************************************************!*\
  !*** ./src/app/common/rest/services/VacationController.ts ***!
  \************************************************************/
/*! exports provided: VacationController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacationController", function() { return VacationController; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../oauth.service */ "./src/app/common/oauth.service.ts");




let VacationController = class VacationController {
    constructor(httpClient, oauth) {
        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/vacationResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
    }
    createVacation(arg0) {
        const metodo = this.url + '/createVacation';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    getListVacation(arg0) {
        const metodo = this.url + '/getListVacation';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    updateVacation(arg0) {
        const metodo = this.url + '/updateVacation';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
};
VacationController.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"] }
];
VacationController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], VacationController);



/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map