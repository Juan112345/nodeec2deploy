function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
  /***/
  "./src/app/common/rest/services/ParameterController.ts":
  /*!*************************************************************!*\
    !*** ./src/app/common/rest/services/ParameterController.ts ***!
    \*************************************************************/

  /*! exports provided: ParameterController */

  /***/
  function srcAppCommonRestServicesParameterControllerTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ParameterController", function () {
      return ParameterController;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../oauth.service */
    "./src/app/common/oauth.service.ts");

    var ParameterController = /*#__PURE__*/function () {
      function ParameterController(httpClient, oauth) {
        _classCallCheck(this, ParameterController);

        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/parametersResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
          'Content-Type': 'application/json'
        }); //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
      }

      _createClass(ParameterController, [{
        key: "getListMaster",
        value: function getListMaster(arg0) {
          var metodo = this.url + '/getListMaster'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "getListParameters",
        value: function getListParameters(arg0) {
          var metodo = this.url + '/getListParameters'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "createParameter",
        value: function createParameter(arg0) {
          var metodo = this.url + '/createParameter'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "updateParameter",
        value: function updateParameter(arg0) {
          var metodo = this.url + '/updateParameter'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }]);

      return ParameterController;
    }();

    ParameterController.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"]
      }];
    };

    ParameterController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], ParameterController);
    /***/
  },

  /***/
  "./src/app/common/rest/services/PermitsController.ts":
  /*!***********************************************************!*\
    !*** ./src/app/common/rest/services/PermitsController.ts ***!
    \***********************************************************/

  /*! exports provided: PermitsController */

  /***/
  function srcAppCommonRestServicesPermitsControllerTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PermitsController", function () {
      return PermitsController;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../oauth.service */
    "./src/app/common/oauth.service.ts");

    var PermitsController = /*#__PURE__*/function () {
      function PermitsController(httpClient, oauth) {
        _classCallCheck(this, PermitsController);

        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/permitsResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
          'Content-Type': 'application/json'
        }); //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
      }

      _createClass(PermitsController, [{
        key: "getListPermits",
        value: function getListPermits(arg0) {
          var metodo = this.url + '/getListPermits'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "createPermits",
        value: function createPermits(arg0) {
          var metodo = this.url + '/createPermits'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "updatePermits",
        value: function updatePermits(arg0) {
          var metodo = this.url + '/updatePermits'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "getListPermitsWithPermitsPk",
        value: function getListPermitsWithPermitsPk(arg0) {
          var metodo = this.url + '/getListPermitsWithPermitsPk'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }]);

      return PermitsController;
    }();

    PermitsController.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"]
      }];
    };

    PermitsController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], PermitsController);
    /***/
  },

  /***/
  "./src/app/common/rest/services/VacationController.ts":
  /*!************************************************************!*\
    !*** ./src/app/common/rest/services/VacationController.ts ***!
    \************************************************************/

  /*! exports provided: VacationController */

  /***/
  function srcAppCommonRestServicesVacationControllerTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VacationController", function () {
      return VacationController;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../oauth.service */
    "./src/app/common/oauth.service.ts");

    var VacationController = /*#__PURE__*/function () {
      function VacationController(httpClient, oauth) {
        _classCallCheck(this, VacationController);

        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/vacationResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
          'Content-Type': 'application/json'
        }); // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
      }

      _createClass(VacationController, [{
        key: "createVacation",
        value: function createVacation(arg0) {
          var metodo = this.url + '/createVacation'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "getListVacation",
        value: function getListVacation(arg0) {
          var metodo = this.url + '/getListVacation'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "updateVacation",
        value: function updateVacation(arg0) {
          var metodo = this.url + '/updateVacation'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }]);

      return VacationController;
    }();

    VacationController.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"]
      }];
    };

    VacationController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], VacationController);
    /***/
  }
}]);
//# sourceMappingURL=common-es5.js.map