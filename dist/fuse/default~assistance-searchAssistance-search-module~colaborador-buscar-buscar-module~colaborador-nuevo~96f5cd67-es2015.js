(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~assistance-searchAssistance-search-module~colaborador-buscar-buscar-module~colaborador-nuevo~96f5cd67"],{

/***/ "./src/app/common/oauth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/common/oauth.service.ts ***!
  \*****************************************/
/*! exports provided: OAuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAuthService", function() { return OAuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let OAuthService = class OAuthService {
    constructor(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
        this.userB = {};
        this._servidor = 'http://localhost:8080';
        this.tokenBean = {};
        this.controller = '/oauth';
        this.tokenBean = JSON.parse(localStorage.getItem('user-id'));
        // this._logeado= false;
    }
    get logueado() { return this._logeado; }
    eventTimer(e) {
        let token;
        if (localStorage.getItem('x-access-token') != null) {
            token = localStorage.getItem('x-access-token').replace(/\\/g, '\\');
        }
        if (this._logeado == true && token != null && token != undefined) {
            this.expira--;
            //console.log("Expira en " + this.expira);
            if (this.expira < 20) {
                this.expira = 40;
                this.refreshToken();
            }
        }
    }
    login(usuario, clave, dataBase) {
        let loginSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        let metodo = this.servidor + this.controller;
        metodo += '/token?grant_type=password&username=' + usuario + '@@' + clave + '@@369@@' + dataBase + '&password=' + clave;
        const body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        this.httpClient.post(metodo, body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
            }), withCredentials: true
        }).subscribe(res => {
            // console.log(res,'TOKEN ORIGINAL');
            this.tokenBean = res;
            if (this.tokenBean.access_token.length === 36) {
                // console.log('Access Token ', this.tokenBean.access_token);
                // console.log('Refresh Token ', this.tokenBean.refresh_token);
                localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
                localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
                localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
                // console.log(this.tokenBean.access_token);
                this.expira = this.tokenBean.expires_in;
                this._logeado = true;
                // console.log(this._logeado);
                loginSub.next(true);
            }
            else {
                this._logeado = false;
                loginSub.next(false);
            }
        }, error => {
            console.log('Error', error);
            this._logeado = false;
            loginSub.next(false);
        });
        return loginSub.asObservable();
    }
    get servidor() {
        return this._servidor;
    }
    get token() {
        return this.tokenBean.access_token;
    }
    getRefreshToken() {
        return localStorage.getItem('x-refresh-token').replace(/\\/g, '\\');
    }
    getAccessToken() {
        return localStorage.getItem('x-access-token').replace(/\\/g, '\\');
    }
    refreshToken() {
        let token;
        if (localStorage.getItem('x-access-token') != null) {
            token = localStorage.getItem('x-access-token').replace(/\\/g, '\\');
        }
        if (token != null && token != undefined) {
            console.log('ejecuta proceso.. refreesh', this._logeado);
            let metodo = this.servidor + this.controller;
            metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
            const body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
            //console.log(metodo);
            this.httpClient.post(metodo, body, {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
                }), withCredentials: true
            })
                .subscribe(res => {
                // console.log('ACA ESTA EL RESTTTTT', res);
                this.tokenBean = res;
                if (this.tokenBean.access_token != null && this.tokenBean.access_token.length === 36) {
                    // console.log('Refresh => Access Token ', this.tokenBean.access_token);
                    // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
                    this._logeado = true;
                    localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
                    localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
                    localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
                    this.setAccessToken(this.tokenBean.access_token);
                    //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
                    //  console.log(new Date(this.tokenBean.expires_in));
                    this.expira = this.tokenBean.expires_in;
                }
            }, error => {
                console.log('Error', error);
                this._logeado = false;
            });
        }
    }
    getNewAccessToken() {
        this.expira = 40;
        let refreshSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        // console.log('EJECUTANDO REFRESH',this._logeado);
        let metodo = this.servidor + this.controller;
        metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
        const body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        this.httpClient.post(metodo, body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
            }), withCredentials: true
        })
            .subscribe(res => {
            //  console.log('ACA ESTA EL RESTTTTT', res);
            this.tokenBean = res;
            if (this.tokenBean.access_token != null && this.tokenBean.access_token.length === 36) {
                // console.log('Refresh => Access Token ', this.tokenBean.access_token);
                // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
                this._logeado = true;
                localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
                localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
                localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
                this.setAccessToken(this.tokenBean.access_token);
                refreshSub.next(true);
                //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
                //  console.log(new Date(this.tokenBean.expires_in));
                this.expira = this.tokenBean.expires_in;
            }
            else {
                refreshSub.next(false);
            }
        }, error => {
            console.log('Error', error);
            this._logeado = false;
            refreshSub.next(false);
        });
        return refreshSub.asObservable();
    }
    setAccessToken(accessToken) {
        localStorage.setItem('x-access-token', accessToken);
    }
    logout() {
        this.removeSession();
        this._logeado = false;
        this.router.navigateByUrl('/pages/auth/login');
    }
    removeSession() {
        localStorage.removeItem('user-id');
        localStorage.removeItem('x-access-token');
        localStorage.removeItem('x-refresh-token');
        this.tokenBean = {};
    }
    userValid() {
        if (!(this.tokenBean.expires_in < 1)) {
            return true;
        }
        else {
            return false;
        }
    }
};
OAuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
OAuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], OAuthService);



/***/ }),

/***/ "./src/app/common/rest/services/EmployeeController.ts":
/*!************************************************************!*\
  !*** ./src/app/common/rest/services/EmployeeController.ts ***!
  \************************************************************/
/*! exports provided: EmployeeController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeController", function() { return EmployeeController; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../oauth.service */ "./src/app/common/oauth.service.ts");




let EmployeeController = class EmployeeController {
    constructor(httpClient, oauth) {
        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/employeeResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
    }
    getListEmployee(arg0) {
        const metodo = this.url + '/getListEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    createEmployee(arg0) {
        const metodo = this.url + '/createEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    updateEmployee(arg0) {
        const metodo = this.url + '/updateEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }
    findByUsername(arg0) {
        const metodo = this.url + '/findByUsername';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        return this.httpClient.post(metodo, arg0, { headers: this.header });
    }
};
EmployeeController.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"] }
];
EmployeeController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], EmployeeController);



/***/ })

}]);
//# sourceMappingURL=default~assistance-searchAssistance-search-module~colaborador-buscar-buscar-module~colaborador-nuevo~96f5cd67-es2015.js.map