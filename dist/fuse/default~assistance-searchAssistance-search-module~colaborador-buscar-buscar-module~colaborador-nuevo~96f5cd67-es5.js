function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~assistance-searchAssistance-search-module~colaborador-buscar-buscar-module~colaborador-nuevo~96f5cd67"], {
  /***/
  "./src/app/common/oauth.service.ts":
  /*!*****************************************!*\
    !*** ./src/app/common/oauth.service.ts ***!
    \*****************************************/

  /*! exports provided: OAuthService */

  /***/
  function srcAppCommonOauthServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OAuthService", function () {
      return OAuthService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var OAuthService = /*#__PURE__*/function () {
      function OAuthService(httpClient, router) {
        _classCallCheck(this, OAuthService);

        this.httpClient = httpClient;
        this.router = router;
        this.userB = {};
        this._servidor = 'http://localhost:8080';
        this.tokenBean = {};
        this.controller = '/oauth';
        this.tokenBean = JSON.parse(localStorage.getItem('user-id')); // this._logeado= false;
      }

      _createClass(OAuthService, [{
        key: "eventTimer",
        value: function eventTimer(e) {
          var token;

          if (localStorage.getItem('x-access-token') != null) {
            token = localStorage.getItem('x-access-token').replace(/\\/g, '\\');
          }

          if (this._logeado == true && token != null && token != undefined) {
            this.expira--; //console.log("Expira en " + this.expira);

            if (this.expira < 20) {
              this.expira = 40;
              this.refreshToken();
            }
          }
        }
      }, {
        key: "login",
        value: function login(usuario, clave, dataBase) {
          var _this = this;

          var loginSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
          var metodo = this.servidor + this.controller;
          metodo += '/token?grant_type=password&username=' + usuario + '@@' + clave + '@@369@@' + dataBase + '&password=' + clave;
          var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
          this.httpClient.post(metodo, body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
              'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
            }),
            withCredentials: true
          }).subscribe(function (res) {
            // console.log(res,'TOKEN ORIGINAL');
            _this.tokenBean = res;

            if (_this.tokenBean.access_token.length === 36) {
              // console.log('Access Token ', this.tokenBean.access_token);
              // console.log('Refresh Token ', this.tokenBean.refresh_token);
              localStorage.setItem('x-access-token', JSON.stringify(_this.tokenBean.access_token));
              localStorage.setItem('x-refresh-token', JSON.stringify(_this.tokenBean.refresh_token));
              localStorage.setItem('user-id', JSON.stringify(_this.tokenBean)); // console.log(this.tokenBean.access_token);

              _this.expira = _this.tokenBean.expires_in;
              _this._logeado = true; // console.log(this._logeado);

              loginSub.next(true);
            } else {
              _this._logeado = false;
              loginSub.next(false);
            }
          }, function (error) {
            console.log('Error', error);
            _this._logeado = false;
            loginSub.next(false);
          });
          return loginSub.asObservable();
        }
      }, {
        key: "getRefreshToken",
        value: function getRefreshToken() {
          return localStorage.getItem('x-refresh-token').replace(/\\/g, '\\');
        }
      }, {
        key: "getAccessToken",
        value: function getAccessToken() {
          return localStorage.getItem('x-access-token').replace(/\\/g, '\\');
        }
      }, {
        key: "refreshToken",
        value: function refreshToken() {
          var _this2 = this;

          var token;

          if (localStorage.getItem('x-access-token') != null) {
            token = localStorage.getItem('x-access-token').replace(/\\/g, '\\');
          }

          if (token != null && token != undefined) {
            console.log('ejecuta proceso.. refreesh', this._logeado);
            var metodo = this.servidor + this.controller;
            metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
            var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"](); //console.log(metodo);

            this.httpClient.post(metodo, body, {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
              }),
              withCredentials: true
            }).subscribe(function (res) {
              // console.log('ACA ESTA EL RESTTTTT', res);
              _this2.tokenBean = res;

              if (_this2.tokenBean.access_token != null && _this2.tokenBean.access_token.length === 36) {
                // console.log('Refresh => Access Token ', this.tokenBean.access_token);
                // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
                _this2._logeado = true;
                localStorage.setItem('x-access-token', JSON.stringify(_this2.tokenBean.access_token));
                localStorage.setItem('x-refresh-token', JSON.stringify(_this2.tokenBean.refresh_token));
                localStorage.setItem('user-id', JSON.stringify(_this2.tokenBean));

                _this2.setAccessToken(_this2.tokenBean.access_token); //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
                //  console.log(new Date(this.tokenBean.expires_in));


                _this2.expira = _this2.tokenBean.expires_in;
              }
            }, function (error) {
              console.log('Error', error);
              _this2._logeado = false;
            });
          }
        }
      }, {
        key: "getNewAccessToken",
        value: function getNewAccessToken() {
          var _this3 = this;

          this.expira = 40;
          var refreshSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"](); // console.log('EJECUTANDO REFRESH',this._logeado);

          var metodo = this.servidor + this.controller;
          metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
          var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
          this.httpClient.post(metodo, body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
              'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
            }),
            withCredentials: true
          }).subscribe(function (res) {
            //  console.log('ACA ESTA EL RESTTTTT', res);
            _this3.tokenBean = res;

            if (_this3.tokenBean.access_token != null && _this3.tokenBean.access_token.length === 36) {
              // console.log('Refresh => Access Token ', this.tokenBean.access_token);
              // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
              _this3._logeado = true;
              localStorage.setItem('x-access-token', JSON.stringify(_this3.tokenBean.access_token));
              localStorage.setItem('x-refresh-token', JSON.stringify(_this3.tokenBean.refresh_token));
              localStorage.setItem('user-id', JSON.stringify(_this3.tokenBean));

              _this3.setAccessToken(_this3.tokenBean.access_token);

              refreshSub.next(true); //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
              //  console.log(new Date(this.tokenBean.expires_in));

              _this3.expira = _this3.tokenBean.expires_in;
            } else {
              refreshSub.next(false);
            }
          }, function (error) {
            console.log('Error', error);
            _this3._logeado = false;
            refreshSub.next(false);
          });
          return refreshSub.asObservable();
        }
      }, {
        key: "setAccessToken",
        value: function setAccessToken(accessToken) {
          localStorage.setItem('x-access-token', accessToken);
        }
      }, {
        key: "logout",
        value: function logout() {
          this.removeSession();
          this._logeado = false;
          this.router.navigateByUrl('/pages/auth/login');
        }
      }, {
        key: "removeSession",
        value: function removeSession() {
          localStorage.removeItem('user-id');
          localStorage.removeItem('x-access-token');
          localStorage.removeItem('x-refresh-token');
          this.tokenBean = {};
        }
      }, {
        key: "userValid",
        value: function userValid() {
          if (!(this.tokenBean.expires_in < 1)) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "logueado",
        get: function get() {
          return this._logeado;
        }
      }, {
        key: "servidor",
        get: function get() {
          return this._servidor;
        }
      }, {
        key: "token",
        get: function get() {
          return this.tokenBean.access_token;
        }
      }]);

      return OAuthService;
    }();

    OAuthService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    OAuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], OAuthService);
    /***/
  },

  /***/
  "./src/app/common/rest/services/EmployeeController.ts":
  /*!************************************************************!*\
    !*** ./src/app/common/rest/services/EmployeeController.ts ***!
    \************************************************************/

  /*! exports provided: EmployeeController */

  /***/
  function srcAppCommonRestServicesEmployeeControllerTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmployeeController", function () {
      return EmployeeController;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../oauth.service */
    "./src/app/common/oauth.service.ts");

    var EmployeeController = /*#__PURE__*/function () {
      function EmployeeController(httpClient, oauth) {
        _classCallCheck(this, EmployeeController);

        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/employeeResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
          'Content-Type': 'application/json'
        }); // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
      }

      _createClass(EmployeeController, [{
        key: "getListEmployee",
        value: function getListEmployee(arg0) {
          var metodo = this.url + '/getListEmployee'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "createEmployee",
        value: function createEmployee(arg0) {
          var metodo = this.url + '/createEmployee'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "updateEmployee",
        value: function updateEmployee(arg0) {
          var metodo = this.url + '/updateEmployee'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "findByUsername",
        value: function findByUsername(arg0) {
          var metodo = this.url + '/findByUsername'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, arg0, {
            headers: this.header
          });
        }
      }]);

      return EmployeeController;
    }();

    EmployeeController.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"]
      }];
    };

    EmployeeController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], EmployeeController);
    /***/
  }
}]);
//# sourceMappingURL=default~assistance-searchAssistance-search-module~colaborador-buscar-buscar-module~colaborador-nuevo~96f5cd67-es5.js.map