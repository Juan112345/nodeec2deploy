function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~calendar-calendar-module~report-assistanceReport-assistanceReport-module"], {
  /***/
  "./node_modules/angular-calendar/date-adapters/date-fns/index.js":
  /*!***********************************************************************!*\
    !*** ./node_modules/angular-calendar/date-adapters/date-fns/index.js ***!
    \***********************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesAngularCalendarDateAdaptersDateFnsIndexJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var tslib_1 = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var date_fns_1 = __webpack_require__(
    /*! calendar-utils/date-adapters/date-fns */
    "./node_modules/calendar-utils/date-adapters/date-fns/index.js");

    var addWeeks = __webpack_require__(
    /*! date-fns/add_weeks/index */
    "./node_modules/date-fns/add_weeks/index.js");

    var addMonths = __webpack_require__(
    /*! date-fns/add_months/index */
    "./node_modules/date-fns/add_months/index.js");

    var subDays = __webpack_require__(
    /*! date-fns/sub_days/index */
    "./node_modules/date-fns/sub_days/index.js");

    var subWeeks = __webpack_require__(
    /*! date-fns/sub_weeks/index */
    "./node_modules/date-fns/sub_weeks/index.js");

    var subMonths = __webpack_require__(
    /*! date-fns/sub_months/index */
    "./node_modules/date-fns/sub_months/index.js");

    var getISOWeek = __webpack_require__(
    /*! date-fns/get_iso_week/index */
    "./node_modules/date-fns/get_iso_week/index.js");

    var setDate = __webpack_require__(
    /*! date-fns/set_date/index */
    "./node_modules/date-fns/set_date/index.js");

    var setMonth = __webpack_require__(
    /*! date-fns/set_month/index */
    "./node_modules/date-fns/set_month/index.js");

    var setYear = __webpack_require__(
    /*! date-fns/set_year/index */
    "./node_modules/date-fns/set_year/index.js");

    var getDate = __webpack_require__(
    /*! date-fns/get_date/index */
    "./node_modules/date-fns/get_date/index.js");

    var getYear = __webpack_require__(
    /*! date-fns/get_year/index */
    "./node_modules/date-fns/get_year/index.js");

    function adapterFactory() {
      return tslib_1.__assign({}, date_fns_1.adapterFactory(), {
        addWeeks: addWeeks,
        addMonths: addMonths,
        subDays: subDays,
        subWeeks: subWeeks,
        subMonths: subMonths,
        getISOWeek: getISOWeek,
        setDate: setDate,
        setMonth: setMonth,
        setYear: setYear,
        getDate: getDate,
        getYear: getYear
      });
    }

    exports.adapterFactory = adapterFactory; //# sourceMappingURL=index.js.map

    /***/
  },

  /***/
  "./node_modules/angular-calendar/fesm2015/angular-calendar.js":
  /*!********************************************************************!*\
    !*** ./node_modules/angular-calendar/fesm2015/angular-calendar.js ***!
    \********************************************************************/

  /*! exports provided: DAYS_OF_WEEK, CalendarAngularDateFormatter, CalendarCommonModule, CalendarDateFormatter, CalendarDayModule, CalendarDayViewComponent, CalendarEventTimesChangedEventType, CalendarEventTitleFormatter, CalendarModule, CalendarMomentDateFormatter, CalendarMonthModule, CalendarMonthViewComponent, CalendarNativeDateFormatter, CalendarUtils, CalendarView, CalendarWeekModule, CalendarWeekViewComponent, DateAdapter, MOMENT, collapseAnimation, getWeekViewPeriod, ɵa, ɵb, ɵc, ɵd, ɵe, ɵf, ɵg, ɵh, ɵi, ɵj, ɵk, ɵl, ɵm, ɵn, ɵo, ɵp, ɵq, ɵr */

  /***/
  function node_modulesAngularCalendarFesm2015AngularCalendarJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarAngularDateFormatter", function () {
      return CalendarAngularDateFormatter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarCommonModule", function () {
      return CalendarCommonModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarDateFormatter", function () {
      return CalendarDateFormatter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarDayModule", function () {
      return CalendarDayModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarDayViewComponent", function () {
      return CalendarDayViewComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarEventTimesChangedEventType", function () {
      return CalendarEventTimesChangedEventType;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarEventTitleFormatter", function () {
      return CalendarEventTitleFormatter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarModule", function () {
      return CalendarModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarMomentDateFormatter", function () {
      return CalendarMomentDateFormatter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarMonthModule", function () {
      return CalendarMonthModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarMonthViewComponent", function () {
      return CalendarMonthViewComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarNativeDateFormatter", function () {
      return CalendarNativeDateFormatter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarUtils", function () {
      return CalendarUtils;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarView", function () {
      return CalendarView;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarWeekModule", function () {
      return CalendarWeekModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarWeekViewComponent", function () {
      return CalendarWeekViewComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DateAdapter", function () {
      return DateAdapter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MOMENT", function () {
      return MOMENT;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "collapseAnimation", function () {
      return collapseAnimation;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getWeekViewPeriod", function () {
      return getWeekViewPeriod;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵa", function () {
      return CalendarOpenDayEventsComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵb", function () {
      return CalendarEventActionsComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵc", function () {
      return CalendarEventTitleComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵd", function () {
      return CalendarTooltipWindowComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵe", function () {
      return CalendarTooltipDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵf", function () {
      return CalendarPreviousViewDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵg", function () {
      return CalendarNextViewDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵh", function () {
      return CalendarTodayDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵi", function () {
      return CalendarDatePipe;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵj", function () {
      return CalendarEventTitlePipe;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵk", function () {
      return ClickDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵl", function () {
      return CalendarMonthCellComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵm", function () {
      return CalendarMonthViewHeaderComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵn", function () {
      return CalendarWeekViewHeaderComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵo", function () {
      return CalendarWeekViewEventComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵp", function () {
      return CalendarWeekViewHourSegmentComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵq", function () {
      return CalendarDayViewHourSegmentComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵr", function () {
      return CalendarDayViewEventComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var positioning__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! positioning */
    "./node_modules/positioning/dist/positioning.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var calendar_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! calendar-utils */
    "./node_modules/calendar-utils/calendar-utils.js");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "DAYS_OF_WEEK", function () {
      return calendar_utils__WEBPACK_IMPORTED_MODULE_5__["DAYS_OF_WEEK"];
    });
    /* harmony import */


    var angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! angular-draggable-droppable */
    "./node_modules/angular-draggable-droppable/fesm2015/angular-draggable-droppable.js");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var angular_resizable_element__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-resizable-element */
    "./node_modules/angular-resizable-element/fesm2015/angular-resizable-element.js");
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var CalendarEventActionsComponent = function CalendarEventActionsComponent() {
      _classCallCheck(this, CalendarEventActionsComponent);

      this.trackByActionId =
      /**
      * @param {?} index
      * @param {?} action
      * @return {?}
      */
      function (index, action) {
        return action.id ? action.id : action;
      };
    };

    CalendarEventActionsComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-event-actions',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-event=\"event\"\n      let-trackByActionId=\"trackByActionId\"\n    >\n      <span *ngIf=\"event.actions\" class=\"cal-event-actions\">\n        <a\n          class=\"cal-event-action\"\n          href=\"javascript:;\"\n          *ngFor=\"let action of event.actions; trackBy: trackByActionId\"\n          (mwlClick)=\"action.onClick({ event: event })\"\n          [ngClass]=\"action.cssClass\"\n          [innerHtml]=\"action.label\"\n        >\n        </a>\n      </span>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        event: event,\n        trackByActionId: trackByActionId\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarEventActionsComponent.propDecorators = {
      event: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarEventTitleComponent = function CalendarEventTitleComponent() {
      _classCallCheck(this, CalendarEventTitleComponent);
    };

    CalendarEventTitleComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-event-title',
        template: "\n    <ng-template #defaultTemplate let-event=\"event\" let-view=\"view\">\n      <span\n        class=\"cal-event-title\"\n        [innerHTML]=\"event.title | calendarEventTitle: view:event\"\n      >\n      </span>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        event: event,\n        view: view\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarEventTitleComponent.propDecorators = {
      event: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      view: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarTooltipWindowComponent = function CalendarTooltipWindowComponent() {
      _classCallCheck(this, CalendarTooltipWindowComponent);
    };

    CalendarTooltipWindowComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-tooltip-window',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-contents=\"contents\"\n      let-placement=\"placement\"\n      let-event=\"event\"\n    >\n      <div class=\"cal-tooltip\" [ngClass]=\"'cal-tooltip-' + placement\">\n        <div class=\"cal-tooltip-arrow\"></div>\n        <div class=\"cal-tooltip-inner\" [innerHtml]=\"contents\"></div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        contents: contents,\n        placement: placement,\n        event: event\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarTooltipWindowComponent.propDecorators = {
      contents: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      placement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      event: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };

    var CalendarTooltipDirective = /*#__PURE__*/function () {
      /**
       * @param {?} elementRef
       * @param {?} injector
       * @param {?} renderer
       * @param {?} componentFactoryResolver
       * @param {?} viewContainerRef
       * @param {?} document
       */
      function CalendarTooltipDirective(elementRef, injector, renderer, componentFactoryResolver, viewContainerRef, document //tslint:disable-line
      ) {
        _classCallCheck(this, CalendarTooltipDirective);

        this.elementRef = elementRef;
        this.injector = injector;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.document = document; // tslint:disable-line no-input-rename

        this.placement = 'auto'; // tslint:disable-line no-input-rename
        // tslint:disable-line no-input-rename

        this.delay = null; // tslint:disable-line no-input-rename

        this.cancelTooltipDelay$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.tooltipFactory = componentFactoryResolver.resolveComponentFactory(CalendarTooltipWindowComponent);
      }
      /**
       * @param {?} changes
       * @return {?}
       */


      _createClass(CalendarTooltipDirective, [{
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          if (this.tooltipRef && (changes.contents || changes.customTemplate || changes.event)) {
            this.tooltipRef.instance.contents = this.contents;
            this.tooltipRef.instance.customTemplate = this.customTemplate;
            this.tooltipRef.instance.event = this.event;
            this.tooltipRef.changeDetectorRef.markForCheck();
          }
        }
        /**
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.hide();
        }
        /**
         * @return {?}
         */

      }, {
        key: "onMouseOver",
        value: function onMouseOver() {
          var _this2 = this;

          /** @type {?} */
          var delay$ = this.delay === null ? Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])('now') : Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["timer"])(this.delay);
          delay$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.cancelTooltipDelay$)).subscribe(
          /**
          * @return {?}
          */
          function () {
            _this2.show();
          });
        }
        /**
         * @return {?}
         */

      }, {
        key: "onMouseOut",
        value: function onMouseOut() {
          this.hide();
        }
        /**
         * @private
         * @return {?}
         */

      }, {
        key: "show",
        value: function show() {
          var _this3 = this;

          if (!this.tooltipRef && this.contents) {
            this.tooltipRef = this.viewContainerRef.createComponent(this.tooltipFactory, 0, this.injector, []);
            this.tooltipRef.instance.contents = this.contents;
            this.tooltipRef.instance.customTemplate = this.customTemplate;
            this.tooltipRef.instance.event = this.event;

            if (this.appendToBody) {
              this.document.body.appendChild(this.tooltipRef.location.nativeElement);
            }

            requestAnimationFrame(
            /**
            * @return {?}
            */
            function () {
              _this3.positionTooltip();
            });
          }
        }
        /**
         * @private
         * @return {?}
         */

      }, {
        key: "hide",
        value: function hide() {
          if (this.tooltipRef) {
            this.viewContainerRef.remove(this.viewContainerRef.indexOf(this.tooltipRef.hostView));
            this.tooltipRef = null;
          }

          this.cancelTooltipDelay$.next();
        }
        /**
         * @private
         * @param {?=} previousPosition
         * @return {?}
         */

      }, {
        key: "positionTooltip",
        value: function positionTooltip(previousPosition) {
          if (this.tooltipRef) {
            this.tooltipRef.changeDetectorRef.detectChanges();
            this.tooltipRef.instance.placement = Object(positioning__WEBPACK_IMPORTED_MODULE_2__["positionElements"])(this.elementRef.nativeElement, this.tooltipRef.location.nativeElement.children[0], this.placement, this.appendToBody); // keep re-positioning the tooltip until the arrow position doesn't make a difference

            if (previousPosition !== this.tooltipRef.instance.placement) {
              this.positionTooltip(this.tooltipRef.instance.placement);
            }
          }
        }
      }]);

      return CalendarTooltipDirective;
    }();

    CalendarTooltipDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[mwlCalendarTooltip]'
      }]
    }];
    /** @nocollapse */

    CalendarTooltipDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
        }]
      }];
    };

    CalendarTooltipDirective.propDecorators = {
      contents: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['mwlCalendarTooltip']
      }],
      placement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['tooltipPlacement']
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['tooltipTemplate']
      }],
      event: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['tooltipEvent']
      }],
      appendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['tooltipAppendToBody']
      }],
      delay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
        args: ['tooltipDelay']
      }],
      onMouseOver: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['mouseenter']
      }],
      onMouseOut: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['mouseleave']
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @abstract
     */

    var DateAdapter = function DateAdapter() {
      _classCallCheck(this, DateAdapter);
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @enum {string} */


    var CalendarView = {
      Month: 'month',
      Week: 'week',
      Day: 'day'
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @type {?} */

    var validateEvents =
    /**
    * @param {?} events
    * @return {?}
    */
    function validateEvents(events) {
      /** @type {?} */
      var warn =
      /**
      * @param {...?} args
      * @return {?}
      */
      function warn() {
        var _console;

        for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
          args[_key4] = arguments[_key4];
        }

        return (_console = console).warn.apply(_console, ['angular-calendar'].concat(args));
      };

      return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["validateEvents"])(events, warn);
    };
    /**
     * @param {?} outer
     * @param {?} inner
     * @return {?}
     */


    function isInside(outer, inner) {
      return Math.floor(outer.left) <= Math.ceil(inner.left) && Math.floor(inner.left) <= Math.ceil(outer.right) && Math.floor(outer.left) <= Math.ceil(inner.right) && Math.floor(inner.right) <= Math.ceil(outer.right) && Math.floor(outer.top) <= Math.ceil(inner.top) && Math.floor(inner.top) <= Math.ceil(outer.bottom) && Math.floor(outer.top) <= Math.ceil(inner.bottom) && Math.floor(inner.bottom) <= Math.ceil(outer.bottom);
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */


    function roundToNearest(amount, precision) {
      return Math.round(amount / precision) * precision;
    }
    /** @type {?} */


    var trackByEventId =
    /**
    * @param {?} index
    * @param {?} event
    * @return {?}
    */
    function trackByEventId(index, event) {
      return event.id ? event.id : event;
    };
    /** @type {?} */


    var trackByWeekDayHeaderDate =
    /**
    * @param {?} index
    * @param {?} day
    * @return {?}
    */
    function trackByWeekDayHeaderDate(index, day) {
      return day.date.toISOString();
    };
    /** @type {?} */


    var trackByHourSegment =
    /**
    * @param {?} index
    * @param {?} segment
    * @return {?}
    */
    function trackByHourSegment(index, segment) {
      return segment.date.toISOString();
    };
    /** @type {?} */


    var trackByHour =
    /**
    * @param {?} index
    * @param {?} hour
    * @return {?}
    */
    function trackByHour(index, hour) {
      return hour.segments[0].date.toISOString();
    };
    /** @type {?} */


    var trackByDayOrWeekEvent =
    /**
    * @param {?} index
    * @param {?} weekEvent
    * @return {?}
    */
    function trackByDayOrWeekEvent(index, weekEvent) {
      return weekEvent.event.id ? weekEvent.event.id : weekEvent.event;
    };
    /** @type {?} */


    var MINUTES_IN_HOUR = 60;
    /**
     * @param {?} movedY
     * @param {?} hourSegments
     * @param {?} hourSegmentHeight
     * @param {?} eventSnapSize
     * @return {?}
     */

    function getMinutesMoved(movedY, hourSegments, hourSegmentHeight, eventSnapSize) {
      /** @type {?} */
      var draggedInPixelsSnapSize = roundToNearest(movedY, eventSnapSize || hourSegmentHeight);
      /** @type {?} */

      var pixelAmountInMinutes = MINUTES_IN_HOUR / (hourSegments * hourSegmentHeight);
      return draggedInPixelsSnapSize * pixelAmountInMinutes;
    }
    /**
     * @param {?} hourSegments
     * @param {?} hourSegmentHeight
     * @return {?}
     */


    function getMinimumEventHeightInMinutes(hourSegments, hourSegmentHeight) {
      return MINUTES_IN_HOUR / (hourSegments * hourSegmentHeight) * hourSegmentHeight;
    }
    /**
     * @param {?} dateAdapter
     * @param {?} event
     * @param {?} minimumMinutes
     * @return {?}
     */


    function getDefaultEventEnd(dateAdapter, event, minimumMinutes) {
      if (event.end) {
        return event.end;
      } else {
        return dateAdapter.addMinutes(event.start, minimumMinutes);
      }
    }
    /**
     * @param {?} dateAdapter
     * @param {?} date
     * @param {?} days
     * @param {?} excluded
     * @return {?}
     */


    function addDaysWithExclusions(dateAdapter, date, days, excluded) {
      /** @type {?} */
      var daysCounter = 0;
      /** @type {?} */

      var daysToAdd = 0;
      /** @type {?} */

      var changeDays = days < 0 ? dateAdapter.subDays : dateAdapter.addDays;
      /** @type {?} */

      var result = date;

      while (daysToAdd <= Math.abs(days)) {
        result = changeDays(date, daysCounter);
        /** @type {?} */

        var day = dateAdapter.getDay(result);

        if (excluded.indexOf(day) === -1) {
          daysToAdd++;
        }

        daysCounter++;
      }

      return result;
    }
    /**
     * @param {?} newStart
     * @param {?} newEnd
     * @param {?} period
     * @return {?}
     */


    function isDraggedWithinPeriod(newStart, newEnd, period) {
      /** @type {?} */
      var end = newEnd || newStart;
      return period.start <= newStart && newStart <= period.end || period.start <= end && end <= period.end;
    }
    /**
     * @param {?} dropEvent
     * @param {?} date
     * @param {?} allDay
     * @param {?} calendarId
     * @return {?}
     */


    function shouldFireDroppedEvent(dropEvent, date, allDay, calendarId) {
      return dropEvent.dropData && dropEvent.dropData.event && (dropEvent.dropData.calendarId !== calendarId || dropEvent.dropData.event.allDay && !allDay || !dropEvent.dropData.event.allDay && allDay);
    }
    /**
     * @param {?} dateAdapter
     * @param {?} viewDate
     * @param {?} weekStartsOn
     * @param {?=} excluded
     * @param {?=} daysInWeek
     * @return {?}
     */


    function getWeekViewPeriod(dateAdapter, viewDate, weekStartsOn) {
      var excluded = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
      var daysInWeek = arguments.length > 4 ? arguments[4] : undefined;

      /** @type {?} */
      var viewStart = daysInWeek ? dateAdapter.startOfDay(viewDate) : dateAdapter.startOfWeek(viewDate, {
        weekStartsOn: weekStartsOn
      });

      if (excluded.indexOf(dateAdapter.getDay(viewStart)) > -1) {
        viewStart = dateAdapter.subDays(addDaysWithExclusions(dateAdapter, viewStart, 1, excluded), 1);
      }

      if (daysInWeek) {
        /** @type {?} */
        var viewEnd = dateAdapter.endOfDay(addDaysWithExclusions(dateAdapter, viewStart, daysInWeek - 1, excluded));
        return {
          viewStart: viewStart,
          viewEnd: viewEnd
        };
      } else {
        /** @type {?} */
        var _viewEnd = dateAdapter.endOfWeek(viewDate, {
          weekStartsOn: weekStartsOn
        });

        if (excluded.indexOf(dateAdapter.getDay(_viewEnd)) > -1) {
          _viewEnd = dateAdapter.addDays(addDaysWithExclusions(dateAdapter, _viewEnd, -1, excluded), 1);
        }

        return {
          viewStart: viewStart,
          viewEnd: _viewEnd
        };
      }
    }
    /**
     * @param {?} __0
     * @return {?}
     */


    function isWithinThreshold(_ref) {
      var x = _ref.x,
          y = _ref.y;

      /** @type {?} */
      var DRAG_THRESHOLD = 1;
      return Math.abs(x) > DRAG_THRESHOLD || Math.abs(y) > DRAG_THRESHOLD;
    }
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Change the view date to the previous view. For example:
     *
     * ```typescript
     * <button
     *  mwlCalendarPreviousView
     *  [(viewDate)]="viewDate"
     *  [view]="view">
     *  Previous
     * </button>
     * ```
     */


    var CalendarPreviousViewDirective = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarPreviousViewDirective(dateAdapter) {
        _classCallCheck(this, CalendarPreviousViewDirective);

        this.dateAdapter = dateAdapter;
        /**
         * Days to skip when going back by 1 day
         */

        this.excludeDays = [];
        /**
         * Called when the view date is changed
         */

        this.viewDateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarPreviousViewDirective, [{
        key: "onClick",
        value: function onClick() {
          /** @type {?} */
          var subFn = {
            day: this.dateAdapter.subDays,
            week: this.dateAdapter.subWeeks,
            month: this.dateAdapter.subMonths
          }[this.view];

          if (this.view === CalendarView.Day) {
            this.viewDateChange.emit(addDaysWithExclusions(this.dateAdapter, this.viewDate, -1, this.excludeDays));
          } else if (this.view === CalendarView.Week && this.daysInWeek) {
            this.viewDateChange.emit(addDaysWithExclusions(this.dateAdapter, this.viewDate, -this.daysInWeek, this.excludeDays));
          } else {
            this.viewDateChange.emit(subFn(this.viewDate, 1));
          }
        }
      }]);

      return CalendarPreviousViewDirective;
    }();

    CalendarPreviousViewDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[mwlCalendarPreviousView]'
      }]
    }];
    /** @nocollapse */

    CalendarPreviousViewDirective.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };

    CalendarPreviousViewDirective.propDecorators = {
      view: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      excludeDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      daysInWeek: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      viewDateChange: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      onClick: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['click']
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Change the view date to the next view. For example:
     *
     * ```typescript
     * <button
     *  mwlCalendarNextView
     *  [(viewDate)]="viewDate"
     *  [view]="view">
     *  Next
     * </button>
     * ```
     */

    var CalendarNextViewDirective = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarNextViewDirective(dateAdapter) {
        _classCallCheck(this, CalendarNextViewDirective);

        this.dateAdapter = dateAdapter;
        /**
         * Days to skip when going forward by 1 day
         */

        this.excludeDays = [];
        /**
         * Called when the view date is changed
         */

        this.viewDateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarNextViewDirective, [{
        key: "onClick",
        value: function onClick() {
          /** @type {?} */
          var addFn = {
            day: this.dateAdapter.addDays,
            week: this.dateAdapter.addWeeks,
            month: this.dateAdapter.addMonths
          }[this.view];

          if (this.view === CalendarView.Day) {
            this.viewDateChange.emit(addDaysWithExclusions(this.dateAdapter, this.viewDate, 1, this.excludeDays));
          } else if (this.view === CalendarView.Week && this.daysInWeek) {
            this.viewDateChange.emit(addDaysWithExclusions(this.dateAdapter, this.viewDate, this.daysInWeek, this.excludeDays));
          } else {
            this.viewDateChange.emit(addFn(this.viewDate, 1));
          }
        }
      }]);

      return CalendarNextViewDirective;
    }();

    CalendarNextViewDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[mwlCalendarNextView]'
      }]
    }];
    /** @nocollapse */

    CalendarNextViewDirective.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };

    CalendarNextViewDirective.propDecorators = {
      view: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      excludeDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      daysInWeek: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      viewDateChange: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      onClick: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['click']
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Change the view date to the current day. For example:
     *
     * ```typescript
     * <button
     *  mwlCalendarToday
     *  [(viewDate)]="viewDate">
     *  Today
     * </button>
     * ```
     */

    var CalendarTodayDirective = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarTodayDirective(dateAdapter) {
        _classCallCheck(this, CalendarTodayDirective);

        this.dateAdapter = dateAdapter;
        /**
         * Called when the view date is changed
         */

        this.viewDateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarTodayDirective, [{
        key: "onClick",
        value: function onClick() {
          this.viewDateChange.emit(this.dateAdapter.startOfDay(new Date()));
        }
      }]);

      return CalendarTodayDirective;
    }();

    CalendarTodayDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[mwlCalendarToday]'
      }]
    }];
    /** @nocollapse */

    CalendarTodayDirective.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };

    CalendarTodayDirective.propDecorators = {
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      viewDateChange: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      onClick: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['click']
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * This will use the angular date pipe to do all date formatting. It is the default date formatter used by the calendar.
     */

    var CalendarAngularDateFormatter = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarAngularDateFormatter(dateAdapter) {
        _classCallCheck(this, CalendarAngularDateFormatter);

        this.dateAdapter = dateAdapter;
      }
      /**
       * The month view header week day labels
       * @param {?} __0
       * @return {?}
       */


      _createClass(CalendarAngularDateFormatter, [{
        key: "monthViewColumnHeader",
        value: function monthViewColumnHeader(_ref2) {
          var date = _ref2.date,
              locale = _ref2.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'EEEE', locale);
        }
        /**
         * The month view cell day number
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewDayNumber",
        value: function monthViewDayNumber(_ref3) {
          var date = _ref3.date,
              locale = _ref3.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'd', locale);
        }
        /**
         * The month view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewTitle",
        value: function monthViewTitle(_ref4) {
          var date = _ref4.date,
              locale = _ref4.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'LLLL y', locale);
        }
        /**
         * The week view header week day labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnHeader",
        value: function weekViewColumnHeader(_ref5) {
          var date = _ref5.date,
              locale = _ref5.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'EEEE', locale);
        }
        /**
         * The week view sub header day and month labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnSubHeader",
        value: function weekViewColumnSubHeader(_ref6) {
          var date = _ref6.date,
              locale = _ref6.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'MMM d', locale);
        }
        /**
         * The week view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewTitle",
        value: function weekViewTitle(_ref7) {
          var date = _ref7.date,
              locale = _ref7.locale,
              weekStartsOn = _ref7.weekStartsOn,
              excludeDays = _ref7.excludeDays,
              daysInWeek = _ref7.daysInWeek;

          var _getWeekViewPeriod = getWeekViewPeriod(this.dateAdapter, date, weekStartsOn, excludeDays, daysInWeek),
              viewStart = _getWeekViewPeriod.viewStart,
              viewEnd = _getWeekViewPeriod.viewEnd;
          /** @type {?} */


          var format =
          /**
          * @param {?} dateToFormat
          * @param {?} showYear
          * @return {?}
          */
          function format(dateToFormat, showYear) {
            return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(dateToFormat, 'MMM d' + (showYear ? ', yyyy' : ''), locale);
          };

          return "".concat(format(viewStart, viewStart.getUTCFullYear() !== viewEnd.getUTCFullYear()), " - ").concat(format(viewEnd, true));
        }
        /**
         * The time formatting down the left hand side of the week view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewHour",
        value: function weekViewHour(_ref8) {
          var date = _ref8.date,
              locale = _ref8.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'h a', locale);
        }
        /**
         * The time formatting down the left hand side of the day view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewHour",
        value: function dayViewHour(_ref9) {
          var date = _ref9.date,
              locale = _ref9.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'h a', locale);
        }
        /**
         * The day view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewTitle",
        value: function dayViewTitle(_ref10) {
          var date = _ref10.date,
              locale = _ref10.locale;
          return Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["formatDate"])(date, 'EEEE, MMMM d, y', locale);
        }
      }]);

      return CalendarAngularDateFormatter;
    }();

    CalendarAngularDateFormatter.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];
    /** @nocollapse */

    CalendarAngularDateFormatter.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * This class is responsible for all formatting of dates. There are 3 implementations available, the `CalendarAngularDateFormatter` (default) which uses the angular date pipe to format dates, the `CalendarNativeDateFormatter` which will use the <a href="https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Intl" target="_blank">Intl</a> API to format dates, or there is the `CalendarMomentDateFormatter` which uses <a href="http://momentjs.com/" target="_blank">moment</a>.
     *
     * If you wish, you may override any of the defaults via angulars DI. For example:
     *
     * ```typescript
     * import { CalendarDateFormatter, DateFormatterParams } from 'angular-calendar';
     * import { formatDate } from '\@angular/common';
     *
     * class CustomDateFormatter extends CalendarDateFormatter {
     *
     *   public monthViewColumnHeader({date, locale}: DateFormatterParams): string {
     *     return formatDate(date, 'EEE', locale); // use short week days
     *   }
     *
     * }
     *
     * // in your component that uses the calendar
     * providers: [{
     *   provide: CalendarDateFormatter,
     *   useClass: CustomDateFormatter
     * }]
     * ```
     */


    var CalendarDateFormatter = /*#__PURE__*/function (_CalendarAngularDateF) {
      _inherits(CalendarDateFormatter, _CalendarAngularDateF);

      var _super = _createSuper(CalendarDateFormatter);

      function CalendarDateFormatter() {
        _classCallCheck(this, CalendarDateFormatter);

        return _super.apply(this, arguments);
      }

      return CalendarDateFormatter;
    }(CalendarAngularDateFormatter);

    CalendarDateFormatter.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * This pipe is primarily for rendering the current view title. Example usage:
     * ```typescript
     * // where `viewDate` is a `Date` and view is `'month' | 'week' | 'day'`
     * {{ viewDate | calendarDate:(view + 'ViewTitle'):'en' }}
     * ```
     */

    var CalendarDatePipe = /*#__PURE__*/function () {
      /**
       * @param {?} dateFormatter
       * @param {?} locale
       */
      function CalendarDatePipe(dateFormatter, locale) {
        _classCallCheck(this, CalendarDatePipe);

        this.dateFormatter = dateFormatter;
        this.locale = locale;
      }
      /**
       * @param {?} date
       * @param {?} method
       * @param {?=} locale
       * @param {?=} weekStartsOn
       * @param {?=} excludeDays
       * @param {?=} daysInWeek
       * @return {?}
       */


      _createClass(CalendarDatePipe, [{
        key: "transform",
        value: function transform(date, method) {
          var locale = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.locale;
          var weekStartsOn = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
          var excludeDays = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
          var daysInWeek = arguments.length > 5 ? arguments[5] : undefined;

          if (typeof this.dateFormatter[method] === 'undefined') {
            /** @type {?} */
            var allowedMethods = Object.getOwnPropertyNames(Object.getPrototypeOf(CalendarDateFormatter.prototype)).filter(
            /**
            * @param {?} iMethod
            * @return {?}
            */
            function (iMethod) {
              return iMethod !== 'constructor';
            });
            throw new Error("".concat(method, " is not a valid date formatter. Can only be one of ").concat(allowedMethods.join(', ')));
          }

          return this.dateFormatter[method]({
            date: date,
            locale: locale,
            weekStartsOn: weekStartsOn,
            excludeDays: excludeDays,
            daysInWeek: daysInWeek
          });
        }
      }]);

      return CalendarDatePipe;
    }();

    CalendarDatePipe.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
      args: [{
        name: 'calendarDate'
      }]
    }];
    /** @nocollapse */

    CalendarDatePipe.ctorParameters = function () {
      return [{
        type: CalendarDateFormatter
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]
        }]
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * This class is responsible for displaying all event titles within the calendar. You may override any of its methods via angulars DI to suit your requirements. For example:
     *
     * ```typescript
     * import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
     *
     * class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
     *
     *   month(event: CalendarEvent): string {
     *     return `Custom prefix: ${event.title}`;
     *   }
     *
     * }
     *
     * // in your component
     * providers: [{
     *  provide: CalendarEventTitleFormatter,
     *  useClass: CustomEventTitleFormatter
     * }]
     * ```
     */


    var CalendarEventTitleFormatter = /*#__PURE__*/function () {
      function CalendarEventTitleFormatter() {
        _classCallCheck(this, CalendarEventTitleFormatter);
      }

      _createClass(CalendarEventTitleFormatter, [{
        key: "month",

        /**
         * The month view event title.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */
        value: function month(event, title) {
          return event.title;
        }
        /**
         * The month view event tooltip. Return a falsey value from this to disable the tooltip.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */

      }, {
        key: "monthTooltip",
        value: function monthTooltip(event, title) {
          return event.title;
        }
        /**
         * The week view event title.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */

      }, {
        key: "week",
        value: function week(event, title) {
          return event.title;
        }
        /**
         * The week view event tooltip. Return a falsey value from this to disable the tooltip.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */

      }, {
        key: "weekTooltip",
        value: function weekTooltip(event, title) {
          return event.title;
        }
        /**
         * The day view event title.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */

      }, {
        key: "day",
        value: function day(event, title) {
          return event.title;
        }
        /**
         * The day view event tooltip. Return a falsey value from this to disable the tooltip.
         * @param {?} event
         * @param {?} title
         * @return {?}
         */

      }, {
        key: "dayTooltip",
        value: function dayTooltip(event, title) {
          return event.title;
        }
      }]);

      return CalendarEventTitleFormatter;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var CalendarEventTitlePipe = /*#__PURE__*/function () {
      /**
       * @param {?} calendarEventTitle
       */
      function CalendarEventTitlePipe(calendarEventTitle) {
        _classCallCheck(this, CalendarEventTitlePipe);

        this.calendarEventTitle = calendarEventTitle;
      }
      /**
       * @param {?} title
       * @param {?} titleType
       * @param {?} event
       * @return {?}
       */


      _createClass(CalendarEventTitlePipe, [{
        key: "transform",
        value: function transform(title, titleType, event) {
          return this.calendarEventTitle[titleType](event, title);
        }
      }]);

      return CalendarEventTitlePipe;
    }();

    CalendarEventTitlePipe.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
      args: [{
        name: 'calendarEventTitle'
      }]
    }];
    /** @nocollapse */

    CalendarEventTitlePipe.ctorParameters = function () {
      return [{
        type: CalendarEventTitleFormatter
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @type {?} */


    var isSupported = typeof window !== 'undefined' && typeof window['requestIdleCallback'] !== 'undefined';
    /**
     * @return {?}
     */

    function requestIdleCallbackObservable() {
      return new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](
      /**
      * @param {?} observer
      * @return {?}
      */
      function (observer) {
        /* istanbul ignore else  */
        if (isSupported) {
          /** @type {?} */
          var id = window['requestIdleCallback'](
          /**
          * @return {?}
          */
          function () {
            observer.next();
            observer.complete();
          });
          return (
            /**
            * @return {?}
            */
            function () {
              window['cancelIdleCallback'](id);
            }
          );
        } else {
          /** @type {?} */
          var timeoutId = setTimeout(
          /**
          * @return {?}
          */
          function () {
            observer.next();
            observer.complete();
          }, 1);
          return (
            /**
            * @return {?}
            */
            function () {
              clearTimeout(timeoutId);
            }
          );
        }
      });
    }
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @type {?} */


    var clickElements = new Set();
    /** @type {?} */

    var eventName = typeof window !== 'undefined' && typeof window['Hammer'] !== 'undefined' ? 'tap' : 'click';

    var ClickDirective = /*#__PURE__*/function () {
      /**
       * @param {?} renderer
       * @param {?} elm
       * @param {?} document
       * @param {?} zone
       */
      function ClickDirective(renderer, elm, document, zone) {
        _classCallCheck(this, ClickDirective);

        this.renderer = renderer;
        this.elm = elm;
        this.document = document;
        this.zone = zone;
        this.clickListenerDisabled = false;
        this.click = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); // tslint:disable-line
        // tslint:disable-line

        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
      }
      /**
       * @return {?}
       */


      _createClass(ClickDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          if (!this.clickListenerDisabled) {
            this.renderer.setAttribute(this.elm.nativeElement, 'data-calendar-clickable', 'true');
            clickElements.add(this.elm.nativeElement); // issue #942 - lazily initialise all click handlers after initial render as hammerjs is slow

            requestIdleCallbackObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMapTo"])(this.listen()), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.destroy$)).subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              // prevent child click events from firing on parent elements that also have click events

              /** @type {?} */
              var nearestClickableParent =
              /** @type {?} */
              event.target;

              while (!clickElements.has(nearestClickableParent) && nearestClickableParent !== _this4.document.body) {
                nearestClickableParent = nearestClickableParent.parentElement;
              }
              /** @type {?} */


              var isThisClickableElement = _this4.elm.nativeElement === nearestClickableParent;

              if (isThisClickableElement) {
                _this4.zone.run(
                /**
                * @return {?}
                */
                function () {
                  _this4.click.next(event);
                });
              }
            });
          }
        }
        /**
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.destroy$.next();
          clickElements["delete"](this.elm.nativeElement);
        }
        /**
         * @private
         * @return {?}
         */

      }, {
        key: "listen",
        value: function listen() {
          var _this5 = this;

          return new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](
          /**
          * @param {?} observer
          * @return {?}
          */
          function (observer) {
            return _this5.renderer.listen(_this5.elm.nativeElement, eventName,
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              observer.next(event);
            });
          });
        }
      }]);

      return ClickDirective;
    }();

    ClickDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[mwlClick]'
      }]
    }];
    /** @nocollapse */

    ClickDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
      }];
    };

    ClickDirective.propDecorators = {
      clickListenerDisabled: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      click: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
        args: ['mwlClick']
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarUtils = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarUtils(dateAdapter) {
        _classCallCheck(this, CalendarUtils);

        this.dateAdapter = dateAdapter;
      }
      /**
       * @param {?} args
       * @return {?}
       */


      _createClass(CalendarUtils, [{
        key: "getMonthView",
        value: function getMonthView(args) {
          return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["getMonthView"])(this.dateAdapter, args);
        }
        /**
         * @param {?} args
         * @return {?}
         */

      }, {
        key: "getWeekViewHeader",
        value: function getWeekViewHeader(args) {
          return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["getWeekViewHeader"])(this.dateAdapter, args);
        }
        /**
         * @param {?} args
         * @return {?}
         */

      }, {
        key: "getWeekView",
        value: function getWeekView(args) {
          return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["getWeekView"])(this.dateAdapter, args);
        }
        /**
         * @param {?} args
         * @return {?}
         */

      }, {
        key: "getDayView",
        value: function getDayView(args) {
          return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["getDayView"])(this.dateAdapter, args);
        }
        /**
         * @param {?} args
         * @return {?}
         */

      }, {
        key: "getDayViewHourGrid",
        value: function getDayViewHourGrid(args) {
          return Object(calendar_utils__WEBPACK_IMPORTED_MODULE_5__["getDayViewHourGrid"])(this.dateAdapter, args);
        }
      }]);

      return CalendarUtils;
    }();

    CalendarUtils.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];
    /** @nocollapse */

    CalendarUtils.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @type {?} */


    var MOMENT = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('Moment');
    /**
     * This will use <a href="http://momentjs.com/" target="_blank">moment</a> to do all date formatting. To use this class:
     *
     * ```typescript
     * import { CalendarDateFormatter, CalendarMomentDateFormatter, MOMENT } from 'angular-calendar';
     * import moment from 'moment';
     *
     * // in your component
     * provide: [{
     *   provide: MOMENT, useValue: moment
     * }, {
     *   provide: CalendarDateFormatter, useClass: CalendarMomentDateFormatter
     * }]
     *
     * ```
     */

    var CalendarMomentDateFormatter = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} moment
       * @param {?} dateAdapter
       */
      function CalendarMomentDateFormatter(moment, dateAdapter) {
        _classCallCheck(this, CalendarMomentDateFormatter);

        this.moment = moment;
        this.dateAdapter = dateAdapter;
      }
      /**
       * The month view header week day labels
       * @param {?} __0
       * @return {?}
       */


      _createClass(CalendarMomentDateFormatter, [{
        key: "monthViewColumnHeader",
        value: function monthViewColumnHeader(_ref11) {
          var date = _ref11.date,
              locale = _ref11.locale;
          return this.moment(date).locale(locale).format('dddd');
        }
        /**
         * The month view cell day number
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewDayNumber",
        value: function monthViewDayNumber(_ref12) {
          var date = _ref12.date,
              locale = _ref12.locale;
          return this.moment(date).locale(locale).format('D');
        }
        /**
         * The month view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewTitle",
        value: function monthViewTitle(_ref13) {
          var date = _ref13.date,
              locale = _ref13.locale;
          return this.moment(date).locale(locale).format('MMMM YYYY');
        }
        /**
         * The week view header week day labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnHeader",
        value: function weekViewColumnHeader(_ref14) {
          var date = _ref14.date,
              locale = _ref14.locale;
          return this.moment(date).locale(locale).format('dddd');
        }
        /**
         * The week view sub header day and month labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnSubHeader",
        value: function weekViewColumnSubHeader(_ref15) {
          var date = _ref15.date,
              locale = _ref15.locale;
          return this.moment(date).locale(locale).format('MMM D');
        }
        /**
         * The week view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewTitle",
        value: function weekViewTitle(_ref16) {
          var _this6 = this;

          var date = _ref16.date,
              locale = _ref16.locale,
              weekStartsOn = _ref16.weekStartsOn,
              excludeDays = _ref16.excludeDays,
              daysInWeek = _ref16.daysInWeek;

          var _getWeekViewPeriod2 = getWeekViewPeriod(this.dateAdapter, date, weekStartsOn, excludeDays, daysInWeek),
              viewStart = _getWeekViewPeriod2.viewStart,
              viewEnd = _getWeekViewPeriod2.viewEnd;
          /** @type {?} */


          var format =
          /**
          * @param {?} dateToFormat
          * @param {?} showYear
          * @return {?}
          */
          function format(dateToFormat, showYear) {
            return _this6.moment(dateToFormat).locale(locale).format('MMM D' + (showYear ? ', YYYY' : ''));
          };

          return "".concat(format(viewStart, viewStart.getUTCFullYear() !== viewEnd.getUTCFullYear()), " - ").concat(format(viewEnd, true));
        }
        /**
         * The time formatting down the left hand side of the week view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewHour",
        value: function weekViewHour(_ref17) {
          var date = _ref17.date,
              locale = _ref17.locale;
          return this.moment(date).locale(locale).format('ha');
        }
        /**
         * The time formatting down the left hand side of the day view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewHour",
        value: function dayViewHour(_ref18) {
          var date = _ref18.date,
              locale = _ref18.locale;
          return this.moment(date).locale(locale).format('ha');
        }
        /**
         * The day view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewTitle",
        value: function dayViewTitle(_ref19) {
          var date = _ref19.date,
              locale = _ref19.locale;
          return this.moment(date).locale(locale).format('dddd, D MMMM, YYYY');
        }
      }]);

      return CalendarMomentDateFormatter;
    }();

    CalendarMomentDateFormatter.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];
    /** @nocollapse */

    CalendarMomentDateFormatter.ctorParameters = function () {
      return [{
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [MOMENT]
        }]
      }, {
        type: DateAdapter
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * This will use <a href="https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Intl" target="_blank">Intl</a> API to do all date formatting.
     *
     * You will need to include a <a href="https://github.com/andyearnshaw/Intl.js/">polyfill</a> for older browsers.
     */


    var CalendarNativeDateFormatter = /*#__PURE__*/function () {
      /**
       * @param {?} dateAdapter
       */
      function CalendarNativeDateFormatter(dateAdapter) {
        _classCallCheck(this, CalendarNativeDateFormatter);

        this.dateAdapter = dateAdapter;
      }
      /**
       * The month view header week day labels
       * @param {?} __0
       * @return {?}
       */


      _createClass(CalendarNativeDateFormatter, [{
        key: "monthViewColumnHeader",
        value: function monthViewColumnHeader(_ref20) {
          var date = _ref20.date,
              locale = _ref20.locale;
          return new Intl.DateTimeFormat(locale, {
            weekday: 'long'
          }).format(date);
        }
        /**
         * The month view cell day number
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewDayNumber",
        value: function monthViewDayNumber(_ref21) {
          var date = _ref21.date,
              locale = _ref21.locale;
          return new Intl.DateTimeFormat(locale, {
            day: 'numeric'
          }).format(date);
        }
        /**
         * The month view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "monthViewTitle",
        value: function monthViewTitle(_ref22) {
          var date = _ref22.date,
              locale = _ref22.locale;
          return new Intl.DateTimeFormat(locale, {
            year: 'numeric',
            month: 'long'
          }).format(date);
        }
        /**
         * The week view header week day labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnHeader",
        value: function weekViewColumnHeader(_ref23) {
          var date = _ref23.date,
              locale = _ref23.locale;
          return new Intl.DateTimeFormat(locale, {
            weekday: 'long'
          }).format(date);
        }
        /**
         * The week view sub header day and month labels
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewColumnSubHeader",
        value: function weekViewColumnSubHeader(_ref24) {
          var date = _ref24.date,
              locale = _ref24.locale;
          return new Intl.DateTimeFormat(locale, {
            day: 'numeric',
            month: 'short'
          }).format(date);
        }
        /**
         * The week view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewTitle",
        value: function weekViewTitle(_ref25) {
          var date = _ref25.date,
              locale = _ref25.locale,
              weekStartsOn = _ref25.weekStartsOn,
              excludeDays = _ref25.excludeDays,
              daysInWeek = _ref25.daysInWeek;

          var _getWeekViewPeriod3 = getWeekViewPeriod(this.dateAdapter, date, weekStartsOn, excludeDays, daysInWeek),
              viewStart = _getWeekViewPeriod3.viewStart,
              viewEnd = _getWeekViewPeriod3.viewEnd;
          /** @type {?} */


          var format =
          /**
          * @param {?} dateToFormat
          * @param {?} showYear
          * @return {?}
          */
          function format(dateToFormat, showYear) {
            return new Intl.DateTimeFormat(locale, {
              day: 'numeric',
              month: 'short',
              year: showYear ? 'numeric' : undefined
            }).format(dateToFormat);
          };

          return "".concat(format(viewStart, viewStart.getUTCFullYear() !== viewEnd.getUTCFullYear()), " - ").concat(format(viewEnd, true));
        }
        /**
         * The time formatting down the left hand side of the week view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "weekViewHour",
        value: function weekViewHour(_ref26) {
          var date = _ref26.date,
              locale = _ref26.locale;
          return new Intl.DateTimeFormat(locale, {
            hour: 'numeric'
          }).format(date);
        }
        /**
         * The time formatting down the left hand side of the day view
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewHour",
        value: function dayViewHour(_ref27) {
          var date = _ref27.date,
              locale = _ref27.locale;
          return new Intl.DateTimeFormat(locale, {
            hour: 'numeric'
          }).format(date);
        }
        /**
         * The day view title
         * @param {?} __0
         * @return {?}
         */

      }, {
        key: "dayViewTitle",
        value: function dayViewTitle(_ref28) {
          var date = _ref28.date,
              locale = _ref28.locale;
          return new Intl.DateTimeFormat(locale, {
            day: 'numeric',
            month: 'long',
            year: 'numeric',
            weekday: 'long'
          }).format(date);
        }
      }]);

      return CalendarNativeDateFormatter;
    }();

    CalendarNativeDateFormatter.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];
    /** @nocollapse */

    CalendarNativeDateFormatter.ctorParameters = function () {
      return [{
        type: DateAdapter
      }];
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @enum {string} */


    var CalendarEventTimesChangedEventType = {
      Drag: 'drag',
      Drop: 'drop',
      Resize: 'resize'
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Import this module to if you're just using a singular view and want to save on bundle size. Example usage:
     *
     * ```typescript
     * import { CalendarCommonModule, CalendarMonthModule } from 'angular-calendar';
     *
     * \@NgModule({
     *   imports: [
     *     CalendarCommonModule.forRoot(),
     *     CalendarMonthModule
     *   ]
     * })
     * class MyModule {}
     * ```
     *
     */

    var CalendarCommonModule = /*#__PURE__*/function () {
      function CalendarCommonModule() {
        _classCallCheck(this, CalendarCommonModule);
      }

      _createClass(CalendarCommonModule, null, [{
        key: "forRoot",

        /**
         * @param {?} dateAdapter
         * @param {?=} config
         * @return {?}
         */
        value: function forRoot(dateAdapter) {
          var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          return {
            ngModule: CalendarCommonModule,
            providers: [dateAdapter, config.eventTitleFormatter || CalendarEventTitleFormatter, config.dateFormatter || CalendarDateFormatter, config.utils || CalendarUtils]
          };
        }
      }]);

      return CalendarCommonModule;
    }();

    CalendarCommonModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        declarations: [CalendarEventActionsComponent, CalendarEventTitleComponent, CalendarTooltipWindowComponent, CalendarTooltipDirective, CalendarPreviousViewDirective, CalendarNextViewDirective, CalendarTodayDirective, CalendarDatePipe, CalendarEventTitlePipe, ClickDirective],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
        exports: [CalendarEventActionsComponent, CalendarEventTitleComponent, CalendarTooltipWindowComponent, CalendarTooltipDirective, CalendarPreviousViewDirective, CalendarNextViewDirective, CalendarTodayDirective, CalendarDatePipe, CalendarEventTitlePipe, ClickDirective],
        entryComponents: [CalendarTooltipWindowComponent]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Shows all events on a given month. Example usage:
     *
     * ```typescript
     * <mwl-calendar-month-view
     *  [viewDate]="viewDate"
     *  [events]="events">
     * </mwl-calendar-month-view>
     * ```
     */

    var CalendarMonthViewComponent = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} cdr
       * @param {?} utils
       * @param {?} locale
       * @param {?} dateAdapter
       */
      function CalendarMonthViewComponent(cdr, utils, locale, dateAdapter) {
        var _this7 = this;

        _classCallCheck(this, CalendarMonthViewComponent);

        this.cdr = cdr;
        this.utils = utils;
        this.dateAdapter = dateAdapter;
        /**
         * An array of events to display on view.
         * The schema is available here: https://github.com/mattlewis92/calendar-utils/blob/c51689985f59a271940e30bc4e2c4e1fee3fcb5c/src/calendarUtils.ts#L49-L63
         */

        this.events = [];
        /**
         * An array of day indexes (0 = sunday, 1 = monday etc) that will be hidden on the view
         */

        this.excludeDays = [];
        /**
         * Whether the events list for the day of the `viewDate` option is visible or not
         */

        this.activeDayIsOpen = false;
        /**
         * The placement of the event tooltip
         */

        this.tooltipPlacement = 'auto';
        /**
         * Whether to append tooltips to the body or next to the trigger element
         */

        this.tooltipAppendToBody = true;
        /**
         * The delay in milliseconds before the tooltip should be displayed. If not provided the tooltip
         * will be displayed immediately.
         */

        this.tooltipDelay = null;
        /**
         * An output that will be called before the view is rendered for the current month.
         * If you add the `cssClass` property to a day in the body it will add that class to the cell element in the template
         */

        this.beforeViewRender = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when the day cell is clicked
         */

        this.dayClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when the event title is clicked
         */

        this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when a header week day is clicked. Returns ISO day number.
         */

        this.columnHeaderClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when an event is dragged and dropped
         */

        this.eventTimesChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * @hidden
         */

        this.trackByRowOffset =
        /**
        * @param {?} index
        * @param {?} offset
        * @return {?}
        */
        function (index, offset) {
          return _this7.view.days.slice(offset, _this7.view.totalDaysVisibleInWeek).map(
          /**
          * @param {?} day
          * @return {?}
          */
          function (day) {
            return day.date.toISOString();
          }).join('-');
        };
        /**
         * @hidden
         */


        this.trackByDate =
        /**
        * @param {?} index
        * @param {?} day
        * @return {?}
        */
        function (index, day) {
          return day.date.toISOString();
        };

        this.locale = locale;
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarMonthViewComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this8 = this;

          if (this.refresh) {
            this.refreshSubscription = this.refresh.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this8.refreshAll();

              _this8.cdr.markForCheck();
            });
          }
        }
        /**
         * @hidden
         * @param {?} changes
         * @return {?}
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          /** @type {?} */
          var refreshHeader = changes.viewDate || changes.excludeDays || changes.weekendDays;
          /** @type {?} */

          var refreshBody = changes.viewDate || changes.events || changes.excludeDays || changes.weekendDays;

          if (refreshHeader) {
            this.refreshHeader();
          }

          if (changes.events) {
            validateEvents(this.events);
          }

          if (refreshBody) {
            this.refreshBody();
          }

          if (refreshHeader || refreshBody) {
            this.emitBeforeViewRender();
          }

          if (changes.activeDayIsOpen || changes.viewDate || changes.events || changes.excludeDays || changes.activeDay) {
            this.checkActiveDayIsOpen();
          }
        }
        /**
         * @hidden
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
          }
        }
        /**
         * @hidden
         * @param {?} event
         * @param {?} isHighlighted
         * @return {?}
         */

      }, {
        key: "toggleDayHighlight",
        value: function toggleDayHighlight(event, isHighlighted) {
          this.view.days.forEach(
          /**
          * @param {?} day
          * @return {?}
          */
          function (day) {
            if (isHighlighted && day.events.indexOf(event) > -1) {
              day.backgroundColor = event.color && event.color.secondary || '#D1E8FF';
            } else {
              delete day.backgroundColor;
            }
          });
        }
        /**
         * @hidden
         * @param {?} droppedOn
         * @param {?} event
         * @param {?=} draggedFrom
         * @return {?}
         */

      }, {
        key: "eventDropped",
        value: function eventDropped(droppedOn, event, draggedFrom) {
          if (droppedOn !== draggedFrom) {
            /** @type {?} */
            var year = this.dateAdapter.getYear(droppedOn.date);
            /** @type {?} */

            var month = this.dateAdapter.getMonth(droppedOn.date);
            /** @type {?} */

            var date = this.dateAdapter.getDate(droppedOn.date);
            /** @type {?} */

            var newStart = this.dateAdapter.setDate(this.dateAdapter.setMonth(this.dateAdapter.setYear(event.start, year), month), date);
            /** @type {?} */

            var newEnd;

            if (event.end) {
              /** @type {?} */
              var secondsDiff = this.dateAdapter.differenceInSeconds(newStart, event.start);
              newEnd = this.dateAdapter.addSeconds(event.end, secondsDiff);
            }

            this.eventTimesChanged.emit({
              event: event,
              newStart: newStart,
              newEnd: newEnd,
              day: droppedOn,
              type: CalendarEventTimesChangedEventType.Drop
            });
          }
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshHeader",
        value: function refreshHeader() {
          this.columnHeaders = this.utils.getWeekViewHeader({
            viewDate: this.viewDate,
            weekStartsOn: this.weekStartsOn,
            excluded: this.excludeDays,
            weekendDays: this.weekendDays
          });
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshBody",
        value: function refreshBody() {
          this.view = this.utils.getMonthView({
            events: this.events,
            viewDate: this.viewDate,
            weekStartsOn: this.weekStartsOn,
            excluded: this.excludeDays,
            weekendDays: this.weekendDays
          });
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "checkActiveDayIsOpen",
        value: function checkActiveDayIsOpen() {
          var _this9 = this;

          if (this.activeDayIsOpen === true) {
            /** @type {?} */
            var activeDay = this.activeDay || this.viewDate;
            this.openDay = this.view.days.find(
            /**
            * @param {?} day
            * @return {?}
            */
            function (day) {
              return _this9.dateAdapter.isSameDay(day.date, activeDay);
            });
            /** @type {?} */

            var index = this.view.days.indexOf(this.openDay);
            this.openRowIndex = Math.floor(index / this.view.totalDaysVisibleInWeek) * this.view.totalDaysVisibleInWeek;
          } else {
            this.openRowIndex = null;
            this.openDay = null;
          }
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshAll",
        value: function refreshAll() {
          this.refreshHeader();
          this.refreshBody();
          this.emitBeforeViewRender();
          this.checkActiveDayIsOpen();
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "emitBeforeViewRender",
        value: function emitBeforeViewRender() {
          if (this.columnHeaders && this.view) {
            this.beforeViewRender.emit({
              header: this.columnHeaders,
              body: this.view.days,
              period: this.view.period
            });
          }
        }
      }]);

      return CalendarMonthViewComponent;
    }();

    CalendarMonthViewComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-month-view',
        template: "\n    <div class=\"cal-month-view\">\n      <mwl-calendar-month-view-header\n        [days]=\"columnHeaders\"\n        [locale]=\"locale\"\n        (columnHeaderClicked)=\"columnHeaderClicked.emit($event)\"\n        [customTemplate]=\"headerTemplate\"\n      >\n        >\n      </mwl-calendar-month-view-header>\n      <div class=\"cal-days\">\n        <div\n          *ngFor=\"let rowIndex of view.rowOffsets; trackBy: trackByRowOffset\"\n        >\n          <div class=\"cal-cell-row\">\n            <mwl-calendar-month-cell\n              *ngFor=\"\n                let day of view.days\n                  | slice: rowIndex:rowIndex + view.totalDaysVisibleInWeek;\n                trackBy: trackByDate\n              \"\n              [ngClass]=\"day?.cssClass\"\n              [day]=\"day\"\n              [openDay]=\"openDay\"\n              [locale]=\"locale\"\n              [tooltipPlacement]=\"tooltipPlacement\"\n              [tooltipAppendToBody]=\"tooltipAppendToBody\"\n              [tooltipTemplate]=\"tooltipTemplate\"\n              [tooltipDelay]=\"tooltipDelay\"\n              [customTemplate]=\"cellTemplate\"\n              [ngStyle]=\"{ backgroundColor: day.backgroundColor }\"\n              (mwlClick)=\"dayClicked.emit({ day: day })\"\n              [clickListenerDisabled]=\"dayClicked.observers.length === 0\"\n              (highlightDay)=\"toggleDayHighlight($event.event, true)\"\n              (unhighlightDay)=\"toggleDayHighlight($event.event, false)\"\n              mwlDroppable\n              dragOverClass=\"cal-drag-over\"\n              (drop)=\"\n                eventDropped(\n                  day,\n                  $event.dropData.event,\n                  $event.dropData.draggedFrom\n                )\n              \"\n              (eventClicked)=\"eventClicked.emit({ event: $event.event })\"\n            >\n            </mwl-calendar-month-cell>\n          </div>\n          <mwl-calendar-open-day-events\n            [isOpen]=\"openRowIndex === rowIndex\"\n            [events]=\"openDay?.events\"\n            [customTemplate]=\"openDayEventsTemplate\"\n            [eventTitleTemplate]=\"eventTitleTemplate\"\n            [eventActionsTemplate]=\"eventActionsTemplate\"\n            (eventClicked)=\"eventClicked.emit({ event: $event.event })\"\n            mwlDroppable\n            dragOverClass=\"cal-drag-over\"\n            (drop)=\"\n              eventDropped(\n                openDay,\n                $event.dropData.event,\n                $event.dropData.draggedFrom\n              )\n            \"\n          >\n          </mwl-calendar-open-day-events>\n        </div>\n      </div>\n    </div>\n  "
      }]
    }];
    /** @nocollapse */

    CalendarMonthViewComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: CalendarUtils
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]
        }]
      }, {
        type: DateAdapter
      }];
    };

    CalendarMonthViewComponent.propDecorators = {
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      events: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      excludeDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      activeDayIsOpen: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      activeDay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      refresh: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      weekStartsOn: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      headerTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      cellTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      openDayEventsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      weekendDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      beforeViewRender: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      dayClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      columnHeaderClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventTimesChanged: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarMonthViewHeaderComponent = function CalendarMonthViewHeaderComponent() {
      _classCallCheck(this, CalendarMonthViewHeaderComponent);

      this.columnHeaderClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.trackByWeekDayHeaderDate = trackByWeekDayHeaderDate;
    };

    CalendarMonthViewHeaderComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-month-view-header',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-days=\"days\"\n      let-locale=\"locale\"\n      let-trackByWeekDayHeaderDate=\"trackByWeekDayHeaderDate\"\n    >\n      <div class=\"cal-cell-row cal-header\">\n        <div\n          class=\"cal-cell\"\n          *ngFor=\"let day of days; trackBy: trackByWeekDayHeaderDate\"\n          [class.cal-past]=\"day.isPast\"\n          [class.cal-today]=\"day.isToday\"\n          [class.cal-future]=\"day.isFuture\"\n          [class.cal-weekend]=\"day.isWeekend\"\n          (click)=\"columnHeaderClicked.emit(day.day)\"\n          [ngClass]=\"day.cssClass\"\n        >\n          {{ day.date | calendarDate: 'monthViewColumnHeader':locale }}\n        </div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        days: days,\n        locale: locale,\n        trackByWeekDayHeaderDate: trackByWeekDayHeaderDate\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarMonthViewHeaderComponent.propDecorators = {
      days: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      columnHeaderClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarMonthCellComponent = function CalendarMonthCellComponent() {
      _classCallCheck(this, CalendarMonthCellComponent);

      this.highlightDay = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.unhighlightDay = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.trackByEventId = trackByEventId;
      this.validateDrag = isWithinThreshold;
    };

    CalendarMonthCellComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-month-cell',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-day=\"day\"\n      let-openDay=\"openDay\"\n      let-locale=\"locale\"\n      let-tooltipPlacement=\"tooltipPlacement\"\n      let-highlightDay=\"highlightDay\"\n      let-unhighlightDay=\"unhighlightDay\"\n      let-eventClicked=\"eventClicked\"\n      let-tooltipTemplate=\"tooltipTemplate\"\n      let-tooltipAppendToBody=\"tooltipAppendToBody\"\n      let-tooltipDelay=\"tooltipDelay\"\n      let-trackByEventId=\"trackByEventId\"\n      let-validateDrag=\"validateDrag\"\n    >\n      <div class=\"cal-cell-top\">\n        <span class=\"cal-day-badge\" *ngIf=\"day.badgeTotal > 0\">{{\n          day.badgeTotal\n        }}</span>\n        <span class=\"cal-day-number\">{{\n          day.date | calendarDate: 'monthViewDayNumber':locale\n        }}</span>\n      </div>\n      <div class=\"cal-events\" *ngIf=\"day.events.length > 0\">\n        <div\n          class=\"cal-event\"\n          *ngFor=\"let event of day.events; trackBy: trackByEventId\"\n          [ngStyle]=\"{ backgroundColor: event.color?.primary }\"\n          [ngClass]=\"event?.cssClass\"\n          (mouseenter)=\"highlightDay.emit({ event: event })\"\n          (mouseleave)=\"unhighlightDay.emit({ event: event })\"\n          [mwlCalendarTooltip]=\"\n            event.title | calendarEventTitle: 'monthTooltip':event\n          \"\n          [tooltipPlacement]=\"tooltipPlacement\"\n          [tooltipEvent]=\"event\"\n          [tooltipTemplate]=\"tooltipTemplate\"\n          [tooltipAppendToBody]=\"tooltipAppendToBody\"\n          [tooltipDelay]=\"tooltipDelay\"\n          mwlDraggable\n          [class.cal-draggable]=\"event.draggable\"\n          dragActiveClass=\"cal-drag-active\"\n          [dropData]=\"{ event: event, draggedFrom: day }\"\n          [dragAxis]=\"{ x: event.draggable, y: event.draggable }\"\n          [validateDrag]=\"validateDrag\"\n          (mwlClick)=\"eventClicked.emit({ event: event })\"\n        ></div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        day: day,\n        openDay: openDay,\n        locale: locale,\n        tooltipPlacement: tooltipPlacement,\n        highlightDay: highlightDay,\n        unhighlightDay: unhighlightDay,\n        eventClicked: eventClicked,\n        tooltipTemplate: tooltipTemplate,\n        tooltipAppendToBody: tooltipAppendToBody,\n        tooltipDelay: tooltipDelay,\n        trackByEventId: trackByEventId,\n        validateDrag: validateDrag\n      }\"\n    >\n    </ng-template>\n  ",
        host: {
          "class": 'cal-cell cal-day-cell',
          '[class.cal-past]': 'day.isPast',
          '[class.cal-today]': 'day.isToday',
          '[class.cal-future]': 'day.isFuture',
          '[class.cal-weekend]': 'day.isWeekend',
          '[class.cal-in-month]': 'day.inMonth',
          '[class.cal-out-month]': '!day.inMonth',
          '[class.cal-has-events]': 'day.events.length > 0',
          '[class.cal-open]': 'day === openDay',
          '[class.cal-event-highlight]': '!!day.backgroundColor'
        }
      }]
    }];
    CalendarMonthCellComponent.propDecorators = {
      day: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      openDay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      highlightDay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      unhighlightDay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @type {?} */

    var collapseAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["trigger"])('collapse', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["style"])({
      height: 0,
      overflow: 'hidden',
      'padding-top': 0,
      'padding-bottom': 0
    })), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["style"])({
      height: '*',
      overflow: 'hidden',
      'padding-top': '*',
      'padding-bottom': '*'
    })), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["transition"])('* => void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["animate"])('150ms ease-out')), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["transition"])('void => *', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_7__["animate"])('150ms ease-in'))]);

    var CalendarOpenDayEventsComponent = function CalendarOpenDayEventsComponent() {
      _classCallCheck(this, CalendarOpenDayEventsComponent);

      this.isOpen = false;
      this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.trackByEventId = trackByEventId;
      this.validateDrag = isWithinThreshold;
    };

    CalendarOpenDayEventsComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-open-day-events',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-events=\"events\"\n      let-eventClicked=\"eventClicked\"\n      let-isOpen=\"isOpen\"\n      let-trackByEventId=\"trackByEventId\"\n      let-validateDrag=\"validateDrag\"\n    >\n      <div class=\"cal-open-day-events\" [@collapse] *ngIf=\"isOpen\">\n        <div\n          *ngFor=\"let event of events; trackBy: trackByEventId\"\n          [ngClass]=\"event?.cssClass\"\n          mwlDraggable\n          [class.cal-draggable]=\"event.draggable\"\n          dragActiveClass=\"cal-drag-active\"\n          [dropData]=\"{ event: event }\"\n          [dragAxis]=\"{ x: event.draggable, y: event.draggable }\"\n          [validateDrag]=\"validateDrag\"\n        >\n          <span\n            class=\"cal-event\"\n            [ngStyle]=\"{ backgroundColor: event.color?.primary }\"\n          >\n          </span>\n          &ngsp;\n          <mwl-calendar-event-title\n            [event]=\"event\"\n            [customTemplate]=\"eventTitleTemplate\"\n            view=\"month\"\n            (mwlClick)=\"eventClicked.emit({ event: event })\"\n          >\n          </mwl-calendar-event-title>\n          &ngsp;\n          <mwl-calendar-event-actions\n            [event]=\"event\"\n            [customTemplate]=\"eventActionsTemplate\"\n          >\n          </mwl-calendar-event-actions>\n        </div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        events: events,\n        eventClicked: eventClicked,\n        isOpen: isOpen,\n        trackByEventId: trackByEventId,\n        validateDrag: validateDrag\n      }\"\n    >\n    </ng-template>\n  ",
        animations: [collapseAnimation]
      }]
    }];
    CalendarOpenDayEventsComponent.propDecorators = {
      isOpen: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      events: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarMonthModule = function CalendarMonthModule() {
      _classCallCheck(this, CalendarMonthModule);
    };

    CalendarMonthModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarCommonModule],
        declarations: [CalendarMonthViewComponent, CalendarMonthCellComponent, CalendarOpenDayEventsComponent, CalendarMonthViewHeaderComponent],
        exports: [angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarMonthViewComponent, CalendarMonthCellComponent, CalendarOpenDayEventsComponent, CalendarMonthViewHeaderComponent]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarDragHelper = /*#__PURE__*/function () {
      /**
       * @param {?} dragContainerElement
       * @param {?} draggableElement
       */
      function CalendarDragHelper(dragContainerElement, draggableElement) {
        _classCallCheck(this, CalendarDragHelper);

        this.dragContainerElement = dragContainerElement;
        this.startPosition = draggableElement.getBoundingClientRect();
      }
      /**
       * @param {?} __0
       * @return {?}
       */


      _createClass(CalendarDragHelper, [{
        key: "validateDrag",
        value: function validateDrag(_ref29) {
          var x = _ref29.x,
              y = _ref29.y,
              snapDraggedEvents = _ref29.snapDraggedEvents,
              dragAlreadyMoved = _ref29.dragAlreadyMoved,
              transform = _ref29.transform;

          if (snapDraggedEvents) {
            /** @type {?} */
            var newRect = Object.assign({}, this.startPosition, {
              left: this.startPosition.left + transform.x,
              right: this.startPosition.right + transform.x,
              top: this.startPosition.top + transform.y,
              bottom: this.startPosition.bottom + transform.y
            });
            return (isWithinThreshold({
              x: x,
              y: y
            }) || dragAlreadyMoved) && isInside(this.dragContainerElement.getBoundingClientRect(), newRect);
          } else {
            return isWithinThreshold({
              x: x,
              y: y
            }) || dragAlreadyMoved;
          }
        }
      }]);

      return CalendarDragHelper;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var CalendarResizeHelper = /*#__PURE__*/function () {
      /**
       * @param {?} resizeContainerElement
       * @param {?=} minWidth
       */
      function CalendarResizeHelper(resizeContainerElement, minWidth) {
        _classCallCheck(this, CalendarResizeHelper);

        this.resizeContainerElement = resizeContainerElement;
        this.minWidth = minWidth;
      }
      /**
       * @param {?} __0
       * @return {?}
       */


      _createClass(CalendarResizeHelper, [{
        key: "validateResize",
        value: function validateResize(_ref30) {
          var rectangle = _ref30.rectangle;

          if (this.minWidth && Math.ceil(rectangle.width) < Math.ceil(this.minWidth)) {
            return false;
          }

          return isInside(this.resizeContainerElement.getBoundingClientRect(), rectangle);
        }
      }]);

      return CalendarResizeHelper;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Shows all events on a given week. Example usage:
     *
     * ```typescript
     * <mwl-calendar-week-view
     *  [viewDate]="viewDate"
     *  [events]="events">
     * </mwl-calendar-week-view>
     * ```
     */


    var CalendarWeekViewComponent = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} cdr
       * @param {?} utils
       * @param {?} locale
       * @param {?} dateAdapter
       */
      function CalendarWeekViewComponent(cdr, utils, locale, dateAdapter) {
        _classCallCheck(this, CalendarWeekViewComponent);

        this.cdr = cdr;
        this.utils = utils;
        this.dateAdapter = dateAdapter;
        /**
         * An array of events to display on view
         * The schema is available here: https://github.com/mattlewis92/calendar-utils/blob/c51689985f59a271940e30bc4e2c4e1fee3fcb5c/src/calendarUtils.ts#L49-L63
         */

        this.events = [];
        /**
         * An array of day indexes (0 = sunday, 1 = monday etc) that will be hidden on the view
         */

        this.excludeDays = [];
        /**
         * The placement of the event tooltip
         */

        this.tooltipPlacement = 'auto';
        /**
         * Whether to append tooltips to the body or next to the trigger element
         */

        this.tooltipAppendToBody = true;
        /**
         * The delay in milliseconds before the tooltip should be displayed. If not provided the tooltip
         * will be displayed immediately.
         */

        this.tooltipDelay = null;
        /**
         * The precision to display events.
         * `days` will round event start and end dates to the nearest day and `minutes` will not do this rounding
         */

        this.precision = 'days';
        /**
         * Whether to snap events to a grid when dragging
         */

        this.snapDraggedEvents = true;
        /**
         * The number of segments in an hour. Must be <= 6
         */

        this.hourSegments = 2;
        /**
         * The height in pixels of each hour segment
         */

        this.hourSegmentHeight = 30;
        /**
         * The day start hours in 24 hour time. Must be 0-23
         */

        this.dayStartHour = 0;
        /**
         * The day start minutes. Must be 0-59
         */

        this.dayStartMinute = 0;
        /**
         * The day end hours in 24 hour time. Must be 0-23
         */

        this.dayEndHour = 23;
        /**
         * The day end minutes. Must be 0-59
         */

        this.dayEndMinute = 59;
        /**
         * Called when a header week day is clicked. Adding a `cssClass` property on `$event.day` will add that class to the header element
         */

        this.dayHeaderClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when the event title is clicked
         */

        this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when an event is resized or dragged and dropped
         */

        this.eventTimesChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * An output that will be called before the view is rendered for the current week.
         * If you add the `cssClass` property to a day in the header it will add that class to the cell element in the template
         */

        this.beforeViewRender = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when an hour segment is clicked
         */

        this.hourSegmentClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * @hidden
         */

        this.allDayEventResizes = new Map();
        /**
         * @hidden
         */

        this.timeEventResizes = new Map();
        /**
         * @hidden
         */

        this.eventDragEnter = 0;
        /**
         * @hidden
         */

        this.dragActive = false;
        /**
         * @hidden
         */

        this.dragAlreadyMoved = false;
        /**
         * @hidden
         */

        this.calendarId = Symbol('angular calendar week view id');
        /**
         * @hidden
         */

        this.trackByWeekDayHeaderDate = trackByWeekDayHeaderDate;
        /**
         * @hidden
         */

        this.trackByHourSegment = trackByHourSegment;
        /**
         * @hidden
         */

        this.trackByHour = trackByHour;
        /**
         * @hidden
         */

        this.trackByDayOrWeekEvent = trackByDayOrWeekEvent;
        /**
         * @hidden
         */

        this.trackByHourColumn =
        /**
        * @param {?} index
        * @param {?} column
        * @return {?}
        */
        function (index, column) {
          return column.hours[0] ? column.hours[0].segments[0].date.toISOString() : column;
        };
        /**
         * @hidden
         */


        this.trackById =
        /**
        * @param {?} index
        * @param {?} row
        * @return {?}
        */
        function (index, row) {
          return row.id;
        };

        this.locale = locale;
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarWeekViewComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this10 = this;

          if (this.refresh) {
            this.refreshSubscription = this.refresh.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this10.refreshAll();

              _this10.cdr.markForCheck();
            });
          }
        }
        /**
         * @hidden
         * @param {?} changes
         * @return {?}
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          /** @type {?} */
          var refreshHeader = changes.viewDate || changes.excludeDays || changes.weekendDays || changes.daysInWeek || changes.weekStartsOn;
          /** @type {?} */

          var refreshBody = changes.viewDate || changes.dayStartHour || changes.dayStartMinute || changes.dayEndHour || changes.dayEndMinute || changes.hourSegments || changes.weekStartsOn || changes.weekendDays || changes.excludeDays || changes.hourSegmentHeight || changes.events || changes.daysInWeek;

          if (refreshHeader) {
            this.refreshHeader();
          }

          if (changes.events) {
            validateEvents(this.events);
          }

          if (refreshBody) {
            this.refreshBody();
          }

          if (refreshHeader || refreshBody) {
            this.emitBeforeViewRender();
          }
        }
        /**
         * @hidden
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
          }
        }
        /**
         * @protected
         * @param {?} eventsContainer
         * @param {?=} minWidth
         * @return {?}
         */

      }, {
        key: "resizeStarted",
        value: function resizeStarted(eventsContainer, minWidth) {
          this.dayColumnWidth = this.getDayColumnWidth(eventsContainer);
          /** @type {?} */

          var resizeHelper = new CalendarResizeHelper(eventsContainer, minWidth);

          this.validateResize =
          /**
          * @param {?} __0
          * @return {?}
          */
          function (_ref31) {
            var rectangle = _ref31.rectangle;
            return resizeHelper.validateResize({
              rectangle: rectangle
            });
          };

          this.cdr.markForCheck();
        }
        /**
         * @hidden
         * @param {?} eventsContainer
         * @param {?} timeEvent
         * @param {?} resizeEvent
         * @return {?}
         */

      }, {
        key: "timeEventResizeStarted",
        value: function timeEventResizeStarted(eventsContainer, timeEvent, resizeEvent) {
          this.timeEventResizes.set(timeEvent.event, resizeEvent);
          this.resizeStarted(eventsContainer);
        }
        /**
         * @hidden
         * @param {?} timeEvent
         * @param {?} resizeEvent
         * @return {?}
         */

      }, {
        key: "timeEventResizing",
        value: function timeEventResizing(timeEvent, resizeEvent) {
          var _this11 = this;

          this.timeEventResizes.set(timeEvent.event, resizeEvent);
          /** @type {?} */

          var adjustedEvents = new Map();
          /** @type {?} */

          var tempEvents = _toConsumableArray(this.events);

          this.timeEventResizes.forEach(
          /**
          * @param {?} lastResizeEvent
          * @param {?} event
          * @return {?}
          */
          function (lastResizeEvent, event) {
            /** @type {?} */
            var newEventDates = _this11.getTimeEventResizedDates(event, lastResizeEvent);
            /** @type {?} */


            var adjustedEvent = Object.assign({}, event, newEventDates);
            adjustedEvents.set(adjustedEvent, event);
            /** @type {?} */

            var eventIndex = tempEvents.indexOf(event);
            tempEvents[eventIndex] = adjustedEvent;
          });
          this.restoreOriginalEvents(tempEvents, adjustedEvents);
        }
        /**
         * @hidden
         * @param {?} timeEvent
         * @return {?}
         */

      }, {
        key: "timeEventResizeEnded",
        value: function timeEventResizeEnded(timeEvent) {
          this.view = this.getWeekView(this.events);
          /** @type {?} */

          var lastResizeEvent = this.timeEventResizes.get(timeEvent.event);

          if (lastResizeEvent) {
            this.timeEventResizes["delete"](timeEvent.event);
            /** @type {?} */

            var newEventDates = this.getTimeEventResizedDates(timeEvent.event, lastResizeEvent);
            this.eventTimesChanged.emit({
              newStart: newEventDates.start,
              newEnd: newEventDates.end,
              event: timeEvent.event,
              type: CalendarEventTimesChangedEventType.Resize
            });
          }
        }
        /**
         * @hidden
         * @param {?} allDayEventsContainer
         * @param {?} allDayEvent
         * @param {?} resizeEvent
         * @return {?}
         */

      }, {
        key: "allDayEventResizeStarted",
        value: function allDayEventResizeStarted(allDayEventsContainer, allDayEvent, resizeEvent) {
          this.allDayEventResizes.set(allDayEvent, {
            originalOffset: allDayEvent.offset,
            originalSpan: allDayEvent.span,
            edge: typeof resizeEvent.edges.left !== 'undefined' ? 'left' : 'right'
          });
          this.resizeStarted(allDayEventsContainer, this.getDayColumnWidth(allDayEventsContainer));
        }
        /**
         * @hidden
         * @param {?} allDayEvent
         * @param {?} resizeEvent
         * @param {?} dayWidth
         * @return {?}
         */

      }, {
        key: "allDayEventResizing",
        value: function allDayEventResizing(allDayEvent, resizeEvent, dayWidth) {
          /** @type {?} */
          var currentResize = this.allDayEventResizes.get(allDayEvent);

          if (typeof resizeEvent.edges.left !== 'undefined') {
            /** @type {?} */
            var diff = Math.round(+resizeEvent.edges.left / dayWidth);
            allDayEvent.offset = currentResize.originalOffset + diff;
            allDayEvent.span = currentResize.originalSpan - diff;
          } else if (typeof resizeEvent.edges.right !== 'undefined') {
            /** @type {?} */
            var _diff = Math.round(+resizeEvent.edges.right / dayWidth);

            allDayEvent.span = currentResize.originalSpan + _diff;
          }
        }
        /**
         * @hidden
         * @param {?} allDayEvent
         * @return {?}
         */

      }, {
        key: "allDayEventResizeEnded",
        value: function allDayEventResizeEnded(allDayEvent) {
          /** @type {?} */
          var currentResize = this.allDayEventResizes.get(allDayEvent);

          if (currentResize) {
            /** @type {?} */
            var allDayEventResizingBeforeStart = currentResize.edge === 'left';
            /** @type {?} */

            var daysDiff;

            if (allDayEventResizingBeforeStart) {
              daysDiff = allDayEvent.offset - currentResize.originalOffset;
            } else {
              daysDiff = allDayEvent.span - currentResize.originalSpan;
            }

            allDayEvent.offset = currentResize.originalOffset;
            allDayEvent.span = currentResize.originalSpan;
            /** @type {?} */

            var newStart = allDayEvent.event.start;
            /** @type {?} */

            var newEnd = allDayEvent.event.end || allDayEvent.event.start;

            if (allDayEventResizingBeforeStart) {
              newStart = addDaysWithExclusions(this.dateAdapter, newStart, daysDiff, this.excludeDays);
            } else {
              newEnd = addDaysWithExclusions(this.dateAdapter, newEnd, daysDiff, this.excludeDays);
            }

            this.eventTimesChanged.emit({
              newStart: newStart,
              newEnd: newEnd,
              event: allDayEvent.event,
              type: CalendarEventTimesChangedEventType.Resize
            });
            this.allDayEventResizes["delete"](allDayEvent);
          }
        }
        /**
         * @hidden
         * @param {?} eventRowContainer
         * @return {?}
         */

      }, {
        key: "getDayColumnWidth",
        value: function getDayColumnWidth(eventRowContainer) {
          return Math.floor(eventRowContainer.offsetWidth / this.days.length);
        }
        /**
         * @hidden
         * @param {?} dropEvent
         * @param {?} date
         * @param {?} allDay
         * @return {?}
         */

      }, {
        key: "eventDropped",
        value: function eventDropped(dropEvent, date, allDay) {
          if (shouldFireDroppedEvent(dropEvent, date, allDay, this.calendarId)) {
            this.eventTimesChanged.emit({
              type: CalendarEventTimesChangedEventType.Drop,
              event: dropEvent.dropData.event,
              newStart: date,
              allDay: allDay
            });
          }
        }
        /**
         * @hidden
         * @param {?} eventsContainer
         * @param {?} event
         * @param {?=} dayEvent
         * @return {?}
         */

      }, {
        key: "dragStarted",
        value: function dragStarted(eventsContainer, event, dayEvent) {
          var _this12 = this;

          this.dayColumnWidth = this.getDayColumnWidth(eventsContainer);
          /** @type {?} */

          var dragHelper = new CalendarDragHelper(eventsContainer, event);

          this.validateDrag =
          /**
          * @param {?} __0
          * @return {?}
          */
          function (_ref32) {
            var x = _ref32.x,
                y = _ref32.y,
                transform = _ref32.transform;
            return _this12.allDayEventResizes.size === 0 && _this12.timeEventResizes.size === 0 && dragHelper.validateDrag({
              x: x,
              y: y,
              snapDraggedEvents: _this12.snapDraggedEvents,
              dragAlreadyMoved: _this12.dragAlreadyMoved,
              transform: transform
            });
          };

          this.dragActive = true;
          this.dragAlreadyMoved = false;
          this.eventDragEnter = 0;

          if (!this.snapDraggedEvents && dayEvent) {
            this.view.hourColumns.forEach(
            /**
            * @param {?} column
            * @return {?}
            */
            function (column) {
              /** @type {?} */
              var linkedEvent = column.events.find(
              /**
              * @param {?} columnEvent
              * @return {?}
              */
              function (columnEvent) {
                return columnEvent.event === dayEvent.event && columnEvent !== dayEvent;
              }); // hide any linked events while dragging

              if (linkedEvent) {
                linkedEvent.width = 0;
                linkedEvent.height = 0;
              }
            });
          }

          this.cdr.markForCheck();
        }
        /**
         * @hidden
         * @param {?} dayEvent
         * @param {?} dragEvent
         * @return {?}
         */

      }, {
        key: "dragMove",
        value: function dragMove(dayEvent, dragEvent) {
          if (this.snapDraggedEvents) {
            /** @type {?} */
            var newEventTimes = this.getDragMovedEventTimes(dayEvent, dragEvent, this.dayColumnWidth, true);
            /** @type {?} */

            var originalEvent = dayEvent.event;
            /** @type {?} */

            var adjustedEvent = Object.assign({}, originalEvent, newEventTimes);
            /** @type {?} */

            var tempEvents = this.events.map(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              if (event === originalEvent) {
                return adjustedEvent;
              }

              return event;
            });
            this.restoreOriginalEvents(tempEvents, new Map([[adjustedEvent, originalEvent]]));
          }

          this.dragAlreadyMoved = true;
        }
        /**
         * @hidden
         * @return {?}
         */

      }, {
        key: "allDayEventDragMove",
        value: function allDayEventDragMove() {
          this.dragAlreadyMoved = true;
        }
        /**
         * @hidden
         * @param {?} weekEvent
         * @param {?} dragEndEvent
         * @param {?} dayWidth
         * @param {?=} useY
         * @return {?}
         */

      }, {
        key: "dragEnded",
        value: function dragEnded(weekEvent, dragEndEvent, dayWidth) {
          var useY = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
          this.view = this.getWeekView(this.events);
          this.dragActive = false;

          var _this$getDragMovedEve = this.getDragMovedEventTimes(weekEvent, dragEndEvent, dayWidth, useY),
              start = _this$getDragMovedEve.start,
              end = _this$getDragMovedEve.end;

          if (this.eventDragEnter > 0 && isDraggedWithinPeriod(start, end, this.view.period)) {
            this.eventTimesChanged.emit({
              newStart: start,
              newEnd: end,
              event: weekEvent.event,
              type: CalendarEventTimesChangedEventType.Drag,
              allDay: !useY
            });
          }
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshHeader",
        value: function refreshHeader() {
          this.days = this.utils.getWeekViewHeader(Object.assign({
            viewDate: this.viewDate,
            weekStartsOn: this.weekStartsOn,
            excluded: this.excludeDays,
            weekendDays: this.weekendDays
          }, getWeekViewPeriod(this.dateAdapter, this.viewDate, this.weekStartsOn, this.excludeDays, this.daysInWeek)));
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshBody",
        value: function refreshBody() {
          this.view = this.getWeekView(this.events);
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshAll",
        value: function refreshAll() {
          this.refreshHeader();
          this.refreshBody();
          this.emitBeforeViewRender();
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "emitBeforeViewRender",
        value: function emitBeforeViewRender() {
          if (this.days && this.view) {
            this.beforeViewRender.emit(Object.assign({
              header: this.days
            }, this.view));
          }
        }
        /**
         * @protected
         * @param {?} events
         * @return {?}
         */

      }, {
        key: "getWeekView",
        value: function getWeekView(events) {
          return this.utils.getWeekView(Object.assign({
            events: events,
            viewDate: this.viewDate,
            weekStartsOn: this.weekStartsOn,
            excluded: this.excludeDays,
            precision: this.precision,
            absolutePositionedEvents: true,
            hourSegments: this.hourSegments,
            dayStart: {
              hour: this.dayStartHour,
              minute: this.dayStartMinute
            },
            dayEnd: {
              hour: this.dayEndHour,
              minute: this.dayEndMinute
            },
            segmentHeight: this.hourSegmentHeight,
            weekendDays: this.weekendDays
          }, getWeekViewPeriod(this.dateAdapter, this.viewDate, this.weekStartsOn, this.excludeDays, this.daysInWeek)));
        }
        /**
         * @protected
         * @param {?} weekEvent
         * @param {?} dragEndEvent
         * @param {?} dayWidth
         * @param {?} useY
         * @return {?}
         */

      }, {
        key: "getDragMovedEventTimes",
        value: function getDragMovedEventTimes(weekEvent, dragEndEvent, dayWidth, useY) {
          /** @type {?} */
          var daysDragged = roundToNearest(dragEndEvent.x, dayWidth) / dayWidth;
          /** @type {?} */

          var minutesMoved = useY ? getMinutesMoved(dragEndEvent.y, this.hourSegments, this.hourSegmentHeight, this.eventSnapSize) : 0;
          /** @type {?} */

          var start = this.dateAdapter.addMinutes(addDaysWithExclusions(this.dateAdapter, weekEvent.event.start, daysDragged, this.excludeDays), minutesMoved);
          /** @type {?} */

          var end;

          if (weekEvent.event.end) {
            end = this.dateAdapter.addMinutes(addDaysWithExclusions(this.dateAdapter, weekEvent.event.end, daysDragged, this.excludeDays), minutesMoved);
          }

          return {
            start: start,
            end: end
          };
        }
        /**
         * @protected
         * @param {?} tempEvents
         * @param {?} adjustedEvents
         * @return {?}
         */

      }, {
        key: "restoreOriginalEvents",
        value: function restoreOriginalEvents(tempEvents, adjustedEvents) {
          /** @type {?} */
          var previousView = this.view;
          this.view = this.getWeekView(tempEvents);
          /** @type {?} */

          var adjustedEventsArray = tempEvents.filter(
          /**
          * @param {?} event
          * @return {?}
          */
          function (event) {
            return adjustedEvents.has(event);
          });
          this.view.hourColumns.forEach(
          /**
          * @param {?} column
          * @param {?} columnIndex
          * @return {?}
          */
          function (column, columnIndex) {
            previousView.hourColumns[columnIndex].hours.forEach(
            /**
            * @param {?} hour
            * @param {?} hourIndex
            * @return {?}
            */
            function (hour, hourIndex) {
              hour.segments.forEach(
              /**
              * @param {?} segment
              * @param {?} segmentIndex
              * @return {?}
              */
              function (segment, segmentIndex) {
                column.hours[hourIndex].segments[segmentIndex].cssClass = segment.cssClass;
              });
            });
            adjustedEventsArray.forEach(
            /**
            * @param {?} adjustedEvent
            * @return {?}
            */
            function (adjustedEvent) {
              /** @type {?} */
              var originalEvent = adjustedEvents.get(adjustedEvent);
              /** @type {?} */

              var existingColumnEvent = column.events.find(
              /**
              * @param {?} columnEvent
              * @return {?}
              */
              function (columnEvent) {
                return columnEvent.event === adjustedEvent;
              });

              if (existingColumnEvent) {
                // restore the original event so trackBy kicks in and the dom isn't changed
                existingColumnEvent.event = originalEvent;
              } else {
                // add a dummy event to the drop so if the event was removed from the original column the drag doesn't end early
                column.events.push({
                  event: originalEvent,
                  left: 0,
                  top: 0,
                  height: 0,
                  width: 0,
                  startsBeforeDay: false,
                  endsAfterDay: false
                });
              }
            });
          });
          adjustedEvents.clear();
        }
        /**
         * @protected
         * @param {?} calendarEvent
         * @param {?} resizeEvent
         * @return {?}
         */

      }, {
        key: "getTimeEventResizedDates",
        value: function getTimeEventResizedDates(calendarEvent, resizeEvent) {
          /** @type {?} */
          var minimumEventHeight = getMinimumEventHeightInMinutes(this.hourSegments, this.hourSegmentHeight);
          /** @type {?} */

          var newEventDates = {
            start: calendarEvent.start,
            end: getDefaultEventEnd(this.dateAdapter, calendarEvent, minimumEventHeight)
          };
          var eventWithoutEnd = Object(tslib__WEBPACK_IMPORTED_MODULE_9__["__rest"])(calendarEvent, ["end"]);
          /** @type {?} */

          var smallestResizes = {
            start: this.dateAdapter.addMinutes(newEventDates.end, minimumEventHeight * -1),
            end: getDefaultEventEnd(this.dateAdapter, eventWithoutEnd, minimumEventHeight)
          };

          if (typeof resizeEvent.edges.left !== 'undefined') {
            /** @type {?} */
            var daysDiff = Math.round(+resizeEvent.edges.left / this.dayColumnWidth);
            /** @type {?} */

            var newStart = addDaysWithExclusions(this.dateAdapter, newEventDates.start, daysDiff, this.excludeDays);

            if (newStart < smallestResizes.start) {
              newEventDates.start = newStart;
            } else {
              newEventDates.start = smallestResizes.start;
            }
          } else if (typeof resizeEvent.edges.right !== 'undefined') {
            /** @type {?} */
            var _daysDiff = Math.round(+resizeEvent.edges.right / this.dayColumnWidth);
            /** @type {?} */


            var newEnd = addDaysWithExclusions(this.dateAdapter, newEventDates.end, _daysDiff, this.excludeDays);

            if (newEnd > smallestResizes.end) {
              newEventDates.end = newEnd;
            } else {
              newEventDates.end = smallestResizes.end;
            }
          }

          if (typeof resizeEvent.edges.top !== 'undefined') {
            /** @type {?} */
            var minutesMoved = getMinutesMoved(
            /** @type {?} */
            resizeEvent.edges.top, this.hourSegments, this.hourSegmentHeight, this.eventSnapSize);
            /** @type {?} */

            var _newStart = this.dateAdapter.addMinutes(newEventDates.start, minutesMoved);

            if (_newStart < smallestResizes.start) {
              newEventDates.start = _newStart;
            } else {
              newEventDates.start = smallestResizes.start;
            }
          } else if (typeof resizeEvent.edges.bottom !== 'undefined') {
            /** @type {?} */
            var _minutesMoved = getMinutesMoved(
            /** @type {?} */
            resizeEvent.edges.bottom, this.hourSegments, this.hourSegmentHeight, this.eventSnapSize);
            /** @type {?} */


            var _newEnd = this.dateAdapter.addMinutes(newEventDates.end, _minutesMoved);

            if (_newEnd > smallestResizes.end) {
              newEventDates.end = _newEnd;
            } else {
              newEventDates.end = smallestResizes.end;
            }
          }

          return newEventDates;
        }
      }]);

      return CalendarWeekViewComponent;
    }();

    CalendarWeekViewComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-week-view',
        template: "\n    <div class=\"cal-week-view\">\n      <mwl-calendar-week-view-header\n        [days]=\"days\"\n        [locale]=\"locale\"\n        [customTemplate]=\"headerTemplate\"\n        (dayHeaderClicked)=\"dayHeaderClicked.emit($event)\"\n        (eventDropped)=\"\n          eventDropped({ dropData: $event }, $event.newStart, true)\n        \"\n      >\n      </mwl-calendar-week-view-header>\n      <div\n        class=\"cal-all-day-events\"\n        #allDayEventsContainer\n        *ngIf=\"view.allDayEventRows.length > 0\"\n        mwlDroppable\n        (dragEnter)=\"eventDragEnter = eventDragEnter + 1\"\n        (dragLeave)=\"eventDragEnter = eventDragEnter - 1\"\n      >\n        <div class=\"cal-day-columns\">\n          <div\n            class=\"cal-time-label-column\"\n            [ngTemplateOutlet]=\"allDayEventsLabelTemplate\"\n          ></div>\n          <div\n            class=\"cal-day-column\"\n            *ngFor=\"let day of days; trackBy: trackByWeekDayHeaderDate\"\n            mwlDroppable\n            dragOverClass=\"cal-drag-over\"\n            (drop)=\"eventDropped($event, day.date, true)\"\n          ></div>\n        </div>\n        <div\n          *ngFor=\"let eventRow of view.allDayEventRows; trackBy: trackById\"\n          #eventRowContainer\n          class=\"cal-events-row\"\n        >\n          <div\n            *ngFor=\"\n              let allDayEvent of eventRow.row;\n              trackBy: trackByDayOrWeekEvent\n            \"\n            #event\n            class=\"cal-event-container\"\n            [class.cal-draggable]=\"\n              allDayEvent.event.draggable && allDayEventResizes.size === 0\n            \"\n            [class.cal-starts-within-week]=\"!allDayEvent.startsBeforeWeek\"\n            [class.cal-ends-within-week]=\"!allDayEvent.endsAfterWeek\"\n            [ngClass]=\"allDayEvent.event?.cssClass\"\n            [style.width.%]=\"(100 / days.length) * allDayEvent.span\"\n            [style.marginLeft.%]=\"(100 / days.length) * allDayEvent.offset\"\n            mwlResizable\n            [resizeSnapGrid]=\"{ left: dayColumnWidth, right: dayColumnWidth }\"\n            [validateResize]=\"validateResize\"\n            (resizeStart)=\"\n              allDayEventResizeStarted(eventRowContainer, allDayEvent, $event)\n            \"\n            (resizing)=\"\n              allDayEventResizing(allDayEvent, $event, dayColumnWidth)\n            \"\n            (resizeEnd)=\"allDayEventResizeEnded(allDayEvent)\"\n            mwlDraggable\n            dragActiveClass=\"cal-drag-active\"\n            [dropData]=\"{ event: allDayEvent.event, calendarId: calendarId }\"\n            [dragAxis]=\"{\n              x: allDayEvent.event.draggable && allDayEventResizes.size === 0,\n              y:\n                !snapDraggedEvents &&\n                allDayEvent.event.draggable &&\n                allDayEventResizes.size === 0\n            }\"\n            [dragSnapGrid]=\"snapDraggedEvents ? { x: dayColumnWidth } : {}\"\n            [validateDrag]=\"validateDrag\"\n            (dragStart)=\"dragStarted(eventRowContainer, event)\"\n            (dragging)=\"allDayEventDragMove()\"\n            (dragEnd)=\"dragEnded(allDayEvent, $event, dayColumnWidth)\"\n          >\n            <div\n              class=\"cal-resize-handle cal-resize-handle-before-start\"\n              *ngIf=\"\n                allDayEvent.event?.resizable?.beforeStart &&\n                !allDayEvent.startsBeforeWeek\n              \"\n              mwlResizeHandle\n              [resizeEdges]=\"{ left: true }\"\n            ></div>\n            <mwl-calendar-week-view-event\n              [weekEvent]=\"allDayEvent\"\n              [tooltipPlacement]=\"tooltipPlacement\"\n              [tooltipTemplate]=\"tooltipTemplate\"\n              [tooltipAppendToBody]=\"tooltipAppendToBody\"\n              [tooltipDelay]=\"tooltipDelay\"\n              [customTemplate]=\"eventTemplate\"\n              [eventTitleTemplate]=\"eventTitleTemplate\"\n              [eventActionsTemplate]=\"eventActionsTemplate\"\n              (eventClicked)=\"eventClicked.emit({ event: allDayEvent.event })\"\n            >\n            </mwl-calendar-week-view-event>\n            <div\n              class=\"cal-resize-handle cal-resize-handle-after-end\"\n              *ngIf=\"\n                allDayEvent.event?.resizable?.afterEnd &&\n                !allDayEvent.endsAfterWeek\n              \"\n              mwlResizeHandle\n              [resizeEdges]=\"{ right: true }\"\n            ></div>\n          </div>\n        </div>\n      </div>\n      <div\n        class=\"cal-time-events\"\n        mwlDroppable\n        (dragEnter)=\"eventDragEnter = eventDragEnter + 1\"\n        (dragLeave)=\"eventDragEnter = eventDragEnter - 1\"\n      >\n        <div class=\"cal-time-label-column\" *ngIf=\"view.hourColumns.length > 0\">\n          <div\n            *ngFor=\"\n              let hour of view.hourColumns[0].hours;\n              trackBy: trackByHour;\n              let odd = odd\n            \"\n            class=\"cal-hour\"\n            [class.cal-hour-odd]=\"odd\"\n          >\n            <mwl-calendar-week-view-hour-segment\n              *ngFor=\"let segment of hour.segments; trackBy: trackByHourSegment\"\n              [style.height.px]=\"hourSegmentHeight\"\n              [segment]=\"segment\"\n              [segmentHeight]=\"hourSegmentHeight\"\n              [locale]=\"locale\"\n              [customTemplate]=\"hourSegmentTemplate\"\n              [isTimeLabel]=\"true\"\n            >\n            </mwl-calendar-week-view-hour-segment>\n          </div>\n        </div>\n        <div\n          class=\"cal-day-columns\"\n          [class.cal-resize-active]=\"timeEventResizes.size > 0\"\n          #dayColumns\n        >\n          <div\n            class=\"cal-day-column\"\n            *ngFor=\"let column of view.hourColumns; trackBy: trackByHourColumn\"\n          >\n            <div\n              *ngFor=\"\n                let timeEvent of column.events;\n                trackBy: trackByDayOrWeekEvent\n              \"\n              #event\n              class=\"cal-event-container\"\n              [class.cal-draggable]=\"\n                timeEvent.event.draggable && timeEventResizes.size === 0\n              \"\n              [class.cal-starts-within-day]=\"!timeEvent.startsBeforeDay\"\n              [class.cal-ends-within-day]=\"!timeEvent.endsAfterDay\"\n              [ngClass]=\"timeEvent.event.cssClass\"\n              [hidden]=\"timeEvent.height === 0 && timeEvent.width === 0\"\n              [style.top.px]=\"timeEvent.top\"\n              [style.height.px]=\"timeEvent.height\"\n              [style.left.%]=\"timeEvent.left\"\n              [style.width.%]=\"timeEvent.width\"\n              mwlResizable\n              [resizeSnapGrid]=\"{\n                left: dayColumnWidth,\n                right: dayColumnWidth,\n                top: eventSnapSize || hourSegmentHeight,\n                bottom: eventSnapSize || hourSegmentHeight\n              }\"\n              [validateResize]=\"validateResize\"\n              [allowNegativeResizes]=\"true\"\n              (resizeStart)=\"\n                timeEventResizeStarted(dayColumns, timeEvent, $event)\n              \"\n              (resizing)=\"timeEventResizing(timeEvent, $event)\"\n              (resizeEnd)=\"timeEventResizeEnded(timeEvent)\"\n              mwlDraggable\n              dragActiveClass=\"cal-drag-active\"\n              [dropData]=\"{ event: timeEvent.event, calendarId: calendarId }\"\n              [dragAxis]=\"{\n                x: timeEvent.event.draggable && timeEventResizes.size === 0,\n                y: timeEvent.event.draggable && timeEventResizes.size === 0\n              }\"\n              [dragSnapGrid]=\"\n                snapDraggedEvents\n                  ? { x: dayColumnWidth, y: eventSnapSize || hourSegmentHeight }\n                  : {}\n              \"\n              [ghostDragEnabled]=\"!snapDraggedEvents\"\n              [validateDrag]=\"validateDrag\"\n              (dragStart)=\"dragStarted(dayColumns, event, timeEvent)\"\n              (dragging)=\"dragMove(timeEvent, $event)\"\n              (dragEnd)=\"dragEnded(timeEvent, $event, dayColumnWidth, true)\"\n            >\n              <div\n                class=\"cal-resize-handle cal-resize-handle-before-start\"\n                *ngIf=\"\n                  timeEvent.event?.resizable?.beforeStart &&\n                  !timeEvent.startsBeforeDay\n                \"\n                mwlResizeHandle\n                [resizeEdges]=\"{\n                  left: true,\n                  top: true\n                }\"\n              ></div>\n              <mwl-calendar-week-view-event\n                [weekEvent]=\"timeEvent\"\n                [tooltipPlacement]=\"tooltipPlacement\"\n                [tooltipTemplate]=\"tooltipTemplate\"\n                [tooltipAppendToBody]=\"tooltipAppendToBody\"\n                [tooltipDisabled]=\"dragActive || timeEventResizes.size > 0\"\n                [tooltipDelay]=\"tooltipDelay\"\n                [customTemplate]=\"eventTemplate\"\n                [eventTitleTemplate]=\"eventTitleTemplate\"\n                [eventActionsTemplate]=\"eventActionsTemplate\"\n                (eventClicked)=\"eventClicked.emit({ event: timeEvent.event })\"\n              >\n              </mwl-calendar-week-view-event>\n              <div\n                class=\"cal-resize-handle cal-resize-handle-after-end\"\n                *ngIf=\"\n                  timeEvent.event?.resizable?.afterEnd &&\n                  !timeEvent.endsAfterDay\n                \"\n                mwlResizeHandle\n                [resizeEdges]=\"{\n                  right: true,\n                  bottom: true\n                }\"\n              ></div>\n            </div>\n\n            <div\n              *ngFor=\"\n                let hour of column.hours;\n                trackBy: trackByHour;\n                let odd = odd\n              \"\n              class=\"cal-hour\"\n              [class.cal-hour-odd]=\"odd\"\n            >\n              <mwl-calendar-week-view-hour-segment\n                *ngFor=\"\n                  let segment of hour.segments;\n                  trackBy: trackByHourSegment\n                \"\n                [style.height.px]=\"hourSegmentHeight\"\n                [segment]=\"segment\"\n                [segmentHeight]=\"hourSegmentHeight\"\n                [locale]=\"locale\"\n                [customTemplate]=\"hourSegmentTemplate\"\n                (mwlClick)=\"hourSegmentClicked.emit({ date: segment.date })\"\n                [clickListenerDisabled]=\"\n                  hourSegmentClicked.observers.length === 0\n                \"\n                mwlDroppable\n                [dragOverClass]=\"\n                  !dragActive || !snapDraggedEvents ? 'cal-drag-over' : null\n                \"\n                dragActiveClass=\"cal-drag-active\"\n                (drop)=\"eventDropped($event, segment.date, false)\"\n              >\n              </mwl-calendar-week-view-hour-segment>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  "
      }]
    }];
    /** @nocollapse */

    CalendarWeekViewComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: CalendarUtils
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]
        }]
      }, {
        type: DateAdapter
      }];
    };

    CalendarWeekViewComponent.propDecorators = {
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      events: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      excludeDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      refresh: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      weekStartsOn: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      headerTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      precision: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      weekendDays: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      snapDraggedEvents: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegments: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegmentHeight: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayStartHour: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayStartMinute: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayEndHour: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayEndMinute: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegmentTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventSnapSize: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      allDayEventsLabelTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      daysInWeek: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayHeaderClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventTimesChanged: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      beforeViewRender: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      hourSegmentClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarWeekViewHeaderComponent = function CalendarWeekViewHeaderComponent() {
      _classCallCheck(this, CalendarWeekViewHeaderComponent);

      this.dayHeaderClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.eventDropped = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      this.trackByWeekDayHeaderDate = trackByWeekDayHeaderDate;
    };

    CalendarWeekViewHeaderComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-week-view-header',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-days=\"days\"\n      let-locale=\"locale\"\n      let-dayHeaderClicked=\"dayHeaderClicked\"\n      let-eventDropped=\"eventDropped\"\n      let-trackByWeekDayHeaderDate=\"trackByWeekDayHeaderDate\"\n    >\n      <div class=\"cal-day-headers\">\n        <div\n          class=\"cal-header\"\n          *ngFor=\"let day of days; trackBy: trackByWeekDayHeaderDate\"\n          [class.cal-past]=\"day.isPast\"\n          [class.cal-today]=\"day.isToday\"\n          [class.cal-future]=\"day.isFuture\"\n          [class.cal-weekend]=\"day.isWeekend\"\n          [ngClass]=\"day.cssClass\"\n          (mwlClick)=\"dayHeaderClicked.emit({ day: day })\"\n          mwlDroppable\n          dragOverClass=\"cal-drag-over\"\n          (drop)=\"\n            eventDropped.emit({\n              event: $event.dropData.event,\n              newStart: day.date\n            })\n          \"\n        >\n          <b>{{ day.date | calendarDate: 'weekViewColumnHeader':locale }}</b\n          ><br />\n          <span>{{\n            day.date | calendarDate: 'weekViewColumnSubHeader':locale\n          }}</span>\n        </div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        days: days,\n        locale: locale,\n        dayHeaderClicked: dayHeaderClicked,\n        eventDropped: eventDropped,\n        trackByWeekDayHeaderDate: trackByWeekDayHeaderDate\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarWeekViewHeaderComponent.propDecorators = {
      days: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayHeaderClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventDropped: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarWeekViewEventComponent = function CalendarWeekViewEventComponent() {
      _classCallCheck(this, CalendarWeekViewEventComponent);

      this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    };

    CalendarWeekViewEventComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-week-view-event',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-weekEvent=\"weekEvent\"\n      let-tooltipPlacement=\"tooltipPlacement\"\n      let-eventClicked=\"eventClicked\"\n      let-tooltipTemplate=\"tooltipTemplate\"\n      let-tooltipAppendToBody=\"tooltipAppendToBody\"\n      let-tooltipDisabled=\"tooltipDisabled\"\n      let-tooltipDelay=\"tooltipDelay\"\n    >\n      <div\n        class=\"cal-event\"\n        [ngStyle]=\"{\n          backgroundColor: weekEvent.event.color?.secondary,\n          borderColor: weekEvent.event.color?.primary\n        }\"\n        [mwlCalendarTooltip]=\"\n          !tooltipDisabled\n            ? (weekEvent.event.title\n              | calendarEventTitle: 'weekTooltip':weekEvent.event)\n            : ''\n        \"\n        [tooltipPlacement]=\"tooltipPlacement\"\n        [tooltipEvent]=\"weekEvent.event\"\n        [tooltipTemplate]=\"tooltipTemplate\"\n        [tooltipAppendToBody]=\"tooltipAppendToBody\"\n        [tooltipDelay]=\"tooltipDelay\"\n        (mwlClick)=\"eventClicked.emit()\"\n      >\n        <mwl-calendar-event-actions\n          [event]=\"weekEvent.event\"\n          [customTemplate]=\"eventActionsTemplate\"\n        >\n        </mwl-calendar-event-actions>\n        &ngsp;\n        <mwl-calendar-event-title\n          [event]=\"weekEvent.event\"\n          [customTemplate]=\"eventTitleTemplate\"\n          view=\"week\"\n        >\n        </mwl-calendar-event-title>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        weekEvent: weekEvent,\n        tooltipPlacement: tooltipPlacement,\n        eventClicked: eventClicked,\n        tooltipTemplate: tooltipTemplate,\n        tooltipAppendToBody: tooltipAppendToBody,\n        tooltipDisabled: tooltipDisabled,\n        tooltipDelay: tooltipDelay\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarWeekViewEventComponent.propDecorators = {
      weekEvent: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDisabled: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarWeekViewHourSegmentComponent = function CalendarWeekViewHourSegmentComponent() {
      _classCallCheck(this, CalendarWeekViewHourSegmentComponent);
    };

    CalendarWeekViewHourSegmentComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-week-view-hour-segment',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-segment=\"segment\"\n      let-locale=\"locale\"\n      let-segmentHeight=\"segmentHeight\"\n      let-isTimeLabel=\"isTimeLabel\"\n    >\n      <div\n        class=\"cal-hour-segment\"\n        [style.height.px]=\"segmentHeight\"\n        [class.cal-hour-start]=\"segment.isStart\"\n        [class.cal-after-hour-start]=\"!segment.isStart\"\n        [ngClass]=\"segment.cssClass\"\n      >\n        <div class=\"cal-time\" *ngIf=\"isTimeLabel\">\n          {{ segment.date | calendarDate: 'weekViewHour':locale }}\n        </div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        segment: segment,\n        locale: locale,\n        segmentHeight: segmentHeight,\n        isTimeLabel: isTimeLabel\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarWeekViewHourSegmentComponent.propDecorators = {
      segment: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      segmentHeight: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      isTimeLabel: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarWeekModule = function CalendarWeekModule() {
      _classCallCheck(this, CalendarWeekModule);
    };

    CalendarWeekModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular_resizable_element__WEBPACK_IMPORTED_MODULE_8__["ResizableModule"], angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarCommonModule],
        declarations: [CalendarWeekViewComponent, CalendarWeekViewHeaderComponent, CalendarWeekViewEventComponent, CalendarWeekViewHourSegmentComponent],
        exports: [angular_resizable_element__WEBPACK_IMPORTED_MODULE_8__["ResizableModule"], angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarWeekViewComponent, CalendarWeekViewHeaderComponent, CalendarWeekViewEventComponent, CalendarWeekViewHourSegmentComponent]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * Shows all events on a given day. Example usage:
     *
     * ```typescript
     * <mwl-calendar-day-view
     *  [viewDate]="viewDate"
     *  [events]="events">
     * </mwl-calendar-day-view>
     * ```
     */

    var CalendarDayViewComponent = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} cdr
       * @param {?} utils
       * @param {?} locale
       * @param {?} dateAdapter
       */
      function CalendarDayViewComponent(cdr, utils, locale, dateAdapter) {
        _classCallCheck(this, CalendarDayViewComponent);

        this.cdr = cdr;
        this.utils = utils;
        this.dateAdapter = dateAdapter;
        /**
         * An array of events to display on view
         * The schema is available here: https://github.com/mattlewis92/calendar-utils/blob/c51689985f59a271940e30bc4e2c4e1fee3fcb5c/src/calendarUtils.ts#L49-L63
         */

        this.events = [];
        /**
         * The number of segments in an hour. Must be <= 6
         */

        this.hourSegments = 2;
        /**
         * The height in pixels of each hour segment
         */

        this.hourSegmentHeight = 30;
        /**
         * The day start hours in 24 hour time. Must be 0-23
         */

        this.dayStartHour = 0;
        /**
         * The day start minutes. Must be 0-59
         */

        this.dayStartMinute = 0;
        /**
         * The day end hours in 24 hour time. Must be 0-23
         */

        this.dayEndHour = 23;
        /**
         * The day end minutes. Must be 0-59
         */

        this.dayEndMinute = 59;
        /**
         * The width in pixels of each event on the view
         */

        this.eventWidth = 150;
        /**
         * The placement of the event tooltip
         */

        this.tooltipPlacement = 'auto';
        /**
         * Whether to append tooltips to the body or next to the trigger element
         */

        this.tooltipAppendToBody = true;
        /**
         * The delay in milliseconds before the tooltip should be displayed. If not provided the tooltip
         * will be displayed immediately.
         */

        this.tooltipDelay = null;
        /**
         * Whether to snap events to a grid when dragging
         */

        this.snapDraggedEvents = true;
        /**
         * Called when an event title is clicked
         */

        this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when an hour segment is clicked
         */

        this.hourSegmentClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Called when an event is resized or dragged and dropped
         */

        this.eventTimesChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * An output that will be called before the view is rendered for the current day.
         * If you add the `cssClass` property to an hour grid segment it will add that class to the hour segment in the template
         */

        this.beforeViewRender = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * @hidden
         */

        this.hours = [];
        /**
         * @hidden
         */

        this.width = 0;
        /**
         * @hidden
         */

        this.currentResizes = new Map();
        /**
         * @hidden
         */

        this.eventDragEnter = 0;
        /**
         * @hidden
         */

        this.calendarId = Symbol('angular calendar day view id');
        /**
         * @hidden
         */

        this.dragAlreadyMoved = false;
        /**
         * @hidden
         */

        this.trackByEventId = trackByEventId;
        /**
         * @hidden
         */

        this.trackByHour = trackByHour;
        /**
         * @hidden
         */

        this.trackByHourSegment = trackByHourSegment;
        /**
         * @hidden
         */

        this.trackByDayEvent = trackByDayOrWeekEvent;
        this.locale = locale;
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(CalendarDayViewComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this13 = this;

          if (this.refresh) {
            this.refreshSubscription = this.refresh.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this13.refreshAll();

              _this13.cdr.markForCheck();
            });
          }
        }
        /**
         * @hidden
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
          }
        }
        /**
         * @hidden
         * @param {?} changes
         * @return {?}
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          /** @type {?} */
          var refreshHourGrid = changes.viewDate || changes.dayStartHour || changes.dayStartMinute || changes.dayEndHour || changes.dayEndMinute || changes.hourSegments;
          /** @type {?} */

          var refreshView = changes.viewDate || changes.events || changes.dayStartHour || changes.dayStartMinute || changes.dayEndHour || changes.dayEndMinute || changes.eventWidth || changes.hourSegments;

          if (refreshHourGrid) {
            this.refreshHourGrid();
          }

          if (changes.events) {
            validateEvents(this.events);
          }

          if (refreshView) {
            this.refreshView();
          }

          if (refreshHourGrid || refreshView) {
            this.emitBeforeViewRender();
          }
        }
        /**
         * @param {?} dropEvent
         * @param {?} date
         * @param {?} allDay
         * @return {?}
         */

      }, {
        key: "eventDropped",
        value: function eventDropped(dropEvent, date, allDay) {
          if (shouldFireDroppedEvent(dropEvent, date, allDay, this.calendarId)) {
            this.eventTimesChanged.emit({
              type: CalendarEventTimesChangedEventType.Drop,
              event: dropEvent.dropData.event,
              newStart: date,
              allDay: allDay
            });
          }
        }
        /**
         * @param {?} event
         * @param {?} resizeEvent
         * @param {?} dayEventsContainer
         * @return {?}
         */

      }, {
        key: "resizeStarted",
        value: function resizeStarted(event, resizeEvent, dayEventsContainer) {
          this.currentResizes.set(event, {
            originalTop: event.top,
            originalHeight: event.height,
            edge: typeof resizeEvent.edges.top !== 'undefined' ? 'top' : 'bottom'
          });
          /** @type {?} */

          var resizeHelper = new CalendarResizeHelper(dayEventsContainer);

          this.validateResize =
          /**
          * @param {?} __0
          * @return {?}
          */
          function (_ref33) {
            var rectangle = _ref33.rectangle;
            return resizeHelper.validateResize({
              rectangle: rectangle
            });
          };

          this.cdr.markForCheck();
        }
        /**
         * @param {?} event
         * @param {?} resizeEvent
         * @return {?}
         */

      }, {
        key: "resizing",
        value: function resizing(event, resizeEvent) {
          /** @type {?} */
          var currentResize = this.currentResizes.get(event);

          if (typeof resizeEvent.edges.top !== 'undefined') {
            event.top = currentResize.originalTop + +resizeEvent.edges.top;
            event.height = currentResize.originalHeight - +resizeEvent.edges.top;
          } else if (typeof resizeEvent.edges.bottom !== 'undefined') {
            event.height = currentResize.originalHeight + +resizeEvent.edges.bottom;
          }
        }
        /**
         * @param {?} dayEvent
         * @return {?}
         */

      }, {
        key: "resizeEnded",
        value: function resizeEnded(dayEvent) {
          /** @type {?} */
          var currentResize = this.currentResizes.get(dayEvent);
          /** @type {?} */

          var resizingBeforeStart = currentResize.edge === 'top';
          /** @type {?} */

          var pixelsMoved;

          if (resizingBeforeStart) {
            pixelsMoved = dayEvent.top - currentResize.originalTop;
          } else {
            pixelsMoved = dayEvent.height - currentResize.originalHeight;
          }

          dayEvent.top = currentResize.originalTop;
          dayEvent.height = currentResize.originalHeight;
          /** @type {?} */

          var minutesMoved = getMinutesMoved(pixelsMoved, this.hourSegments, this.hourSegmentHeight, this.eventSnapSize);
          /** @type {?} */

          var newStart = dayEvent.event.start;
          /** @type {?} */

          var newEnd = getDefaultEventEnd(this.dateAdapter, dayEvent.event, getMinimumEventHeightInMinutes(this.hourSegments, this.hourSegmentHeight));

          if (resizingBeforeStart) {
            newStart = this.dateAdapter.addMinutes(newStart, minutesMoved);
          } else {
            newEnd = this.dateAdapter.addMinutes(newEnd, minutesMoved);
          }

          this.eventTimesChanged.emit({
            newStart: newStart,
            newEnd: newEnd,
            event: dayEvent.event,
            type: CalendarEventTimesChangedEventType.Resize
          });
          this.currentResizes["delete"](dayEvent);
        }
        /**
         * @param {?} event
         * @param {?} dayEventsContainer
         * @param {?} dayEvent
         * @return {?}
         */

      }, {
        key: "dragStarted",
        value: function dragStarted(event, dayEventsContainer, dayEvent) {
          var _this14 = this;

          /** @type {?} */
          var dragHelper = new CalendarDragHelper(dayEventsContainer, event);

          this.validateDrag =
          /**
          * @param {?} __0
          * @return {?}
          */
          function (_ref34) {
            var x = _ref34.x,
                y = _ref34.y,
                transform = _ref34.transform;
            return _this14.currentResizes.size === 0 && dragHelper.validateDrag({
              x: x,
              y: y,
              snapDraggedEvents: _this14.snapDraggedEvents,
              dragAlreadyMoved: _this14.dragAlreadyMoved,
              transform: transform
            });
          };

          this.eventDragEnter = 0;
          this.dragAlreadyMoved = false;
          this.currentDrag = {
            dayEvent: dayEvent,
            originalTop: dayEvent.top,
            originalLeft: dayEvent.left
          };
          this.cdr.markForCheck();
        }
        /**
         * @hidden
         * @param {?} coords
         * @return {?}
         */

      }, {
        key: "dragMove",
        value: function dragMove(coords) {
          this.dragAlreadyMoved = true;

          if (this.snapDraggedEvents) {
            this.currentDrag.dayEvent.top = this.currentDrag.originalTop + coords.y;
            this.currentDrag.dayEvent.left = this.currentDrag.originalLeft + coords.x;
          }
        }
        /**
         * @param {?} dayEvent
         * @param {?} dragEndEvent
         * @return {?}
         */

      }, {
        key: "dragEnded",
        value: function dragEnded(dayEvent, dragEndEvent) {
          this.currentDrag.dayEvent.top = this.currentDrag.originalTop;
          this.currentDrag.dayEvent.left = this.currentDrag.originalLeft;
          this.currentDrag = null;

          if (this.eventDragEnter > 0) {
            /** @type {?} */
            var minutesMoved = getMinutesMoved(dragEndEvent.y, this.hourSegments, this.hourSegmentHeight, this.eventSnapSize);
            /** @type {?} */

            var newStart = this.dateAdapter.addMinutes(dayEvent.event.start, minutesMoved);

            if (dragEndEvent.y < 0 && newStart < this.view.period.start) {
              minutesMoved += this.dateAdapter.differenceInMinutes(this.view.period.start, newStart);
              newStart = this.view.period.start;
            }
            /** @type {?} */


            var newEnd;

            if (dayEvent.event.end) {
              newEnd = this.dateAdapter.addMinutes(dayEvent.event.end, minutesMoved);
            }

            if (isDraggedWithinPeriod(newStart, newEnd, this.view.period)) {
              this.eventTimesChanged.emit({
                newStart: newStart,
                newEnd: newEnd,
                event: dayEvent.event,
                type: CalendarEventTimesChangedEventType.Drag,
                allDay: false
              });
            }
          }
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshHourGrid",
        value: function refreshHourGrid() {
          this.hours = this.utils.getDayViewHourGrid({
            viewDate: this.viewDate,
            hourSegments: this.hourSegments,
            dayStart: {
              hour: this.dayStartHour,
              minute: this.dayStartMinute
            },
            dayEnd: {
              hour: this.dayEndHour,
              minute: this.dayEndMinute
            }
          });
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshView",
        value: function refreshView() {
          this.view = this.utils.getDayView({
            events: this.events,
            viewDate: this.viewDate,
            hourSegments: this.hourSegments,
            dayStart: {
              hour: this.dayStartHour,
              minute: this.dayStartMinute
            },
            dayEnd: {
              hour: this.dayEndHour,
              minute: this.dayEndMinute
            },
            eventWidth: this.eventWidth,
            segmentHeight: this.hourSegmentHeight
          });
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "refreshAll",
        value: function refreshAll() {
          this.refreshHourGrid();
          this.refreshView();
          this.emitBeforeViewRender();
        }
        /**
         * @protected
         * @return {?}
         */

      }, {
        key: "emitBeforeViewRender",
        value: function emitBeforeViewRender() {
          if (this.hours && this.view) {
            this.beforeViewRender.emit({
              body: {
                hourGrid: this.hours,
                allDayEvents: this.view.allDayEvents
              },
              period: this.view.period
            });
          }
        }
      }]);

      return CalendarDayViewComponent;
    }();

    CalendarDayViewComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-day-view',
        template: "\n    <div class=\"cal-day-view\">\n      <div\n        class=\"cal-all-day-events\"\n        mwlDroppable\n        dragOverClass=\"cal-drag-over\"\n        dragActiveClass=\"cal-drag-active\"\n        (drop)=\"eventDropped($event, view.period.start, true)\"\n      >\n        <mwl-calendar-day-view-event\n          *ngFor=\"let event of view.allDayEvents; trackBy: trackByEventId\"\n          [ngClass]=\"event.cssClass\"\n          [dayEvent]=\"{ event: event }\"\n          [tooltipPlacement]=\"tooltipPlacement\"\n          [tooltipTemplate]=\"tooltipTemplate\"\n          [tooltipAppendToBody]=\"tooltipAppendToBody\"\n          [tooltipDelay]=\"tooltipDelay\"\n          [customTemplate]=\"eventTemplate\"\n          [eventTitleTemplate]=\"eventTitleTemplate\"\n          [eventActionsTemplate]=\"eventActionsTemplate\"\n          (eventClicked)=\"eventClicked.emit({ event: event })\"\n          [class.cal-draggable]=\"!snapDraggedEvents && event.draggable\"\n          mwlDraggable\n          dragActiveClass=\"cal-drag-active\"\n          [dropData]=\"{ event: event, calendarId: calendarId }\"\n          [dragAxis]=\"{\n            x: !snapDraggedEvents && event.draggable,\n            y: !snapDraggedEvents && event.draggable\n          }\"\n        >\n        </mwl-calendar-day-view-event>\n      </div>\n      <div\n        class=\"cal-hour-rows\"\n        #dayEventsContainer\n        mwlDroppable\n        (dragEnter)=\"eventDragEnter = eventDragEnter + 1\"\n        (dragLeave)=\"eventDragEnter = eventDragEnter - 1\"\n      >\n        <div class=\"cal-events\">\n          <div\n            #event\n            *ngFor=\"let dayEvent of view?.events; trackBy: trackByDayEvent\"\n            class=\"cal-event-container\"\n            [class.cal-draggable]=\"dayEvent.event.draggable\"\n            [class.cal-starts-within-day]=\"!dayEvent.startsBeforeDay\"\n            [class.cal-ends-within-day]=\"!dayEvent.endsAfterDay\"\n            [ngClass]=\"dayEvent.event.cssClass\"\n            mwlResizable\n            [resizeSnapGrid]=\"{\n              top: eventSnapSize || hourSegmentHeight,\n              bottom: eventSnapSize || hourSegmentHeight\n            }\"\n            [validateResize]=\"validateResize\"\n            (resizeStart)=\"resizeStarted(dayEvent, $event, dayEventsContainer)\"\n            (resizing)=\"resizing(dayEvent, $event)\"\n            (resizeEnd)=\"resizeEnded(dayEvent)\"\n            mwlDraggable\n            dragActiveClass=\"cal-drag-active\"\n            [dropData]=\"{ event: dayEvent.event, calendarId: calendarId }\"\n            [dragAxis]=\"{\n              x:\n                !snapDraggedEvents &&\n                dayEvent.event.draggable &&\n                currentResizes.size === 0,\n              y: dayEvent.event.draggable && currentResizes.size === 0\n            }\"\n            [dragSnapGrid]=\"\n              snapDraggedEvents ? { y: eventSnapSize || hourSegmentHeight } : {}\n            \"\n            [validateDrag]=\"validateDrag\"\n            [ghostDragEnabled]=\"!snapDraggedEvents\"\n            (dragStart)=\"dragStarted(event, dayEventsContainer, dayEvent)\"\n            (dragging)=\"dragMove($event)\"\n            (dragEnd)=\"dragEnded(dayEvent, $event)\"\n            [style.marginTop.px]=\"dayEvent.top\"\n            [style.height.px]=\"dayEvent.height\"\n            [style.marginLeft.px]=\"dayEvent.left + 70\"\n            [style.width.px]=\"dayEvent.width - 1\"\n          >\n            <div\n              class=\"cal-resize-handle cal-resize-handle-before-start\"\n              *ngIf=\"\n                dayEvent.event?.resizable?.beforeStart &&\n                !dayEvent.startsBeforeDay\n              \"\n              mwlResizeHandle\n              [resizeEdges]=\"{ top: true }\"\n            ></div>\n            <mwl-calendar-day-view-event\n              [dayEvent]=\"dayEvent\"\n              [tooltipPlacement]=\"tooltipPlacement\"\n              [tooltipTemplate]=\"tooltipTemplate\"\n              [tooltipAppendToBody]=\"tooltipAppendToBody\"\n              [tooltipDelay]=\"tooltipDelay\"\n              [customTemplate]=\"eventTemplate\"\n              [eventTitleTemplate]=\"eventTitleTemplate\"\n              [eventActionsTemplate]=\"eventActionsTemplate\"\n              (eventClicked)=\"eventClicked.emit({ event: dayEvent.event })\"\n            >\n            </mwl-calendar-day-view-event>\n            <div\n              class=\"cal-resize-handle cal-resize-handle-after-end\"\n              *ngIf=\"\n                dayEvent.event?.resizable?.afterEnd && !dayEvent.endsAfterDay\n              \"\n              mwlResizeHandle\n              [resizeEdges]=\"{ bottom: true }\"\n            ></div>\n          </div>\n        </div>\n        <div\n          class=\"cal-hour\"\n          *ngFor=\"let hour of hours; trackBy: trackByHour\"\n          [style.minWidth.px]=\"view?.width + 70\"\n        >\n          <mwl-calendar-day-view-hour-segment\n            *ngFor=\"let segment of hour.segments; trackBy: trackByHourSegment\"\n            [style.height.px]=\"hourSegmentHeight\"\n            [segment]=\"segment\"\n            [segmentHeight]=\"hourSegmentHeight\"\n            [locale]=\"locale\"\n            [customTemplate]=\"hourSegmentTemplate\"\n            (mwlClick)=\"hourSegmentClicked.emit({ date: segment.date })\"\n            [clickListenerDisabled]=\"hourSegmentClicked.observers.length === 0\"\n            mwlDroppable\n            dragOverClass=\"cal-drag-over\"\n            dragActiveClass=\"cal-drag-active\"\n            (drop)=\"eventDropped($event, segment.date, false)\"\n          >\n          </mwl-calendar-day-view-hour-segment>\n        </div>\n      </div>\n    </div>\n  "
      }]
    }];
    /** @nocollapse */

    CalendarDayViewComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: CalendarUtils
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]
        }]
      }, {
        type: DateAdapter
      }];
    };

    CalendarDayViewComponent.propDecorators = {
      viewDate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      events: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegments: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegmentHeight: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayStartHour: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayStartMinute: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayEndHour: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      dayEndMinute: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventWidth: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      refresh: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventSnapSize: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hourSegmentTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      snapDraggedEvents: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      hourSegmentClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      eventTimesChanged: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      beforeViewRender: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarDayViewHourSegmentComponent = function CalendarDayViewHourSegmentComponent() {
      _classCallCheck(this, CalendarDayViewHourSegmentComponent);
    };

    CalendarDayViewHourSegmentComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-day-view-hour-segment',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-segment=\"segment\"\n      let-locale=\"locale\"\n      let-segmentHeight=\"segmentHeight\"\n    >\n      <div\n        class=\"cal-hour-segment\"\n        [style.height.px]=\"segmentHeight\"\n        [class.cal-hour-start]=\"segment.isStart\"\n        [class.cal-after-hour-start]=\"!segment.isStart\"\n        [ngClass]=\"segment.cssClass\"\n      >\n        <div class=\"cal-time\">\n          {{ segment.date | calendarDate: 'dayViewHour':locale }}\n        </div>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        segment: segment,\n        locale: locale,\n        segmentHeight: segmentHeight\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarDayViewHourSegmentComponent.propDecorators = {
      segment: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      segmentHeight: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      locale: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarDayViewEventComponent = function CalendarDayViewEventComponent() {
      _classCallCheck(this, CalendarDayViewEventComponent);

      this.eventClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    };

    CalendarDayViewEventComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'mwl-calendar-day-view-event',
        template: "\n    <ng-template\n      #defaultTemplate\n      let-dayEvent=\"dayEvent\"\n      let-tooltipPlacement=\"tooltipPlacement\"\n      let-eventClicked=\"eventClicked\"\n      let-tooltipTemplate=\"tooltipTemplate\"\n      let-tooltipAppendToBody=\"tooltipAppendToBody\"\n      let-tooltipDelay=\"tooltipDelay\"\n    >\n      <div\n        class=\"cal-event\"\n        [ngStyle]=\"{\n          backgroundColor: dayEvent.event.color?.secondary,\n          borderColor: dayEvent.event.color?.primary\n        }\"\n        [mwlCalendarTooltip]=\"\n          dayEvent.event.title | calendarEventTitle: 'dayTooltip':dayEvent.event\n        \"\n        [tooltipPlacement]=\"tooltipPlacement\"\n        [tooltipEvent]=\"dayEvent.event\"\n        [tooltipTemplate]=\"tooltipTemplate\"\n        [tooltipAppendToBody]=\"tooltipAppendToBody\"\n        [tooltipDelay]=\"tooltipDelay\"\n        (mwlClick)=\"eventClicked.emit()\"\n      >\n        <mwl-calendar-event-actions\n          [event]=\"dayEvent.event\"\n          [customTemplate]=\"eventActionsTemplate\"\n        >\n        </mwl-calendar-event-actions>\n        &ngsp;\n        <mwl-calendar-event-title\n          [event]=\"dayEvent.event\"\n          [customTemplate]=\"eventTitleTemplate\"\n          view=\"day\"\n        >\n        </mwl-calendar-event-title>\n      </div>\n    </ng-template>\n    <ng-template\n      [ngTemplateOutlet]=\"customTemplate || defaultTemplate\"\n      [ngTemplateOutletContext]=\"{\n        dayEvent: dayEvent,\n        tooltipPlacement: tooltipPlacement,\n        eventClicked: eventClicked,\n        tooltipTemplate: tooltipTemplate,\n        tooltipAppendToBody: tooltipAppendToBody,\n        tooltipDelay: tooltipDelay\n      }\"\n    >\n    </ng-template>\n  "
      }]
    }];
    CalendarDayViewEventComponent.propDecorators = {
      dayEvent: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipPlacement: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipAppendToBody: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      customTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventTitleTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventActionsTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tooltipDelay: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      eventClicked: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var CalendarDayModule = function CalendarDayModule() {
      _classCallCheck(this, CalendarDayModule);
    };

    CalendarDayModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular_resizable_element__WEBPACK_IMPORTED_MODULE_8__["ResizableModule"], angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarCommonModule],
        declarations: [CalendarDayViewComponent, CalendarDayViewHourSegmentComponent, CalendarDayViewEventComponent],
        exports: [angular_resizable_element__WEBPACK_IMPORTED_MODULE_8__["ResizableModule"], angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_6__["DragAndDropModule"], CalendarDayViewComponent, CalendarDayViewHourSegmentComponent, CalendarDayViewEventComponent]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * The main module of this library. Example usage:
     *
     * ```typescript
     * import { CalenderModule } from 'angular-calendar';
     *
     * \@NgModule({
     *   imports: [
     *     CalenderModule.forRoot()
     *   ]
     * })
     * class MyModule {}
     * ```
     *
     */

    var CalendarModule = /*#__PURE__*/function () {
      function CalendarModule() {
        _classCallCheck(this, CalendarModule);
      }

      _createClass(CalendarModule, null, [{
        key: "forRoot",

        /**
         * @param {?} dateAdapter
         * @param {?=} config
         * @return {?}
         */
        value: function forRoot(dateAdapter) {
          var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
          return {
            ngModule: CalendarModule,
            providers: [dateAdapter, config.eventTitleFormatter || CalendarEventTitleFormatter, config.dateFormatter || CalendarDateFormatter, config.utils || CalendarUtils]
          };
        }
      }]);

      return CalendarModule;
    }();

    CalendarModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [CalendarCommonModule, CalendarMonthModule, CalendarWeekModule, CalendarDayModule],
        exports: [CalendarCommonModule, CalendarMonthModule, CalendarWeekModule, CalendarDayModule]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    //# sourceMappingURL=angular-calendar.js.map

    /***/
  },

  /***/
  "./node_modules/angular-draggable-droppable/fesm2015/angular-draggable-droppable.js":
  /*!******************************************************************************************!*\
    !*** ./node_modules/angular-draggable-droppable/fesm2015/angular-draggable-droppable.js ***!
    \******************************************************************************************/

  /*! exports provided: DragAndDropModule, ɵc, ɵd, ɵb, ɵa */

  /***/
  function node_modulesAngularDraggableDroppableFesm2015AngularDraggableDroppableJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DragAndDropModule", function () {
      return DragAndDropModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵc", function () {
      return DraggableHelper;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵd", function () {
      return DraggableScrollContainerDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵb", function () {
      return DraggableDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵa", function () {
      return DroppableDirective;
    });
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var dom_autoscroller__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! dom-autoscroller */
    "./node_modules/dom-autoscroller/dist/bundle.js");
    /* harmony import */


    var dom_autoscroller__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(dom_autoscroller__WEBPACK_IMPORTED_MODULE_4__);
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */


    var DraggableHelper = function DraggableHelper() {
      _classCallCheck(this, DraggableHelper);

      this.currentDrag = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
    };

    DraggableHelper.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
      args: [{
        providedIn: 'root'
      }]
    }];
    /** @nocollapse */

    DraggableHelper.ngInjectableDef = Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["defineInjectable"])({
      factory: function DraggableHelper_Factory() {
        return new DraggableHelper();
      },
      token: DraggableHelper,
      providedIn: "root"
    });
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    var DraggableScrollContainerDirective = /*#__PURE__*/function () {
      /**
       * @param {?} elementRef
       * @param {?} renderer
       * @param {?} zone
       */
      function DraggableScrollContainerDirective(elementRef, renderer, zone) {
        _classCallCheck(this, DraggableScrollContainerDirective);

        this.elementRef = elementRef;
        this.renderer = renderer;
        this.zone = zone;
        /**
         * Trigger the DragStart after a long touch in scrollable container when true
         */

        this.activeLongPressDrag = false;
        /**
         * Configuration of a long touch
         * Duration in ms of a long touch before activating DragStart
         * Delta of the
         */

        this.longPressConfig = {
          duration: 300,
          delta: 30
        };
        this.cancelledScroll = false;
      }
      /**
       * @return {?}
       */


      _createClass(DraggableScrollContainerDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this15 = this;

          this.zone.runOutsideAngular(function () {
            _this15.renderer.listen(_this15.elementRef.nativeElement, 'touchmove', function (event) {
              if (_this15.cancelledScroll && event.cancelable) {
                event.preventDefault();
              }
            });
          });
        }
        /**
         * @return {?}
         */

      }, {
        key: "disableScroll",
        value: function disableScroll() {
          this.cancelledScroll = true;
          this.renderer.setStyle(this.elementRef.nativeElement, 'overflow', 'hidden');
        }
        /**
         * @return {?}
         */

      }, {
        key: "enableScroll",
        value: function enableScroll() {
          this.cancelledScroll = false;
          this.renderer.setStyle(this.elementRef.nativeElement, 'overflow', 'auto');
        }
        /**
         * @return {?}
         */

      }, {
        key: "hasScrollbar",
        value: function hasScrollbar() {
          /** @type {?} */
          var containerHasHorizontalScroll = this.elementRef.nativeElement.scrollWidth - this.elementRef.nativeElement.clientWidth > 0;
          /** @type {?} */

          var containerHasVerticalScroll = this.elementRef.nativeElement.scrollHeight - this.elementRef.nativeElement.clientHeight > 0;
          return containerHasHorizontalScroll || containerHasVerticalScroll;
        }
      }]);

      return DraggableScrollContainerDirective;
    }();

    DraggableScrollContainerDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
      args: [{
        selector: '[mwlDraggableScrollContainer]'
      }]
    }];
    /** @nocollapse */

    DraggableScrollContainerDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }];
    };

    DraggableScrollContainerDirective.propDecorators = {
      activeLongPressDrag: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      longPressConfig: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    var DraggableDirective = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} element
       * @param {?} renderer
       * @param {?} draggableHelper
       * @param {?} zone
       * @param {?} vcr
       * @param {?} scrollContainer
       * @param {?} document
       */
      function DraggableDirective(element, renderer, draggableHelper, zone, vcr, scrollContainer, document) {
        _classCallCheck(this, DraggableDirective);

        this.element = element;
        this.renderer = renderer;
        this.draggableHelper = draggableHelper;
        this.zone = zone;
        this.vcr = vcr;
        this.scrollContainer = scrollContainer;
        this.document = document;
        /**
         * The axis along which the element is draggable
         */

        this.dragAxis = {
          x: true,
          y: true
        };
        /**
         * Snap all drags to an x / y grid
         */

        this.dragSnapGrid = {};
        /**
         * Show a ghost element that shows the drag when dragging
         */

        this.ghostDragEnabled = true;
        /**
         * Show the original element when ghostDragEnabled is true
         */

        this.showOriginalElementWhileDragging = false;
        /**
         * The cursor to use when dragging the element
         */

        this.dragCursor = '';
        /**
         * Called when the element can be dragged along one axis and has the mouse or pointer device pressed on it
         */

        this.dragPointerDown = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called when the element has started to be dragged.
         * Only called after at least one mouse or touch move event.
         * If you call $event.cancelDrag$.emit() it will cancel the current drag
         */

        this.dragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called after the ghost element has been created
         */

        this.ghostElementCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called when the element is being dragged
         */

        this.dragging = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called after the element is dragged
         */

        this.dragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @hidden
         */

        this.pointerDown$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        /**
         * @hidden
         */

        this.pointerMove$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        /**
         * @hidden
         */

        this.pointerUp$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.eventListenerSubscriptions = {};
        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.timeLongPress = {
          timerBegin: 0,
          timerEnd: 0
        };
      }
      /**
       * @return {?}
       */


      _createClass(DraggableDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this16 = this;

          this.checkEventListeners();
          /** @type {?} */

          var pointerDragged$ = this.pointerDown$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function () {
            return _this16.canDrag();
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (pointerDownEvent) {
            // fix for https://github.com/mattlewis92/angular-draggable-droppable/issues/61
            // stop mouse events propagating up the chain
            if (pointerDownEvent.event.stopPropagation && !_this16.scrollContainer) {
              pointerDownEvent.event.stopPropagation();
            }
            /** @type {?} */


            var globalDragStyle = _this16.renderer.createElement('style');

            _this16.renderer.setAttribute(globalDragStyle, 'type', 'text/css');

            _this16.renderer.appendChild(globalDragStyle, _this16.renderer.createText("\n          body * {\n           -moz-user-select: none;\n           -ms-user-select: none;\n           -webkit-user-select: none;\n           user-select: none;\n          }\n        "));

            _this16.document.head.appendChild(globalDragStyle);
            /** @type {?} */


            var startScrollPosition = _this16.getScrollPosition();
            /** @type {?} */


            var scrollContainerScroll$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"](function (observer) {
              /** @type {?} */
              var scrollContainer = _this16.scrollContainer ? _this16.scrollContainer.elementRef.nativeElement : 'window';
              return _this16.renderer.listen(scrollContainer, 'scroll', function (e) {
                return observer.next(e);
              });
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["startWith"])(startScrollPosition), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
              return _this16.getScrollPosition();
            }));
            /** @type {?} */

            var currentDrag$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
            /** @type {?} */

            var cancelDrag$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"]();

            _this16.zone.run(function () {
              _this16.dragPointerDown.next({
                x: 0,
                y: 0
              });
            });
            /** @type {?} */


            var dragComplete$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["merge"])(_this16.pointerUp$, _this16.pointerDown$, cancelDrag$, _this16.destroy$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
            /** @type {?} */

            var pointerMove = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])(_this16.pointerMove$, scrollContainerScroll$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref35) {
              var _ref36 = _slicedToArray(_ref35, 2),
                  pointerMoveEvent = _ref36[0],
                  scroll = _ref36[1];

              return {
                currentDrag$: currentDrag$,
                transformX: pointerMoveEvent.clientX - pointerDownEvent.clientX,
                transformY: pointerMoveEvent.clientY - pointerDownEvent.clientY,
                clientX: pointerMoveEvent.clientX,
                clientY: pointerMoveEvent.clientY,
                scrollLeft: scroll.left,
                scrollTop: scroll.top
              };
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (moveData) {
              if (_this16.dragSnapGrid.x) {
                moveData.transformX = Math.round(moveData.transformX / _this16.dragSnapGrid.x) * _this16.dragSnapGrid.x;
              }

              if (_this16.dragSnapGrid.y) {
                moveData.transformY = Math.round(moveData.transformY / _this16.dragSnapGrid.y) * _this16.dragSnapGrid.y;
              }

              return moveData;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (moveData) {
              if (!_this16.dragAxis.x) {
                moveData.transformX = 0;
              }

              if (!_this16.dragAxis.y) {
                moveData.transformY = 0;
              }

              return moveData;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (moveData) {
              /** @type {?} */
              var scrollX = moveData.scrollLeft - startScrollPosition.left;
              /** @type {?} */

              var scrollY = moveData.scrollTop - startScrollPosition.top;
              return Object.assign({}, moveData, {
                x: moveData.transformX + scrollX,
                y: moveData.transformY + scrollY
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (_ref37) {
              var x = _ref37.x,
                  y = _ref37.y,
                  transformX = _ref37.transformX,
                  transformY = _ref37.transformY;
              return !_this16.validateDrag || _this16.validateDrag({
                x: x,
                y: y,
                transform: {
                  x: transformX,
                  y: transformY
                }
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(dragComplete$), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
            /** @type {?} */

            var dragStarted$ = pointerMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
            /** @type {?} */

            var dragEnded$ = pointerMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeLast"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
            dragStarted$.subscribe(function (_ref38) {
              var clientX = _ref38.clientX,
                  clientY = _ref38.clientY,
                  x = _ref38.x,
                  y = _ref38.y;

              _this16.zone.run(function () {
                _this16.dragStart.next({
                  cancelDrag$: cancelDrag$
                });
              });

              _this16.scroller = dom_autoscroller__WEBPACK_IMPORTED_MODULE_4___default()([_this16.scrollContainer ? _this16.scrollContainer.elementRef.nativeElement : _this16.document.defaultView], {
                margin: 20,

                /**
                 * @return {?}
                 */
                autoScroll: function autoScroll() {
                  return true;
                }
              });

              _this16.renderer.addClass(_this16.element.nativeElement, _this16.dragActiveClass);

              if (_this16.ghostDragEnabled) {
                /** @type {?} */
                var rect = _this16.element.nativeElement.getBoundingClientRect();
                /** @type {?} */


                var clone =
                /** @type {?} */
                _this16.element.nativeElement.cloneNode(true);

                if (!_this16.showOriginalElementWhileDragging) {
                  _this16.renderer.setStyle(_this16.element.nativeElement, 'visibility', 'hidden');
                }

                if (_this16.ghostElementAppendTo) {
                  _this16.ghostElementAppendTo.appendChild(clone);
                } else {
                  /** @type {?} */
                  _this16.element.nativeElement.parentNode.insertBefore(clone, _this16.element.nativeElement.nextSibling);
                }

                _this16.ghostElement = clone;

                _this16.setElementStyles(clone, {
                  position: 'fixed',
                  top: "".concat(rect.top, "px"),
                  left: "".concat(rect.left, "px"),
                  width: "".concat(rect.width, "px"),
                  height: "".concat(rect.height, "px"),
                  cursor: _this16.dragCursor,
                  margin: '0'
                });

                if (_this16.ghostElementTemplate) {
                  /** @type {?} */
                  var viewRef = _this16.vcr.createEmbeddedView(_this16.ghostElementTemplate);

                  clone.innerHTML = '';
                  viewRef.rootNodes.filter(function (node) {
                    return node instanceof Node;
                  }).forEach(function (node) {
                    clone.appendChild(node);
                  });
                  dragEnded$.subscribe(function () {
                    _this16.vcr.remove(_this16.vcr.indexOf(viewRef));
                  });
                }

                _this16.zone.run(function () {
                  _this16.ghostElementCreated.emit({
                    clientX: clientX - x,
                    clientY: clientY - y,
                    element: clone
                  });
                });

                dragEnded$.subscribe(function () {
                  /** @type {?} */
                  clone.parentElement.removeChild(clone);
                  _this16.ghostElement = null;

                  _this16.renderer.setStyle(_this16.element.nativeElement, 'visibility', '');
                });
              }

              _this16.draggableHelper.currentDrag.next(currentDrag$);
            });
            dragEnded$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (dragEndData) {
              /** @type {?} */
              var dragEndData$ = cancelDrag$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["count"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (calledCount) {
                return Object.assign({}, dragEndData, {
                  dragCancelled: calledCount > 0
                });
              }));
              cancelDrag$.complete();
              return dragEndData$;
            })).subscribe(function (_ref39) {
              var x = _ref39.x,
                  y = _ref39.y,
                  dragCancelled = _ref39.dragCancelled;

              _this16.scroller.destroy();

              _this16.zone.run(function () {
                _this16.dragEnd.next({
                  x: x,
                  y: y,
                  dragCancelled: dragCancelled
                });
              });

              _this16.renderer.removeClass(_this16.element.nativeElement, _this16.dragActiveClass);

              currentDrag$.complete();
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["merge"])(dragComplete$, dragEnded$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1)).subscribe(function () {
              _this16.document.head.removeChild(globalDragStyle);
            });
            return pointerMove;
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
          Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["merge"])(pointerDragged$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (value) {
            return [, value];
          })), pointerDragged$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["pairwise"])())).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (_ref40) {
            var _ref41 = _slicedToArray(_ref40, 2),
                previous = _ref41[0],
                next = _ref41[1];

            if (!previous) {
              return true;
            }

            return previous.x !== next.x || previous.y !== next.y;
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref42) {
            var _ref43 = _slicedToArray(_ref42, 2),
                previous = _ref43[0],
                next = _ref43[1];

            return next;
          })).subscribe(function (_ref44) {
            var x = _ref44.x,
                y = _ref44.y,
                currentDrag$ = _ref44.currentDrag$,
                clientX = _ref44.clientX,
                clientY = _ref44.clientY,
                transformX = _ref44.transformX,
                transformY = _ref44.transformY;

            _this16.zone.run(function () {
              _this16.dragging.next({
                x: x,
                y: y
              });
            });

            if (_this16.ghostElement) {
              /** @type {?} */
              var transform = "translate(".concat(transformX, "px, ").concat(transformY, "px)");

              _this16.setElementStyles(_this16.ghostElement, {
                transform: transform,
                '-webkit-transform': transform,
                '-ms-transform': transform,
                '-moz-transform': transform,
                '-o-transform': transform
              });
            }

            currentDrag$.next({
              clientX: clientX,
              clientY: clientY,
              dropData: _this16.dropData
            });
          });
        }
        /**
         * @param {?} changes
         * @return {?}
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          if (changes["dragAxis"]) {
            this.checkEventListeners();
          }
        }
        /**
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeEventListeners();
          this.pointerDown$.complete();
          this.pointerMove$.complete();
          this.pointerUp$.complete();
          this.destroy$.next();
        }
        /**
         * @return {?}
         */

      }, {
        key: "checkEventListeners",
        value: function checkEventListeners() {
          var _this17 = this;

          /** @type {?} */
          var canDrag = this.canDrag();
          /** @type {?} */

          var hasEventListeners = Object.keys(this.eventListenerSubscriptions).length > 0;

          if (canDrag && !hasEventListeners) {
            this.zone.runOutsideAngular(function () {
              _this17.eventListenerSubscriptions.mousedown = _this17.renderer.listen(_this17.element.nativeElement, 'mousedown', function (event) {
                _this17.onMouseDown(event);
              });
              _this17.eventListenerSubscriptions.mouseup = _this17.renderer.listen('document', 'mouseup', function (event) {
                _this17.onMouseUp(event);
              });
              _this17.eventListenerSubscriptions.touchstart = _this17.renderer.listen(_this17.element.nativeElement, 'touchstart', function (event) {
                _this17.onTouchStart(event);
              });
              _this17.eventListenerSubscriptions.touchend = _this17.renderer.listen('document', 'touchend', function (event) {
                _this17.onTouchEnd(event);
              });
              _this17.eventListenerSubscriptions.touchcancel = _this17.renderer.listen('document', 'touchcancel', function (event) {
                _this17.onTouchEnd(event);
              });
              _this17.eventListenerSubscriptions.mouseenter = _this17.renderer.listen(_this17.element.nativeElement, 'mouseenter', function () {
                _this17.onMouseEnter();
              });
              _this17.eventListenerSubscriptions.mouseleave = _this17.renderer.listen(_this17.element.nativeElement, 'mouseleave', function () {
                _this17.onMouseLeave();
              });
            });
          } else if (!canDrag && hasEventListeners) {
            this.unsubscribeEventListeners();
          }
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "onMouseDown",
        value: function onMouseDown(event) {
          var _this18 = this;

          if (!this.eventListenerSubscriptions.mousemove) {
            this.eventListenerSubscriptions.mousemove = this.renderer.listen('document', 'mousemove', function (mouseMoveEvent) {
              _this18.pointerMove$.next({
                event: mouseMoveEvent,
                clientX: mouseMoveEvent.clientX,
                clientY: mouseMoveEvent.clientY
              });
            });
          }

          this.pointerDown$.next({
            event: event,
            clientX: event.clientX,
            clientY: event.clientY
          });
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "onMouseUp",
        value: function onMouseUp(event) {
          if (this.eventListenerSubscriptions.mousemove) {
            this.eventListenerSubscriptions.mousemove();
            delete this.eventListenerSubscriptions.mousemove;
          }

          this.pointerUp$.next({
            event: event,
            clientX: event.clientX,
            clientY: event.clientY
          });
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "onTouchStart",
        value: function onTouchStart(event) {
          var _this19 = this;

          if (!this.scrollContainer) {
            try {
              event.preventDefault();
            } catch (e) {}
          }
          /** @type {?} */


          var hasContainerScrollbar;
          /** @type {?} */

          var startScrollPosition;
          /** @type {?} */

          var isDragActivated;

          if (this.scrollContainer && this.scrollContainer.activeLongPressDrag) {
            this.timeLongPress.timerBegin = Date.now();
            isDragActivated = false;
            hasContainerScrollbar = this.scrollContainer.hasScrollbar();
            startScrollPosition = this.getScrollPosition();
          }

          if (!this.eventListenerSubscriptions.touchmove) {
            this.eventListenerSubscriptions.touchmove = this.renderer.listen('document', 'touchmove', function (touchMoveEvent) {
              if (_this19.scrollContainer && _this19.scrollContainer.activeLongPressDrag && !isDragActivated && hasContainerScrollbar) {
                isDragActivated = _this19.shouldBeginDrag(event, touchMoveEvent, startScrollPosition);
              }

              if (!_this19.scrollContainer || !_this19.scrollContainer.activeLongPressDrag || !hasContainerScrollbar || isDragActivated) {
                _this19.pointerMove$.next({
                  event: touchMoveEvent,
                  clientX: touchMoveEvent.targetTouches[0].clientX,
                  clientY: touchMoveEvent.targetTouches[0].clientY
                });
              }
            });
          }

          this.pointerDown$.next({
            event: event,
            clientX: event.touches[0].clientX,
            clientY: event.touches[0].clientY
          });
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "onTouchEnd",
        value: function onTouchEnd(event) {
          if (this.eventListenerSubscriptions.touchmove) {
            this.eventListenerSubscriptions.touchmove();
            delete this.eventListenerSubscriptions.touchmove;

            if (this.scrollContainer && this.scrollContainer.activeLongPressDrag) {
              this.scrollContainer.enableScroll();
            }
          }

          this.pointerUp$.next({
            event: event,
            clientX: event.changedTouches[0].clientX,
            clientY: event.changedTouches[0].clientY
          });
        }
        /**
         * @return {?}
         */

      }, {
        key: "onMouseEnter",
        value: function onMouseEnter() {
          this.setCursor(this.dragCursor);
        }
        /**
         * @return {?}
         */

      }, {
        key: "onMouseLeave",
        value: function onMouseLeave() {
          this.setCursor('');
        }
        /**
         * @return {?}
         */

      }, {
        key: "canDrag",
        value: function canDrag() {
          return this.dragAxis.x || this.dragAxis.y;
        }
        /**
         * @param {?} value
         * @return {?}
         */

      }, {
        key: "setCursor",
        value: function setCursor(value) {
          this.renderer.setStyle(this.element.nativeElement, 'cursor', value);
        }
        /**
         * @return {?}
         */

      }, {
        key: "unsubscribeEventListeners",
        value: function unsubscribeEventListeners() {
          var _this20 = this;

          Object.keys(this.eventListenerSubscriptions).forEach(function (type) {
            /** @type {?} */
            _this20.eventListenerSubscriptions[type]();

            delete
            /** @type {?} */
            _this20.eventListenerSubscriptions[type];
          });
        }
        /**
         * @param {?} element
         * @param {?} styles
         * @return {?}
         */

      }, {
        key: "setElementStyles",
        value: function setElementStyles(element, styles) {
          var _this21 = this;

          Object.keys(styles).forEach(function (key) {
            _this21.renderer.setStyle(element, key, styles[key]);
          });
        }
        /**
         * @return {?}
         */

      }, {
        key: "getScrollPosition",
        value: function getScrollPosition() {
          if (this.scrollContainer) {
            return {
              top: this.scrollContainer.elementRef.nativeElement.scrollTop,
              left: this.scrollContainer.elementRef.nativeElement.scrollLeft
            };
          } else {
            return {
              top: window.pageYOffset || document.documentElement.scrollTop,
              left: window.pageXOffset || document.documentElement.scrollLeft
            };
          }
        }
        /**
         * @param {?} event
         * @param {?} touchMoveEvent
         * @param {?} startScrollPosition
         * @return {?}
         */

      }, {
        key: "shouldBeginDrag",
        value: function shouldBeginDrag(event, touchMoveEvent, startScrollPosition) {
          /** @type {?} */
          var moveScrollPosition = this.getScrollPosition();
          /** @type {?} */

          var deltaScroll = {
            top: Math.abs(moveScrollPosition.top - startScrollPosition.top),
            left: Math.abs(moveScrollPosition.left - startScrollPosition.left)
          };
          /** @type {?} */

          var deltaX = Math.abs(touchMoveEvent.targetTouches[0].clientX - event.touches[0].clientX) - deltaScroll.left;
          /** @type {?} */

          var deltaY = Math.abs(touchMoveEvent.targetTouches[0].clientY - event.touches[0].clientY) - deltaScroll.top;
          /** @type {?} */

          var deltaTotal = deltaX + deltaY;

          if (deltaTotal > this.scrollContainer.longPressConfig.delta || deltaScroll.top > 0 || deltaScroll.left > 0) {
            this.timeLongPress.timerBegin = Date.now();
          }

          this.timeLongPress.timerEnd = Date.now();
          /** @type {?} */

          var duration = this.timeLongPress.timerEnd - this.timeLongPress.timerBegin;

          if (duration >= this.scrollContainer.longPressConfig.duration) {
            this.scrollContainer.disableScroll();
            return true;
          }

          return false;
        }
      }]);

      return DraggableDirective;
    }();

    DraggableDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
      args: [{
        selector: '[mwlDraggable]'
      }]
    }];
    /** @nocollapse */

    DraggableDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
      }, {
        type: DraggableHelper
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"]
      }, {
        type: DraggableScrollContainerDirective,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"]
        }]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
          args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
        }]
      }];
    };

    DraggableDirective.propDecorators = {
      dropData: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragAxis: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragSnapGrid: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      ghostDragEnabled: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      showOriginalElementWhileDragging: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      validateDrag: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragCursor: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragActiveClass: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      ghostElementAppendTo: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      ghostElementTemplate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragPointerDown: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      dragStart: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      ghostElementCreated: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      dragging: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      dragEnd: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @param {?} clientX
     * @param {?} clientY
     * @param {?} rect
     * @return {?}
     */

    function isCoordinateWithinRectangle(clientX, clientY, rect) {
      return clientX >= rect.left && clientX <= rect.right && clientY >= rect.top && clientY <= rect.bottom;
    }

    var DroppableDirective = /*#__PURE__*/function () {
      /**
       * @param {?} element
       * @param {?} draggableHelper
       * @param {?} zone
       * @param {?} renderer
       * @param {?} scrollContainer
       */
      function DroppableDirective(element, draggableHelper, zone, renderer, scrollContainer) {
        _classCallCheck(this, DroppableDirective);

        this.element = element;
        this.draggableHelper = draggableHelper;
        this.zone = zone;
        this.renderer = renderer;
        this.scrollContainer = scrollContainer;
        /**
         * Called when a draggable element starts overlapping the element
         */

        this.dragEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called when a draggable element stops overlapping the element
         */

        this.dragLeave = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called when a draggable element is moved over the element
         */

        this.dragOver = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * Called when a draggable element is dropped on this element
         */

        this.drop = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * @return {?}
       */


      _createClass(DroppableDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this22 = this;

          this.currentDragSubscription = this.draggableHelper.currentDrag.subscribe(function (drag$) {
            _this22.renderer.addClass(_this22.element.nativeElement, _this22.dragActiveClass);
            /** @type {?} */


            var droppableElement = {
              updateCache: true
            };
            /** @type {?} */

            var deregisterScrollListener = _this22.renderer.listen(_this22.scrollContainer ? _this22.scrollContainer.elementRef.nativeElement : 'window', 'scroll', function () {
              droppableElement.updateCache = true;
            });
            /** @type {?} */


            var currentDragDropData;
            /** @type {?} */

            var overlaps$ = drag$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref45) {
              var clientX = _ref45.clientX,
                  clientY = _ref45.clientY,
                  dropData = _ref45.dropData;
              currentDragDropData = dropData;

              if (droppableElement.updateCache) {
                droppableElement.rect = _this22.element.nativeElement.getBoundingClientRect();

                if (_this22.scrollContainer) {
                  droppableElement.scrollContainerRect = _this22.scrollContainer.elementRef.nativeElement.getBoundingClientRect();
                }

                droppableElement.updateCache = false;
              }
              /** @type {?} */


              var isWithinElement = isCoordinateWithinRectangle(clientX, clientY,
              /** @type {?} */
              droppableElement.rect);

              if (droppableElement.scrollContainerRect) {
                return isWithinElement && isCoordinateWithinRectangle(clientX, clientY,
                /** @type {?} */
                droppableElement.scrollContainerRect);
              } else {
                return isWithinElement;
              }
            }));
            /** @type {?} */

            var overlapsChanged$ = overlaps$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["distinctUntilChanged"])());
            /** @type {?} */

            var dragOverActive; // TODO - see if there's a way of doing this via rxjs

            overlapsChanged$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (overlapsNow) {
              return overlapsNow;
            })).subscribe(function () {
              dragOverActive = true;

              _this22.renderer.addClass(_this22.element.nativeElement, _this22.dragOverClass);

              _this22.zone.run(function () {
                _this22.dragEnter.next({
                  dropData: currentDragDropData
                });
              });
            });
            overlaps$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (overlapsNow) {
              return overlapsNow;
            })).subscribe(function () {
              _this22.zone.run(function () {
                _this22.dragOver.next({
                  dropData: currentDragDropData
                });
              });
            });
            overlapsChanged$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["pairwise"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (_ref46) {
              var _ref47 = _slicedToArray(_ref46, 2),
                  didOverlap = _ref47[0],
                  overlapsNow = _ref47[1];

              return didOverlap && !overlapsNow;
            })).subscribe(function () {
              dragOverActive = false;

              _this22.renderer.removeClass(_this22.element.nativeElement, _this22.dragOverClass);

              _this22.zone.run(function () {
                _this22.dragLeave.next({
                  dropData: currentDragDropData
                });
              });
            });
            drag$.subscribe({
              complete: function complete() {
                deregisterScrollListener();

                _this22.renderer.removeClass(_this22.element.nativeElement, _this22.dragActiveClass);

                if (dragOverActive) {
                  _this22.renderer.removeClass(_this22.element.nativeElement, _this22.dragOverClass);

                  _this22.zone.run(function () {
                    _this22.drop.next({
                      dropData: currentDragDropData
                    });
                  });
                }
              }
            });
          });
        }
        /**
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.currentDragSubscription) {
            this.currentDragSubscription.unsubscribe();
          }
        }
      }]);

      return DroppableDirective;
    }();

    DroppableDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
      args: [{
        selector: '[mwlDroppable]'
      }]
    }];
    /** @nocollapse */

    DroppableDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
      }, {
        type: DraggableHelper
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
      }, {
        type: DraggableScrollContainerDirective,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"]
        }]
      }];
    };

    DroppableDirective.propDecorators = {
      dragOverClass: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragActiveClass: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
      }],
      dragEnter: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      dragLeave: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      dragOver: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }],
      drop: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    var DragAndDropModule = function DragAndDropModule() {
      _classCallCheck(this, DragAndDropModule);
    };

    DragAndDropModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
      args: [{
        declarations: [DraggableDirective, DroppableDirective, DraggableScrollContainerDirective],
        exports: [DraggableDirective, DroppableDirective, DraggableScrollContainerDirective]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    //# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1kcmFnZ2FibGUtZHJvcHBhYmxlLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9hbmd1bGFyLWRyYWdnYWJsZS1kcm9wcGFibGUvbGliL2RyYWdnYWJsZS1oZWxwZXIucHJvdmlkZXIudHMiLCJuZzovL2FuZ3VsYXItZHJhZ2dhYmxlLWRyb3BwYWJsZS9saWIvZHJhZ2dhYmxlLXNjcm9sbC1jb250YWluZXIuZGlyZWN0aXZlLnRzIiwibmc6Ly9hbmd1bGFyLWRyYWdnYWJsZS1kcm9wcGFibGUvbGliL2RyYWdnYWJsZS5kaXJlY3RpdmUudHMiLCJuZzovL2FuZ3VsYXItZHJhZ2dhYmxlLWRyb3BwYWJsZS9saWIvZHJvcHBhYmxlLmRpcmVjdGl2ZS50cyIsIm5nOi8vYW5ndWxhci1kcmFnZ2FibGUtZHJvcHBhYmxlL2xpYi9kcmFnLWFuZC1kcm9wLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQ3VycmVudERyYWdEYXRhIHtcbiAgY2xpZW50WDogbnVtYmVyO1xuICBjbGllbnRZOiBudW1iZXI7XG4gIGRyb3BEYXRhOiBhbnk7XG59XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERyYWdnYWJsZUhlbHBlciB7XG4gIGN1cnJlbnREcmFnID0gbmV3IFN1YmplY3Q8U3ViamVjdDxDdXJyZW50RHJhZ0RhdGE+PigpO1xufVxuIiwiaW1wb3J0IHtcbiAgRGlyZWN0aXZlLFxuICBFbGVtZW50UmVmLFxuICBJbnB1dCxcbiAgTmdab25lLFxuICBPbkluaXQsXG4gIFJlbmRlcmVyMlxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW213bERyYWdnYWJsZVNjcm9sbENvbnRhaW5lcl0nXG59KVxuZXhwb3J0IGNsYXNzIERyYWdnYWJsZVNjcm9sbENvbnRhaW5lckRpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIC8qKlxuICAgKiBUcmlnZ2VyIHRoZSBEcmFnU3RhcnQgYWZ0ZXIgYSBsb25nIHRvdWNoIGluIHNjcm9sbGFibGUgY29udGFpbmVyIHdoZW4gdHJ1ZVxuICAgKi9cbiAgQElucHV0KClcbiAgYWN0aXZlTG9uZ1ByZXNzRHJhZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBDb25maWd1cmF0aW9uIG9mIGEgbG9uZyB0b3VjaFxuICAgKiBEdXJhdGlvbiBpbiBtcyBvZiBhIGxvbmcgdG91Y2ggYmVmb3JlIGFjdGl2YXRpbmcgRHJhZ1N0YXJ0XG4gICAqIERlbHRhIG9mIHRoZVxuICAgKi9cbiAgQElucHV0KClcbiAgbG9uZ1ByZXNzQ29uZmlnID0geyBkdXJhdGlvbjogMzAwLCBkZWx0YTogMzAgfTtcblxuICBwcml2YXRlIGNhbmNlbGxlZFNjcm9sbCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBlbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcbiAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJpdmF0ZSB6b25lOiBOZ1pvbmVcbiAgKSB7fVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuem9uZS5ydW5PdXRzaWRlQW5ndWxhcigoKSA9PiB7XG4gICAgICB0aGlzLnJlbmRlcmVyLmxpc3RlbihcbiAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICd0b3VjaG1vdmUnLFxuICAgICAgICAoZXZlbnQ6IFRvdWNoRXZlbnQpID0+IHtcbiAgICAgICAgICBpZiAodGhpcy5jYW5jZWxsZWRTY3JvbGwgJiYgZXZlbnQuY2FuY2VsYWJsZSkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICk7XG4gICAgfSk7XG4gIH1cblxuICBkaXNhYmxlU2Nyb2xsKCk6IHZvaWQge1xuICAgIHRoaXMuY2FuY2VsbGVkU2Nyb2xsID0gdHJ1ZTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnb3ZlcmZsb3cnLCAnaGlkZGVuJyk7XG4gIH1cblxuICBlbmFibGVTY3JvbGwoKTogdm9pZCB7XG4gICAgdGhpcy5jYW5jZWxsZWRTY3JvbGwgPSBmYWxzZTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnb3ZlcmZsb3cnLCAnYXV0bycpO1xuICB9XG5cbiAgaGFzU2Nyb2xsYmFyKCk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IGNvbnRhaW5lckhhc0hvcml6b250YWxTY3JvbGwgPVxuICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuc2Nyb2xsV2lkdGggLVxuICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5jbGllbnRXaWR0aCA+XG4gICAgICAwO1xuICAgIGNvbnN0IGNvbnRhaW5lckhhc1ZlcnRpY2FsU2Nyb2xsID1cbiAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnNjcm9sbEhlaWdodCAtXG4gICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmNsaWVudEhlaWdodCA+XG4gICAgICAwO1xuICAgIHJldHVybiBjb250YWluZXJIYXNIb3Jpem9udGFsU2Nyb2xsIHx8IGNvbnRhaW5lckhhc1ZlcnRpY2FsU2Nyb2xsO1xuICB9XG59XG4iLCJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIE9uSW5pdCxcbiAgRWxlbWVudFJlZixcbiAgUmVuZGVyZXIyLFxuICBPdXRwdXQsXG4gIEV2ZW50RW1pdHRlcixcbiAgSW5wdXQsXG4gIE9uRGVzdHJveSxcbiAgT25DaGFuZ2VzLFxuICBOZ1pvbmUsXG4gIFNpbXBsZUNoYW5nZXMsXG4gIEluamVjdCxcbiAgVGVtcGxhdGVSZWYsXG4gIFZpZXdDb250YWluZXJSZWYsXG4gIE9wdGlvbmFsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3ViamVjdCwgT2JzZXJ2YWJsZSwgbWVyZ2UsIFJlcGxheVN1YmplY3QsIGNvbWJpbmVMYXRlc3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7XG4gIG1hcCxcbiAgbWVyZ2VNYXAsXG4gIHRha2VVbnRpbCxcbiAgdGFrZSxcbiAgdGFrZUxhc3QsXG4gIHBhaXJ3aXNlLFxuICBzaGFyZSxcbiAgZmlsdGVyLFxuICBjb3VudCxcbiAgc3RhcnRXaXRoXG59IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEN1cnJlbnREcmFnRGF0YSwgRHJhZ2dhYmxlSGVscGVyIH0gZnJvbSAnLi9kcmFnZ2FibGUtaGVscGVyLnByb3ZpZGVyJztcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCBhdXRvU2Nyb2xsIGZyb20gJ2RvbS1hdXRvc2Nyb2xsZXInO1xuaW1wb3J0IHsgRHJhZ2dhYmxlU2Nyb2xsQ29udGFpbmVyRGlyZWN0aXZlIH0gZnJvbSAnLi9kcmFnZ2FibGUtc2Nyb2xsLWNvbnRhaW5lci5kaXJlY3RpdmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIENvb3JkaW5hdGVzIHtcbiAgeDogbnVtYmVyO1xuICB5OiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRHJhZ0F4aXMge1xuICB4OiBib29sZWFuO1xuICB5OiBib29sZWFuO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFNuYXBHcmlkIHtcbiAgeD86IG51bWJlcjtcbiAgeT86IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBEcmFnUG9pbnRlckRvd25FdmVudCBleHRlbmRzIENvb3JkaW5hdGVzIHt9XG5cbmV4cG9ydCBpbnRlcmZhY2UgRHJhZ1N0YXJ0RXZlbnQge1xuICBjYW5jZWxEcmFnJDogUmVwbGF5U3ViamVjdDx2b2lkPjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBEcmFnTW92ZUV2ZW50IGV4dGVuZHMgQ29vcmRpbmF0ZXMge31cblxuZXhwb3J0IGludGVyZmFjZSBEcmFnRW5kRXZlbnQgZXh0ZW5kcyBDb29yZGluYXRlcyB7XG4gIGRyYWdDYW5jZWxsZWQ6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgVmFsaWRhdGVEcmFnUGFyYW1zIGV4dGVuZHMgQ29vcmRpbmF0ZXMge1xuICB0cmFuc2Zvcm06IHtcbiAgICB4OiBudW1iZXI7XG4gICAgeTogbnVtYmVyO1xuICB9O1xufVxuXG5leHBvcnQgdHlwZSBWYWxpZGF0ZURyYWcgPSAocGFyYW1zOiBWYWxpZGF0ZURyYWdQYXJhbXMpID0+IGJvb2xlYW47XG5cbmV4cG9ydCBpbnRlcmZhY2UgUG9pbnRlckV2ZW50IHtcbiAgY2xpZW50WDogbnVtYmVyO1xuICBjbGllbnRZOiBudW1iZXI7XG4gIGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBUaW1lTG9uZ1ByZXNzIHtcbiAgdGltZXJCZWdpbjogbnVtYmVyO1xuICB0aW1lckVuZDogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEdob3N0RWxlbWVudENyZWF0ZWRFdmVudCB7XG4gIGNsaWVudFg6IG51bWJlcjtcbiAgY2xpZW50WTogbnVtYmVyO1xuICBlbGVtZW50OiBIVE1MRWxlbWVudDtcbn1cblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW213bERyYWdnYWJsZV0nXG59KVxuZXhwb3J0IGNsYXNzIERyYWdnYWJsZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xuICAvKipcbiAgICogYW4gb2JqZWN0IG9mIGRhdGEgeW91IGNhbiBwYXNzIHRvIHRoZSBkcm9wIGV2ZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBkcm9wRGF0YTogYW55O1xuXG4gIC8qKlxuICAgKiBUaGUgYXhpcyBhbG9uZyB3aGljaCB0aGUgZWxlbWVudCBpcyBkcmFnZ2FibGVcbiAgICovXG4gIEBJbnB1dCgpXG4gIGRyYWdBeGlzOiBEcmFnQXhpcyA9IHsgeDogdHJ1ZSwgeTogdHJ1ZSB9O1xuXG4gIC8qKlxuICAgKiBTbmFwIGFsbCBkcmFncyB0byBhbiB4IC8geSBncmlkXG4gICAqL1xuICBASW5wdXQoKVxuICBkcmFnU25hcEdyaWQ6IFNuYXBHcmlkID0ge307XG5cbiAgLyoqXG4gICAqIFNob3cgYSBnaG9zdCBlbGVtZW50IHRoYXQgc2hvd3MgdGhlIGRyYWcgd2hlbiBkcmFnZ2luZ1xuICAgKi9cbiAgQElucHV0KClcbiAgZ2hvc3REcmFnRW5hYmxlZDogYm9vbGVhbiA9IHRydWU7XG5cbiAgLyoqXG4gICAqIFNob3cgdGhlIG9yaWdpbmFsIGVsZW1lbnQgd2hlbiBnaG9zdERyYWdFbmFibGVkIGlzIHRydWVcbiAgICovXG4gIEBJbnB1dCgpXG4gIHNob3dPcmlnaW5hbEVsZW1lbnRXaGlsZURyYWdnaW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIEFsbG93IGN1c3RvbSBiZWhhdmlvdXIgdG8gY29udHJvbCB3aGVuIHRoZSBlbGVtZW50IGlzIGRyYWdnZWRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHZhbGlkYXRlRHJhZzogVmFsaWRhdGVEcmFnO1xuXG4gIC8qKlxuICAgKiBUaGUgY3Vyc29yIHRvIHVzZSB3aGVuIGRyYWdnaW5nIHRoZSBlbGVtZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBkcmFnQ3Vyc29yOiBzdHJpbmcgPSAnJztcblxuICAvKipcbiAgICogVGhlIGNzcyBjbGFzcyB0byBhcHBseSB3aGVuIHRoZSBlbGVtZW50IGlzIGJlaW5nIGRyYWdnZWRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGRyYWdBY3RpdmVDbGFzczogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgZWxlbWVudCB0aGUgZ2hvc3QgZWxlbWVudCB3aWxsIGJlIGFwcGVuZGVkIHRvLiBEZWZhdWx0IGlzIG5leHQgdG8gdGhlIGRyYWdnZWQgZWxlbWVudFxuICAgKi9cbiAgQElucHV0KClcbiAgZ2hvc3RFbGVtZW50QXBwZW5kVG86IEhUTUxFbGVtZW50O1xuXG4gIC8qKlxuICAgKiBBbiBuZy10ZW1wbGF0ZSB0byBiZSBpbnNlcnRlZCBpbnRvIHRoZSBwYXJlbnQgZWxlbWVudCBvZiB0aGUgZ2hvc3QgZWxlbWVudC4gSXQgd2lsbCBvdmVyd3JpdGUgYW55IGNoaWxkIG5vZGVzLlxuICAgKi9cbiAgQElucHV0KClcbiAgZ2hvc3RFbGVtZW50VGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgLyoqXG4gICAqIENhbGxlZCB3aGVuIHRoZSBlbGVtZW50IGNhbiBiZSBkcmFnZ2VkIGFsb25nIG9uZSBheGlzIGFuZCBoYXMgdGhlIG1vdXNlIG9yIHBvaW50ZXIgZGV2aWNlIHByZXNzZWQgb24gaXRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBkcmFnUG9pbnRlckRvd24gPSBuZXcgRXZlbnRFbWl0dGVyPERyYWdQb2ludGVyRG93bkV2ZW50PigpO1xuXG4gIC8qKlxuICAgKiBDYWxsZWQgd2hlbiB0aGUgZWxlbWVudCBoYXMgc3RhcnRlZCB0byBiZSBkcmFnZ2VkLlxuICAgKiBPbmx5IGNhbGxlZCBhZnRlciBhdCBsZWFzdCBvbmUgbW91c2Ugb3IgdG91Y2ggbW92ZSBldmVudC5cbiAgICogSWYgeW91IGNhbGwgJGV2ZW50LmNhbmNlbERyYWckLmVtaXQoKSBpdCB3aWxsIGNhbmNlbCB0aGUgY3VycmVudCBkcmFnXG4gICAqL1xuICBAT3V0cHV0KClcbiAgZHJhZ1N0YXJ0ID0gbmV3IEV2ZW50RW1pdHRlcjxEcmFnU3RhcnRFdmVudD4oKTtcblxuICAvKipcbiAgICogQ2FsbGVkIGFmdGVyIHRoZSBnaG9zdCBlbGVtZW50IGhhcyBiZWVuIGNyZWF0ZWRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBnaG9zdEVsZW1lbnRDcmVhdGVkID0gbmV3IEV2ZW50RW1pdHRlcjxHaG9zdEVsZW1lbnRDcmVhdGVkRXZlbnQ+KCk7XG5cbiAgLyoqXG4gICAqIENhbGxlZCB3aGVuIHRoZSBlbGVtZW50IGlzIGJlaW5nIGRyYWdnZWRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBkcmFnZ2luZyA9IG5ldyBFdmVudEVtaXR0ZXI8RHJhZ01vdmVFdmVudD4oKTtcblxuICAvKipcbiAgICogQ2FsbGVkIGFmdGVyIHRoZSBlbGVtZW50IGlzIGRyYWdnZWRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBkcmFnRW5kID0gbmV3IEV2ZW50RW1pdHRlcjxEcmFnRW5kRXZlbnQ+KCk7XG5cbiAgLyoqXG4gICAqIEBoaWRkZW5cbiAgICovXG4gIHBvaW50ZXJEb3duJCA9IG5ldyBTdWJqZWN0PFBvaW50ZXJFdmVudD4oKTtcblxuICAvKipcbiAgICogQGhpZGRlblxuICAgKi9cbiAgcG9pbnRlck1vdmUkID0gbmV3IFN1YmplY3Q8UG9pbnRlckV2ZW50PigpO1xuXG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBwb2ludGVyVXAkID0gbmV3IFN1YmplY3Q8UG9pbnRlckV2ZW50PigpO1xuXG4gIHByaXZhdGUgZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnM6IHtcbiAgICBtb3VzZW1vdmU/OiAoKSA9PiB2b2lkO1xuICAgIG1vdXNlZG93bj86ICgpID0+IHZvaWQ7XG4gICAgbW91c2V1cD86ICgpID0+IHZvaWQ7XG4gICAgbW91c2VlbnRlcj86ICgpID0+IHZvaWQ7XG4gICAgbW91c2VsZWF2ZT86ICgpID0+IHZvaWQ7XG4gICAgdG91Y2hzdGFydD86ICgpID0+IHZvaWQ7XG4gICAgdG91Y2htb3ZlPzogKCkgPT4gdm9pZDtcbiAgICB0b3VjaGVuZD86ICgpID0+IHZvaWQ7XG4gICAgdG91Y2hjYW5jZWw/OiAoKSA9PiB2b2lkO1xuICB9ID0ge307XG5cbiAgcHJpdmF0ZSBnaG9zdEVsZW1lbnQ6IEhUTUxFbGVtZW50IHwgbnVsbDtcblxuICBwcml2YXRlIGRlc3Ryb3kkID0gbmV3IFN1YmplY3QoKTtcblxuICBwcml2YXRlIHRpbWVMb25nUHJlc3M6IFRpbWVMb25nUHJlc3MgPSB7IHRpbWVyQmVnaW46IDAsIHRpbWVyRW5kOiAwIH07XG5cbiAgcHJpdmF0ZSBzY3JvbGxlcjogeyBkZXN0cm95OiAoKSA9PiB2b2lkIH07XG5cbiAgLyoqXG4gICAqIEBoaWRkZW5cbiAgICovXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWxlbWVudDogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxuICAgIHByaXZhdGUgZHJhZ2dhYmxlSGVscGVyOiBEcmFnZ2FibGVIZWxwZXIsXG4gICAgcHJpdmF0ZSB6b25lOiBOZ1pvbmUsXG4gICAgcHJpdmF0ZSB2Y3I6IFZpZXdDb250YWluZXJSZWYsXG4gICAgQE9wdGlvbmFsKCkgcHJpdmF0ZSBzY3JvbGxDb250YWluZXI6IERyYWdnYWJsZVNjcm9sbENvbnRhaW5lckRpcmVjdGl2ZSxcbiAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnlcbiAgKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2hlY2tFdmVudExpc3RlbmVycygpO1xuXG4gICAgY29uc3QgcG9pbnRlckRyYWdnZWQkOiBPYnNlcnZhYmxlPGFueT4gPSB0aGlzLnBvaW50ZXJEb3duJC5waXBlKFxuICAgICAgZmlsdGVyKCgpID0+IHRoaXMuY2FuRHJhZygpKSxcbiAgICAgIG1lcmdlTWFwKChwb2ludGVyRG93bkV2ZW50OiBQb2ludGVyRXZlbnQpID0+IHtcbiAgICAgICAgLy8gZml4IGZvciBodHRwczovL2dpdGh1Yi5jb20vbWF0dGxld2lzOTIvYW5ndWxhci1kcmFnZ2FibGUtZHJvcHBhYmxlL2lzc3Vlcy82MVxuICAgICAgICAvLyBzdG9wIG1vdXNlIGV2ZW50cyBwcm9wYWdhdGluZyB1cCB0aGUgY2hhaW5cbiAgICAgICAgaWYgKHBvaW50ZXJEb3duRXZlbnQuZXZlbnQuc3RvcFByb3BhZ2F0aW9uICYmICF0aGlzLnNjcm9sbENvbnRhaW5lcikge1xuICAgICAgICAgIHBvaW50ZXJEb3duRXZlbnQuZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBoYWNrIHRvIHByZXZlbnQgdGV4dCBnZXR0aW5nIHNlbGVjdGVkIGluIHNhZmFyaSB3aGlsZSBkcmFnZ2luZ1xuICAgICAgICBjb25zdCBnbG9iYWxEcmFnU3R5bGU6IEhUTUxTdHlsZUVsZW1lbnQgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3N0eWxlJ1xuICAgICAgICApO1xuICAgICAgICB0aGlzLnJlbmRlcmVyLnNldEF0dHJpYnV0ZShnbG9iYWxEcmFnU3R5bGUsICd0eXBlJywgJ3RleHQvY3NzJyk7XG4gICAgICAgIHRoaXMucmVuZGVyZXIuYXBwZW5kQ2hpbGQoXG4gICAgICAgICAgZ2xvYmFsRHJhZ1N0eWxlLFxuICAgICAgICAgIHRoaXMucmVuZGVyZXIuY3JlYXRlVGV4dChgXG4gICAgICAgICAgYm9keSAqIHtcbiAgICAgICAgICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICAgICAgICAgICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xuICAgICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgICB9XG4gICAgICAgIGApXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZChnbG9iYWxEcmFnU3R5bGUpO1xuXG4gICAgICAgIGNvbnN0IHN0YXJ0U2Nyb2xsUG9zaXRpb24gPSB0aGlzLmdldFNjcm9sbFBvc2l0aW9uKCk7XG5cbiAgICAgICAgY29uc3Qgc2Nyb2xsQ29udGFpbmVyU2Nyb2xsJCA9IG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcbiAgICAgICAgICBjb25zdCBzY3JvbGxDb250YWluZXIgPSB0aGlzLnNjcm9sbENvbnRhaW5lclxuICAgICAgICAgICAgPyB0aGlzLnNjcm9sbENvbnRhaW5lci5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRcbiAgICAgICAgICAgIDogJ3dpbmRvdyc7XG4gICAgICAgICAgcmV0dXJuIHRoaXMucmVuZGVyZXIubGlzdGVuKHNjcm9sbENvbnRhaW5lciwgJ3Njcm9sbCcsIGUgPT5cbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZSlcbiAgICAgICAgICApO1xuICAgICAgICB9KS5waXBlKFxuICAgICAgICAgIHN0YXJ0V2l0aChzdGFydFNjcm9sbFBvc2l0aW9uKSxcbiAgICAgICAgICBtYXAoKCkgPT4gdGhpcy5nZXRTY3JvbGxQb3NpdGlvbigpKVxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IGN1cnJlbnREcmFnJCA9IG5ldyBTdWJqZWN0PEN1cnJlbnREcmFnRGF0YT4oKTtcbiAgICAgICAgY29uc3QgY2FuY2VsRHJhZyQgPSBuZXcgUmVwbGF5U3ViamVjdDx2b2lkPigpO1xuXG4gICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgIHRoaXMuZHJhZ1BvaW50ZXJEb3duLm5leHQoeyB4OiAwLCB5OiAwIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBjb25zdCBkcmFnQ29tcGxldGUkID0gbWVyZ2UoXG4gICAgICAgICAgdGhpcy5wb2ludGVyVXAkLFxuICAgICAgICAgIHRoaXMucG9pbnRlckRvd24kLFxuICAgICAgICAgIGNhbmNlbERyYWckLFxuICAgICAgICAgIHRoaXMuZGVzdHJveSRcbiAgICAgICAgKS5waXBlKHNoYXJlKCkpO1xuXG4gICAgICAgIGNvbnN0IHBvaW50ZXJNb3ZlID0gY29tYmluZUxhdGVzdDxcbiAgICAgICAgICBQb2ludGVyRXZlbnQsXG4gICAgICAgICAgeyB0b3A6IG51bWJlcjsgbGVmdDogbnVtYmVyIH1cbiAgICAgICAgPih0aGlzLnBvaW50ZXJNb3ZlJCwgc2Nyb2xsQ29udGFpbmVyU2Nyb2xsJCkucGlwZShcbiAgICAgICAgICBtYXAoKFtwb2ludGVyTW92ZUV2ZW50LCBzY3JvbGxdKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBjdXJyZW50RHJhZyQsXG4gICAgICAgICAgICAgIHRyYW5zZm9ybVg6IHBvaW50ZXJNb3ZlRXZlbnQuY2xpZW50WCAtIHBvaW50ZXJEb3duRXZlbnQuY2xpZW50WCxcbiAgICAgICAgICAgICAgdHJhbnNmb3JtWTogcG9pbnRlck1vdmVFdmVudC5jbGllbnRZIC0gcG9pbnRlckRvd25FdmVudC5jbGllbnRZLFxuICAgICAgICAgICAgICBjbGllbnRYOiBwb2ludGVyTW92ZUV2ZW50LmNsaWVudFgsXG4gICAgICAgICAgICAgIGNsaWVudFk6IHBvaW50ZXJNb3ZlRXZlbnQuY2xpZW50WSxcbiAgICAgICAgICAgICAgc2Nyb2xsTGVmdDogc2Nyb2xsLmxlZnQsXG4gICAgICAgICAgICAgIHNjcm9sbFRvcDogc2Nyb2xsLnRvcFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9KSxcbiAgICAgICAgICBtYXAobW92ZURhdGEgPT4ge1xuICAgICAgICAgICAgaWYgKHRoaXMuZHJhZ1NuYXBHcmlkLngpIHtcbiAgICAgICAgICAgICAgbW92ZURhdGEudHJhbnNmb3JtWCA9XG4gICAgICAgICAgICAgICAgTWF0aC5yb3VuZChtb3ZlRGF0YS50cmFuc2Zvcm1YIC8gdGhpcy5kcmFnU25hcEdyaWQueCkgKlxuICAgICAgICAgICAgICAgIHRoaXMuZHJhZ1NuYXBHcmlkLng7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmRyYWdTbmFwR3JpZC55KSB7XG4gICAgICAgICAgICAgIG1vdmVEYXRhLnRyYW5zZm9ybVkgPVxuICAgICAgICAgICAgICAgIE1hdGgucm91bmQobW92ZURhdGEudHJhbnNmb3JtWSAvIHRoaXMuZHJhZ1NuYXBHcmlkLnkpICpcbiAgICAgICAgICAgICAgICB0aGlzLmRyYWdTbmFwR3JpZC55O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gbW92ZURhdGE7XG4gICAgICAgICAgfSksXG4gICAgICAgICAgbWFwKG1vdmVEYXRhID0+IHtcbiAgICAgICAgICAgIGlmICghdGhpcy5kcmFnQXhpcy54KSB7XG4gICAgICAgICAgICAgIG1vdmVEYXRhLnRyYW5zZm9ybVggPSAwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXRoaXMuZHJhZ0F4aXMueSkge1xuICAgICAgICAgICAgICBtb3ZlRGF0YS50cmFuc2Zvcm1ZID0gMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIG1vdmVEYXRhO1xuICAgICAgICAgIH0pLFxuICAgICAgICAgIG1hcChtb3ZlRGF0YSA9PiB7XG4gICAgICAgICAgICBjb25zdCBzY3JvbGxYID0gbW92ZURhdGEuc2Nyb2xsTGVmdCAtIHN0YXJ0U2Nyb2xsUG9zaXRpb24ubGVmdDtcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbFkgPSBtb3ZlRGF0YS5zY3JvbGxUb3AgLSBzdGFydFNjcm9sbFBvc2l0aW9uLnRvcDtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIC4uLm1vdmVEYXRhLFxuICAgICAgICAgICAgICB4OiBtb3ZlRGF0YS50cmFuc2Zvcm1YICsgc2Nyb2xsWCxcbiAgICAgICAgICAgICAgeTogbW92ZURhdGEudHJhbnNmb3JtWSArIHNjcm9sbFlcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfSksXG4gICAgICAgICAgZmlsdGVyKFxuICAgICAgICAgICAgKHsgeCwgeSwgdHJhbnNmb3JtWCwgdHJhbnNmb3JtWSB9KSA9PlxuICAgICAgICAgICAgICAhdGhpcy52YWxpZGF0ZURyYWcgfHxcbiAgICAgICAgICAgICAgdGhpcy52YWxpZGF0ZURyYWcoe1xuICAgICAgICAgICAgICAgIHgsXG4gICAgICAgICAgICAgICAgeSxcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHsgeDogdHJhbnNmb3JtWCwgeTogdHJhbnNmb3JtWSB9XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgKSxcbiAgICAgICAgICB0YWtlVW50aWwoZHJhZ0NvbXBsZXRlJCksXG4gICAgICAgICAgc2hhcmUoKVxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IGRyYWdTdGFydGVkJCA9IHBvaW50ZXJNb3ZlLnBpcGUoXG4gICAgICAgICAgdGFrZSgxKSxcbiAgICAgICAgICBzaGFyZSgpXG4gICAgICAgICk7XG4gICAgICAgIGNvbnN0IGRyYWdFbmRlZCQgPSBwb2ludGVyTW92ZS5waXBlKFxuICAgICAgICAgIHRha2VMYXN0KDEpLFxuICAgICAgICAgIHNoYXJlKClcbiAgICAgICAgKTtcblxuICAgICAgICBkcmFnU3RhcnRlZCQuc3Vic2NyaWJlKCh7IGNsaWVudFgsIGNsaWVudFksIHgsIHkgfSkgPT4ge1xuICAgICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5kcmFnU3RhcnQubmV4dCh7IGNhbmNlbERyYWckIH0pO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgdGhpcy5zY3JvbGxlciA9IGF1dG9TY3JvbGwoXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyXG4gICAgICAgICAgICAgICAgPyB0aGlzLnNjcm9sbENvbnRhaW5lci5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRcbiAgICAgICAgICAgICAgICA6IHRoaXMuZG9jdW1lbnQuZGVmYXVsdFZpZXdcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIG1hcmdpbjogMjAsXG4gICAgICAgICAgICAgIGF1dG9TY3JvbGwoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICApO1xuXG4gICAgICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgICAgdGhpcy5kcmFnQWN0aXZlQ2xhc3NcbiAgICAgICAgICApO1xuXG4gICAgICAgICAgaWYgKHRoaXMuZ2hvc3REcmFnRW5hYmxlZCkge1xuICAgICAgICAgICAgY29uc3QgcmVjdCA9IHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgICAgICAgY29uc3QgY2xvbmUgPSB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5jbG9uZU5vZGUoXG4gICAgICAgICAgICAgIHRydWVcbiAgICAgICAgICAgICkgYXMgSFRNTEVsZW1lbnQ7XG4gICAgICAgICAgICBpZiAoIXRoaXMuc2hvd09yaWdpbmFsRWxlbWVudFdoaWxlRHJhZ2dpbmcpIHtcbiAgICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudCxcbiAgICAgICAgICAgICAgICAndmlzaWJpbGl0eScsXG4gICAgICAgICAgICAgICAgJ2hpZGRlbidcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuZ2hvc3RFbGVtZW50QXBwZW5kVG8pIHtcbiAgICAgICAgICAgICAgdGhpcy5naG9zdEVsZW1lbnRBcHBlbmRUby5hcHBlbmRDaGlsZChjbG9uZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlIS5pbnNlcnRCZWZvcmUoXG4gICAgICAgICAgICAgICAgY2xvbmUsXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQubmV4dFNpYmxpbmdcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5naG9zdEVsZW1lbnQgPSBjbG9uZTtcblxuICAgICAgICAgICAgdGhpcy5zZXRFbGVtZW50U3R5bGVzKGNsb25lLCB7XG4gICAgICAgICAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgICAgICAgICB0b3A6IGAke3JlY3QudG9wfXB4YCxcbiAgICAgICAgICAgICAgbGVmdDogYCR7cmVjdC5sZWZ0fXB4YCxcbiAgICAgICAgICAgICAgd2lkdGg6IGAke3JlY3Qud2lkdGh9cHhgLFxuICAgICAgICAgICAgICBoZWlnaHQ6IGAke3JlY3QuaGVpZ2h0fXB4YCxcbiAgICAgICAgICAgICAgY3Vyc29yOiB0aGlzLmRyYWdDdXJzb3IsXG4gICAgICAgICAgICAgIG1hcmdpbjogJzAnXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuZ2hvc3RFbGVtZW50VGVtcGxhdGUpIHtcbiAgICAgICAgICAgICAgY29uc3Qgdmlld1JlZiA9IHRoaXMudmNyLmNyZWF0ZUVtYmVkZGVkVmlldyhcbiAgICAgICAgICAgICAgICB0aGlzLmdob3N0RWxlbWVudFRlbXBsYXRlXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIGNsb25lLmlubmVySFRNTCA9ICcnO1xuICAgICAgICAgICAgICB2aWV3UmVmLnJvb3ROb2Rlc1xuICAgICAgICAgICAgICAgIC5maWx0ZXIobm9kZSA9PiBub2RlIGluc3RhbmNlb2YgTm9kZSlcbiAgICAgICAgICAgICAgICAuZm9yRWFjaChub2RlID0+IHtcbiAgICAgICAgICAgICAgICAgIGNsb25lLmFwcGVuZENoaWxkKG5vZGUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICBkcmFnRW5kZWQkLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy52Y3IucmVtb3ZlKHRoaXMudmNyLmluZGV4T2Yodmlld1JlZikpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy56b25lLnJ1bigoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2hvc3RFbGVtZW50Q3JlYXRlZC5lbWl0KHtcbiAgICAgICAgICAgICAgICBjbGllbnRYOiBjbGllbnRYIC0geCxcbiAgICAgICAgICAgICAgICBjbGllbnRZOiBjbGllbnRZIC0geSxcbiAgICAgICAgICAgICAgICBlbGVtZW50OiBjbG9uZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBkcmFnRW5kZWQkLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICAgIGNsb25lLnBhcmVudEVsZW1lbnQhLnJlbW92ZUNoaWxkKGNsb25lKTtcbiAgICAgICAgICAgICAgdGhpcy5naG9zdEVsZW1lbnQgPSBudWxsO1xuICAgICAgICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKFxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgICAgICAgICd2aXNpYmlsaXR5JyxcbiAgICAgICAgICAgICAgICAnJ1xuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdGhpcy5kcmFnZ2FibGVIZWxwZXIuY3VycmVudERyYWcubmV4dChjdXJyZW50RHJhZyQpO1xuICAgICAgICB9KTtcblxuICAgICAgICBkcmFnRW5kZWQkXG4gICAgICAgICAgLnBpcGUoXG4gICAgICAgICAgICBtZXJnZU1hcChkcmFnRW5kRGF0YSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IGRyYWdFbmREYXRhJCA9IGNhbmNlbERyYWckLnBpcGUoXG4gICAgICAgICAgICAgICAgY291bnQoKSxcbiAgICAgICAgICAgICAgICB0YWtlKDEpLFxuICAgICAgICAgICAgICAgIG1hcChjYWxsZWRDb3VudCA9PiAoe1xuICAgICAgICAgICAgICAgICAgLi4uZHJhZ0VuZERhdGEsXG4gICAgICAgICAgICAgICAgICBkcmFnQ2FuY2VsbGVkOiBjYWxsZWRDb3VudCA+IDBcbiAgICAgICAgICAgICAgICB9KSlcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgY2FuY2VsRHJhZyQuY29tcGxldGUoKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGRyYWdFbmREYXRhJDtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgKVxuICAgICAgICAgIC5zdWJzY3JpYmUoKHsgeCwgeSwgZHJhZ0NhbmNlbGxlZCB9KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNjcm9sbGVyLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmRyYWdFbmQubmV4dCh7IHgsIHksIGRyYWdDYW5jZWxsZWQgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3MoXG4gICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgICAgICB0aGlzLmRyYWdBY3RpdmVDbGFzc1xuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGN1cnJlbnREcmFnJC5jb21wbGV0ZSgpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgIG1lcmdlKGRyYWdDb21wbGV0ZSQsIGRyYWdFbmRlZCQpXG4gICAgICAgICAgLnBpcGUodGFrZSgxKSlcbiAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnQuaGVhZC5yZW1vdmVDaGlsZChnbG9iYWxEcmFnU3R5bGUpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBwb2ludGVyTW92ZTtcbiAgICAgIH0pLFxuICAgICAgc2hhcmUoKVxuICAgICk7XG5cbiAgICBtZXJnZShcbiAgICAgIHBvaW50ZXJEcmFnZ2VkJC5waXBlKFxuICAgICAgICB0YWtlKDEpLFxuICAgICAgICBtYXAodmFsdWUgPT4gWywgdmFsdWVdKVxuICAgICAgKSxcbiAgICAgIHBvaW50ZXJEcmFnZ2VkJC5waXBlKHBhaXJ3aXNlKCkpXG4gICAgKVxuICAgICAgLnBpcGUoXG4gICAgICAgIGZpbHRlcigoW3ByZXZpb3VzLCBuZXh0XSkgPT4ge1xuICAgICAgICAgIGlmICghcHJldmlvdXMpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gcHJldmlvdXMueCAhPT0gbmV4dC54IHx8IHByZXZpb3VzLnkgIT09IG5leHQueTtcbiAgICAgICAgfSksXG4gICAgICAgIG1hcCgoW3ByZXZpb3VzLCBuZXh0XSkgPT4gbmV4dClcbiAgICAgIClcbiAgICAgIC5zdWJzY3JpYmUoXG4gICAgICAgICh7IHgsIHksIGN1cnJlbnREcmFnJCwgY2xpZW50WCwgY2xpZW50WSwgdHJhbnNmb3JtWCwgdHJhbnNmb3JtWSB9KSA9PiB7XG4gICAgICAgICAgdGhpcy56b25lLnJ1bigoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmRyYWdnaW5nLm5leHQoeyB4LCB5IH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIGlmICh0aGlzLmdob3N0RWxlbWVudCkge1xuICAgICAgICAgICAgY29uc3QgdHJhbnNmb3JtID0gYHRyYW5zbGF0ZSgke3RyYW5zZm9ybVh9cHgsICR7dHJhbnNmb3JtWX1weClgO1xuICAgICAgICAgICAgdGhpcy5zZXRFbGVtZW50U3R5bGVzKHRoaXMuZ2hvc3RFbGVtZW50LCB7XG4gICAgICAgICAgICAgIHRyYW5zZm9ybSxcbiAgICAgICAgICAgICAgJy13ZWJraXQtdHJhbnNmb3JtJzogdHJhbnNmb3JtLFxuICAgICAgICAgICAgICAnLW1zLXRyYW5zZm9ybSc6IHRyYW5zZm9ybSxcbiAgICAgICAgICAgICAgJy1tb3otdHJhbnNmb3JtJzogdHJhbnNmb3JtLFxuICAgICAgICAgICAgICAnLW8tdHJhbnNmb3JtJzogdHJhbnNmb3JtXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgY3VycmVudERyYWckLm5leHQoe1xuICAgICAgICAgICAgY2xpZW50WCxcbiAgICAgICAgICAgIGNsaWVudFksXG4gICAgICAgICAgICBkcm9wRGF0YTogdGhpcy5kcm9wRGF0YVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICApO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGlmIChjaGFuZ2VzLmRyYWdBeGlzKSB7XG4gICAgICB0aGlzLmNoZWNrRXZlbnRMaXN0ZW5lcnMoKTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnVuc3Vic2NyaWJlRXZlbnRMaXN0ZW5lcnMoKTtcbiAgICB0aGlzLnBvaW50ZXJEb3duJC5jb21wbGV0ZSgpO1xuICAgIHRoaXMucG9pbnRlck1vdmUkLmNvbXBsZXRlKCk7XG4gICAgdGhpcy5wb2ludGVyVXAkLmNvbXBsZXRlKCk7XG4gICAgdGhpcy5kZXN0cm95JC5uZXh0KCk7XG4gIH1cblxuICBwcml2YXRlIGNoZWNrRXZlbnRMaXN0ZW5lcnMoKTogdm9pZCB7XG4gICAgY29uc3QgY2FuRHJhZzogYm9vbGVhbiA9IHRoaXMuY2FuRHJhZygpO1xuICAgIGNvbnN0IGhhc0V2ZW50TGlzdGVuZXJzOiBib29sZWFuID1cbiAgICAgIE9iamVjdC5rZXlzKHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMpLmxlbmd0aCA+IDA7XG5cbiAgICBpZiAoY2FuRHJhZyAmJiAhaGFzRXZlbnRMaXN0ZW5lcnMpIHtcbiAgICAgIHRoaXMuem9uZS5ydW5PdXRzaWRlQW5ndWxhcigoKSA9PiB7XG4gICAgICAgIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMubW91c2Vkb3duID0gdGhpcy5yZW5kZXJlci5saXN0ZW4oXG4gICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICAgJ21vdXNlZG93bicsXG4gICAgICAgICAgKGV2ZW50OiBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uTW91c2VEb3duKGV2ZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy5tb3VzZXVwID0gdGhpcy5yZW5kZXJlci5saXN0ZW4oXG4gICAgICAgICAgJ2RvY3VtZW50JyxcbiAgICAgICAgICAnbW91c2V1cCcsXG4gICAgICAgICAgKGV2ZW50OiBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uTW91c2VVcChldmVudCk7XG4gICAgICAgICAgfVxuICAgICAgICApO1xuXG4gICAgICAgIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMudG91Y2hzdGFydCA9IHRoaXMucmVuZGVyZXIubGlzdGVuKFxuICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgICd0b3VjaHN0YXJ0JyxcbiAgICAgICAgICAoZXZlbnQ6IFRvdWNoRXZlbnQpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25Ub3VjaFN0YXJ0KGV2ZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy50b3VjaGVuZCA9IHRoaXMucmVuZGVyZXIubGlzdGVuKFxuICAgICAgICAgICdkb2N1bWVudCcsXG4gICAgICAgICAgJ3RvdWNoZW5kJyxcbiAgICAgICAgICAoZXZlbnQ6IFRvdWNoRXZlbnQpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25Ub3VjaEVuZChldmVudCk7XG4gICAgICAgICAgfVxuICAgICAgICApO1xuXG4gICAgICAgIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMudG91Y2hjYW5jZWwgPSB0aGlzLnJlbmRlcmVyLmxpc3RlbihcbiAgICAgICAgICAnZG9jdW1lbnQnLFxuICAgICAgICAgICd0b3VjaGNhbmNlbCcsXG4gICAgICAgICAgKGV2ZW50OiBUb3VjaEV2ZW50KSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uVG91Y2hFbmQoZXZlbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLmV2ZW50TGlzdGVuZXJTdWJzY3JpcHRpb25zLm1vdXNlZW50ZXIgPSB0aGlzLnJlbmRlcmVyLmxpc3RlbihcbiAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudCxcbiAgICAgICAgICAnbW91c2VlbnRlcicsXG4gICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vbk1vdXNlRW50ZXIoKTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy5tb3VzZWxlYXZlID0gdGhpcy5yZW5kZXJlci5saXN0ZW4oXG4gICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICAgJ21vdXNlbGVhdmUnLFxuICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25Nb3VzZUxlYXZlKCk7XG4gICAgICAgICAgfVxuICAgICAgICApO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmICghY2FuRHJhZyAmJiBoYXNFdmVudExpc3RlbmVycykge1xuICAgICAgdGhpcy51bnN1YnNjcmliZUV2ZW50TGlzdGVuZXJzKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBvbk1vdXNlRG93bihldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy5tb3VzZW1vdmUpIHtcbiAgICAgIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMubW91c2Vtb3ZlID0gdGhpcy5yZW5kZXJlci5saXN0ZW4oXG4gICAgICAgICdkb2N1bWVudCcsXG4gICAgICAgICdtb3VzZW1vdmUnLFxuICAgICAgICAobW91c2VNb3ZlRXZlbnQ6IE1vdXNlRXZlbnQpID0+IHtcbiAgICAgICAgICB0aGlzLnBvaW50ZXJNb3ZlJC5uZXh0KHtcbiAgICAgICAgICAgIGV2ZW50OiBtb3VzZU1vdmVFdmVudCxcbiAgICAgICAgICAgIGNsaWVudFg6IG1vdXNlTW92ZUV2ZW50LmNsaWVudFgsXG4gICAgICAgICAgICBjbGllbnRZOiBtb3VzZU1vdmVFdmVudC5jbGllbnRZXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICk7XG4gICAgfVxuICAgIHRoaXMucG9pbnRlckRvd24kLm5leHQoe1xuICAgICAgZXZlbnQsXG4gICAgICBjbGllbnRYOiBldmVudC5jbGllbnRYLFxuICAgICAgY2xpZW50WTogZXZlbnQuY2xpZW50WVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBvbk1vdXNlVXAoZXZlbnQ6IE1vdXNlRXZlbnQpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy5tb3VzZW1vdmUpIHtcbiAgICAgIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMubW91c2Vtb3ZlKCk7XG4gICAgICBkZWxldGUgdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy5tb3VzZW1vdmU7XG4gICAgfVxuICAgIHRoaXMucG9pbnRlclVwJC5uZXh0KHtcbiAgICAgIGV2ZW50LFxuICAgICAgY2xpZW50WDogZXZlbnQuY2xpZW50WCxcbiAgICAgIGNsaWVudFk6IGV2ZW50LmNsaWVudFlcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgb25Ub3VjaFN0YXJ0KGV2ZW50OiBUb3VjaEV2ZW50KTogdm9pZCB7XG4gICAgaWYgKCF0aGlzLnNjcm9sbENvbnRhaW5lcikge1xuICAgICAgdHJ5IHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHt9XG4gICAgfVxuICAgIGxldCBoYXNDb250YWluZXJTY3JvbGxiYXI6IGJvb2xlYW47XG4gICAgbGV0IHN0YXJ0U2Nyb2xsUG9zaXRpb246IGFueTtcbiAgICBsZXQgaXNEcmFnQWN0aXZhdGVkOiBib29sZWFuO1xuICAgIGlmICh0aGlzLnNjcm9sbENvbnRhaW5lciAmJiB0aGlzLnNjcm9sbENvbnRhaW5lci5hY3RpdmVMb25nUHJlc3NEcmFnKSB7XG4gICAgICB0aGlzLnRpbWVMb25nUHJlc3MudGltZXJCZWdpbiA9IERhdGUubm93KCk7XG4gICAgICBpc0RyYWdBY3RpdmF0ZWQgPSBmYWxzZTtcbiAgICAgIGhhc0NvbnRhaW5lclNjcm9sbGJhciA9IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmhhc1Njcm9sbGJhcigpO1xuICAgICAgc3RhcnRTY3JvbGxQb3NpdGlvbiA9IHRoaXMuZ2V0U2Nyb2xsUG9zaXRpb24oKTtcbiAgICB9XG4gICAgaWYgKCF0aGlzLmV2ZW50TGlzdGVuZXJTdWJzY3JpcHRpb25zLnRvdWNobW92ZSkge1xuICAgICAgdGhpcy5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9ucy50b3VjaG1vdmUgPSB0aGlzLnJlbmRlcmVyLmxpc3RlbihcbiAgICAgICAgJ2RvY3VtZW50JyxcbiAgICAgICAgJ3RvdWNobW92ZScsXG4gICAgICAgICh0b3VjaE1vdmVFdmVudDogVG91Y2hFdmVudCkgPT4ge1xuICAgICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyICYmXG4gICAgICAgICAgICB0aGlzLnNjcm9sbENvbnRhaW5lci5hY3RpdmVMb25nUHJlc3NEcmFnICYmXG4gICAgICAgICAgICAhaXNEcmFnQWN0aXZhdGVkICYmXG4gICAgICAgICAgICBoYXNDb250YWluZXJTY3JvbGxiYXJcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIGlzRHJhZ0FjdGl2YXRlZCA9IHRoaXMuc2hvdWxkQmVnaW5EcmFnKFxuICAgICAgICAgICAgICBldmVudCxcbiAgICAgICAgICAgICAgdG91Y2hNb3ZlRXZlbnQsXG4gICAgICAgICAgICAgIHN0YXJ0U2Nyb2xsUG9zaXRpb25cbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICF0aGlzLnNjcm9sbENvbnRhaW5lciB8fFxuICAgICAgICAgICAgIXRoaXMuc2Nyb2xsQ29udGFpbmVyLmFjdGl2ZUxvbmdQcmVzc0RyYWcgfHxcbiAgICAgICAgICAgICFoYXNDb250YWluZXJTY3JvbGxiYXIgfHxcbiAgICAgICAgICAgIGlzRHJhZ0FjdGl2YXRlZFxuICAgICAgICAgICkge1xuICAgICAgICAgICAgdGhpcy5wb2ludGVyTW92ZSQubmV4dCh7XG4gICAgICAgICAgICAgIGV2ZW50OiB0b3VjaE1vdmVFdmVudCxcbiAgICAgICAgICAgICAgY2xpZW50WDogdG91Y2hNb3ZlRXZlbnQudGFyZ2V0VG91Y2hlc1swXS5jbGllbnRYLFxuICAgICAgICAgICAgICBjbGllbnRZOiB0b3VjaE1vdmVFdmVudC50YXJnZXRUb3VjaGVzWzBdLmNsaWVudFlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgKTtcbiAgICB9XG4gICAgdGhpcy5wb2ludGVyRG93biQubmV4dCh7XG4gICAgICBldmVudCxcbiAgICAgIGNsaWVudFg6IGV2ZW50LnRvdWNoZXNbMF0uY2xpZW50WCxcbiAgICAgIGNsaWVudFk6IGV2ZW50LnRvdWNoZXNbMF0uY2xpZW50WVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBvblRvdWNoRW5kKGV2ZW50OiBUb3VjaEV2ZW50KTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMudG91Y2htb3ZlKSB7XG4gICAgICB0aGlzLmV2ZW50TGlzdGVuZXJTdWJzY3JpcHRpb25zLnRvdWNobW92ZSgpO1xuICAgICAgZGVsZXRlIHRoaXMuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnMudG91Y2htb3ZlO1xuICAgICAgaWYgKHRoaXMuc2Nyb2xsQ29udGFpbmVyICYmIHRoaXMuc2Nyb2xsQ29udGFpbmVyLmFjdGl2ZUxvbmdQcmVzc0RyYWcpIHtcbiAgICAgICAgdGhpcy5zY3JvbGxDb250YWluZXIuZW5hYmxlU2Nyb2xsKCk7XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMucG9pbnRlclVwJC5uZXh0KHtcbiAgICAgIGV2ZW50LFxuICAgICAgY2xpZW50WDogZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0uY2xpZW50WCxcbiAgICAgIGNsaWVudFk6IGV2ZW50LmNoYW5nZWRUb3VjaGVzWzBdLmNsaWVudFlcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgb25Nb3VzZUVudGVyKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0Q3Vyc29yKHRoaXMuZHJhZ0N1cnNvcik7XG4gIH1cblxuICBwcml2YXRlIG9uTW91c2VMZWF2ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnNldEN1cnNvcignJyk7XG4gIH1cblxuICBwcml2YXRlIGNhbkRyYWcoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuZHJhZ0F4aXMueCB8fCB0aGlzLmRyYWdBeGlzLnk7XG4gIH1cblxuICBwcml2YXRlIHNldEN1cnNvcih2YWx1ZTogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudCwgJ2N1cnNvcicsIHZhbHVlKTtcbiAgfVxuXG4gIHByaXZhdGUgdW5zdWJzY3JpYmVFdmVudExpc3RlbmVycygpOiB2b2lkIHtcbiAgICBPYmplY3Qua2V5cyh0aGlzLmV2ZW50TGlzdGVuZXJTdWJzY3JpcHRpb25zKS5mb3JFYWNoKHR5cGUgPT4ge1xuICAgICAgKHRoaXMgYXMgYW55KS5ldmVudExpc3RlbmVyU3Vic2NyaXB0aW9uc1t0eXBlXSgpO1xuICAgICAgZGVsZXRlICh0aGlzIGFzIGFueSkuZXZlbnRMaXN0ZW5lclN1YnNjcmlwdGlvbnNbdHlwZV07XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHNldEVsZW1lbnRTdHlsZXMoXG4gICAgZWxlbWVudDogSFRNTEVsZW1lbnQsXG4gICAgc3R5bGVzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9XG4gICkge1xuICAgIE9iamVjdC5rZXlzKHN0eWxlcykuZm9yRWFjaChrZXkgPT4ge1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShlbGVtZW50LCBrZXksIHN0eWxlc1trZXldKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0U2Nyb2xsUG9zaXRpb24oKSB7XG4gICAgaWYgKHRoaXMuc2Nyb2xsQ29udGFpbmVyKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0b3A6IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5zY3JvbGxUb3AsXG4gICAgICAgIGxlZnQ6IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5zY3JvbGxMZWZ0XG4gICAgICB9O1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0b3A6IHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wLFxuICAgICAgICBsZWZ0OiB3aW5kb3cucGFnZVhPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnRcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzaG91bGRCZWdpbkRyYWcoXG4gICAgZXZlbnQ6IFRvdWNoRXZlbnQsXG4gICAgdG91Y2hNb3ZlRXZlbnQ6IFRvdWNoRXZlbnQsXG4gICAgc3RhcnRTY3JvbGxQb3NpdGlvbjogYW55XG4gICk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IG1vdmVTY3JvbGxQb3NpdGlvbiA9IHRoaXMuZ2V0U2Nyb2xsUG9zaXRpb24oKTtcbiAgICBjb25zdCBkZWx0YVNjcm9sbCA9IHtcbiAgICAgIHRvcDogTWF0aC5hYnMobW92ZVNjcm9sbFBvc2l0aW9uLnRvcCAtIHN0YXJ0U2Nyb2xsUG9zaXRpb24udG9wKSxcbiAgICAgIGxlZnQ6IE1hdGguYWJzKG1vdmVTY3JvbGxQb3NpdGlvbi5sZWZ0IC0gc3RhcnRTY3JvbGxQb3NpdGlvbi5sZWZ0KVxuICAgIH07XG4gICAgY29uc3QgZGVsdGFYID1cbiAgICAgIE1hdGguYWJzKFxuICAgICAgICB0b3VjaE1vdmVFdmVudC50YXJnZXRUb3VjaGVzWzBdLmNsaWVudFggLSBldmVudC50b3VjaGVzWzBdLmNsaWVudFhcbiAgICAgICkgLSBkZWx0YVNjcm9sbC5sZWZ0O1xuICAgIGNvbnN0IGRlbHRhWSA9XG4gICAgICBNYXRoLmFicyhcbiAgICAgICAgdG91Y2hNb3ZlRXZlbnQudGFyZ2V0VG91Y2hlc1swXS5jbGllbnRZIC0gZXZlbnQudG91Y2hlc1swXS5jbGllbnRZXG4gICAgICApIC0gZGVsdGFTY3JvbGwudG9wO1xuICAgIGNvbnN0IGRlbHRhVG90YWwgPSBkZWx0YVggKyBkZWx0YVk7XG4gICAgaWYgKFxuICAgICAgZGVsdGFUb3RhbCA+IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmxvbmdQcmVzc0NvbmZpZy5kZWx0YSB8fFxuICAgICAgZGVsdGFTY3JvbGwudG9wID4gMCB8fFxuICAgICAgZGVsdGFTY3JvbGwubGVmdCA+IDBcbiAgICApIHtcbiAgICAgIHRoaXMudGltZUxvbmdQcmVzcy50aW1lckJlZ2luID0gRGF0ZS5ub3coKTtcbiAgICB9XG4gICAgdGhpcy50aW1lTG9uZ1ByZXNzLnRpbWVyRW5kID0gRGF0ZS5ub3coKTtcbiAgICBjb25zdCBkdXJhdGlvbiA9XG4gICAgICB0aGlzLnRpbWVMb25nUHJlc3MudGltZXJFbmQgLSB0aGlzLnRpbWVMb25nUHJlc3MudGltZXJCZWdpbjtcbiAgICBpZiAoZHVyYXRpb24gPj0gdGhpcy5zY3JvbGxDb250YWluZXIubG9uZ1ByZXNzQ29uZmlnLmR1cmF0aW9uKSB7XG4gICAgICB0aGlzLnNjcm9sbENvbnRhaW5lci5kaXNhYmxlU2Nyb2xsKCk7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iLCJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIE9uSW5pdCxcbiAgRWxlbWVudFJlZixcbiAgT25EZXN0cm95LFxuICBPdXRwdXQsXG4gIEV2ZW50RW1pdHRlcixcbiAgTmdab25lLFxuICBJbnB1dCxcbiAgUmVuZGVyZXIyLFxuICBPcHRpb25hbFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZGlzdGluY3RVbnRpbENoYW5nZWQsIHBhaXJ3aXNlLCBmaWx0ZXIsIG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IERyYWdnYWJsZUhlbHBlciB9IGZyb20gJy4vZHJhZ2dhYmxlLWhlbHBlci5wcm92aWRlcic7XG5pbXBvcnQgeyBEcmFnZ2FibGVTY3JvbGxDb250YWluZXJEaXJlY3RpdmUgfSBmcm9tICcuL2RyYWdnYWJsZS1zY3JvbGwtY29udGFpbmVyLmRpcmVjdGl2ZSc7XG5cbmZ1bmN0aW9uIGlzQ29vcmRpbmF0ZVdpdGhpblJlY3RhbmdsZShcbiAgY2xpZW50WDogbnVtYmVyLFxuICBjbGllbnRZOiBudW1iZXIsXG4gIHJlY3Q6IENsaWVudFJlY3Rcbik6IGJvb2xlYW4ge1xuICByZXR1cm4gKFxuICAgIGNsaWVudFggPj0gcmVjdC5sZWZ0ICYmXG4gICAgY2xpZW50WCA8PSByZWN0LnJpZ2h0ICYmXG4gICAgY2xpZW50WSA+PSByZWN0LnRvcCAmJlxuICAgIGNsaWVudFkgPD0gcmVjdC5ib3R0b21cbiAgKTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBEcm9wRXZlbnQ8VCA9IGFueT4ge1xuICBkcm9wRGF0YTogVDtcbn1cblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW213bERyb3BwYWJsZV0nXG59KVxuZXhwb3J0IGNsYXNzIERyb3BwYWJsZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgLyoqXG4gICAqIEFkZGVkIHRvIHRoZSBlbGVtZW50IHdoZW4gYW4gZWxlbWVudCBpcyBkcmFnZ2VkIG92ZXIgaXRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGRyYWdPdmVyQ2xhc3M6IHN0cmluZztcblxuICAvKipcbiAgICogQWRkZWQgdG8gdGhlIGVsZW1lbnQgYW55IHRpbWUgYSBkcmFnZ2FibGUgZWxlbWVudCBpcyBiZWluZyBkcmFnZ2VkXG4gICAqL1xuICBASW5wdXQoKVxuICBkcmFnQWN0aXZlQ2xhc3M6IHN0cmluZztcblxuICAvKipcbiAgICogQ2FsbGVkIHdoZW4gYSBkcmFnZ2FibGUgZWxlbWVudCBzdGFydHMgb3ZlcmxhcHBpbmcgdGhlIGVsZW1lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBkcmFnRW50ZXIgPSBuZXcgRXZlbnRFbWl0dGVyPERyb3BFdmVudD4oKTtcblxuICAvKipcbiAgICogQ2FsbGVkIHdoZW4gYSBkcmFnZ2FibGUgZWxlbWVudCBzdG9wcyBvdmVybGFwcGluZyB0aGUgZWxlbWVudFxuICAgKi9cbiAgQE91dHB1dCgpXG4gIGRyYWdMZWF2ZSA9IG5ldyBFdmVudEVtaXR0ZXI8RHJvcEV2ZW50PigpO1xuXG4gIC8qKlxuICAgKiBDYWxsZWQgd2hlbiBhIGRyYWdnYWJsZSBlbGVtZW50IGlzIG1vdmVkIG92ZXIgdGhlIGVsZW1lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBkcmFnT3ZlciA9IG5ldyBFdmVudEVtaXR0ZXI8RHJvcEV2ZW50PigpO1xuXG4gIC8qKlxuICAgKiBDYWxsZWQgd2hlbiBhIGRyYWdnYWJsZSBlbGVtZW50IGlzIGRyb3BwZWQgb24gdGhpcyBlbGVtZW50XG4gICAqL1xuICBAT3V0cHV0KClcbiAgZHJvcCA9IG5ldyBFdmVudEVtaXR0ZXI8RHJvcEV2ZW50PigpOyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lIG5vLW91dHB1dC1uYW1lZC1hZnRlci1zdGFuZGFyZC1ldmVudFxuXG4gIGN1cnJlbnREcmFnU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbGVtZW50OiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcbiAgICBwcml2YXRlIGRyYWdnYWJsZUhlbHBlcjogRHJhZ2dhYmxlSGVscGVyLFxuICAgIHByaXZhdGUgem9uZTogTmdab25lLFxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBAT3B0aW9uYWwoKSBwcml2YXRlIHNjcm9sbENvbnRhaW5lcjogRHJhZ2dhYmxlU2Nyb2xsQ29udGFpbmVyRGlyZWN0aXZlXG4gICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmN1cnJlbnREcmFnU3Vic2NyaXB0aW9uID0gdGhpcy5kcmFnZ2FibGVIZWxwZXIuY3VycmVudERyYWcuc3Vic2NyaWJlKFxuICAgICAgZHJhZyQgPT4ge1xuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKFxuICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgIHRoaXMuZHJhZ0FjdGl2ZUNsYXNzXG4gICAgICAgICk7XG4gICAgICAgIGNvbnN0IGRyb3BwYWJsZUVsZW1lbnQ6IHtcbiAgICAgICAgICByZWN0PzogQ2xpZW50UmVjdDtcbiAgICAgICAgICB1cGRhdGVDYWNoZTogYm9vbGVhbjtcbiAgICAgICAgICBzY3JvbGxDb250YWluZXJSZWN0PzogQ2xpZW50UmVjdDtcbiAgICAgICAgfSA9IHtcbiAgICAgICAgICB1cGRhdGVDYWNoZTogdHJ1ZVxuICAgICAgICB9O1xuXG4gICAgICAgIGNvbnN0IGRlcmVnaXN0ZXJTY3JvbGxMaXN0ZW5lciA9IHRoaXMucmVuZGVyZXIubGlzdGVuKFxuICAgICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyXG4gICAgICAgICAgICA/IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudFxuICAgICAgICAgICAgOiAnd2luZG93JyxcbiAgICAgICAgICAnc2Nyb2xsJyxcbiAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICBkcm9wcGFibGVFbGVtZW50LnVwZGF0ZUNhY2hlID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgbGV0IGN1cnJlbnREcmFnRHJvcERhdGE6IGFueTtcbiAgICAgICAgY29uc3Qgb3ZlcmxhcHMkID0gZHJhZyQucGlwZShcbiAgICAgICAgICBtYXAoKHsgY2xpZW50WCwgY2xpZW50WSwgZHJvcERhdGEgfSkgPT4ge1xuICAgICAgICAgICAgY3VycmVudERyYWdEcm9wRGF0YSA9IGRyb3BEYXRhO1xuICAgICAgICAgICAgaWYgKGRyb3BwYWJsZUVsZW1lbnQudXBkYXRlQ2FjaGUpIHtcbiAgICAgICAgICAgICAgZHJvcHBhYmxlRWxlbWVudC5yZWN0ID0gdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgICAgICAgIGlmICh0aGlzLnNjcm9sbENvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgIGRyb3BwYWJsZUVsZW1lbnQuc2Nyb2xsQ29udGFpbmVyUmVjdCA9IHRoaXMuc2Nyb2xsQ29udGFpbmVyLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBkcm9wcGFibGVFbGVtZW50LnVwZGF0ZUNhY2hlID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBpc1dpdGhpbkVsZW1lbnQgPSBpc0Nvb3JkaW5hdGVXaXRoaW5SZWN0YW5nbGUoXG4gICAgICAgICAgICAgIGNsaWVudFgsXG4gICAgICAgICAgICAgIGNsaWVudFksXG4gICAgICAgICAgICAgIGRyb3BwYWJsZUVsZW1lbnQucmVjdCBhcyBDbGllbnRSZWN0XG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKGRyb3BwYWJsZUVsZW1lbnQuc2Nyb2xsQ29udGFpbmVyUmVjdCkge1xuICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIGlzV2l0aGluRWxlbWVudCAmJlxuICAgICAgICAgICAgICAgIGlzQ29vcmRpbmF0ZVdpdGhpblJlY3RhbmdsZShcbiAgICAgICAgICAgICAgICAgIGNsaWVudFgsXG4gICAgICAgICAgICAgICAgICBjbGllbnRZLFxuICAgICAgICAgICAgICAgICAgZHJvcHBhYmxlRWxlbWVudC5zY3JvbGxDb250YWluZXJSZWN0IGFzIENsaWVudFJlY3RcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gaXNXaXRoaW5FbGVtZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3Qgb3ZlcmxhcHNDaGFuZ2VkJCA9IG92ZXJsYXBzJC5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpO1xuXG4gICAgICAgIGxldCBkcmFnT3ZlckFjdGl2ZTogYm9vbGVhbjsgLy8gVE9ETyAtIHNlZSBpZiB0aGVyZSdzIGEgd2F5IG9mIGRvaW5nIHRoaXMgdmlhIHJ4anNcblxuICAgICAgICBvdmVybGFwc0NoYW5nZWQkXG4gICAgICAgICAgLnBpcGUoZmlsdGVyKG92ZXJsYXBzTm93ID0+IG92ZXJsYXBzTm93KSlcbiAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgIGRyYWdPdmVyQWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3MoXG4gICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LFxuICAgICAgICAgICAgICB0aGlzLmRyYWdPdmVyQ2xhc3NcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICB0aGlzLnpvbmUucnVuKCgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5kcmFnRW50ZXIubmV4dCh7XG4gICAgICAgICAgICAgICAgZHJvcERhdGE6IGN1cnJlbnREcmFnRHJvcERhdGFcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9KTtcblxuICAgICAgICBvdmVybGFwcyQucGlwZShmaWx0ZXIob3ZlcmxhcHNOb3cgPT4gb3ZlcmxhcHNOb3cpKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5kcmFnT3Zlci5uZXh0KHtcbiAgICAgICAgICAgICAgZHJvcERhdGE6IGN1cnJlbnREcmFnRHJvcERhdGFcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBvdmVybGFwc0NoYW5nZWQkXG4gICAgICAgICAgLnBpcGUoXG4gICAgICAgICAgICBwYWlyd2lzZSgpLFxuICAgICAgICAgICAgZmlsdGVyKChbZGlkT3ZlcmxhcCwgb3ZlcmxhcHNOb3ddKSA9PiBkaWRPdmVybGFwICYmICFvdmVybGFwc05vdylcbiAgICAgICAgICApXG4gICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICBkcmFnT3ZlckFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhcbiAgICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICAgICAgIHRoaXMuZHJhZ092ZXJDbGFzc1xuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmRyYWdMZWF2ZS5uZXh0KHtcbiAgICAgICAgICAgICAgICBkcm9wRGF0YTogY3VycmVudERyYWdEcm9wRGF0YVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgIGRyYWckLnN1YnNjcmliZSh7XG4gICAgICAgICAgY29tcGxldGU6ICgpID0+IHtcbiAgICAgICAgICAgIGRlcmVnaXN0ZXJTY3JvbGxMaXN0ZW5lcigpO1xuICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhcbiAgICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICAgICAgIHRoaXMuZHJhZ0FjdGl2ZUNsYXNzXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKGRyYWdPdmVyQWN0aXZlKSB7XG4gICAgICAgICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3MoXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICAgICAgICAgdGhpcy5kcmFnT3ZlckNsYXNzXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZHJvcC5uZXh0KHtcbiAgICAgICAgICAgICAgICAgIGRyb3BEYXRhOiBjdXJyZW50RHJhZ0Ryb3BEYXRhXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIGlmICh0aGlzLmN1cnJlbnREcmFnU3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aGlzLmN1cnJlbnREcmFnU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRHJhZ2dhYmxlRGlyZWN0aXZlIH0gZnJvbSAnLi9kcmFnZ2FibGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IERyb3BwYWJsZURpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcHBhYmxlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBEcmFnZ2FibGVTY3JvbGxDb250YWluZXJEaXJlY3RpdmUgfSBmcm9tICcuL2RyYWdnYWJsZS1zY3JvbGwtY29udGFpbmVyLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIERyYWdnYWJsZURpcmVjdGl2ZSxcbiAgICBEcm9wcGFibGVEaXJlY3RpdmUsXG4gICAgRHJhZ2dhYmxlU2Nyb2xsQ29udGFpbmVyRGlyZWN0aXZlXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBEcmFnZ2FibGVEaXJlY3RpdmUsXG4gICAgRHJvcHBhYmxlRGlyZWN0aXZlLFxuICAgIERyYWdnYWJsZVNjcm9sbENvbnRhaW5lckRpcmVjdGl2ZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERyYWdBbmREcm9wTW9kdWxlIHt9XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOzsyQkFhZ0IsSUFBSSxPQUFPLEVBQTRCOzs7O1lBSnRELFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7Ozs7QUNYRDs7Ozs7O0lBNkJFLFlBQ1MsWUFDQyxVQUNBO1FBRkQsZUFBVSxHQUFWLFVBQVU7UUFDVCxhQUFRLEdBQVIsUUFBUTtRQUNSLFNBQUksR0FBSixJQUFJOzs7O21DQWZpQixLQUFLOzs7Ozs7K0JBUWxCLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFOytCQUVwQixLQUFLO0tBTTNCOzs7O0lBRUosUUFBUTtRQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7WUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUM3QixXQUFXLEVBQ1gsQ0FBQyxLQUFpQjtnQkFDaEIsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7b0JBQzVDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDeEI7YUFDRixDQUNGLENBQUM7U0FDSCxDQUFDLENBQUM7S0FDSjs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7S0FDN0U7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQzNFOzs7O0lBRUQsWUFBWTs7UUFDVixNQUFNLDRCQUE0QixHQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxXQUFXO1lBQ3ZDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVc7WUFDM0MsQ0FBQyxDQUFDOztRQUNKLE1BQU0sMEJBQTBCLEdBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVk7WUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsWUFBWTtZQUM1QyxDQUFDLENBQUM7UUFDSixPQUFPLDRCQUE0QixJQUFJLDBCQUEwQixDQUFDO0tBQ25FOzs7WUE1REYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwrQkFBK0I7YUFDMUM7Ozs7WUFUQyxVQUFVO1lBSVYsU0FBUztZQUZULE1BQU07OztrQ0FZTCxLQUFLOzhCQVFMLEtBQUs7Ozs7Ozs7QUN4QlI7Ozs7Ozs7Ozs7O0lBOE5FLFlBQ1UsU0FDQSxVQUNBLGlCQUNBLE1BQ0EsS0FDWSxlQUFrRCxFQUM1QyxRQUFhO1FBTi9CLFlBQU8sR0FBUCxPQUFPO1FBQ1AsYUFBUSxHQUFSLFFBQVE7UUFDUixvQkFBZSxHQUFmLGVBQWU7UUFDZixTQUFJLEdBQUosSUFBSTtRQUNKLFFBQUcsR0FBSCxHQUFHO1FBQ1Msb0JBQWUsR0FBZixlQUFlLENBQW1DO1FBQzVDLGFBQVEsR0FBUixRQUFRLENBQUs7Ozs7d0JBL0hwQixFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRTs7Ozs0QkFNaEIsRUFBRTs7OztnQ0FNQyxJQUFJOzs7O2dEQU1ZLEtBQUs7Ozs7MEJBWTVCLEVBQUU7Ozs7K0JBd0JMLElBQUksWUFBWSxFQUF3Qjs7Ozs7O3lCQVE5QyxJQUFJLFlBQVksRUFBa0I7Ozs7bUNBTXhCLElBQUksWUFBWSxFQUE0Qjs7Ozt3QkFNdkQsSUFBSSxZQUFZLEVBQWlCOzs7O3VCQU1sQyxJQUFJLFlBQVksRUFBZ0I7Ozs7NEJBSzNCLElBQUksT0FBTyxFQUFnQjs7Ozs0QkFLM0IsSUFBSSxPQUFPLEVBQWdCOzs7OzBCQUs3QixJQUFJLE9BQU8sRUFBZ0I7MENBWXBDLEVBQUU7d0JBSWEsSUFBSSxPQUFPLEVBQUU7NkJBRU8sRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUU7S0FlakU7Ozs7SUFFSixRQUFRO1FBQ04sSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7O1FBRTNCLE1BQU0sZUFBZSxHQUFvQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDN0QsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQzVCLFFBQVEsQ0FBQyxDQUFDLGdCQUE4Qjs7O1lBR3RDLElBQUksZ0JBQWdCLENBQUMsS0FBSyxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ25FLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUMxQzs7WUFHRCxNQUFNLGVBQWUsR0FBcUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQ25FLE9BQU8sQ0FDUixDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FDdkIsZUFBZSxFQUNmLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDOzs7Ozs7O1NBTzFCLENBQUMsQ0FDRCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDOztZQUVoRCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDOztZQUVyRCxNQUFNLHNCQUFzQixHQUFHLElBQUksVUFBVSxDQUFDLFFBQVE7O2dCQUNwRCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZTtzQkFDeEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsYUFBYTtzQkFDN0MsUUFBUSxDQUFDO2dCQUNiLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxDQUFDLElBQ3RELFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQ2pCLENBQUM7YUFDSCxDQUFDLENBQUMsSUFBSSxDQUNMLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUM5QixHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUNwQyxDQUFDOztZQUVGLE1BQU0sWUFBWSxHQUFHLElBQUksT0FBTyxFQUFtQixDQUFDOztZQUNwRCxNQUFNLFdBQVcsR0FBRyxJQUFJLGFBQWEsRUFBUSxDQUFDO1lBRTlDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO2dCQUNaLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMzQyxDQUFDLENBQUM7O1lBRUgsTUFBTSxhQUFhLEdBQUcsS0FBSyxDQUN6QixJQUFJLENBQUMsVUFBVSxFQUNmLElBQUksQ0FBQyxZQUFZLEVBQ2pCLFdBQVcsRUFDWCxJQUFJLENBQUMsUUFBUSxDQUNkLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7O1lBRWhCLE1BQU0sV0FBVyxHQUFHLGFBQWEsQ0FHL0IsSUFBSSxDQUFDLFlBQVksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDLElBQUksQ0FDL0MsR0FBRyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLENBQUM7Z0JBQzdCLE9BQU87b0JBQ0wsWUFBWTtvQkFDWixVQUFVLEVBQUUsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDLE9BQU87b0JBQy9ELFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsT0FBTztvQkFDL0QsT0FBTyxFQUFFLGdCQUFnQixDQUFDLE9BQU87b0JBQ2pDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO29CQUNqQyxVQUFVLEVBQUUsTUFBTSxDQUFDLElBQUk7b0JBQ3ZCLFNBQVMsRUFBRSxNQUFNLENBQUMsR0FBRztpQkFDdEIsQ0FBQzthQUNILENBQUMsRUFDRixHQUFHLENBQUMsUUFBUTtnQkFDVixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFO29CQUN2QixRQUFRLENBQUMsVUFBVTt3QkFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDOzRCQUNyRCxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztpQkFDdkI7Z0JBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRTtvQkFDdkIsUUFBUSxDQUFDLFVBQVU7d0JBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzs0QkFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7aUJBQ3ZCO2dCQUVELE9BQU8sUUFBUSxDQUFDO2FBQ2pCLENBQUMsRUFDRixHQUFHLENBQUMsUUFBUTtnQkFDVixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUU7b0JBQ3BCLFFBQVEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2lCQUN6QjtnQkFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUU7b0JBQ3BCLFFBQVEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2lCQUN6QjtnQkFFRCxPQUFPLFFBQVEsQ0FBQzthQUNqQixDQUFDLEVBQ0YsR0FBRyxDQUFDLFFBQVE7O2dCQUNWLE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxVQUFVLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDOztnQkFDL0QsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLENBQUM7Z0JBQzdELHlCQUNLLFFBQVEsSUFDWCxDQUFDLEVBQUUsUUFBUSxDQUFDLFVBQVUsR0FBRyxPQUFPLEVBQ2hDLENBQUMsRUFBRSxRQUFRLENBQUMsVUFBVSxHQUFHLE9BQU8sSUFDaEM7YUFDSCxDQUFDLEVBQ0YsTUFBTSxDQUNKLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsS0FDL0IsQ0FBQyxJQUFJLENBQUMsWUFBWTtnQkFDbEIsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDaEIsQ0FBQztvQkFDRCxDQUFDO29CQUNELFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRTtpQkFDNUMsQ0FBQyxDQUNMLEVBQ0QsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUN4QixLQUFLLEVBQUUsQ0FDUixDQUFDOztZQUVGLE1BQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQ25DLElBQUksQ0FBQyxDQUFDLENBQUMsRUFDUCxLQUFLLEVBQUUsQ0FDUixDQUFDOztZQUNGLE1BQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQ2pDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFDWCxLQUFLLEVBQUUsQ0FDUixDQUFDO1lBRUYsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUNoRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztvQkFDWixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7aUJBQ3RDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FDeEI7b0JBQ0UsSUFBSSxDQUFDLGVBQWU7MEJBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGFBQWE7MEJBQzdDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVztpQkFDOUIsRUFDRDtvQkFDRSxNQUFNLEVBQUUsRUFBRTs7OztvQkFDVixVQUFVO3dCQUNSLE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLElBQUksQ0FBQyxlQUFlLENBQ3JCLENBQUM7Z0JBRUYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7O29CQUN6QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDOztvQkFDaEUsTUFBTSxLQUFLLHFCQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FDaEQsSUFBSSxDQUNVLEVBQUM7b0JBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLEVBQUU7d0JBQzFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUNwQixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFDMUIsWUFBWSxFQUNaLFFBQVEsQ0FDVCxDQUFDO3FCQUNIO29CQUVELElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO3dCQUM3QixJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM5Qzt5QkFBTTsyQ0FDTCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUUsWUFBWSxDQUNqRCxLQUFLLEVBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsV0FBVztxQkFFekM7b0JBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBRTFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUU7d0JBQzNCLFFBQVEsRUFBRSxPQUFPO3dCQUNqQixHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJO3dCQUNwQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJO3dCQUN0QixLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJO3dCQUN4QixNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJO3dCQUMxQixNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVU7d0JBQ3ZCLE1BQU0sRUFBRSxHQUFHO3FCQUNaLENBQUMsQ0FBQztvQkFFSCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTs7d0JBQzdCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQ3pDLElBQUksQ0FBQyxvQkFBb0IsQ0FDMUIsQ0FBQzt3QkFDRixLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzt3QkFDckIsT0FBTyxDQUFDLFNBQVM7NkJBQ2QsTUFBTSxDQUFDLElBQUksSUFBSSxJQUFJLFlBQVksSUFBSSxDQUFDOzZCQUNwQyxPQUFPLENBQUMsSUFBSTs0QkFDWCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUN6QixDQUFDLENBQUM7d0JBQ0wsVUFBVSxDQUFDLFNBQVMsQ0FBQzs0QkFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt5QkFDNUMsQ0FBQyxDQUFDO3FCQUNKO29CQUVELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNaLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7NEJBQzVCLE9BQU8sRUFBRSxPQUFPLEdBQUcsQ0FBQzs0QkFDcEIsT0FBTyxFQUFFLE9BQU8sR0FBRyxDQUFDOzRCQUNwQixPQUFPLEVBQUUsS0FBSzt5QkFDZixDQUFDLENBQUM7cUJBQ0osQ0FBQyxDQUFDO29CQUVILFVBQVUsQ0FBQyxTQUFTLENBQUM7MkNBQ25CLEtBQUssQ0FBQyxhQUFhLEdBQUUsV0FBVyxDQUFDLEtBQUs7d0JBQ3RDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO3dCQUN6QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLFlBQVksRUFDWixFQUFFLENBQ0gsQ0FBQztxQkFDSCxDQUFDLENBQUM7aUJBQ0o7Z0JBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ3JELENBQUMsQ0FBQztZQUVILFVBQVU7aUJBQ1AsSUFBSSxDQUNILFFBQVEsQ0FBQyxXQUFXOztnQkFDbEIsTUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FDbkMsS0FBSyxFQUFFLEVBQ1AsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUNQLEdBQUcsQ0FBQyxXQUFXLHVCQUNWLFdBQVcsSUFDZCxhQUFhLEVBQUUsV0FBVyxHQUFHLENBQUMsSUFDOUIsQ0FBQyxDQUNKLENBQUM7Z0JBQ0YsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN2QixPQUFPLFlBQVksQ0FBQzthQUNyQixDQUFDLENBQ0g7aUJBQ0EsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRTtnQkFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7b0JBQ1osSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7aUJBQzVDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLElBQUksQ0FBQyxlQUFlLENBQ3JCLENBQUM7Z0JBQ0YsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3pCLENBQUMsQ0FBQztZQUVMLEtBQUssQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDO2lCQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNiLFNBQVMsQ0FBQztnQkFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDakQsQ0FBQyxDQUFDO1lBRUwsT0FBTyxXQUFXLENBQUM7U0FDcEIsQ0FBQyxFQUNGLEtBQUssRUFBRSxDQUNSLENBQUM7UUFFRixLQUFLLENBQ0gsZUFBZSxDQUFDLElBQUksQ0FDbEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUNQLEdBQUcsQ0FBQyxLQUFLLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUN4QixFQUNELGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FDakM7YUFDRSxJQUFJLENBQ0gsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2IsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sUUFBUSxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztTQUN2RCxDQUFDLEVBQ0YsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQ2hDO2FBQ0EsU0FBUyxDQUNSLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUU7WUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM5QixDQUFDLENBQUM7WUFDSCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7O2dCQUNyQixNQUFNLFNBQVMsR0FBRyxhQUFhLFVBQVUsT0FBTyxVQUFVLEtBQUssQ0FBQztnQkFDaEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3ZDLFNBQVM7b0JBQ1QsbUJBQW1CLEVBQUUsU0FBUztvQkFDOUIsZUFBZSxFQUFFLFNBQVM7b0JBQzFCLGdCQUFnQixFQUFFLFNBQVM7b0JBQzNCLGNBQWMsRUFBRSxTQUFTO2lCQUMxQixDQUFDLENBQUM7YUFDSjtZQUNELFlBQVksQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLE9BQU87Z0JBQ1AsT0FBTztnQkFDUCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7YUFDeEIsQ0FBQyxDQUFDO1NBQ0osQ0FDRixDQUFDO0tBQ0w7Ozs7O0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxjQUFXO1lBQ3BCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1NBQzVCO0tBQ0Y7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUN0Qjs7OztJQUVPLG1CQUFtQjs7UUFDekIsTUFBTSxPQUFPLEdBQVksSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDOztRQUN4QyxNQUFNLGlCQUFpQixHQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFMUQsSUFBSSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2dCQUMxQixJQUFJLENBQUMsMEJBQTBCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUM5RCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFDMUIsV0FBVyxFQUNYLENBQUMsS0FBaUI7b0JBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pCLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUM1RCxVQUFVLEVBQ1YsU0FBUyxFQUNULENBQUMsS0FBaUI7b0JBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZCLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsMEJBQTBCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUMvRCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFDMUIsWUFBWSxFQUNaLENBQUMsS0FBaUI7b0JBQ2hCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzFCLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsMEJBQTBCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUM3RCxVQUFVLEVBQ1YsVUFBVSxFQUNWLENBQUMsS0FBaUI7b0JBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3hCLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsMEJBQTBCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUNoRSxVQUFVLEVBQ1YsYUFBYSxFQUNiLENBQUMsS0FBaUI7b0JBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3hCLENBQ0YsQ0FBQztnQkFFRixJQUFJLENBQUMsMEJBQTBCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUMvRCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFDMUIsWUFBWSxFQUNaO29CQUNFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDckIsQ0FDRixDQUFDO2dCQUVGLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQy9ELElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUMxQixZQUFZLEVBQ1o7b0JBQ0UsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUNyQixDQUNGLENBQUM7YUFDSCxDQUFDLENBQUM7U0FDSjthQUFNLElBQUksQ0FBQyxPQUFPLElBQUksaUJBQWlCLEVBQUU7WUFDeEMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7U0FDbEM7Ozs7OztJQUdLLFdBQVcsQ0FBQyxLQUFpQjtRQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRTtZQUM5QyxJQUFJLENBQUMsMEJBQTBCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUM5RCxVQUFVLEVBQ1YsV0FBVyxFQUNYLENBQUMsY0FBMEI7Z0JBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO29CQUNyQixLQUFLLEVBQUUsY0FBYztvQkFDckIsT0FBTyxFQUFFLGNBQWMsQ0FBQyxPQUFPO29CQUMvQixPQUFPLEVBQUUsY0FBYyxDQUFDLE9BQU87aUJBQ2hDLENBQUMsQ0FBQzthQUNKLENBQ0YsQ0FBQztTQUNIO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDckIsS0FBSztZQUNMLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTztZQUN0QixPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU87U0FDdkIsQ0FBQyxDQUFDOzs7Ozs7SUFHRyxTQUFTLENBQUMsS0FBaUI7UUFDakMsSUFBSSxJQUFJLENBQUMsMEJBQTBCLENBQUMsU0FBUyxFQUFFO1lBQzdDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUM1QyxPQUFPLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLENBQUM7U0FDbEQ7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNuQixLQUFLO1lBQ0wsT0FBTyxFQUFFLEtBQUssQ0FBQyxPQUFPO1lBQ3RCLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTztTQUN2QixDQUFDLENBQUM7Ozs7OztJQUdHLFlBQVksQ0FBQyxLQUFpQjtRQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN6QixJQUFJO2dCQUNGLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN4QjtZQUFDLE9BQU8sQ0FBQyxFQUFFLEdBQUU7U0FDZjs7UUFDRCxJQUFJLHFCQUFxQixDQUFVOztRQUNuQyxJQUFJLG1CQUFtQixDQUFNOztRQUM3QixJQUFJLGVBQWUsQ0FBVTtRQUM3QixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsRUFBRTtZQUNwRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDM0MsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUN4QixxQkFBcUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzVELG1CQUFtQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLEVBQUU7WUFDOUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FDOUQsVUFBVSxFQUNWLFdBQVcsRUFDWCxDQUFDLGNBQTBCO2dCQUN6QixJQUNFLElBQUksQ0FBQyxlQUFlO29CQUNwQixJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQjtvQkFDeEMsQ0FBQyxlQUFlO29CQUNoQixxQkFBcUIsRUFDckI7b0JBQ0EsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQ3BDLEtBQUssRUFDTCxjQUFjLEVBQ2QsbUJBQW1CLENBQ3BCLENBQUM7aUJBQ0g7Z0JBQ0QsSUFDRSxDQUFDLElBQUksQ0FBQyxlQUFlO29CQUNyQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CO29CQUN6QyxDQUFDLHFCQUFxQjtvQkFDdEIsZUFBZSxFQUNmO29CQUNBLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO3dCQUNyQixLQUFLLEVBQUUsY0FBYzt3QkFDckIsT0FBTyxFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTzt3QkFDaEQsT0FBTyxFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztxQkFDakQsQ0FBQyxDQUFDO2lCQUNKO2FBQ0YsQ0FDRixDQUFDO1NBQ0g7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUNyQixLQUFLO1lBQ0wsT0FBTyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUNqQyxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO1NBQ2xDLENBQUMsQ0FBQzs7Ozs7O0lBR0csVUFBVSxDQUFDLEtBQWlCO1FBQ2xDLElBQUksSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRTtZQUM3QyxJQUFJLENBQUMsMEJBQTBCLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDNUMsT0FBTyxJQUFJLENBQUMsMEJBQTBCLENBQUMsU0FBUyxDQUFDO1lBQ2pELElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixFQUFFO2dCQUNwRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ3JDO1NBQ0Y7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNuQixLQUFLO1lBQ0wsT0FBTyxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUN4QyxPQUFPLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO1NBQ3pDLENBQUMsQ0FBQzs7Ozs7SUFHRyxZQUFZO1FBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzs7OztJQUcxQixZQUFZO1FBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Ozs7O0lBR2IsT0FBTztRQUNiLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Ozs7OztJQUdwQyxTQUFTLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7Ozs7O0lBRzlELHlCQUF5QjtRQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQ3ZELG1CQUFDLElBQVcsR0FBRSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQ2pELE9BQU8sbUJBQUMsSUFBVyxHQUFFLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZELENBQUMsQ0FBQzs7Ozs7OztJQUdHLGdCQUFnQixDQUN0QixPQUFvQixFQUNwQixNQUFpQztRQUVqQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHO1lBQzdCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDbkQsQ0FBQyxDQUFDOzs7OztJQUdHLGlCQUFpQjtRQUN2QixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsT0FBTztnQkFDTCxHQUFHLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFNBQVM7Z0JBQzVELElBQUksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVTthQUMvRCxDQUFDO1NBQ0g7YUFBTTtZQUNMLE9BQU87Z0JBQ0wsR0FBRyxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxTQUFTO2dCQUM3RCxJQUFJLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVU7YUFDaEUsQ0FBQztTQUNIOzs7Ozs7OztJQUdLLGVBQWUsQ0FDckIsS0FBaUIsRUFDakIsY0FBMEIsRUFDMUIsbUJBQXdCOztRQUV4QixNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDOztRQUNwRCxNQUFNLFdBQVcsR0FBRztZQUNsQixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEdBQUcsbUJBQW1CLENBQUMsR0FBRyxDQUFDO1lBQy9ELElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLElBQUksR0FBRyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7U0FDbkUsQ0FBQzs7UUFDRixNQUFNLE1BQU0sR0FDVixJQUFJLENBQUMsR0FBRyxDQUNOLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUNuRSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7O1FBQ3ZCLE1BQU0sTUFBTSxHQUNWLElBQUksQ0FBQyxHQUFHLENBQ04sY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQ25FLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQzs7UUFDdEIsTUFBTSxVQUFVLEdBQUcsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNuQyxJQUNFLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxLQUFLO1lBQ3ZELFdBQVcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztZQUNuQixXQUFXLENBQUMsSUFBSSxHQUFHLENBQUMsRUFDcEI7WUFDQSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDNUM7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7O1FBQ3pDLE1BQU0sUUFBUSxHQUNaLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO1FBQzlELElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTtZQUM3RCxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxPQUFPLEtBQUssQ0FBQzs7OztZQXRzQmhCLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2FBQzNCOzs7O1lBdkZDLFVBQVU7WUFDVixTQUFTO1lBMEJlLGVBQWU7WUFwQnZDLE1BQU07WUFJTixnQkFBZ0I7WUFtQlQsaUNBQWlDLHVCQW1NckMsUUFBUTs0Q0FDUixNQUFNLFNBQUMsUUFBUTs7O3VCQXRJakIsS0FBSzt1QkFNTCxLQUFLOzJCQU1MLEtBQUs7K0JBTUwsS0FBSzsrQ0FNTCxLQUFLOzJCQU1MLEtBQUs7eUJBTUwsS0FBSzs4QkFNTCxLQUFLO21DQU1MLEtBQUs7bUNBTUwsS0FBSzs4QkFNTCxNQUFNO3dCQVFOLE1BQU07a0NBTU4sTUFBTTt1QkFNTixNQUFNO3NCQU1OLE1BQU07Ozs7Ozs7QUNyTFQ7Ozs7OztBQWlCQSxxQ0FDRSxPQUFlLEVBQ2YsT0FBZSxFQUNmLElBQWdCO0lBRWhCLFFBQ0UsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJO1FBQ3BCLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSztRQUNyQixPQUFPLElBQUksSUFBSSxDQUFDLEdBQUc7UUFDbkIsT0FBTyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQ3RCO0NBQ0g7Ozs7Ozs7OztJQWdEQyxZQUNVLFNBQ0EsaUJBQ0EsTUFDQSxVQUNZLGVBQWtEO1FBSjlELFlBQU8sR0FBUCxPQUFPO1FBQ1Asb0JBQWUsR0FBZixlQUFlO1FBQ2YsU0FBSSxHQUFKLElBQUk7UUFDSixhQUFRLEdBQVIsUUFBUTtRQUNJLG9CQUFlLEdBQWYsZUFBZSxDQUFtQzs7Ozt5QkEzQjVELElBQUksWUFBWSxFQUFhOzs7O3lCQU03QixJQUFJLFlBQVksRUFBYTs7Ozt3QkFNOUIsSUFBSSxZQUFZLEVBQWE7Ozs7b0JBTWpDLElBQUksWUFBWSxFQUFhO0tBVWhDOzs7O0lBRUosUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQ3ZFLEtBQUs7WUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLElBQUksQ0FBQyxlQUFlLENBQ3JCLENBQUM7O1lBQ0YsTUFBTSxnQkFBZ0IsR0FJbEI7Z0JBQ0YsV0FBVyxFQUFFLElBQUk7YUFDbEIsQ0FBQzs7WUFFRixNQUFNLHdCQUF3QixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUNuRCxJQUFJLENBQUMsZUFBZTtrQkFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsYUFBYTtrQkFDN0MsUUFBUSxFQUNaLFFBQVEsRUFDUjtnQkFDRSxnQkFBZ0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2FBQ3JDLENBQ0YsQ0FBQzs7WUFFRixJQUFJLG1CQUFtQixDQUFNOztZQUM3QixNQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUMxQixHQUFHLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFO2dCQUNqQyxtQkFBbUIsR0FBRyxRQUFRLENBQUM7Z0JBQy9CLElBQUksZ0JBQWdCLENBQUMsV0FBVyxFQUFFO29CQUNoQyxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMscUJBQXFCLEVBQUUsQ0FBQztvQkFDM0UsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO3dCQUN4QixnQkFBZ0IsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLEVBQUUsQ0FBQztxQkFDOUc7b0JBQ0QsZ0JBQWdCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDdEM7O2dCQUNELE1BQU0sZUFBZSxHQUFHLDJCQUEyQixDQUNqRCxPQUFPLEVBQ1AsT0FBTyxvQkFDUCxnQkFBZ0IsQ0FBQyxJQUFrQixFQUNwQyxDQUFDO2dCQUNGLElBQUksZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUU7b0JBQ3hDLFFBQ0UsZUFBZTt3QkFDZiwyQkFBMkIsQ0FDekIsT0FBTyxFQUNQLE9BQU8sb0JBQ1AsZ0JBQWdCLENBQUMsbUJBQWlDLEVBQ25ELEVBQ0Q7aUJBQ0g7cUJBQU07b0JBQ0wsT0FBTyxlQUFlLENBQUM7aUJBQ3hCO2FBQ0YsQ0FBQyxDQUNILENBQUM7O1lBRUYsTUFBTSxnQkFBZ0IsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQzs7WUFFaEUsSUFBSSxjQUFjLENBQVU7WUFFNUIsZ0JBQWdCO2lCQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxDQUFDO2lCQUN4QyxTQUFTLENBQUM7Z0JBQ1QsY0FBYyxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUMxQixJQUFJLENBQUMsYUFBYSxDQUNuQixDQUFDO2dCQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO29CQUNaLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO3dCQUNsQixRQUFRLEVBQUUsbUJBQW1CO3FCQUM5QixDQUFDLENBQUM7aUJBQ0osQ0FBQyxDQUFDO2FBQ0osQ0FBQyxDQUFDO1lBRUwsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUMzRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztvQkFDWixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQzt3QkFDakIsUUFBUSxFQUFFLG1CQUFtQjtxQkFDOUIsQ0FBQyxDQUFDO2lCQUNKLENBQUMsQ0FBQzthQUNKLENBQUMsQ0FBQztZQUVILGdCQUFnQjtpQkFDYixJQUFJLENBQ0gsUUFBUSxFQUFFLEVBQ1YsTUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUssVUFBVSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQ2xFO2lCQUNBLFNBQVMsQ0FBQztnQkFDVCxjQUFjLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLElBQUksQ0FBQyxhQUFhLENBQ25CLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7b0JBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7d0JBQ2xCLFFBQVEsRUFBRSxtQkFBbUI7cUJBQzlCLENBQUMsQ0FBQztpQkFDSixDQUFDLENBQUM7YUFDSixDQUFDLENBQUM7WUFFTCxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNkLFFBQVEsRUFBRTtvQkFDUix3QkFBd0IsRUFBRSxDQUFDO29CQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQzFCLElBQUksQ0FBQyxlQUFlLENBQ3JCLENBQUM7b0JBQ0YsSUFBSSxjQUFjLEVBQUU7d0JBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FDbkIsQ0FBQzt3QkFDRixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzs0QkFDWixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQ0FDYixRQUFRLEVBQUUsbUJBQW1COzZCQUM5QixDQUFDLENBQUM7eUJBQ0osQ0FBQyxDQUFDO3FCQUNKO2lCQUNGO2FBQ0YsQ0FBQyxDQUFDO1NBQ0osQ0FDRixDQUFDO0tBQ0g7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzVDO0tBQ0Y7OztZQW5MRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjthQUMzQjs7OztZQWpDQyxVQUFVO1lBV0gsZUFBZTtZQVB0QixNQUFNO1lBRU4sU0FBUztZQU1GLGlDQUFpQyx1QkFrRXJDLFFBQVE7Ozs0QkF4Q1YsS0FBSzs4QkFNTCxLQUFLO3dCQU1MLE1BQU07d0JBTU4sTUFBTTt1QkFNTixNQUFNO21CQU1OLE1BQU07Ozs7Ozs7QUN2RVQ7OztZQUtDLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1osa0JBQWtCO29CQUNsQixrQkFBa0I7b0JBQ2xCLGlDQUFpQztpQkFDbEM7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGtCQUFrQjtvQkFDbEIsa0JBQWtCO29CQUNsQixpQ0FBaUM7aUJBQ2xDO2FBQ0Y7Ozs7Ozs7Ozs7Ozs7OzsifQ==

    /***/
  },

  /***/
  "./node_modules/angular-resizable-element/fesm2015/angular-resizable-element.js":
  /*!**************************************************************************************!*\
    !*** ./node_modules/angular-resizable-element/fesm2015/angular-resizable-element.js ***!
    \**************************************************************************************/

  /*! exports provided: ResizableDirective, ResizeHandleDirective, ResizableModule */

  /***/
  function node_modulesAngularResizableElementFesm2015AngularResizableElementJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResizableDirective", function () {
      return ResizableDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResizeHandleDirective", function () {
      return ResizeHandleDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResizableModule", function () {
      return ResizableModule;
    });
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @param {?} value1
     * @param {?} value2
     * @param {?=} precision
     * @return {?}
     */


    function isNumberCloseTo(value1, value2) {
      var precision = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;

      /** @type {?} */
      var diff = Math.abs(value1 - value2);
      return diff < precision;
    }
    /**
     * @param {?} startingRect
     * @param {?} edges
     * @param {?} clientX
     * @param {?} clientY
     * @return {?}
     */


    function getNewBoundingRectangle(startingRect, edges, clientX, clientY) {
      /** @type {?} */
      var newBoundingRect = {
        top: startingRect.top,
        bottom: startingRect.bottom,
        left: startingRect.left,
        right: startingRect.right
      };

      if (edges.top) {
        newBoundingRect.top += clientY;
      }

      if (edges.bottom) {
        newBoundingRect.bottom += clientY;
      }

      if (edges.left) {
        newBoundingRect.left += clientX;
      }

      if (edges.right) {
        newBoundingRect.right += clientX;
      }

      newBoundingRect.height = newBoundingRect.bottom - newBoundingRect.top;
      newBoundingRect.width = newBoundingRect.right - newBoundingRect.left;
      return newBoundingRect;
    }
    /**
     * @param {?} element
     * @param {?} ghostElementPositioning
     * @return {?}
     */


    function getElementRect(element, ghostElementPositioning) {
      /** @type {?} */
      var translateX = 0;
      /** @type {?} */

      var translateY = 0;
      /** @type {?} */

      var style = element.nativeElement.style;
      /** @type {?} */

      var transformProperties = ['transform', '-ms-transform', '-moz-transform', '-o-transform'];
      /** @type {?} */

      var transform = transformProperties.map(function (property) {
        return style[property];
      }).find(function (value) {
        return !!value;
      });

      if (transform && transform.includes('translate')) {
        translateX = transform.replace(/.*translate\((.*)px, (.*)px\).*/, '$1');
        translateY = transform.replace(/.*translate\((.*)px, (.*)px\).*/, '$2');
      }

      if (ghostElementPositioning === 'absolute') {
        return {
          height: element.nativeElement.offsetHeight,
          width: element.nativeElement.offsetWidth,
          top: element.nativeElement.offsetTop - translateY,
          bottom: element.nativeElement.offsetHeight + element.nativeElement.offsetTop - translateY,
          left: element.nativeElement.offsetLeft - translateX,
          right: element.nativeElement.offsetWidth + element.nativeElement.offsetLeft - translateX
        };
      } else {
        /** @type {?} */
        var boundingRect = element.nativeElement.getBoundingClientRect();
        return {
          height: boundingRect.height,
          width: boundingRect.width,
          top: boundingRect.top - translateY,
          bottom: boundingRect.bottom - translateY,
          left: boundingRect.left - translateX,
          right: boundingRect.right - translateX,
          scrollTop: element.nativeElement.scrollTop,
          scrollLeft: element.nativeElement.scrollLeft
        };
      }
    }
    /**
     * @param {?} __0
     * @return {?}
     */


    function isWithinBoundingY(_ref48) {
      var clientY = _ref48.clientY,
          rect = _ref48.rect;
      return clientY >= rect.top && clientY <= rect.bottom;
    }
    /**
     * @param {?} __0
     * @return {?}
     */


    function isWithinBoundingX(_ref49) {
      var clientX = _ref49.clientX,
          rect = _ref49.rect;
      return clientX >= rect.left && clientX <= rect.right;
    }
    /**
     * @param {?} __0
     * @return {?}
     */


    function getResizeEdges(_ref50) {
      var clientX = _ref50.clientX,
          clientY = _ref50.clientY,
          elm = _ref50.elm,
          allowedEdges = _ref50.allowedEdges,
          cursorPrecision = _ref50.cursorPrecision;

      /** @type {?} */
      var elmPosition = elm.nativeElement.getBoundingClientRect();
      /** @type {?} */

      var edges = {};

      if (allowedEdges.left && isNumberCloseTo(clientX, elmPosition.left, cursorPrecision) && isWithinBoundingY({
        clientY: clientY,
        rect: elmPosition
      })) {
        edges.left = true;
      }

      if (allowedEdges.right && isNumberCloseTo(clientX, elmPosition.right, cursorPrecision) && isWithinBoundingY({
        clientY: clientY,
        rect: elmPosition
      })) {
        edges.right = true;
      }

      if (allowedEdges.top && isNumberCloseTo(clientY, elmPosition.top, cursorPrecision) && isWithinBoundingX({
        clientX: clientX,
        rect: elmPosition
      })) {
        edges.top = true;
      }

      if (allowedEdges.bottom && isNumberCloseTo(clientY, elmPosition.bottom, cursorPrecision) && isWithinBoundingX({
        clientX: clientX,
        rect: elmPosition
      })) {
        edges.bottom = true;
      }

      return edges;
    }
    /** @type {?} */


    var DEFAULT_RESIZE_CURSORS = Object.freeze({
      topLeft: 'nw-resize',
      topRight: 'ne-resize',
      bottomLeft: 'sw-resize',
      bottomRight: 'se-resize',
      leftOrRight: 'col-resize',
      topOrBottom: 'row-resize'
    });
    /**
     * @param {?} edges
     * @param {?} cursors
     * @return {?}
     */

    function getResizeCursor(edges, cursors) {
      if (edges.left && edges.top) {
        return cursors.topLeft;
      } else if (edges.right && edges.top) {
        return cursors.topRight;
      } else if (edges.left && edges.bottom) {
        return cursors.bottomLeft;
      } else if (edges.right && edges.bottom) {
        return cursors.bottomRight;
      } else if (edges.left || edges.right) {
        return cursors.leftOrRight;
      } else if (edges.top || edges.bottom) {
        return cursors.topOrBottom;
      } else {
        return '';
      }
    }
    /**
     * @param {?} __0
     * @return {?}
     */


    function getEdgesDiff(_ref51) {
      var edges = _ref51.edges,
          initialRectangle = _ref51.initialRectangle,
          newRectangle = _ref51.newRectangle;

      /** @type {?} */
      var edgesDiff = {};
      Object.keys(edges).forEach(function (edge) {
        edgesDiff[edge] = (newRectangle[edge] || 0) - (initialRectangle[edge] || 0);
      });
      return edgesDiff;
    }
    /** @type {?} */


    var RESIZE_ACTIVE_CLASS = 'resize-active';
    /** @type {?} */

    var RESIZE_LEFT_HOVER_CLASS = 'resize-left-hover';
    /** @type {?} */

    var RESIZE_RIGHT_HOVER_CLASS = 'resize-right-hover';
    /** @type {?} */

    var RESIZE_TOP_HOVER_CLASS = 'resize-top-hover';
    /** @type {?} */

    var RESIZE_BOTTOM_HOVER_CLASS = 'resize-bottom-hover';
    /** @type {?} */

    var RESIZE_GHOST_ELEMENT_CLASS = 'resize-ghost-element';
    /** @type {?} */

    var MOUSE_MOVE_THROTTLE_MS = 50;
    /**
     * Place this on an element to make it resizable. For example:
     *
     * ```html
     * <div
     *   mwlResizable
     *   [resizeEdges]="{bottom: true, right: true, top: true, left: true}"
     *   [enableGhostResize]="true">
     * </div>
     * ```
     */

    var ResizableDirective = /*#__PURE__*/function () {
      /**
       * @hidden
       * @param {?} platformId
       * @param {?} renderer
       * @param {?} elm
       * @param {?} zone
       */
      function ResizableDirective(platformId, renderer, elm, zone) {
        _classCallCheck(this, ResizableDirective);

        this.platformId = platformId;
        this.renderer = renderer;
        this.elm = elm;
        this.zone = zone;
        /**
         * The edges that an element can be resized from. Pass an object like `{top: true, bottom: false}`. By default no edges can be resized.
         * @deprecated use a resize handle instead that positions itself to the side of the element you would like to resize
         */

        this.resizeEdges = {};
        /**
         * Set to `true` to enable a temporary resizing effect of the element in between the `resizeStart` and `resizeEnd` events.
         */

        this.enableGhostResize = false;
        /**
         * A snap grid that resize events will be locked to.
         *
         * e.g. to only allow the element to be resized every 10px set it to `{left: 10, right: 10}`
         */

        this.resizeSnapGrid = {};
        /**
         * The mouse cursors that will be set on the resize edges
         */

        this.resizeCursors = DEFAULT_RESIZE_CURSORS;
        /**
         * Mouse over thickness to active cursor.
         * @deprecated invalid when you migrate to use resize handles instead of setting resizeEdges on the element
         */

        this.resizeCursorPrecision = 3;
        /**
         * Define the positioning of the ghost element (can be fixed or absolute)
         */

        this.ghostElementPositioning = 'fixed';
        /**
         * Allow elements to be resized to negative dimensions
         */

        this.allowNegativeResizes = false;
        /**
         * Called when the mouse is pressed and a resize event is about to begin. `$event` is a `ResizeEvent` object.
         */

        this.resizeStart = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        /**
         * Called as the mouse is dragged after a resize event has begun. `$event` is a `ResizeEvent` object.
         */

        this.resizing = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        /**
         * Called after the mouse is released after a resize event. `$event` is a `ResizeEvent` object.
         */

        this.resizeEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        /**
         * @hidden
         */

        this.mouseup = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        /**
         * @hidden
         */

        this.mousedown = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        /**
         * @hidden
         */

        this.mousemove = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.resizeEdges$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.pointerEventListeners = PointerEventListeners.getInstance(renderer, zone);
      }
      /**
       * @hidden
       * @return {?}
       */


      _createClass(ResizableDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this23 = this;

          // TODO - use some fancy Observable.merge's for this
          this.pointerEventListeners.pointerDown.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.destroy$)).subscribe(function (_ref52) {
            var clientX = _ref52.clientX,
                clientY = _ref52.clientY;

            _this23.mousedown.next({
              clientX: clientX,
              clientY: clientY
            });
          });
          this.pointerEventListeners.pointerMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.destroy$)).subscribe(function (_ref53) {
            var clientX = _ref53.clientX,
                clientY = _ref53.clientY,
                event = _ref53.event;

            _this23.mousemove.next({
              clientX: clientX,
              clientY: clientY,
              event: event
            });
          });
          this.pointerEventListeners.pointerUp.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.destroy$)).subscribe(function (_ref54) {
            var clientX = _ref54.clientX,
                clientY = _ref54.clientY;

            _this23.mouseup.next({
              clientX: clientX,
              clientY: clientY
            });
          });
          /** @type {?} */

          var currentResize;
          /** @type {?} */

          var removeGhostElement = function removeGhostElement() {
            if (currentResize && currentResize.clonedNode) {
              _this23.elm.nativeElement.parentElement.removeChild(currentResize.clonedNode);

              _this23.renderer.setStyle(_this23.elm.nativeElement, 'visibility', 'inherit');
            }
          };
          /** @type {?} */


          var getResizeCursors = function getResizeCursors() {
            return Object.assign({}, DEFAULT_RESIZE_CURSORS, _this23.resizeCursors);
          };
          /** @type {?} */


          var mouseMove = this.mousemove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
          mouseMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function () {
            return !!currentResize;
          })).subscribe(function (_ref55) {
            var event = _ref55.event;
            event.preventDefault();
          });
          this.resizeEdges$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["startWith"])(this.resizeEdges), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            return _this23.resizeEdges && Object.keys(_this23.resizeEdges).some(function (edge) {
              return !!_this23.resizeEdges[edge];
            });
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (legacyResizeEdgesEnabled) {
            return legacyResizeEdgesEnabled ? mouseMove : rxjs__WEBPACK_IMPORTED_MODULE_1__["EMPTY"];
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["auditTime"])(MOUSE_MOVE_THROTTLE_MS)).subscribe(function (_ref56) {
            var clientX = _ref56.clientX,
                clientY = _ref56.clientY;

            /** @type {?} */
            var resizeEdges = getResizeEdges({
              clientX: clientX,
              clientY: clientY,
              elm: _this23.elm,
              allowedEdges: _this23.resizeEdges,
              cursorPrecision: _this23.resizeCursorPrecision
            });
            /** @type {?} */

            var resizeCursors = getResizeCursors();

            if (!currentResize) {
              /** @type {?} */
              var cursor = getResizeCursor(resizeEdges, resizeCursors);

              _this23.renderer.setStyle(_this23.elm.nativeElement, 'cursor', cursor);
            }

            _this23.setElementClass(_this23.elm, RESIZE_LEFT_HOVER_CLASS, resizeEdges.left === true);

            _this23.setElementClass(_this23.elm, RESIZE_RIGHT_HOVER_CLASS, resizeEdges.right === true);

            _this23.setElementClass(_this23.elm, RESIZE_TOP_HOVER_CLASS, resizeEdges.top === true);

            _this23.setElementClass(_this23.elm, RESIZE_BOTTOM_HOVER_CLASS, resizeEdges.bottom === true);
          });
          /** @type {?} */

          var mousedrag = this.mousedown.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (startCoords) {
            /**
             * @param {?} moveCoords
             * @return {?}
             */
            function getDiff(moveCoords) {
              return {
                clientX: moveCoords.clientX - startCoords.clientX,
                clientY: moveCoords.clientY - startCoords.clientY
              };
            }
            /** @type {?} */


            var getSnapGrid = function getSnapGrid() {
              /** @type {?} */
              var snapGrid = {
                x: 1,
                y: 1
              };

              if (currentResize) {
                if (_this23.resizeSnapGrid.left && currentResize.edges.left) {
                  snapGrid.x = +_this23.resizeSnapGrid.left;
                } else if (_this23.resizeSnapGrid.right && currentResize.edges.right) {
                  snapGrid.x = +_this23.resizeSnapGrid.right;
                }

                if (_this23.resizeSnapGrid.top && currentResize.edges.top) {
                  snapGrid.y = +_this23.resizeSnapGrid.top;
                } else if (_this23.resizeSnapGrid.bottom && currentResize.edges.bottom) {
                  snapGrid.y = +_this23.resizeSnapGrid.bottom;
                }
              }

              return snapGrid;
            };
            /**
             * @param {?} coords
             * @param {?} snapGrid
             * @return {?}
             */


            function getGrid(coords, snapGrid) {
              return {
                x: Math.ceil(coords.clientX / snapGrid.x),
                y: Math.ceil(coords.clientY / snapGrid.y)
              };
            }

            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["merge"])(mouseMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (coords) {
              return [, coords];
            })), mouseMove.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["pairwise"])())).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref57) {
              var _ref58 = _slicedToArray(_ref57, 2),
                  previousCoords = _ref58[0],
                  newCoords = _ref58[1];

              return [previousCoords ? getDiff(previousCoords) : previousCoords, getDiff(newCoords)];
            })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (_ref59) {
              var _ref60 = _slicedToArray(_ref59, 2),
                  previousCoords = _ref60[0],
                  newCoords = _ref60[1];

              if (!previousCoords) {
                return true;
              }
              /** @type {?} */


              var snapGrid = getSnapGrid();
              /** @type {?} */

              var previousGrid = getGrid(previousCoords, snapGrid);
              /** @type {?} */

              var newGrid = getGrid(newCoords, snapGrid);
              return previousGrid.x !== newGrid.x || previousGrid.y !== newGrid.y;
            })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref61) {
              var _ref62 = _slicedToArray(_ref61, 2),
                  newCoords = _ref62[1];

              /** @type {?} */
              var snapGrid = getSnapGrid();
              return {
                clientX: Math.round(newCoords.clientX / snapGrid.x) * snapGrid.x,
                clientY: Math.round(newCoords.clientY / snapGrid.y) * snapGrid.y
              };
            })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["merge"])(_this23.mouseup, _this23.mousedown)));
          })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function () {
            return !!currentResize;
          }));
          mousedrag.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref63) {
            var clientX = _ref63.clientX,
                clientY = _ref63.clientY;
            return getNewBoundingRectangle(
            /** @type {?} */
            currentResize.startingRect,
            /** @type {?} */
            currentResize.edges, clientX, clientY);
          })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (newBoundingRect) {
            return _this23.allowNegativeResizes || !!(newBoundingRect.height && newBoundingRect.width && newBoundingRect.height > 0 && newBoundingRect.width > 0);
          })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (newBoundingRect) {
            return _this23.validateResize ? _this23.validateResize({
              rectangle: newBoundingRect,
              edges: getEdgesDiff({
                edges:
                /** @type {?} */
                currentResize.edges,
                initialRectangle:
                /** @type {?} */
                currentResize.startingRect,
                newRectangle: newBoundingRect
              })
            }) : true;
          })).subscribe(function (newBoundingRect) {
            if (currentResize && currentResize.clonedNode) {
              _this23.renderer.setStyle(currentResize.clonedNode, 'height', "".concat(newBoundingRect.height, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'width', "".concat(newBoundingRect.width, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'top', "".concat(newBoundingRect.top, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'left', "".concat(newBoundingRect.left, "px"));
            }

            _this23.zone.run(function () {
              _this23.resizing.emit({
                edges: getEdgesDiff({
                  edges:
                  /** @type {?} */
                  currentResize.edges,
                  initialRectangle:
                  /** @type {?} */
                  currentResize.startingRect,
                  newRectangle: newBoundingRect
                }),
                rectangle: newBoundingRect
              });
            });

            /** @type {?} */
            currentResize.currentRect = newBoundingRect;
          });
          this.mousedown.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (_ref64) {
            var clientX = _ref64.clientX,
                clientY = _ref64.clientY,
                edges = _ref64.edges;
            return edges || getResizeEdges({
              clientX: clientX,
              clientY: clientY,
              elm: _this23.elm,
              allowedEdges: _this23.resizeEdges,
              cursorPrecision: _this23.resizeCursorPrecision
            });
          })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (edges) {
            return Object.keys(edges).length > 0;
          })).subscribe(function (edges) {
            if (currentResize) {
              removeGhostElement();
            }
            /** @type {?} */


            var startingRect = getElementRect(_this23.elm, _this23.ghostElementPositioning);
            currentResize = {
              edges: edges,
              startingRect: startingRect,
              currentRect: startingRect
            };
            /** @type {?} */

            var resizeCursors = getResizeCursors();
            /** @type {?} */

            var cursor = getResizeCursor(currentResize.edges, resizeCursors);

            _this23.renderer.setStyle(document.body, 'cursor', cursor);

            _this23.setElementClass(_this23.elm, RESIZE_ACTIVE_CLASS, true);

            if (_this23.enableGhostResize) {
              currentResize.clonedNode = _this23.elm.nativeElement.cloneNode(true);

              _this23.elm.nativeElement.parentElement.appendChild(currentResize.clonedNode);

              _this23.renderer.setStyle(_this23.elm.nativeElement, 'visibility', 'hidden');

              _this23.renderer.setStyle(currentResize.clonedNode, 'position', _this23.ghostElementPositioning);

              _this23.renderer.setStyle(currentResize.clonedNode, 'left', "".concat(currentResize.startingRect.left, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'top', "".concat(currentResize.startingRect.top, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'height', "".concat(currentResize.startingRect.height, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'width', "".concat(currentResize.startingRect.width, "px"));

              _this23.renderer.setStyle(currentResize.clonedNode, 'cursor', getResizeCursor(currentResize.edges, resizeCursors));

              _this23.renderer.addClass(currentResize.clonedNode, RESIZE_GHOST_ELEMENT_CLASS);

              /** @type {?} */
              currentResize.clonedNode.scrollTop =
              /** @type {?} */
              currentResize.startingRect.scrollTop;

              /** @type {?} */
              currentResize.clonedNode.scrollLeft =
              /** @type {?} */
              currentResize.startingRect.scrollLeft;
            }

            _this23.zone.run(function () {
              _this23.resizeStart.emit({
                edges: getEdgesDiff({
                  edges: edges,
                  initialRectangle: startingRect,
                  newRectangle: startingRect
                }),
                rectangle: getNewBoundingRectangle(startingRect, {}, 0, 0)
              });
            });
          });
          this.mouseup.subscribe(function () {
            if (currentResize) {
              _this23.renderer.removeClass(_this23.elm.nativeElement, RESIZE_ACTIVE_CLASS);

              _this23.renderer.setStyle(document.body, 'cursor', '');

              _this23.renderer.setStyle(_this23.elm.nativeElement, 'cursor', '');

              _this23.zone.run(function () {
                _this23.resizeEnd.emit({
                  edges: getEdgesDiff({
                    edges:
                    /** @type {?} */
                    currentResize.edges,
                    initialRectangle:
                    /** @type {?} */
                    currentResize.startingRect,
                    newRectangle:
                    /** @type {?} */
                    currentResize.currentRect
                  }),
                  rectangle:
                  /** @type {?} */
                  currentResize.currentRect
                });
              });

              removeGhostElement();
              currentResize = null;
            }
          });
        }
        /**
         * @hidden
         * @param {?} changes
         * @return {?}
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          if (changes.resizeEdges) {
            this.resizeEdges$.next(this.resizeEdges);
          }
        }
        /**
         * @hidden
         * @return {?}
         */

      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // browser check for angular universal, because it doesn't know what document is
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_0__["isPlatformBrowser"])(this.platformId)) {
            this.renderer.setStyle(document.body, 'cursor', '');
          }

          this.mousedown.complete();
          this.mouseup.complete();
          this.mousemove.complete();
          this.resizeEdges$.complete();
          this.destroy$.next();
        }
        /**
         * @private
         * @param {?} elm
         * @param {?} name
         * @param {?} add
         * @return {?}
         */

      }, {
        key: "setElementClass",
        value: function setElementClass(elm, name, add) {
          if (add) {
            this.renderer.addClass(elm.nativeElement, name);
          } else {
            this.renderer.removeClass(elm.nativeElement, name);
          }
        }
      }]);

      return ResizableDirective;
    }();

    ResizableDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Directive"],
      args: [{
        selector: '[mwlResizable]'
      }]
    }];
    /** @nocollapse */

    ResizableDirective.ctorParameters = function () {
      return [{
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["PLATFORM_ID"]]
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgZone"]
      }];
    };

    ResizableDirective.propDecorators = {
      validateResize: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      resizeEdges: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      enableGhostResize: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      resizeSnapGrid: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      resizeCursors: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      resizeCursorPrecision: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      ghostElementPositioning: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      allowNegativeResizes: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      resizeStart: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"]
      }],
      resizing: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"]
      }],
      resizeEnd: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"]
      }]
    };

    var PointerEventListeners = /*#__PURE__*/function () {
      _createClass(PointerEventListeners, null, [{
        key: "getInstance",
        // tslint:disable-line

        /**
         * @param {?} renderer
         * @param {?} zone
         * @return {?}
         */
        value: function getInstance(renderer, zone) {
          if (!PointerEventListeners.instance) {
            PointerEventListeners.instance = new PointerEventListeners(renderer, zone);
          }

          return PointerEventListeners.instance;
        }
        /**
         * @param {?} renderer
         * @param {?} zone
         */

      }]);

      function PointerEventListeners(renderer, zone) {
        _classCallCheck(this, PointerEventListeners);

        this.pointerDown = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
          /** @type {?} */
          var unsubscribeMouseDown;
          /** @type {?} */

          var unsubscribeTouchStart;
          zone.runOutsideAngular(function () {
            unsubscribeMouseDown = renderer.listen('document', 'mousedown', function (event) {
              observer.next({
                clientX: event.clientX,
                clientY: event.clientY,
                event: event
              });
            });
            unsubscribeTouchStart = renderer.listen('document', 'touchstart', function (event) {
              observer.next({
                clientX: event.touches[0].clientX,
                clientY: event.touches[0].clientY,
                event: event
              });
            });
          });
          return function () {
            unsubscribeMouseDown();
            unsubscribeTouchStart();
          };
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
        this.pointerMove = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
          /** @type {?} */
          var unsubscribeMouseMove;
          /** @type {?} */

          var unsubscribeTouchMove;
          zone.runOutsideAngular(function () {
            unsubscribeMouseMove = renderer.listen('document', 'mousemove', function (event) {
              observer.next({
                clientX: event.clientX,
                clientY: event.clientY,
                event: event
              });
            });
            unsubscribeTouchMove = renderer.listen('document', 'touchmove', function (event) {
              observer.next({
                clientX: event.targetTouches[0].clientX,
                clientY: event.targetTouches[0].clientY,
                event: event
              });
            });
          });
          return function () {
            unsubscribeMouseMove();
            unsubscribeTouchMove();
          };
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
        this.pointerUp = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
          /** @type {?} */
          var unsubscribeMouseUp;
          /** @type {?} */

          var unsubscribeTouchEnd;
          /** @type {?} */

          var unsubscribeTouchCancel;
          zone.runOutsideAngular(function () {
            unsubscribeMouseUp = renderer.listen('document', 'mouseup', function (event) {
              observer.next({
                clientX: event.clientX,
                clientY: event.clientY,
                event: event
              });
            });
            unsubscribeTouchEnd = renderer.listen('document', 'touchend', function (event) {
              observer.next({
                clientX: event.changedTouches[0].clientX,
                clientY: event.changedTouches[0].clientY,
                event: event
              });
            });
            unsubscribeTouchCancel = renderer.listen('document', 'touchcancel', function (event) {
              observer.next({
                clientX: event.changedTouches[0].clientX,
                clientY: event.changedTouches[0].clientY,
                event: event
              });
            });
          });
          return function () {
            unsubscribeMouseUp();
            unsubscribeTouchEnd();
            unsubscribeTouchCancel();
          };
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["share"])());
      }

      return PointerEventListeners;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * An element placed inside a `mwlResizable` directive to be used as a drag and resize handle
     *
     * For example
     *
     * ```html
     * <div mwlResizable>
     *   <div mwlResizeHandle [resizeEdges]="{bottom: true, right: true}"></div>
     * </div>
     * ```
     */


    var ResizeHandleDirective = /*#__PURE__*/function () {
      /**
       * @param {?} renderer
       * @param {?} element
       * @param {?} zone
       * @param {?} resizable
       */
      function ResizeHandleDirective(renderer, element, zone, resizable) {
        _classCallCheck(this, ResizeHandleDirective);

        this.renderer = renderer;
        this.element = element;
        this.zone = zone;
        this.resizable = resizable;
        /**
         * The `Edges` object that contains the edges of the parent element that dragging the handle will trigger a resize on
         */

        this.resizeEdges = {};
        this.eventListeners = {};
      }
      /**
       * @return {?}
       */


      _createClass(ResizeHandleDirective, [{
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeEventListeners();
        }
        /**
         * @hidden
         * @param {?} event
         * @param {?} clientX
         * @param {?} clientY
         * @return {?}
         */

      }, {
        key: "onMousedown",
        value: function onMousedown(event, clientX, clientY) {
          var _this24 = this;

          event.preventDefault();
          this.zone.runOutsideAngular(function () {
            if (!_this24.eventListeners.touchmove) {
              _this24.eventListeners.touchmove = _this24.renderer.listen(_this24.element.nativeElement, 'touchmove', function (touchMoveEvent) {
                _this24.onMousemove(touchMoveEvent, touchMoveEvent.targetTouches[0].clientX, touchMoveEvent.targetTouches[0].clientY);
              });
            }

            if (!_this24.eventListeners.mousemove) {
              _this24.eventListeners.mousemove = _this24.renderer.listen(_this24.element.nativeElement, 'mousemove', function (mouseMoveEvent) {
                _this24.onMousemove(mouseMoveEvent, mouseMoveEvent.clientX, mouseMoveEvent.clientY);
              });
            }

            _this24.resizable.mousedown.next({
              clientX: clientX,
              clientY: clientY,
              edges: _this24.resizeEdges
            });
          });
        }
        /**
         * @hidden
         * @param {?} clientX
         * @param {?} clientY
         * @return {?}
         */

      }, {
        key: "onMouseup",
        value: function onMouseup(clientX, clientY) {
          var _this25 = this;

          this.zone.runOutsideAngular(function () {
            _this25.unsubscribeEventListeners();

            _this25.resizable.mouseup.next({
              clientX: clientX,
              clientY: clientY,
              edges: _this25.resizeEdges
            });
          });
        }
        /**
         * @private
         * @param {?} event
         * @param {?} clientX
         * @param {?} clientY
         * @return {?}
         */

      }, {
        key: "onMousemove",
        value: function onMousemove(event, clientX, clientY) {
          this.resizable.mousemove.next({
            clientX: clientX,
            clientY: clientY,
            edges: this.resizeEdges,
            event: event
          });
        }
        /**
         * @private
         * @return {?}
         */

      }, {
        key: "unsubscribeEventListeners",
        value: function unsubscribeEventListeners() {
          var _this26 = this;

          Object.keys(this.eventListeners).forEach(function (type) {
            /** @type {?} */
            _this26.eventListeners[type]();

            delete _this26.eventListeners[type];
          });
        }
      }]);

      return ResizeHandleDirective;
    }();

    ResizeHandleDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Directive"],
      args: [{
        selector: '[mwlResizeHandle]'
      }]
    }];
    /** @nocollapse */

    ResizeHandleDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgZone"]
      }, {
        type: ResizableDirective
      }];
    };

    ResizeHandleDirective.propDecorators = {
      resizeEdges: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
      }],
      onMousedown: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"],
        args: ['touchstart', ['$event', '$event.touches[0].clientX', '$event.touches[0].clientY']]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"],
        args: ['mousedown', ['$event', '$event.clientX', '$event.clientY']]
      }],
      onMouseup: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"],
        args: ['touchend', ['$event.changedTouches[0].clientX', '$event.changedTouches[0].clientY']]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"],
        args: ['touchcancel', ['$event.changedTouches[0].clientX', '$event.changedTouches[0].clientY']]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"],
        args: ['mouseup', ['$event.clientX', '$event.clientY']]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var ResizableModule = function ResizableModule() {
      _classCallCheck(this, ResizableModule);
    };

    ResizableModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"],
      args: [{
        declarations: [ResizableDirective, ResizeHandleDirective],
        exports: [ResizableDirective, ResizeHandleDirective]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    //# sourceMappingURL=angular-resizable-element.js.map

    /***/
  },

  /***/
  "./node_modules/animation-frame-polyfill/lib/animation-frame-polyfill.cjs.js":
  /*!***********************************************************************************!*\
    !*** ./node_modules/animation-frame-polyfill/lib/animation-frame-polyfill.cjs.js ***!
    \***********************************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesAnimationFramePolyfillLibAnimationFramePolyfillCjsJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var prefix = ['webkit', 'moz', 'ms', 'o'];

    var requestAnimationFrame = exports.requestAnimationFrame = function () {
      for (var i = 0, limit = prefix.length; i < limit && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[prefix[i] + 'RequestAnimationFrame'];
      }

      if (!window.requestAnimationFrame) {
        (function () {
          var lastTime = 0;

          window.requestAnimationFrame = function (callback) {
            var now = new Date().getTime();
            var ttc = Math.max(0, 16 - now - lastTime);
            var timer = window.setTimeout(function () {
              return callback(now + ttc);
            }, ttc);
            lastTime = now + ttc;
            return timer;
          };
        })();
      }

      return window.requestAnimationFrame.bind(window);
    }();

    var cancelAnimationFrame = exports.cancelAnimationFrame = function () {
      for (var i = 0, limit = prefix.length; i < limit && !window.cancelAnimationFrame; ++i) {
        window.cancelAnimationFrame = window[prefix[i] + 'CancelAnimationFrame'] || window[prefix[i] + 'CancelRequestAnimationFrame'];
      }

      if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (timer) {
          window.clearTimeout(timer);
        };
      }

      return window.cancelAnimationFrame.bind(window);
    }();
    /***/

  },

  /***/
  "./node_modules/array-from/index.js":
  /*!******************************************!*\
    !*** ./node_modules/array-from/index.js ***!
    \******************************************/

  /*! no static exports found */

  /***/
  function node_modulesArrayFromIndexJs(module, exports, __webpack_require__) {
    module.exports = typeof Array.from === 'function' ? Array.from : __webpack_require__(
    /*! ./polyfill */
    "./node_modules/array-from/polyfill.js");
    /***/
  },

  /***/
  "./node_modules/array-from/polyfill.js":
  /*!*********************************************!*\
    !*** ./node_modules/array-from/polyfill.js ***!
    \*********************************************/

  /*! no static exports found */

  /***/
  function node_modulesArrayFromPolyfillJs(module, exports) {
    // Production steps of ECMA-262, Edition 6, 22.1.2.1
    // Reference: http://www.ecma-international.org/ecma-262/6.0/#sec-array.from
    module.exports = function () {
      var isCallable = function isCallable(fn) {
        return typeof fn === 'function';
      };

      var toInteger = function toInteger(value) {
        var number = Number(value);

        if (isNaN(number)) {
          return 0;
        }

        if (number === 0 || !isFinite(number)) {
          return number;
        }

        return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
      };

      var maxSafeInteger = Math.pow(2, 53) - 1;

      var toLength = function toLength(value) {
        var len = toInteger(value);
        return Math.min(Math.max(len, 0), maxSafeInteger);
      };

      var iteratorProp = function iteratorProp(value) {
        if (value != null) {
          if (['string', 'number', 'boolean', 'symbol'].indexOf(typeof value) > -1) {
            return Symbol.iterator;
          } else if (typeof Symbol !== 'undefined' && 'iterator' in Symbol && Symbol.iterator in value) {
            return Symbol.iterator;
          } // Support "@@iterator" placeholder, Gecko 27 to Gecko 35
          else if ('@@iterator' in value) {
              return '@@iterator';
            }
        }
      };

      var getMethod = function getMethod(O, P) {
        // Assert: IsPropertyKey(P) is true.
        if (O != null && P != null) {
          // Let func be GetV(O, P).
          var func = O[P]; // ReturnIfAbrupt(func).
          // If func is either undefined or null, return undefined.

          if (func == null) {
            return void 0;
          } // If IsCallable(func) is false, throw a TypeError exception.


          if (!isCallable(func)) {
            throw new TypeError(func + ' is not a function');
          }

          return func;
        }
      };

      var iteratorStep = function iteratorStep(iterator) {
        // Let result be IteratorNext(iterator).
        // ReturnIfAbrupt(result).
        var result = iterator.next(); // Let done be IteratorComplete(result).
        // ReturnIfAbrupt(done).

        var done = Boolean(result.done); // If done is true, return false.

        if (done) {
          return false;
        } // Return result.


        return result;
      }; // The length property of the from method is 1.


      return function from(items
      /*, mapFn, thisArg */
      ) {
        'use strict'; // 1. Let C be the this value.

        var C = this; // 2. If mapfn is undefined, let mapping be false.

        var mapFn = arguments.length > 1 ? arguments[1] : void 0;
        var T;

        if (typeof mapFn !== 'undefined') {
          // 3. else
          //   a. If IsCallable(mapfn) is false, throw a TypeError exception.
          if (!isCallable(mapFn)) {
            throw new TypeError('Array.from: when provided, the second argument must be a function');
          } //   b. If thisArg was supplied, let T be thisArg; else let T
          //      be undefined.


          if (arguments.length > 2) {
            T = arguments[2];
          } //   c. Let mapping be true (implied by mapFn)

        }

        var A, k; // 4. Let usingIterator be GetMethod(items, @@iterator).
        // 5. ReturnIfAbrupt(usingIterator).

        var usingIterator = getMethod(items, iteratorProp(items)); // 6. If usingIterator is not undefined, then

        if (usingIterator !== void 0) {
          // a. If IsConstructor(C) is true, then
          //   i. Let A be the result of calling the [[Construct]]
          //      internal method of C with an empty argument list.
          // b. Else,
          //   i. Let A be the result of the abstract operation ArrayCreate
          //      with argument 0.
          // c. ReturnIfAbrupt(A).
          A = isCallable(C) ? Object(new C()) : []; // d. Let iterator be GetIterator(items, usingIterator).

          var iterator = usingIterator.call(items); // e. ReturnIfAbrupt(iterator).

          if (iterator == null) {
            throw new TypeError('Array.from requires an array-like or iterable object');
          } // f. Let k be 0.


          k = 0; // g. Repeat

          var next, nextValue;

          while (true) {
            // i. Let Pk be ToString(k).
            // ii. Let next be IteratorStep(iterator).
            // iii. ReturnIfAbrupt(next).
            next = iteratorStep(iterator); // iv. If next is false, then

            if (!next) {
              // 1. Let setStatus be Set(A, "length", k, true).
              // 2. ReturnIfAbrupt(setStatus).
              A.length = k; // 3. Return A.

              return A;
            } // v. Let nextValue be IteratorValue(next).
            // vi. ReturnIfAbrupt(nextValue)


            nextValue = next.value; // vii. If mapping is true, then
            //   1. Let mappedValue be Call(mapfn, T, «nextValue, k»).
            //   2. If mappedValue is an abrupt completion, return
            //      IteratorClose(iterator, mappedValue).
            //   3. Let mappedValue be mappedValue.[[value]].
            // viii. Else, let mappedValue be nextValue.
            // ix.  Let defineStatus be the result of
            //      CreateDataPropertyOrThrow(A, Pk, mappedValue).
            // x. [TODO] If defineStatus is an abrupt completion, return
            //    IteratorClose(iterator, defineStatus).

            if (mapFn) {
              A[k] = mapFn.call(T, nextValue, k);
            } else {
              A[k] = nextValue;
            } // xi. Increase k by 1.


            k++;
          } // 7. Assert: items is not an Iterable so assume it is
          //    an array-like object.

        } else {
          // 8. Let arrayLike be ToObject(items).
          var arrayLike = Object(items); // 9. ReturnIfAbrupt(items).

          if (items == null) {
            throw new TypeError('Array.from requires an array-like object - not null or undefined');
          } // 10. Let len be ToLength(Get(arrayLike, "length")).
          // 11. ReturnIfAbrupt(len).


          var len = toLength(arrayLike.length); // 12. If IsConstructor(C) is true, then
          //     a. Let A be Construct(C, «len»).
          // 13. Else
          //     a. Let A be ArrayCreate(len).
          // 14. ReturnIfAbrupt(A).

          A = isCallable(C) ? Object(new C(len)) : new Array(len); // 15. Let k be 0.

          k = 0; // 16. Repeat, while k < len… (also steps a - h)

          var kValue;

          while (k < len) {
            kValue = arrayLike[k];

            if (mapFn) {
              A[k] = mapFn.call(T, kValue, k);
            } else {
              A[k] = kValue;
            }

            k++;
          } // 17. Let setStatus be Set(A, "length", len, true).
          // 18. ReturnIfAbrupt(setStatus).


          A.length = len; // 19. Return A.
        }

        return A;
      };
    }();
    /***/

  },

  /***/
  "./node_modules/calendar-utils/calendar-utils.js":
  /*!*******************************************************!*\
    !*** ./node_modules/calendar-utils/calendar-utils.js ***!
    \*******************************************************/

  /*! exports provided: DAYS_OF_WEEK, SECONDS_IN_DAY, getWeekViewEventOffset, getEventsInPeriod, getWeekViewHeader, getDifferenceInDaysWithExclusions, getWeekView, getMonthView, getDayView, getDayViewHourGrid, EventValidationErrorMessage, validateEvents */

  /***/
  function node_modulesCalendarUtilsCalendarUtilsJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DAYS_OF_WEEK", function () {
      return DAYS_OF_WEEK;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SECONDS_IN_DAY", function () {
      return SECONDS_IN_DAY;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getWeekViewEventOffset", function () {
      return getWeekViewEventOffset;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getEventsInPeriod", function () {
      return getEventsInPeriod;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getWeekViewHeader", function () {
      return getWeekViewHeader;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getDifferenceInDaysWithExclusions", function () {
      return getDifferenceInDaysWithExclusions;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getWeekView", function () {
      return getWeekView;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getMonthView", function () {
      return getMonthView;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getDayView", function () {
      return getDayView;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getDayViewHourGrid", function () {
      return getDayViewHourGrid;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EventValidationErrorMessage", function () {
      return EventValidationErrorMessage;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "validateEvents", function () {
      return validateEvents;
    });

    var __assign = undefined && undefined.__assign || function () {
      __assign = Object.assign || function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return __assign.apply(this, arguments);
    };

    var DAYS_OF_WEEK;

    (function (DAYS_OF_WEEK) {
      DAYS_OF_WEEK[DAYS_OF_WEEK["SUNDAY"] = 0] = "SUNDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["MONDAY"] = 1] = "MONDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["TUESDAY"] = 2] = "TUESDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["WEDNESDAY"] = 3] = "WEDNESDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["THURSDAY"] = 4] = "THURSDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["FRIDAY"] = 5] = "FRIDAY";
      DAYS_OF_WEEK[DAYS_OF_WEEK["SATURDAY"] = 6] = "SATURDAY";
    })(DAYS_OF_WEEK || (DAYS_OF_WEEK = {}));

    var DEFAULT_WEEKEND_DAYS = [DAYS_OF_WEEK.SUNDAY, DAYS_OF_WEEK.SATURDAY];
    var DAYS_IN_WEEK = 7;
    var HOURS_IN_DAY = 24;
    var MINUTES_IN_HOUR = 60;
    var SECONDS_IN_DAY = 60 * 60 * 24;

    function getExcludedSeconds(dateAdapter, _a) {
      var startDate = _a.startDate,
          seconds = _a.seconds,
          excluded = _a.excluded,
          _b = _a.precision,
          precision = _b === void 0 ? 'days' : _b;

      if (excluded.length < 1) {
        return 0;
      }

      var addSeconds = dateAdapter.addSeconds,
          getDay = dateAdapter.getDay,
          addDays = dateAdapter.addDays;
      var endDate = addSeconds(startDate, seconds - 1);
      var dayStart = getDay(startDate);
      var dayEnd = getDay(endDate);
      var result = 0; // Calculated in seconds

      var current = startDate;

      var _loop_1 = function _loop_1() {
        var day = getDay(current);

        if (excluded.some(function (excludedDay) {
          return excludedDay === day;
        })) {
          result += calculateExcludedSeconds(dateAdapter, {
            dayStart: dayStart,
            dayEnd: dayEnd,
            day: day,
            precision: precision,
            startDate: startDate,
            endDate: endDate
          });
        }

        current = addDays(current, 1);
      };

      while (current < endDate) {
        _loop_1();
      }

      return result;
    }

    function calculateExcludedSeconds(dateAdapter, _a) {
      var precision = _a.precision,
          day = _a.day,
          dayStart = _a.dayStart,
          dayEnd = _a.dayEnd,
          startDate = _a.startDate,
          endDate = _a.endDate;
      var differenceInSeconds = dateAdapter.differenceInSeconds,
          endOfDay = dateAdapter.endOfDay,
          startOfDay = dateAdapter.startOfDay;

      if (precision === 'minutes') {
        if (day === dayStart) {
          return differenceInSeconds(endOfDay(startDate), startDate) + 1;
        } else if (day === dayEnd) {
          return differenceInSeconds(endDate, startOfDay(endDate)) + 1;
        }
      }

      return SECONDS_IN_DAY;
    }

    function getWeekViewEventSpan(dateAdapter, _a) {
      var event = _a.event,
          offset = _a.offset,
          startOfWeekDate = _a.startOfWeekDate,
          excluded = _a.excluded,
          _b = _a.precision,
          precision = _b === void 0 ? 'days' : _b,
          totalDaysInView = _a.totalDaysInView;
      var max = dateAdapter.max,
          differenceInSeconds = dateAdapter.differenceInSeconds,
          addDays = dateAdapter.addDays,
          endOfDay = dateAdapter.endOfDay,
          differenceInDays = dateAdapter.differenceInDays;
      var span = SECONDS_IN_DAY;
      var begin = max(event.start, startOfWeekDate);

      if (event.end) {
        switch (precision) {
          case 'minutes':
            span = differenceInSeconds(event.end, begin);
            break;

          default:
            span = differenceInDays(addDays(endOfDay(event.end), 1), begin) * SECONDS_IN_DAY;
            break;
        }
      }

      var offsetSeconds = offset * SECONDS_IN_DAY;
      var totalLength = offsetSeconds + span; // the best way to detect if an event is outside the week-view
      // is to check if the total span beginning (from startOfWeekDay or event start) exceeds the total days in the view

      var secondsInView = totalDaysInView * SECONDS_IN_DAY;

      if (totalLength > secondsInView) {
        span = secondsInView - offsetSeconds;
      }

      span -= getExcludedSeconds(dateAdapter, {
        startDate: begin,
        seconds: span,
        excluded: excluded,
        precision: precision
      });
      return span / SECONDS_IN_DAY;
    }

    function getWeekViewEventOffset(dateAdapter, _a) {
      var event = _a.event,
          startOfWeekDate = _a.startOfWeek,
          _b = _a.excluded,
          excluded = _b === void 0 ? [] : _b,
          _c = _a.precision,
          precision = _c === void 0 ? 'days' : _c;
      var differenceInDays = dateAdapter.differenceInDays,
          startOfDay = dateAdapter.startOfDay,
          differenceInSeconds = dateAdapter.differenceInSeconds;

      if (event.start < startOfWeekDate) {
        return 0;
      }

      var offset = 0;

      switch (precision) {
        case 'days':
          offset = differenceInDays(startOfDay(event.start), startOfWeekDate) * SECONDS_IN_DAY;
          break;

        case 'minutes':
          offset = differenceInSeconds(event.start, startOfWeekDate);
          break;
      }

      offset -= getExcludedSeconds(dateAdapter, {
        startDate: startOfWeekDate,
        seconds: offset,
        excluded: excluded,
        precision: precision
      });
      return Math.abs(offset / SECONDS_IN_DAY);
    }

    function isEventIsPeriod(dateAdapter, _a) {
      var event = _a.event,
          periodStart = _a.periodStart,
          periodEnd = _a.periodEnd;
      var isSameSecond = dateAdapter.isSameSecond;
      var eventStart = event.start;
      var eventEnd = event.end || event.start;

      if (eventStart > periodStart && eventStart < periodEnd) {
        return true;
      }

      if (eventEnd > periodStart && eventEnd < periodEnd) {
        return true;
      }

      if (eventStart < periodStart && eventEnd > periodEnd) {
        return true;
      }

      if (isSameSecond(eventStart, periodStart) || isSameSecond(eventStart, periodEnd)) {
        return true;
      }

      if (isSameSecond(eventEnd, periodStart) || isSameSecond(eventEnd, periodEnd)) {
        return true;
      }

      return false;
    }

    function getEventsInPeriod(dateAdapter, _a) {
      var events = _a.events,
          periodStart = _a.periodStart,
          periodEnd = _a.periodEnd;
      return events.filter(function (event) {
        return isEventIsPeriod(dateAdapter, {
          event: event,
          periodStart: periodStart,
          periodEnd: periodEnd
        });
      });
    }

    function getWeekDay(dateAdapter, _a) {
      var date = _a.date,
          _b = _a.weekendDays,
          weekendDays = _b === void 0 ? DEFAULT_WEEKEND_DAYS : _b;
      var startOfDay = dateAdapter.startOfDay,
          isSameDay = dateAdapter.isSameDay,
          getDay = dateAdapter.getDay;
      var today = startOfDay(new Date());
      var day = getDay(date);
      return {
        date: date,
        day: day,
        isPast: date < today,
        isToday: isSameDay(date, today),
        isFuture: date > today,
        isWeekend: weekendDays.indexOf(day) > -1
      };
    }

    function getWeekViewHeader(dateAdapter, _a) {
      var viewDate = _a.viewDate,
          weekStartsOn = _a.weekStartsOn,
          _b = _a.excluded,
          excluded = _b === void 0 ? [] : _b,
          weekendDays = _a.weekendDays,
          _c = _a.viewStart,
          viewStart = _c === void 0 ? dateAdapter.startOfWeek(viewDate, {
        weekStartsOn: weekStartsOn
      }) : _c,
          _d = _a.viewEnd,
          viewEnd = _d === void 0 ? dateAdapter.addDays(viewStart, DAYS_IN_WEEK) : _d;
      var addDays = dateAdapter.addDays,
          getDay = dateAdapter.getDay;
      var days = [];
      var date = viewStart;

      while (date < viewEnd) {
        if (!excluded.some(function (e) {
          return getDay(date) === e;
        })) {
          days.push(getWeekDay(dateAdapter, {
            date: date,
            weekendDays: weekendDays
          }));
        }

        date = addDays(date, 1);
      }

      return days;
    }

    function getDifferenceInDaysWithExclusions(dateAdapter, _a) {
      var date1 = _a.date1,
          date2 = _a.date2,
          excluded = _a.excluded;
      var date = date1;
      var diff = 0;

      while (date < date2) {
        if (excluded.indexOf(dateAdapter.getDay(date)) === -1) {
          diff++;
        }

        date = dateAdapter.addDays(date, 1);
      }

      return diff;
    }

    function getAllDayWeekEvents(dateAdapter, _a) {
      var events = _a.events,
          excluded = _a.excluded,
          precision = _a.precision,
          absolutePositionedEvents = _a.absolutePositionedEvents,
          viewStart = _a.viewStart,
          viewEnd = _a.viewEnd,
          eventsInPeriod = _a.eventsInPeriod;
      var differenceInSeconds = dateAdapter.differenceInSeconds,
          differenceInDays = dateAdapter.differenceInDays;
      var maxRange = getDifferenceInDaysWithExclusions(dateAdapter, {
        date1: viewStart,
        date2: viewEnd,
        excluded: excluded
      });
      var totalDaysInView = differenceInDays(viewEnd, viewStart) + 1;
      var eventsMapped = eventsInPeriod.filter(function (event) {
        return event.allDay;
      }).map(function (event) {
        var offset = getWeekViewEventOffset(dateAdapter, {
          event: event,
          startOfWeek: viewStart,
          excluded: excluded,
          precision: precision
        });
        var span = getWeekViewEventSpan(dateAdapter, {
          event: event,
          offset: offset,
          startOfWeekDate: viewStart,
          excluded: excluded,
          precision: precision,
          totalDaysInView: totalDaysInView
        });
        return {
          event: event,
          offset: offset,
          span: span
        };
      }).filter(function (e) {
        return e.offset < maxRange;
      }).filter(function (e) {
        return e.span > 0;
      }).map(function (entry) {
        return {
          event: entry.event,
          offset: entry.offset,
          span: entry.span,
          startsBeforeWeek: entry.event.start < viewStart,
          endsAfterWeek: (entry.event.end || entry.event.start) > viewEnd
        };
      }).sort(function (itemA, itemB) {
        var startSecondsDiff = differenceInSeconds(itemA.event.start, itemB.event.start);

        if (startSecondsDiff === 0) {
          return differenceInSeconds(itemB.event.end || itemB.event.start, itemA.event.end || itemA.event.start);
        }

        return startSecondsDiff;
      });
      var allDayEventRows = [];
      var allocatedEvents = [];
      eventsMapped.forEach(function (event, index) {
        if (allocatedEvents.indexOf(event) === -1) {
          allocatedEvents.push(event);
          var rowSpan_1 = event.span + event.offset;
          var otherRowEvents = eventsMapped.slice(index + 1).filter(function (nextEvent) {
            if (nextEvent.offset >= rowSpan_1 && rowSpan_1 + nextEvent.span <= totalDaysInView && allocatedEvents.indexOf(nextEvent) === -1) {
              var nextEventOffset = nextEvent.offset - rowSpan_1;

              if (!absolutePositionedEvents) {
                nextEvent.offset = nextEventOffset;
              }

              rowSpan_1 += nextEvent.span + nextEventOffset;
              allocatedEvents.push(nextEvent);
              return true;
            }
          });
          var weekEvents = [event].concat(otherRowEvents);
          var id = weekEvents.filter(function (weekEvent) {
            return weekEvent.event.id;
          }).map(function (weekEvent) {
            return weekEvent.event.id;
          }).join('-');
          allDayEventRows.push(__assign({
            row: weekEvents
          }, id ? {
            id: id
          } : {}));
        }
      });
      return allDayEventRows;
    }

    function getWeekViewHourGrid(dateAdapter, _a) {
      var events = _a.events,
          viewDate = _a.viewDate,
          hourSegments = _a.hourSegments,
          dayStart = _a.dayStart,
          dayEnd = _a.dayEnd,
          weekStartsOn = _a.weekStartsOn,
          excluded = _a.excluded,
          weekendDays = _a.weekendDays,
          segmentHeight = _a.segmentHeight,
          viewStart = _a.viewStart,
          viewEnd = _a.viewEnd;
      var dayViewHourGrid = getDayViewHourGrid(dateAdapter, {
        viewDate: viewDate,
        hourSegments: hourSegments,
        dayStart: dayStart,
        dayEnd: dayEnd
      });
      var weekDays = getWeekViewHeader(dateAdapter, {
        viewDate: viewDate,
        weekStartsOn: weekStartsOn,
        excluded: excluded,
        weekendDays: weekendDays,
        viewStart: viewStart,
        viewEnd: viewEnd
      });
      var setHours = dateAdapter.setHours,
          setMinutes = dateAdapter.setMinutes,
          getHours = dateAdapter.getHours,
          getMinutes = dateAdapter.getMinutes;
      return weekDays.map(function (day) {
        var dayView = getDayView(dateAdapter, {
          events: events,
          viewDate: day.date,
          hourSegments: hourSegments,
          dayStart: dayStart,
          dayEnd: dayEnd,
          segmentHeight: segmentHeight,
          eventWidth: 1
        });
        var hours = dayViewHourGrid.map(function (hour) {
          var segments = hour.segments.map(function (segment) {
            var date = setMinutes(setHours(day.date, getHours(segment.date)), getMinutes(segment.date));
            return __assign({}, segment, {
              date: date
            });
          });
          return __assign({}, hour, {
            segments: segments
          });
        });

        function getColumnCount(allEvents, prevOverlappingEvents) {
          var columnCount = Math.max.apply(Math, prevOverlappingEvents.map(function (iEvent) {
            return iEvent.left + 1;
          }));
          var nextOverlappingEvents = allEvents.filter(function (iEvent) {
            return iEvent.left >= columnCount;
          }).filter(function (iEvent) {
            return getOverLappingDayViewEvents(prevOverlappingEvents, iEvent.top, iEvent.top + iEvent.height).length > 0;
          });

          if (nextOverlappingEvents.length > 0) {
            return getColumnCount(allEvents, nextOverlappingEvents);
          } else {
            return columnCount;
          }
        }

        var mappedEvents = dayView.events.map(function (event) {
          var columnCount = getColumnCount(dayView.events, getOverLappingDayViewEvents(dayView.events, event.top, event.top + event.height));
          var width = 100 / columnCount;
          return __assign({}, event, {
            left: event.left * width,
            width: width
          });
        });
        return {
          hours: hours,
          date: day.date,
          events: mappedEvents.map(function (event) {
            var overLappingEvents = getOverLappingDayViewEvents(mappedEvents.filter(function (otherEvent) {
              return otherEvent.left > event.left;
            }), event.top, event.top + event.height);

            if (overLappingEvents.length > 0) {
              return __assign({}, event, {
                width: Math.min.apply(Math, overLappingEvents.map(function (otherEvent) {
                  return otherEvent.left;
                })) - event.left
              });
            }

            return event;
          })
        };
      });
    }

    function getWeekView(dateAdapter, _a) {
      var _b = _a.events,
          events = _b === void 0 ? [] : _b,
          viewDate = _a.viewDate,
          weekStartsOn = _a.weekStartsOn,
          _c = _a.excluded,
          excluded = _c === void 0 ? [] : _c,
          _d = _a.precision,
          precision = _d === void 0 ? 'days' : _d,
          _e = _a.absolutePositionedEvents,
          absolutePositionedEvents = _e === void 0 ? false : _e,
          hourSegments = _a.hourSegments,
          dayStart = _a.dayStart,
          dayEnd = _a.dayEnd,
          weekendDays = _a.weekendDays,
          segmentHeight = _a.segmentHeight,
          _f = _a.viewStart,
          viewStart = _f === void 0 ? dateAdapter.startOfWeek(viewDate, {
        weekStartsOn: weekStartsOn
      }) : _f,
          _g = _a.viewEnd,
          viewEnd = _g === void 0 ? dateAdapter.endOfWeek(viewDate, {
        weekStartsOn: weekStartsOn
      }) : _g;

      if (!events) {
        events = [];
      }

      var startOfDay = dateAdapter.startOfDay,
          endOfDay = dateAdapter.endOfDay;
      viewStart = startOfDay(viewStart);
      viewEnd = endOfDay(viewEnd);
      var eventsInPeriod = getEventsInPeriod(dateAdapter, {
        events: events,
        periodStart: viewStart,
        periodEnd: viewEnd
      });
      var header = getWeekViewHeader(dateAdapter, {
        viewDate: viewDate,
        weekStartsOn: weekStartsOn,
        excluded: excluded,
        weekendDays: weekendDays,
        viewStart: viewStart,
        viewEnd: viewEnd
      });
      return {
        allDayEventRows: getAllDayWeekEvents(dateAdapter, {
          events: events,
          excluded: excluded,
          precision: precision,
          absolutePositionedEvents: absolutePositionedEvents,
          viewStart: viewStart,
          viewEnd: viewEnd,
          eventsInPeriod: eventsInPeriod
        }),
        period: {
          events: eventsInPeriod,
          start: header[0].date,
          end: endOfDay(header[header.length - 1].date)
        },
        hourColumns: getWeekViewHourGrid(dateAdapter, {
          events: events,
          viewDate: viewDate,
          hourSegments: hourSegments,
          dayStart: dayStart,
          dayEnd: dayEnd,
          weekStartsOn: weekStartsOn,
          excluded: excluded,
          weekendDays: weekendDays,
          segmentHeight: segmentHeight,
          viewStart: viewStart,
          viewEnd: viewEnd
        })
      };
    }

    function getMonthView(dateAdapter, _a) {
      var _b = _a.events,
          events = _b === void 0 ? [] : _b,
          viewDate = _a.viewDate,
          weekStartsOn = _a.weekStartsOn,
          _c = _a.excluded,
          excluded = _c === void 0 ? [] : _c,
          _d = _a.viewStart,
          viewStart = _d === void 0 ? dateAdapter.startOfMonth(viewDate) : _d,
          _e = _a.viewEnd,
          viewEnd = _e === void 0 ? dateAdapter.endOfMonth(viewDate) : _e,
          weekendDays = _a.weekendDays;

      if (!events) {
        events = [];
      }

      var startOfWeek = dateAdapter.startOfWeek,
          endOfWeek = dateAdapter.endOfWeek,
          differenceInDays = dateAdapter.differenceInDays,
          startOfDay = dateAdapter.startOfDay,
          addHours = dateAdapter.addHours,
          endOfDay = dateAdapter.endOfDay,
          isSameMonth = dateAdapter.isSameMonth,
          getDay = dateAdapter.getDay,
          getMonth = dateAdapter.getMonth;
      var start = startOfWeek(viewStart, {
        weekStartsOn: weekStartsOn
      });
      var end = endOfWeek(viewEnd, {
        weekStartsOn: weekStartsOn
      });
      var eventsInMonth = getEventsInPeriod(dateAdapter, {
        events: events,
        periodStart: start,
        periodEnd: end
      });
      var initialViewDays = [];
      var previousDate;

      var _loop_2 = function _loop_2(i) {
        // hacky fix for https://github.com/mattlewis92/angular-calendar/issues/173
        var date;

        if (previousDate) {
          date = startOfDay(addHours(previousDate, HOURS_IN_DAY));

          if (previousDate.getTime() === date.getTime()) {
            // DST change, so need to add 25 hours

            /* istanbul ignore next */
            date = startOfDay(addHours(previousDate, HOURS_IN_DAY + 1));
          }

          previousDate = date;
        } else {
          date = previousDate = start;
        }

        if (!excluded.some(function (e) {
          return getDay(date) === e;
        })) {
          var day = getWeekDay(dateAdapter, {
            date: date,
            weekendDays: weekendDays
          });
          var eventsInPeriod = getEventsInPeriod(dateAdapter, {
            events: eventsInMonth,
            periodStart: startOfDay(date),
            periodEnd: endOfDay(date)
          });
          day.inMonth = isSameMonth(date, viewDate);
          day.events = eventsInPeriod;
          day.badgeTotal = eventsInPeriod.length;
          initialViewDays.push(day);
        }
      };

      for (var i = 0; i < differenceInDays(end, start) + 1; i++) {
        _loop_2(i);
      }

      var days = [];
      var totalDaysVisibleInWeek = DAYS_IN_WEEK - excluded.length;

      if (totalDaysVisibleInWeek < DAYS_IN_WEEK) {
        for (var i = 0; i < initialViewDays.length; i += totalDaysVisibleInWeek) {
          var row = initialViewDays.slice(i, i + totalDaysVisibleInWeek);
          var isRowInMonth = row.some(function (day) {
            return getMonth(day.date) === getMonth(viewDate);
          });

          if (isRowInMonth) {
            days = days.concat(row);
          }
        }
      } else {
        days = initialViewDays;
      }

      var rows = Math.floor(days.length / totalDaysVisibleInWeek);
      var rowOffsets = [];

      for (var i = 0; i < rows; i++) {
        rowOffsets.push(i * totalDaysVisibleInWeek);
      }

      return {
        rowOffsets: rowOffsets,
        totalDaysVisibleInWeek: totalDaysVisibleInWeek,
        days: days,
        period: {
          start: days[0].date,
          end: endOfDay(days[days.length - 1].date),
          events: eventsInMonth
        }
      };
    }

    function getOverLappingDayViewEvents(events, top, bottom) {
      return events.filter(function (previousEvent) {
        var previousEventTop = previousEvent.top;
        var previousEventBottom = previousEvent.top + previousEvent.height;

        if (top < previousEventBottom && previousEventBottom < bottom) {
          return true;
        } else if (top < previousEventTop && previousEventTop < bottom) {
          return true;
        } else if (previousEventTop <= top && bottom <= previousEventBottom) {
          return true;
        }

        return false;
      });
    }

    function getDayView(dateAdapter, _a) {
      var _b = _a.events,
          events = _b === void 0 ? [] : _b,
          viewDate = _a.viewDate,
          hourSegments = _a.hourSegments,
          dayStart = _a.dayStart,
          dayEnd = _a.dayEnd,
          eventWidth = _a.eventWidth,
          segmentHeight = _a.segmentHeight;

      if (!events) {
        events = [];
      }

      var setMinutes = dateAdapter.setMinutes,
          setHours = dateAdapter.setHours,
          startOfDay = dateAdapter.startOfDay,
          startOfMinute = dateAdapter.startOfMinute,
          endOfDay = dateAdapter.endOfDay,
          differenceInMinutes = dateAdapter.differenceInMinutes;
      var startOfView = setMinutes(setHours(startOfDay(viewDate), sanitiseHours(dayStart.hour)), sanitiseMinutes(dayStart.minute));
      var endOfView = setMinutes(setHours(startOfMinute(endOfDay(viewDate)), sanitiseHours(dayEnd.hour)), sanitiseMinutes(dayEnd.minute));
      var previousDayEvents = [];
      var eventsInPeriod = getEventsInPeriod(dateAdapter, {
        events: events.filter(function (event) {
          return !event.allDay;
        }),
        periodStart: startOfView,
        periodEnd: endOfView
      });
      var dayViewEvents = eventsInPeriod.sort(function (eventA, eventB) {
        return eventA.start.valueOf() - eventB.start.valueOf();
      }).map(function (event) {
        var eventStart = event.start;
        var eventEnd = event.end || eventStart;
        var startsBeforeDay = eventStart < startOfView;
        var endsAfterDay = eventEnd > endOfView;
        var hourHeightModifier = hourSegments * segmentHeight / MINUTES_IN_HOUR;
        var top = 0;

        if (eventStart > startOfView) {
          top += differenceInMinutes(eventStart, startOfView);
        }

        top *= hourHeightModifier;
        var startDate = startsBeforeDay ? startOfView : eventStart;
        var endDate = endsAfterDay ? endOfView : eventEnd;
        var height = differenceInMinutes(endDate, startDate);

        if (!event.end) {
          height = segmentHeight;
        } else {
          height *= hourHeightModifier;
        }

        var bottom = top + height;
        var overlappingPreviousEvents = getOverLappingDayViewEvents(previousDayEvents, top, bottom);
        var left = 0;

        while (overlappingPreviousEvents.some(function (previousEvent) {
          return previousEvent.left === left;
        })) {
          left += eventWidth;
        }

        var dayEvent = {
          event: event,
          height: height,
          width: eventWidth,
          top: top,
          left: left,
          startsBeforeDay: startsBeforeDay,
          endsAfterDay: endsAfterDay
        };
        previousDayEvents.push(dayEvent);
        return dayEvent;
      });
      var width = Math.max.apply(Math, dayViewEvents.map(function (event) {
        return event.left + event.width;
      }));
      var allDayEvents = getEventsInPeriod(dateAdapter, {
        events: events.filter(function (event) {
          return event.allDay;
        }),
        periodStart: startOfDay(startOfView),
        periodEnd: endOfDay(endOfView)
      });
      return {
        events: dayViewEvents,
        width: width,
        allDayEvents: allDayEvents,
        period: {
          events: eventsInPeriod,
          start: startOfView,
          end: endOfView
        }
      };
    }

    function sanitiseHours(hours) {
      return Math.max(Math.min(23, hours), 0);
    }

    function sanitiseMinutes(minutes) {
      return Math.max(Math.min(59, minutes), 0);
    }

    function getDayViewHourGrid(dateAdapter, _a) {
      var viewDate = _a.viewDate,
          hourSegments = _a.hourSegments,
          dayStart = _a.dayStart,
          dayEnd = _a.dayEnd;
      var setMinutes = dateAdapter.setMinutes,
          setHours = dateAdapter.setHours,
          startOfDay = dateAdapter.startOfDay,
          startOfMinute = dateAdapter.startOfMinute,
          endOfDay = dateAdapter.endOfDay,
          addMinutes = dateAdapter.addMinutes,
          addHours = dateAdapter.addHours;
      var hours = [];
      var startOfView = setMinutes(setHours(startOfDay(viewDate), sanitiseHours(dayStart.hour)), sanitiseMinutes(dayStart.minute));
      var endOfView = setMinutes(setHours(startOfMinute(endOfDay(viewDate)), sanitiseHours(dayEnd.hour)), sanitiseMinutes(dayEnd.minute));
      var segmentDuration = MINUTES_IN_HOUR / hourSegments;
      var startOfViewDay = startOfDay(viewDate);

      for (var i = 0; i < HOURS_IN_DAY; i++) {
        var segments = [];

        for (var j = 0; j < hourSegments; j++) {
          var date = addMinutes(addHours(startOfViewDay, i), j * segmentDuration);

          if (date >= startOfView && date < endOfView) {
            segments.push({
              date: date,
              isStart: j === 0
            });
          }
        }

        if (segments.length > 0) {
          hours.push({
            segments: segments
          });
        }
      }

      return hours;
    }

    var EventValidationErrorMessage;

    (function (EventValidationErrorMessage) {
      EventValidationErrorMessage["NotArray"] = "Events must be an array";
      EventValidationErrorMessage["StartPropertyMissing"] = "Event is missing the `start` property";
      EventValidationErrorMessage["StartPropertyNotDate"] = "Event `start` property should be a javascript date object. Do `new Date(event.start)` to fix it.";
      EventValidationErrorMessage["EndPropertyNotDate"] = "Event `end` property should be a javascript date object. Do `new Date(event.end)` to fix it.";
      EventValidationErrorMessage["EndsBeforeStart"] = "Event `start` property occurs after the `end`";
    })(EventValidationErrorMessage || (EventValidationErrorMessage = {}));

    function validateEvents(events, log) {
      var isValid = true;

      function isError(msg, event) {
        log(msg, event);
        isValid = false;
      }

      if (!Array.isArray(events)) {
        log(EventValidationErrorMessage.NotArray, events);
        return false;
      }

      events.forEach(function (event) {
        if (!event.start) {
          isError(EventValidationErrorMessage.StartPropertyMissing, event);
        } else if (!(event.start instanceof Date)) {
          isError(EventValidationErrorMessage.StartPropertyNotDate, event);
        }

        if (event.end) {
          if (!(event.end instanceof Date)) {
            isError(EventValidationErrorMessage.EndPropertyNotDate, event);
          }

          if (event.start > event.end) {
            isError(EventValidationErrorMessage.EndsBeforeStart, event);
          }
        }
      });
      return isValid;
    } //# sourceMappingURL=calendar-utils.js.map

    /***/

  },

  /***/
  "./node_modules/calendar-utils/date-adapters/date-fns/index.js":
  /*!*********************************************************************!*\
    !*** ./node_modules/calendar-utils/date-adapters/date-fns/index.js ***!
    \*********************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesCalendarUtilsDateAdaptersDateFnsIndexJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var addDays = __webpack_require__(
    /*! date-fns/add_days/index */
    "./node_modules/date-fns/add_days/index.js");

    var addHours = __webpack_require__(
    /*! date-fns/add_hours/index */
    "./node_modules/date-fns/add_hours/index.js");

    var addMinutes = __webpack_require__(
    /*! date-fns/add_minutes/index */
    "./node_modules/date-fns/add_minutes/index.js");

    var addSeconds = __webpack_require__(
    /*! date-fns/add_seconds/index */
    "./node_modules/date-fns/add_seconds/index.js");

    var differenceInDays = __webpack_require__(
    /*! date-fns/difference_in_days/index */
    "./node_modules/date-fns/difference_in_days/index.js");

    var differenceInMinutes = __webpack_require__(
    /*! date-fns/difference_in_minutes/index */
    "./node_modules/date-fns/difference_in_minutes/index.js");

    var differenceInSeconds = __webpack_require__(
    /*! date-fns/difference_in_seconds/index */
    "./node_modules/date-fns/difference_in_seconds/index.js");

    var endOfDay = __webpack_require__(
    /*! date-fns/end_of_day/index */
    "./node_modules/date-fns/end_of_day/index.js");

    var endOfMonth = __webpack_require__(
    /*! date-fns/end_of_month/index */
    "./node_modules/date-fns/end_of_month/index.js");

    var endOfWeek = __webpack_require__(
    /*! date-fns/end_of_week/index */
    "./node_modules/date-fns/end_of_week/index.js");

    var getDay = __webpack_require__(
    /*! date-fns/get_day/index */
    "./node_modules/date-fns/get_day/index.js");

    var getMonth = __webpack_require__(
    /*! date-fns/get_month/index */
    "./node_modules/date-fns/get_month/index.js");

    var isSameDay = __webpack_require__(
    /*! date-fns/is_same_day/index */
    "./node_modules/date-fns/is_same_day/index.js");

    var isSameMonth = __webpack_require__(
    /*! date-fns/is_same_month/index */
    "./node_modules/date-fns/is_same_month/index.js");

    var isSameSecond = __webpack_require__(
    /*! date-fns/is_same_second/index */
    "./node_modules/date-fns/is_same_second/index.js");

    var max = __webpack_require__(
    /*! date-fns/max/index */
    "./node_modules/date-fns/max/index.js");

    var setHours = __webpack_require__(
    /*! date-fns/set_hours/index */
    "./node_modules/date-fns/set_hours/index.js");

    var setMinutes = __webpack_require__(
    /*! date-fns/set_minutes/index */
    "./node_modules/date-fns/set_minutes/index.js");

    var startOfDay = __webpack_require__(
    /*! date-fns/start_of_day/index */
    "./node_modules/date-fns/start_of_day/index.js");

    var startOfMinute = __webpack_require__(
    /*! date-fns/start_of_minute/index */
    "./node_modules/date-fns/start_of_minute/index.js");

    var startOfMonth = __webpack_require__(
    /*! date-fns/start_of_month/index */
    "./node_modules/date-fns/start_of_month/index.js");

    var startOfWeek = __webpack_require__(
    /*! date-fns/start_of_week/index */
    "./node_modules/date-fns/start_of_week/index.js");

    var getHours = __webpack_require__(
    /*! date-fns/get_hours/index */
    "./node_modules/date-fns/get_hours/index.js");

    var getMinutes = __webpack_require__(
    /*! date-fns/get_minutes/index */
    "./node_modules/date-fns/get_minutes/index.js");

    function adapterFactory() {
      return {
        addDays: addDays,
        addHours: addHours,
        addMinutes: addMinutes,
        addSeconds: addSeconds,
        differenceInDays: differenceInDays,
        differenceInMinutes: differenceInMinutes,
        differenceInSeconds: differenceInSeconds,
        endOfDay: endOfDay,
        endOfMonth: endOfMonth,
        endOfWeek: endOfWeek,
        getDay: getDay,
        getMonth: getMonth,
        isSameDay: isSameDay,
        isSameMonth: isSameMonth,
        isSameSecond: isSameSecond,
        max: max,
        setHours: setHours,
        setMinutes: setMinutes,
        startOfDay: startOfDay,
        startOfMinute: startOfMinute,
        startOfMonth: startOfMonth,
        startOfWeek: startOfWeek,
        getHours: getHours,
        getMinutes: getMinutes
      };
    }

    exports.adapterFactory = adapterFactory; //# sourceMappingURL=index.js.map

    /***/
  },

  /***/
  "./node_modules/create-point-cb/dist/bundle.js":
  /*!*****************************************************!*\
    !*** ./node_modules/create-point-cb/dist/bundle.js ***!
    \*****************************************************/

  /*! no static exports found */

  /***/
  function node_modulesCreatePointCbDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    var typeFunc = __webpack_require__(
    /*! type-func */
    "./node_modules/type-func/dist/bundle.js");

    function createPointCB(object, options) {
      // A persistent object (as opposed to returned object) is used to save memory
      // This is good to prevent layout thrashing, or for games, and such
      // NOTE
      // This uses IE fixes which should be OK to remove some day. :)
      // Some speed will be gained by removal of these.
      // pointCB should be saved in a variable on return
      // This allows the usage of element.removeEventListener
      options = options || {};
      var allowUpdate = typeFunc["boolean"](options.allowUpdate, true);
      /*if(typeof options.allowUpdate === 'function'){
          allowUpdate = options.allowUpdate;
      }else{
          allowUpdate = function(){return true;};
      }*/

      return function pointCB(event) {
        event = event || window.event; // IE-ism

        object.target = event.target || event.srcElement || event.originalTarget;
        object.element = this;
        object.type = event.type;

        if (!allowUpdate(event)) {
          return;
        } // Support touch
        // http://www.creativebloq.com/javascript/make-your-site-work-touch-devices-51411644


        if (event.targetTouches) {
          object.x = event.targetTouches[0].clientX;
          object.y = event.targetTouches[0].clientY;
          object.pageX = event.targetTouches[0].pageX;
          object.pageY = event.targetTouches[0].pageY;
          object.screenX = event.targetTouches[0].screenX;
          object.screenY = event.targetTouches[0].screenY;
        } else {
          // If pageX/Y aren't available and clientX/Y are,
          // calculate pageX/Y - logic taken from jQuery.
          // (This is to support old IE)
          // NOTE Hopefully this can be removed soon.
          if (event.pageX === null && event.clientX !== null) {
            var eventDoc = event.target && event.target.ownerDocument || document;
            var doc = eventDoc.documentElement;
            var body = eventDoc.body;
            object.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
            object.pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
          } else {
            object.pageX = event.pageX;
            object.pageY = event.pageY;
          } // pageX, and pageY change with page scroll
          // so we're not going to use those for x, and y.
          // NOTE Most browsers also alias clientX/Y with x/y
          // so that's something to consider down the road.


          object.x = event.clientX;
          object.y = event.clientY;
          object.screenX = event.screenX;
          object.screenY = event.screenY;
        }

        object.clientX = object.x;
        object.clientY = object.y;
      }; //NOTE Remember accessibility, Aria roles, and labels.
    }
    /*
    git remote add origin https://github.com/hollowdoor/create_point_cb.git
    git push -u origin master
    */


    module.exports = createPointCB; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./node_modules/dom-autoscroller/dist/bundle.js":
  /*!******************************************************!*\
    !*** ./node_modules/dom-autoscroller/dist/bundle.js ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function node_modulesDomAutoscrollerDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    function _interopDefault(ex) {
      return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var typeFunc = __webpack_require__(
    /*! type-func */
    "./node_modules/type-func/dist/bundle.js");

    var animationFramePolyfill = __webpack_require__(
    /*! animation-frame-polyfill */
    "./node_modules/animation-frame-polyfill/lib/animation-frame-polyfill.cjs.js");

    var domSet = __webpack_require__(
    /*! dom-set */
    "./node_modules/dom-set/dist/bundle.js");

    var domPlane = __webpack_require__(
    /*! dom-plane */
    "./node_modules/dom-plane/dist/bundle.js");

    var mousemoveDispatcher = _interopDefault(__webpack_require__(
    /*! dom-mousemove-dispatcher */
    "./node_modules/dom-mousemove-dispatcher/dist/bundle.js"));

    function AutoScroller(elements, options) {
      if (options === void 0) options = {};
      var self = this;
      var maxSpeed = 4,
          scrolling = false;
      this.margin = options.margin || -1; //this.scrolling = false;

      this.scrollWhenOutside = options.scrollWhenOutside || false;
      var point = {},
          pointCB = domPlane.createPointCB(point),
          dispatcher = mousemoveDispatcher(),
          down = false;
      window.addEventListener('mousemove', pointCB, false);
      window.addEventListener('touchmove', pointCB, false);

      if (!isNaN(options.maxSpeed)) {
        maxSpeed = options.maxSpeed;
      }

      this.autoScroll = typeFunc["boolean"](options.autoScroll);
      this.syncMove = typeFunc["boolean"](options.syncMove, false);

      this.destroy = function (forceCleanAnimation) {
        window.removeEventListener('mousemove', pointCB, false);
        window.removeEventListener('touchmove', pointCB, false);
        window.removeEventListener('mousedown', onDown, false);
        window.removeEventListener('touchstart', onDown, false);
        window.removeEventListener('mouseup', onUp, false);
        window.removeEventListener('touchend', onUp, false);
        window.removeEventListener('pointerup', onUp, false);
        window.removeEventListener('mouseleave', onMouseOut, false);
        window.removeEventListener('mousemove', onMove, false);
        window.removeEventListener('touchmove', onMove, false);
        window.removeEventListener('scroll', setScroll, true);
        elements = [];

        if (forceCleanAnimation) {
          cleanAnimation();
        }
      };

      this.add = function () {
        var element = [],
            len = arguments.length;

        while (len--) {
          element[len] = arguments[len];
        }

        domSet.addElements.apply(void 0, [elements].concat(element));
        return this;
      };

      this.remove = function () {
        var element = [],
            len = arguments.length;

        while (len--) {
          element[len] = arguments[len];
        }

        return domSet.removeElements.apply(void 0, [elements].concat(element));
      };

      var hasWindow = null,
          windowAnimationFrame;

      if (Object.prototype.toString.call(elements) !== '[object Array]') {
        elements = [elements];
      }

      (function (temp) {
        elements = [];
        temp.forEach(function (element) {
          if (element === window) {
            hasWindow = window;
          } else {
            self.add(element);
          }
        });
      })(elements);

      Object.defineProperties(this, {
        down: {
          get: function get() {
            return down;
          }
        },
        maxSpeed: {
          get: function get() {
            return maxSpeed;
          }
        },
        point: {
          get: function get() {
            return point;
          }
        },
        scrolling: {
          get: function get() {
            return scrolling;
          }
        }
      });
      var n = 0,
          current = null,
          animationFrame;
      window.addEventListener('mousedown', onDown, false);
      window.addEventListener('touchstart', onDown, false);
      window.addEventListener('mouseup', onUp, false);
      window.addEventListener('touchend', onUp, false);
      /*
      IE does not trigger mouseup event when scrolling.
      It is a known issue that Microsoft won't fix.
      https://connect.microsoft.com/IE/feedback/details/783058/scrollbar-trigger-mousedown-but-not-mouseup
      IE supports pointer events instead
      */

      window.addEventListener('pointerup', onUp, false);
      window.addEventListener('mousemove', onMove, false);
      window.addEventListener('touchmove', onMove, false);
      window.addEventListener('mouseleave', onMouseOut, false);
      window.addEventListener('scroll', setScroll, true);

      function setScroll(e) {
        for (var i = 0; i < elements.length; i++) {
          if (elements[i] === e.target) {
            scrolling = true;
            break;
          }
        }

        if (scrolling) {
          animationFramePolyfill.requestAnimationFrame(function () {
            return scrolling = false;
          });
        }
      }

      function onDown() {
        down = true;
      }

      function onUp() {
        down = false;
        cleanAnimation();
      }

      function cleanAnimation() {
        animationFramePolyfill.cancelAnimationFrame(animationFrame);
        animationFramePolyfill.cancelAnimationFrame(windowAnimationFrame);
      }

      function onMouseOut() {
        down = false;
      }

      function getTarget(target) {
        if (!target) {
          return null;
        }

        if (current === target) {
          return target;
        }

        if (domSet.hasElement(elements, target)) {
          return target;
        }

        while (target = target.parentNode) {
          if (domSet.hasElement(elements, target)) {
            return target;
          }
        }

        return null;
      }

      function getElementUnderPoint() {
        var underPoint = null;

        for (var i = 0; i < elements.length; i++) {
          if (inside(point, elements[i])) {
            underPoint = elements[i];
          }
        }

        return underPoint;
      }

      function onMove(event) {
        if (!self.autoScroll()) {
          return;
        }

        if (event['dispatched']) {
          return;
        }

        var target = event.target,
            body = document.body;

        if (current && !inside(point, current)) {
          if (!self.scrollWhenOutside) {
            current = null;
          }
        }

        if (target && target.parentNode === body) {
          //The special condition to improve speed.
          target = getElementUnderPoint();
        } else {
          target = getTarget(target);

          if (!target) {
            target = getElementUnderPoint();
          }
        }

        if (target && target !== current) {
          current = target;
        }

        if (hasWindow) {
          animationFramePolyfill.cancelAnimationFrame(windowAnimationFrame);
          windowAnimationFrame = animationFramePolyfill.requestAnimationFrame(scrollWindow);
        }

        if (!current) {
          return;
        }

        animationFramePolyfill.cancelAnimationFrame(animationFrame);
        animationFrame = animationFramePolyfill.requestAnimationFrame(scrollTick);
      }

      function scrollWindow() {
        autoScroll(hasWindow);
        animationFramePolyfill.cancelAnimationFrame(windowAnimationFrame);
        windowAnimationFrame = animationFramePolyfill.requestAnimationFrame(scrollWindow);
      }

      function scrollTick() {
        if (!current) {
          return;
        }

        autoScroll(current);
        animationFramePolyfill.cancelAnimationFrame(animationFrame);
        animationFrame = animationFramePolyfill.requestAnimationFrame(scrollTick);
      }

      function autoScroll(el) {
        var rect = domPlane.getClientRect(el),
            scrollx,
            scrolly;

        if (point.x < rect.left + self.margin) {
          scrollx = Math.floor(Math.max(-1, (point.x - rect.left) / self.margin - 1) * self.maxSpeed);
        } else if (point.x > rect.right - self.margin) {
          scrollx = Math.ceil(Math.min(1, (point.x - rect.right) / self.margin + 1) * self.maxSpeed);
        } else {
          scrollx = 0;
        }

        if (point.y < rect.top + self.margin) {
          scrolly = Math.floor(Math.max(-1, (point.y - rect.top) / self.margin - 1) * self.maxSpeed);
        } else if (point.y > rect.bottom - self.margin) {
          scrolly = Math.ceil(Math.min(1, (point.y - rect.bottom) / self.margin + 1) * self.maxSpeed);
        } else {
          scrolly = 0;
        }

        if (self.syncMove()) {
          /*
          Notes about mousemove event dispatch.
          screen(X/Y) should need to be updated.
          Some other properties might need to be set.
          Keep the syncMove option default false until all inconsistencies are taken care of.
          */
          dispatcher.dispatch(el, {
            pageX: point.pageX + scrollx,
            pageY: point.pageY + scrolly,
            clientX: point.x + scrollx,
            clientY: point.y + scrolly
          });
        }

        setTimeout(function () {
          if (scrolly) {
            scrollY(el, scrolly);
          }

          if (scrollx) {
            scrollX(el, scrollx);
          }
        });
      }

      function scrollY(el, amount) {
        if (el === window) {
          window.scrollTo(el.pageXOffset, el.pageYOffset + amount);
        } else {
          el.scrollTop += amount;
        }
      }

      function scrollX(el, amount) {
        if (el === window) {
          window.scrollTo(el.pageXOffset + amount, el.pageYOffset);
        } else {
          el.scrollLeft += amount;
        }
      }
    }

    function AutoScrollerFactory(element, options) {
      return new AutoScroller(element, options);
    }

    function inside(point, el, rect) {
      if (!rect) {
        return domPlane.pointInside(point, el);
      } else {
        return point.y > rect.top && point.y < rect.bottom && point.x > rect.left && point.x < rect.right;
      }
    }
    /*
    git remote add origin https://github.com/hollowdoor/dom_autoscroller.git
    git push -u origin master
    */


    module.exports = AutoScrollerFactory; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./node_modules/dom-mousemove-dispatcher/dist/bundle.js":
  /*!**************************************************************!*\
    !*** ./node_modules/dom-mousemove-dispatcher/dist/bundle.js ***!
    \**************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesDomMousemoveDispatcherDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    var objectCreate = void 0;

    if (typeof Object.create != 'function') {
      objectCreate = function (undefined) {
        var Temp = function Temp() {};

        return function (prototype, propertiesObject) {
          if (prototype !== Object(prototype) && prototype !== null) {
            throw TypeError('Argument must be an object, or null');
          }

          Temp.prototype = prototype || {};
          var result = new Temp();
          Temp.prototype = null;

          if (propertiesObject !== undefined) {
            Object.defineProperties(result, propertiesObject);
          } // to imitate the case of Object.create(null)


          if (prototype === null) {
            result.__proto__ = null;
          }

          return result;
        };
      }();
    } else {
      objectCreate = Object.create;
    }

    var objectCreate$1 = objectCreate;
    var mouseEventProps = ['altKey', 'button', 'buttons', 'clientX', 'clientY', 'ctrlKey', 'metaKey', 'movementX', 'movementY', 'offsetX', 'offsetY', 'pageX', 'pageY', 'region', 'relatedTarget', 'screenX', 'screenY', 'shiftKey', 'which', 'x', 'y'];

    function createDispatcher(element) {
      var defaultSettings = {
        screenX: 0,
        screenY: 0,
        clientX: 0,
        clientY: 0,
        ctrlKey: false,
        shiftKey: false,
        altKey: false,
        metaKey: false,
        button: 0,
        buttons: 1,
        relatedTarget: null,
        region: null
      };

      if (element !== undefined) {
        element.addEventListener('mousemove', onMove);
      }

      function onMove(e) {
        for (var i = 0; i < mouseEventProps.length; i++) {
          defaultSettings[mouseEventProps[i]] = e[mouseEventProps[i]];
        }
      }

      var dispatch = function () {
        if (MouseEvent) {
          return function m1(element, initMove, data) {
            var evt = new MouseEvent('mousemove', createMoveInit(defaultSettings, initMove)); //evt.dispatched = 'mousemove';

            setSpecial(evt, data);
            return element.dispatchEvent(evt);
          };
        } else if (typeof document.createEvent === 'function') {
          return function m2(element, initMove, data) {
            var settings = createMoveInit(defaultSettings, initMove);
            var evt = document.createEvent('MouseEvents');
            evt.initMouseEvent("mousemove", true, //can bubble
            true, //cancelable
            window, //view
            0, //detail
            settings.screenX, //0, //screenX
            settings.screenY, //0, //screenY
            settings.clientX, //80, //clientX
            settings.clientY, //20, //clientY
            settings.ctrlKey, //false, //ctrlKey
            settings.altKey, //false, //altKey
            settings.shiftKey, //false, //shiftKey
            settings.metaKey, //false, //metaKey
            settings.button, //0, //button
            settings.relatedTarget //null //relatedTarget
            ); //evt.dispatched = 'mousemove';

            setSpecial(evt, data);
            return element.dispatchEvent(evt);
          };
        } else if (typeof document.createEventObject === 'function') {
          return function m3(element, initMove, data) {
            var evt = document.createEventObject();
            var settings = createMoveInit(defaultSettings, initMove);

            for (var name in settings) {
              evt[name] = settings[name];
            } //evt.dispatched = 'mousemove';


            setSpecial(evt, data);
            return element.dispatchEvent(evt);
          };
        }
      }();

      function destroy() {
        if (element) element.removeEventListener('mousemove', onMove, false);
        defaultSettings = null;
      }

      return {
        destroy: destroy,
        dispatch: dispatch
      };
    }

    function createMoveInit(defaultSettings, initMove) {
      initMove = initMove || {};
      var settings = objectCreate$1(defaultSettings);

      for (var i = 0; i < mouseEventProps.length; i++) {
        if (initMove[mouseEventProps[i]] !== undefined) settings[mouseEventProps[i]] = initMove[mouseEventProps[i]];
      }

      return settings;
    }

    function setSpecial(e, data) {
      console.log('data ', data);
      e.data = data || {};
      e.dispatched = 'mousemove';
    }
    /*
    http://marcgrabanski.com/simulating-mouse-click-events-in-javascript/
    */


    module.exports = createDispatcher; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./node_modules/dom-plane/dist/bundle.js":
  /*!***********************************************!*\
    !*** ./node_modules/dom-plane/dist/bundle.js ***!
    \***********************************************/

  /*! no static exports found */

  /***/
  function node_modulesDomPlaneDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, '__esModule', {
      value: true
    });

    function _interopDefault(ex) {
      return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var createPointCb = _interopDefault(__webpack_require__(
    /*! create-point-cb */
    "./node_modules/create-point-cb/dist/bundle.js"));

    function createWindowRect() {
      var props = {
        top: {
          value: 0,
          enumerable: true
        },
        left: {
          value: 0,
          enumerable: true
        },
        right: {
          value: window.innerWidth,
          enumerable: true
        },
        bottom: {
          value: window.innerHeight,
          enumerable: true
        },
        width: {
          value: window.innerWidth,
          enumerable: true
        },
        height: {
          value: window.innerHeight,
          enumerable: true
        },
        x: {
          value: 0,
          enumerable: true
        },
        y: {
          value: 0,
          enumerable: true
        }
      };

      if (Object.create) {
        return Object.create({}, props);
      } else {
        var rect = {};
        Object.defineProperties(rect, props);
        return rect;
      }
    }

    function getClientRect(el) {
      if (el === window) {
        return createWindowRect();
      } else {
        try {
          var rect = el.getBoundingClientRect();

          if (rect.x === undefined) {
            rect.x = rect.left;
            rect.y = rect.top;
          }

          return rect;
        } catch (e) {
          throw new TypeError("Can't call getBoundingClientRect on " + el);
        }
      }
    }

    function pointInside(point, el) {
      var rect = getClientRect(el);
      return point.y > rect.top && point.y < rect.bottom && point.x > rect.left && point.x < rect.right;
    }

    exports.createPointCB = createPointCb;
    exports.getClientRect = getClientRect;
    exports.pointInside = pointInside; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./node_modules/dom-set/dist/bundle.js":
  /*!*********************************************!*\
    !*** ./node_modules/dom-set/dist/bundle.js ***!
    \*********************************************/

  /*! no static exports found */

  /***/
  function node_modulesDomSetDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, '__esModule', {
      value: true
    });

    function _interopDefault(ex) {
      return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var arrayFrom = _interopDefault(__webpack_require__(
    /*! array-from */
    "./node_modules/array-from/index.js"));

    var isArray = _interopDefault(__webpack_require__(
    /*! is-array */
    "./node_modules/is-array/index.js"));

    var isElement = _interopDefault(__webpack_require__(
    /*! iselement */
    "./node_modules/iselement/module/index.js"));

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
    };
    /**
     * Returns `true` if provided input is Element.
     * @name isElement
     * @param {*} [input]
     * @returns {boolean}
     */


    var isElement$1 = function isElement$1(input) {
      return input != null && (typeof input === 'undefined' ? 'undefined' : _typeof(input)) === 'object' && input.nodeType === 1 && _typeof(input.style) === 'object' && _typeof(input.ownerDocument) === 'object';
    };

    function select(selector) {
      if (typeof selector === 'string') {
        try {
          return document.querySelector(selector);
        } catch (e) {
          throw e;
        }
      } else if (isElement(selector)) {
        return selector;
      }
    }

    function selectAll(selector) {
      if (typeof selector === 'string') {
        return Array.prototype.slice.apply(document.querySelectorAll(selector));
      } else if (isArray(selector)) {
        return selector.map(select);
      } else if ('length' in selector) {
        return arrayFrom(selector).map(select);
      }
    }

    function indexOfElement(elements, element) {
      element = resolveElement(element, true);

      if (!isElement$1(element)) {
        return -1;
      }

      for (var i = 0; i < elements.length; i++) {
        if (elements[i] === element) {
          return i;
        }
      }

      return -1;
    }

    function hasElement(elements, element) {
      return -1 !== indexOfElement(elements, element);
    }

    function domListOf(arr) {
      if (!arr) {
        return [];
      }

      try {
        if (typeof arr === 'string') {
          return arrayFrom(document.querySelectorAll(arr));
        } else if (isArray(arr)) {
          return arr.map(resolveElement);
        } else {
          if (typeof arr.length === 'undefined') {
            return [resolveElement(arr)];
          }

          return arrayFrom(arr, resolveElement);
        }
      } catch (e) {
        throw new Error(e);
      }
    }

    function concatElementLists() {
      var lists = [],
          len = arguments.length;

      while (len--) {
        lists[len] = arguments[len];
      }

      return lists.reduce(function (last, list) {
        return list.length ? last : last.concat(domListOf(list));
      }, []);
    }

    function pushElements(elements, toAdd) {
      for (var i = 0; i < toAdd.length; i++) {
        if (!hasElement(elements, toAdd[i])) {
          elements.push(toAdd[i]);
        }
      }

      return toAdd;
    }

    function addElements(elements) {
      var toAdd = [],
          len = arguments.length - 1;

      while (len-- > 0) {
        toAdd[len] = arguments[len + 1];
      }

      toAdd = toAdd.map(resolveElement);
      return pushElements(elements, toAdd);
    }

    function removeElements(elements) {
      var toRemove = [],
          len = arguments.length - 1;

      while (len-- > 0) {
        toRemove[len] = arguments[len + 1];
      }

      return toRemove.map(resolveElement).reduce(function (last, e) {
        var index = indexOfElement(elements, e);

        if (index !== -1) {
          return last.concat(elements.splice(index, 1));
        }

        return last;
      }, []);
    }

    function resolveElement(element, noThrow) {
      if (typeof element === 'string') {
        try {
          return document.querySelector(element);
        } catch (e) {
          throw e;
        }
      }

      if (!isElement$1(element) && !noThrow) {
        throw new TypeError(element + " is not a DOM element.");
      }

      return element;
    }

    exports.indexOfElement = indexOfElement;
    exports.hasElement = hasElement;
    exports.domListOf = domListOf;
    exports.concatElementLists = concatElementLists;
    exports.addElements = addElements;
    exports.removeElements = removeElements;
    exports.resolveElement = resolveElement;
    exports.select = select;
    exports.selectAll = selectAll; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./node_modules/is-array/index.js":
  /*!****************************************!*\
    !*** ./node_modules/is-array/index.js ***!
    \****************************************/

  /*! no static exports found */

  /***/
  function node_modulesIsArrayIndexJs(module, exports) {
    /**
     * isArray
     */
    var isArray = Array.isArray;
    /**
     * toString
     */

    var str = Object.prototype.toString;
    /**
     * Whether or not the given `val`
     * is an array.
     *
     * example:
     *
     *        isArray([]);
     *        // > true
     *        isArray(arguments);
     *        // > false
     *        isArray('');
     *        // > false
     *
     * @param {mixed} val
     * @return {bool}
     */

    module.exports = isArray || function (val) {
      return !!val && '[object Array]' == str.call(val);
    };
    /***/

  },

  /***/
  "./node_modules/iselement/module/index.js":
  /*!************************************************!*\
    !*** ./node_modules/iselement/module/index.js ***!
    \************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesIselementModuleIndexJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
    };
    /**
     * Returns `true` if provided input is Element.
     * @name isElement
     * @param {*} [input]
     * @returns {boolean}
     */

    /* harmony default export */


    __webpack_exports__["default"] = function (input) {
      return input != null && (typeof input === 'undefined' ? 'undefined' : _typeof(input)) === 'object' && input.nodeType === 1 && _typeof(input.style) === 'object' && _typeof(input.ownerDocument) === 'object';
    };
    /***/

  },

  /***/
  "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js":
  /*!********************************************************************!*\
    !*** ./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js ***!
    \********************************************************************/

  /*! exports provided: Cmyk, ColorPickerComponent, ColorPickerDirective, ColorPickerModule, ColorPickerService, Hsla, Hsva, Rgba, SliderDirective, TextDirective */

  /***/
  function node_modulesNgxColorPickerDistNgxColorPickerEs5Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Cmyk", function () {
      return Cmyk;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ColorPickerComponent", function () {
      return ColorPickerComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ColorPickerDirective", function () {
      return ColorPickerDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ColorPickerModule", function () {
      return ColorPickerModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ColorPickerService", function () {
      return ColorPickerService;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Hsla", function () {
      return Hsla;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Hsva", function () {
      return Hsva;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Rgba", function () {
      return Rgba;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SliderDirective", function () {
      return SliderDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TextDirective", function () {
      return TextDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /** @enum {number} */


    var ColorFormats = {
      HEX: 0,
      RGBA: 1,
      HSLA: 2,
      CMYK: 3
    };
    ColorFormats[ColorFormats.HEX] = 'HEX';
    ColorFormats[ColorFormats.RGBA] = 'RGBA';
    ColorFormats[ColorFormats.HSLA] = 'HSLA';
    ColorFormats[ColorFormats.CMYK] = 'CMYK';

    var Rgba =
    /** @class */
    function () {
      function Rgba(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
      }

      return Rgba;
    }();

    var Hsva =
    /** @class */
    function () {
      function Hsva(h, s, v, a) {
        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      }

      return Hsva;
    }();

    var Hsla =
    /** @class */
    function () {
      function Hsla(h, s, l, a) {
        this.h = h;
        this.s = s;
        this.l = l;
        this.a = a;
      }

      return Hsla;
    }();

    var Cmyk =
    /** @class */
    function () {
      function Cmyk(c, m, y, k, a) {
        if (a === void 0) {
          a = 1;
        }

        this.c = c;
        this.m = m;
        this.y = y;
        this.k = k;
        this.a = a;
      }

      return Cmyk;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @return {?}
     */


    function detectIE() {
      /** @type {?} */
      var ua = '';

      if (typeof navigator !== 'undefined') {
        ua = navigator.userAgent.toLowerCase();
      }
      /** @type {?} */


      var msie = ua.indexOf('msie ');

      if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      } // Other browser


      return false;
    }

    var TextDirective =
    /** @class */
    function () {
      function TextDirective() {
        this.newValue = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }
      /**
       * @param {?} event
       * @return {?}
       */


      TextDirective.prototype.inputChange =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        /** @type {?} */
        var value = event.target.value;

        if (this.rg === undefined) {
          this.newValue.emit(value);
        } else {
          /** @type {?} */
          var numeric = parseFloat(value);
          this.newValue.emit({
            v: numeric,
            rg: this.rg
          });
        }
      };

      TextDirective.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
        args: [{
          selector: '[text]'
        }]
      }];
      TextDirective.propDecorators = {
        rg: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        text: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        newValue: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        inputChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['input', ['$event']]
        }]
      };
      return TextDirective;
    }();

    var SliderDirective =
    /** @class */
    function () {
      function SliderDirective(elRef) {
        var _this = this;

        this.elRef = elRef;
        this.dragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.newValue = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();

        this.listenerMove =
        /**
        * @param {?} event
        * @return {?}
        */
        function (event) {
          return _this.move(event);
        };

        this.listenerStop =
        /**
        * @return {?}
        */
        function () {
          return _this.stop();
        };
      }
      /**
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.mouseDown =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.start(event);
      };
      /**
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.touchStart =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.start(event);
      };
      /**
       * @private
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.move =
      /**
      * @private
      * @param {?} event
      * @return {?}
      */
      function (event) {
        event.preventDefault();
        this.setCursor(event);
      };
      /**
       * @private
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.start =
      /**
      * @private
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.setCursor(event);
        event.stopPropagation();
        document.addEventListener('mouseup', this.listenerStop);
        document.addEventListener('touchend', this.listenerStop);
        document.addEventListener('mousemove', this.listenerMove);
        document.addEventListener('touchmove', this.listenerMove);
        this.dragStart.emit();
      };
      /**
       * @private
       * @return {?}
       */


      SliderDirective.prototype.stop =
      /**
      * @private
      * @return {?}
      */
      function () {
        document.removeEventListener('mouseup', this.listenerStop);
        document.removeEventListener('touchend', this.listenerStop);
        document.removeEventListener('mousemove', this.listenerMove);
        document.removeEventListener('touchmove', this.listenerMove);
        this.dragEnd.emit();
      };
      /**
       * @private
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.getX =
      /**
      * @private
      * @param {?} event
      * @return {?}
      */
      function (event) {
        /** @type {?} */
        var position = this.elRef.nativeElement.getBoundingClientRect();
        /** @type {?} */

        var pageX = event.pageX !== undefined ? event.pageX : event.touches[0].pageX;
        return pageX - position.left - window.pageXOffset;
      };
      /**
       * @private
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.getY =
      /**
      * @private
      * @param {?} event
      * @return {?}
      */
      function (event) {
        /** @type {?} */
        var position = this.elRef.nativeElement.getBoundingClientRect();
        /** @type {?} */

        var pageY = event.pageY !== undefined ? event.pageY : event.touches[0].pageY;
        return pageY - position.top - window.pageYOffset;
      };
      /**
       * @private
       * @param {?} event
       * @return {?}
       */


      SliderDirective.prototype.setCursor =
      /**
      * @private
      * @param {?} event
      * @return {?}
      */
      function (event) {
        /** @type {?} */
        var width = this.elRef.nativeElement.offsetWidth;
        /** @type {?} */

        var height = this.elRef.nativeElement.offsetHeight;
        /** @type {?} */

        var x = Math.max(0, Math.min(this.getX(event), width));
        /** @type {?} */

        var y = Math.max(0, Math.min(this.getY(event), height));

        if (this.rgX !== undefined && this.rgY !== undefined) {
          this.newValue.emit({
            s: x / width,
            v: 1 - y / height,
            rgX: this.rgX,
            rgY: this.rgY
          });
        } else if (this.rgX === undefined && this.rgY !== undefined) {
          this.newValue.emit({
            v: y / height,
            rgY: this.rgY
          });
        } else if (this.rgX !== undefined && this.rgY === undefined) {
          this.newValue.emit({
            v: x / width,
            rgX: this.rgX
          });
        }
      };

      SliderDirective.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
        args: [{
          selector: '[slider]'
        }]
      }];
      /** @nocollapse */

      SliderDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }];
      };

      SliderDirective.propDecorators = {
        rgX: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        rgY: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        dragEnd: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        dragStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        newValue: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        mouseDown: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['mousedown', ['$event']]
        }],
        touchStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['touchstart', ['$event']]
        }]
      };
      return SliderDirective;
    }();

    var SliderPosition =
    /** @class */
    function () {
      function SliderPosition(h, s, v, a) {
        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      }

      return SliderPosition;
    }();

    var SliderDimension =
    /** @class */
    function () {
      function SliderDimension(h, s, v, a) {
        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      }

      return SliderDimension;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var ColorPickerService =
    /** @class */
    function () {
      function ColorPickerService() {
        this.active = null;
      }
      /**
       * @param {?} active
       * @return {?}
       */


      ColorPickerService.prototype.setActive =
      /**
      * @param {?} active
      * @return {?}
      */
      function (active) {
        if (this.active && this.active !== active && this.active.cpDialogDisplay !== 'inline') {
          this.active.closeDialog();
        }

        this.active = active;
      };
      /**
       * @param {?} hsva
       * @return {?}
       */


      ColorPickerService.prototype.hsva2hsla =
      /**
      * @param {?} hsva
      * @return {?}
      */
      function (hsva) {
        /** @type {?} */
        var h = hsva.h;
        /** @type {?} */

        var s = hsva.s;
        /** @type {?} */

        var v = hsva.v;
        /** @type {?} */

        var a = hsva.a;

        if (v === 0) {
          return new Hsla(h, 0, 0, a);
        } else if (s === 0 && v === 1) {
          return new Hsla(h, 1, 1, a);
        } else {
          /** @type {?} */
          var l = v * (2 - s) / 2;
          return new Hsla(h, v * s / (1 - Math.abs(2 * l - 1)), l, a);
        }
      };
      /**
       * @param {?} hsla
       * @return {?}
       */


      ColorPickerService.prototype.hsla2hsva =
      /**
      * @param {?} hsla
      * @return {?}
      */
      function (hsla) {
        /** @type {?} */
        var h = Math.min(hsla.h, 1);
        /** @type {?} */

        var s = Math.min(hsla.s, 1);
        /** @type {?} */

        var l = Math.min(hsla.l, 1);
        /** @type {?} */

        var a = Math.min(hsla.a, 1);

        if (l === 0) {
          return new Hsva(h, 0, 0, a);
        } else {
          /** @type {?} */
          var v = l + s * (1 - Math.abs(2 * l - 1)) / 2;
          return new Hsva(h, 2 * (v - l) / v, v, a);
        }
      };
      /**
       * @param {?} hsva
       * @return {?}
       */


      ColorPickerService.prototype.hsvaToRgba =
      /**
      * @param {?} hsva
      * @return {?}
      */
      function (hsva) {
        /** @type {?} */
        var r;
        /** @type {?} */

        var g;
        /** @type {?} */

        var b;
        /** @type {?} */

        var h = hsva.h;
        /** @type {?} */

        var s = hsva.s;
        /** @type {?} */

        var v = hsva.v;
        /** @type {?} */

        var a = hsva.a;
        /** @type {?} */

        var i = Math.floor(h * 6);
        /** @type {?} */

        var f = h * 6 - i;
        /** @type {?} */

        var p = v * (1 - s);
        /** @type {?} */

        var q = v * (1 - f * s);
        /** @type {?} */

        var t = v * (1 - (1 - f) * s);

        switch (i % 6) {
          case 0:
            r = v, g = t, b = p;
            break;

          case 1:
            r = q, g = v, b = p;
            break;

          case 2:
            r = p, g = v, b = t;
            break;

          case 3:
            r = p, g = q, b = v;
            break;

          case 4:
            r = t, g = p, b = v;
            break;

          case 5:
            r = v, g = p, b = q;
            break;

          default:
            r = 0, g = 0, b = 0;
        }

        return new Rgba(r, g, b, a);
      };
      /**
       * @param {?} cmyk
       * @return {?}
       */


      ColorPickerService.prototype.cmykToRgb =
      /**
      * @param {?} cmyk
      * @return {?}
      */
      function (cmyk) {
        /** @type {?} */
        var r = (1 - cmyk.c) * (1 - cmyk.k);
        /** @type {?} */

        var g = (1 - cmyk.m) * (1 - cmyk.k);
        /** @type {?} */

        var b = (1 - cmyk.y) * (1 - cmyk.k);
        return new Rgba(r, g, b, cmyk.a);
      };
      /**
       * @param {?} rgba
       * @return {?}
       */


      ColorPickerService.prototype.rgbaToCmyk =
      /**
      * @param {?} rgba
      * @return {?}
      */
      function (rgba) {
        /** @type {?} */
        var k = 1 - Math.max(rgba.r, rgba.g, rgba.b);

        if (k === 1) {
          return new Cmyk(0, 0, 0, 1, rgba.a);
        } else {
          /** @type {?} */
          var c = (1 - rgba.r - k) / (1 - k);
          /** @type {?} */

          var m = (1 - rgba.g - k) / (1 - k);
          /** @type {?} */

          var y = (1 - rgba.b - k) / (1 - k);
          return new Cmyk(c, m, y, k, rgba.a);
        }
      };
      /**
       * @param {?} rgba
       * @return {?}
       */


      ColorPickerService.prototype.rgbaToHsva =
      /**
      * @param {?} rgba
      * @return {?}
      */
      function (rgba) {
        /** @type {?} */
        var h;
        /** @type {?} */

        var s;
        /** @type {?} */

        var r = Math.min(rgba.r, 1);
        /** @type {?} */

        var g = Math.min(rgba.g, 1);
        /** @type {?} */

        var b = Math.min(rgba.b, 1);
        /** @type {?} */

        var a = Math.min(rgba.a, 1);
        /** @type {?} */

        var max = Math.max(r, g, b);
        /** @type {?} */

        var min = Math.min(r, g, b);
        /** @type {?} */

        var v = max;
        /** @type {?} */

        var d = max - min;
        s = max === 0 ? 0 : d / max;

        if (max === min) {
          h = 0;
        } else {
          switch (max) {
            case r:
              h = (g - b) / d + (g < b ? 6 : 0);
              break;

            case g:
              h = (b - r) / d + 2;
              break;

            case b:
              h = (r - g) / d + 4;
              break;

            default:
              h = 0;
          }

          h /= 6;
        }

        return new Hsva(h, s, v, a);
      };
      /**
       * @param {?} rgba
       * @param {?=} allowHex8
       * @return {?}
       */


      ColorPickerService.prototype.rgbaToHex =
      /**
      * @param {?} rgba
      * @param {?=} allowHex8
      * @return {?}
      */
      function (rgba, allowHex8) {
        /* tslint:disable:no-bitwise */

        /** @type {?} */
        var hex = '#' + (1 << 24 | rgba.r << 16 | rgba.g << 8 | rgba.b).toString(16).substr(1);

        if (allowHex8) {
          hex += (1 << 8 | Math.round(rgba.a * 255)).toString(16).substr(1);
        }
        /* tslint:enable:no-bitwise */


        return hex;
      };
      /**
       * @param {?} cmyk
       * @return {?}
       */


      ColorPickerService.prototype.normalizeCMYK =
      /**
      * @param {?} cmyk
      * @return {?}
      */
      function (cmyk) {
        return new Cmyk(cmyk.c / 100, cmyk.m / 100, cmyk.y / 100, cmyk.k / 100, cmyk.a);
      };
      /**
       * @param {?} cmyk
       * @return {?}
       */


      ColorPickerService.prototype.denormalizeCMYK =
      /**
      * @param {?} cmyk
      * @return {?}
      */
      function (cmyk) {
        return new Cmyk(Math.floor(cmyk.c * 100), Math.floor(cmyk.m * 100), Math.floor(cmyk.y * 100), Math.floor(cmyk.k * 100), cmyk.a);
      };
      /**
       * @param {?} rgba
       * @return {?}
       */


      ColorPickerService.prototype.denormalizeRGBA =
      /**
      * @param {?} rgba
      * @return {?}
      */
      function (rgba) {
        return new Rgba(Math.round(rgba.r * 255), Math.round(rgba.g * 255), Math.round(rgba.b * 255), rgba.a);
      };
      /**
       * @param {?=} colorString
       * @param {?=} allowHex8
       * @return {?}
       */


      ColorPickerService.prototype.stringToHsva =
      /**
      * @param {?=} colorString
      * @param {?=} allowHex8
      * @return {?}
      */
      function (colorString, allowHex8) {
        if (colorString === void 0) {
          colorString = '';
        }

        if (allowHex8 === void 0) {
          allowHex8 = false;
        }
        /** @type {?} */


        var hsva = null;
        colorString = (colorString || '').toLowerCase();
        /** @type {?} */

        var stringParsers = [{
          re: /(rgb)a?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*%?,\s*(\d{1,3})\s*%?(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
          parse:
          /**
          * @param {?} execResult
          * @return {?}
          */
          function parse(execResult) {
            return new Rgba(parseInt(execResult[2], 10) / 255, parseInt(execResult[3], 10) / 255, parseInt(execResult[4], 10) / 255, isNaN(parseFloat(execResult[5])) ? 1 : parseFloat(execResult[5]));
          }
        }, {
          re: /(hsl)a?\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
          parse:
          /**
          * @param {?} execResult
          * @return {?}
          */
          function parse(execResult) {
            return new Hsla(parseInt(execResult[2], 10) / 360, parseInt(execResult[3], 10) / 100, parseInt(execResult[4], 10) / 100, isNaN(parseFloat(execResult[5])) ? 1 : parseFloat(execResult[5]));
          }
        }];

        if (allowHex8) {
          stringParsers.push({
            re: /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})?$/,
            parse:
            /**
            * @param {?} execResult
            * @return {?}
            */
            function parse(execResult) {
              return new Rgba(parseInt(execResult[1], 16) / 255, parseInt(execResult[2], 16) / 255, parseInt(execResult[3], 16) / 255, parseInt(execResult[4] || 'FF', 16) / 255);
            }
          });
        } else {
          stringParsers.push({
            re: /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})$/,
            parse:
            /**
            * @param {?} execResult
            * @return {?}
            */
            function parse(execResult) {
              return new Rgba(parseInt(execResult[1], 16) / 255, parseInt(execResult[2], 16) / 255, parseInt(execResult[3], 16) / 255, 1);
            }
          });
        }

        stringParsers.push({
          re: /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])$/,
          parse:
          /**
          * @param {?} execResult
          * @return {?}
          */
          function parse(execResult) {
            return new Rgba(parseInt(execResult[1] + execResult[1], 16) / 255, parseInt(execResult[2] + execResult[2], 16) / 255, parseInt(execResult[3] + execResult[3], 16) / 255, 1);
          }
        });

        for (var key in stringParsers) {
          if (stringParsers.hasOwnProperty(key)) {
            /** @type {?} */
            var parser = stringParsers[key];
            /** @type {?} */

            var match = parser.re.exec(colorString);
            /** @type {?} */

            var color = match && parser.parse(match);

            if (color) {
              if (color instanceof Rgba) {
                hsva = this.rgbaToHsva(color);
              } else if (color instanceof Hsla) {
                hsva = this.hsla2hsva(color);
              }

              return hsva;
            }
          }
        }

        return hsva;
      };
      /**
       * @param {?} hsva
       * @param {?} outputFormat
       * @param {?} alphaChannel
       * @return {?}
       */


      ColorPickerService.prototype.outputFormat =
      /**
      * @param {?} hsva
      * @param {?} outputFormat
      * @param {?} alphaChannel
      * @return {?}
      */
      function (hsva, outputFormat, alphaChannel) {
        if (outputFormat === 'auto') {
          outputFormat = hsva.a < 1 ? 'rgba' : 'hex';
        }

        switch (outputFormat) {
          case 'hsla':
            /** @type {?} */
            var hsla = this.hsva2hsla(hsva);
            /** @type {?} */

            var hslaText = new Hsla(Math.round(hsla.h * 360), Math.round(hsla.s * 100), Math.round(hsla.l * 100), Math.round(hsla.a * 100) / 100);

            if (hsva.a < 1 || alphaChannel === 'always') {
              return 'hsla(' + hslaText.h + ',' + hslaText.s + '%,' + hslaText.l + '%,' + hslaText.a + ')';
            } else {
              return 'hsl(' + hslaText.h + ',' + hslaText.s + '%,' + hslaText.l + '%)';
            }

          case 'rgba':
            /** @type {?} */
            var rgba = this.denormalizeRGBA(this.hsvaToRgba(hsva));

            if (hsva.a < 1 || alphaChannel === 'always') {
              return 'rgba(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ',' + Math.round(rgba.a * 100) / 100 + ')';
            } else {
              return 'rgb(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ')';
            }

          default:
            /** @type {?} */
            var allowHex8 = alphaChannel === 'always' || alphaChannel === 'forced';
            return this.rgbaToHex(this.denormalizeRGBA(this.hsvaToRgba(hsva)), allowHex8);
        }
      };

      ColorPickerService.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }];
      /** @nocollapse */

      ColorPickerService.ctorParameters = function () {
        return [];
      };

      return ColorPickerService;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var ColorPickerComponent =
    /** @class */
    function () {
      function ColorPickerComponent(elRef, cdRef, service) {
        this.elRef = elRef;
        this.cdRef = cdRef;
        this.service = service;
        this.isIE10 = false;
        this.dialogArrowSize = 10;
        this.dialogArrowOffset = 15;
        this.dialogInputFields = [ColorFormats.HEX, ColorFormats.RGBA, ColorFormats.HSLA, ColorFormats.CMYK];
        this.useRootViewContainer = false;
      }
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerComponent.prototype.handleEsc =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        if (this.show && this.cpDialogDisplay === 'popup') {
          this.onCancelColor(event);
        }
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerComponent.prototype.handleEnter =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        if (this.show && this.cpDialogDisplay === 'popup') {
          this.onAcceptColor(event);
        }
      };
      /**
       * @return {?}
       */


      ColorPickerComponent.prototype.ngOnInit =
      /**
      * @return {?}
      */
      function () {
        var _this = this;

        this.slider = new SliderPosition(0, 0, 0, 0);
        /** @type {?} */

        var hueWidth = this.hueSlider.nativeElement.offsetWidth || 140;
        /** @type {?} */

        var alphaWidth = this.alphaSlider.nativeElement.offsetWidth || 140;
        this.sliderDimMax = new SliderDimension(hueWidth, this.cpWidth, 130, alphaWidth);

        if (this.cpCmykEnabled) {
          this.format = ColorFormats.CMYK;
        } else if (this.cpOutputFormat === 'rgba') {
          this.format = ColorFormats.RGBA;
        } else if (this.cpOutputFormat === 'hsla') {
          this.format = ColorFormats.HSLA;
        } else {
          this.format = ColorFormats.HEX;
        }

        this.listenerMouseDown =
        /**
        * @param {?} event
        * @return {?}
        */
        function (event) {
          _this.onMouseDown(event);
        };

        this.listenerResize =
        /**
        * @return {?}
        */
        function () {
          _this.onResize();
        };

        this.openDialog(this.initialColor, false);
      };
      /**
       * @return {?}
       */


      ColorPickerComponent.prototype.ngOnDestroy =
      /**
      * @return {?}
      */
      function () {
        this.closeDialog();
      };
      /**
       * @return {?}
       */


      ColorPickerComponent.prototype.ngAfterViewInit =
      /**
      * @return {?}
      */
      function () {
        if (this.cpWidth !== 230 || this.cpDialogDisplay === 'inline') {
          /** @type {?} */
          var hueWidth = this.hueSlider.nativeElement.offsetWidth || 140;
          /** @type {?} */

          var alphaWidth = this.alphaSlider.nativeElement.offsetWidth || 140;
          this.sliderDimMax = new SliderDimension(hueWidth, this.cpWidth, 130, alphaWidth);
          this.updateColorPicker(false);
          this.cdRef.detectChanges();
        }
      };
      /**
       * @param {?} color
       * @param {?=} emit
       * @return {?}
       */


      ColorPickerComponent.prototype.openDialog =
      /**
      * @param {?} color
      * @param {?=} emit
      * @return {?}
      */
      function (color, emit) {
        if (emit === void 0) {
          emit = true;
        }

        this.service.setActive(this);

        if (!this.width) {
          this.cpWidth = this.directiveElementRef.nativeElement.offsetWidth;
        }

        if (!this.height) {
          this.height = 320;
        }

        this.setInitialColor(color);
        this.setColorFromString(color, emit);
        this.openColorPicker();
      };
      /**
       * @return {?}
       */


      ColorPickerComponent.prototype.closeDialog =
      /**
      * @return {?}
      */
      function () {
        this.closeColorPicker();
      };
      /**
       * @param {?} instance
       * @param {?} elementRef
       * @param {?} color
       * @param {?} cpWidth
       * @param {?} cpHeight
       * @param {?} cpDialogDisplay
       * @param {?} cpFallbackColor
       * @param {?} cpColorMode
       * @param {?} cpCmykEnabled
       * @param {?} cpAlphaChannel
       * @param {?} cpOutputFormat
       * @param {?} cpDisableInput
       * @param {?} cpIgnoredElements
       * @param {?} cpSaveClickOutside
       * @param {?} cpCloseClickOutside
       * @param {?} cpUseRootViewContainer
       * @param {?} cpPosition
       * @param {?} cpPositionOffset
       * @param {?} cpPositionRelativeToArrow
       * @param {?} cpPresetLabel
       * @param {?} cpPresetColors
       * @param {?} cpMaxPresetColorsLength
       * @param {?} cpPresetEmptyMessage
       * @param {?} cpPresetEmptyMessageClass
       * @param {?} cpOKButton
       * @param {?} cpOKButtonClass
       * @param {?} cpOKButtonText
       * @param {?} cpCancelButton
       * @param {?} cpCancelButtonClass
       * @param {?} cpCancelButtonText
       * @param {?} cpAddColorButton
       * @param {?} cpAddColorButtonClass
       * @param {?} cpAddColorButtonText
       * @param {?} cpRemoveColorButtonClass
       * @return {?}
       */


      ColorPickerComponent.prototype.setupDialog =
      /**
      * @param {?} instance
      * @param {?} elementRef
      * @param {?} color
      * @param {?} cpWidth
      * @param {?} cpHeight
      * @param {?} cpDialogDisplay
      * @param {?} cpFallbackColor
      * @param {?} cpColorMode
      * @param {?} cpCmykEnabled
      * @param {?} cpAlphaChannel
      * @param {?} cpOutputFormat
      * @param {?} cpDisableInput
      * @param {?} cpIgnoredElements
      * @param {?} cpSaveClickOutside
      * @param {?} cpCloseClickOutside
      * @param {?} cpUseRootViewContainer
      * @param {?} cpPosition
      * @param {?} cpPositionOffset
      * @param {?} cpPositionRelativeToArrow
      * @param {?} cpPresetLabel
      * @param {?} cpPresetColors
      * @param {?} cpMaxPresetColorsLength
      * @param {?} cpPresetEmptyMessage
      * @param {?} cpPresetEmptyMessageClass
      * @param {?} cpOKButton
      * @param {?} cpOKButtonClass
      * @param {?} cpOKButtonText
      * @param {?} cpCancelButton
      * @param {?} cpCancelButtonClass
      * @param {?} cpCancelButtonText
      * @param {?} cpAddColorButton
      * @param {?} cpAddColorButtonClass
      * @param {?} cpAddColorButtonText
      * @param {?} cpRemoveColorButtonClass
      * @return {?}
      */
      function (instance, elementRef, color, cpWidth, cpHeight, cpDialogDisplay, cpFallbackColor, cpColorMode, cpCmykEnabled, cpAlphaChannel, cpOutputFormat, cpDisableInput, cpIgnoredElements, cpSaveClickOutside, cpCloseClickOutside, cpUseRootViewContainer, cpPosition, cpPositionOffset, cpPositionRelativeToArrow, cpPresetLabel, cpPresetColors, cpMaxPresetColorsLength, cpPresetEmptyMessage, cpPresetEmptyMessageClass, cpOKButton, cpOKButtonClass, cpOKButtonText, cpCancelButton, cpCancelButtonClass, cpCancelButtonText, cpAddColorButton, cpAddColorButtonClass, cpAddColorButtonText, cpRemoveColorButtonClass) {
        this.setInitialColor(color);
        this.setColorMode(cpColorMode);
        this.isIE10 = detectIE() === 10;
        this.directiveInstance = instance;
        this.directiveElementRef = elementRef;
        this.cpDisableInput = cpDisableInput;
        this.cpCmykEnabled = cpCmykEnabled;
        this.cpAlphaChannel = cpAlphaChannel;
        this.cpOutputFormat = cpOutputFormat;
        this.cpDialogDisplay = cpDialogDisplay;
        this.cpIgnoredElements = cpIgnoredElements;
        this.cpSaveClickOutside = cpSaveClickOutside;
        this.cpCloseClickOutside = cpCloseClickOutside;
        this.useRootViewContainer = cpUseRootViewContainer;
        this.width = this.cpWidth = parseInt(cpWidth, 10);
        this.height = this.cpHeight = parseInt(cpHeight, 10);
        this.cpPosition = cpPosition;
        this.cpPositionOffset = parseInt(cpPositionOffset, 10);
        this.cpOKButton = cpOKButton;
        this.cpOKButtonText = cpOKButtonText;
        this.cpOKButtonClass = cpOKButtonClass;
        this.cpCancelButton = cpCancelButton;
        this.cpCancelButtonText = cpCancelButtonText;
        this.cpCancelButtonClass = cpCancelButtonClass;
        this.fallbackColor = cpFallbackColor || '#fff';
        this.setPresetConfig(cpPresetLabel, cpPresetColors);
        this.cpMaxPresetColorsLength = cpMaxPresetColorsLength;
        this.cpPresetEmptyMessage = cpPresetEmptyMessage;
        this.cpPresetEmptyMessageClass = cpPresetEmptyMessageClass;
        this.cpAddColorButton = cpAddColorButton;
        this.cpAddColorButtonText = cpAddColorButtonText;
        this.cpAddColorButtonClass = cpAddColorButtonClass;
        this.cpRemoveColorButtonClass = cpRemoveColorButtonClass;

        if (!cpPositionRelativeToArrow) {
          this.dialogArrowOffset = 0;
        }

        if (cpDialogDisplay === 'inline') {
          this.dialogArrowSize = 0;
          this.dialogArrowOffset = 0;
        }

        if (cpOutputFormat === 'hex' && cpAlphaChannel !== 'always' && cpAlphaChannel !== 'forced') {
          this.cpAlphaChannel = 'disabled';
        }
      };
      /**
       * @param {?} mode
       * @return {?}
       */


      ColorPickerComponent.prototype.setColorMode =
      /**
      * @param {?} mode
      * @return {?}
      */
      function (mode) {
        switch (mode.toString().toUpperCase()) {
          case '1':
          case 'C':
          case 'COLOR':
            this.cpColorMode = 1;
            break;

          case '2':
          case 'G':
          case 'GRAYSCALE':
            this.cpColorMode = 2;
            break;

          case '3':
          case 'P':
          case 'PRESETS':
            this.cpColorMode = 3;
            break;

          default:
            this.cpColorMode = 1;
        }
      };
      /**
       * @param {?} color
       * @return {?}
       */


      ColorPickerComponent.prototype.setInitialColor =
      /**
      * @param {?} color
      * @return {?}
      */
      function (color) {
        this.initialColor = color;
      };
      /**
       * @param {?} cpPresetLabel
       * @param {?} cpPresetColors
       * @return {?}
       */


      ColorPickerComponent.prototype.setPresetConfig =
      /**
      * @param {?} cpPresetLabel
      * @param {?} cpPresetColors
      * @return {?}
      */
      function (cpPresetLabel, cpPresetColors) {
        this.cpPresetLabel = cpPresetLabel;
        this.cpPresetColors = cpPresetColors;
      };
      /**
       * @param {?} value
       * @param {?=} emit
       * @param {?=} update
       * @return {?}
       */


      ColorPickerComponent.prototype.setColorFromString =
      /**
      * @param {?} value
      * @param {?=} emit
      * @param {?=} update
      * @return {?}
      */
      function (value, emit, update) {
        if (emit === void 0) {
          emit = true;
        }

        if (update === void 0) {
          update = true;
        }
        /** @type {?} */


        var hsva;

        if (this.cpAlphaChannel === 'always' || this.cpAlphaChannel === 'forced') {
          hsva = this.service.stringToHsva(value, true);

          if (!hsva && !this.hsva) {
            hsva = this.service.stringToHsva(value, false);
          }
        } else {
          hsva = this.service.stringToHsva(value, false);
        }

        if (!hsva && !this.hsva) {
          hsva = this.service.stringToHsva(this.fallbackColor, false);
        }

        if (hsva) {
          this.hsva = hsva;
          this.sliderH = this.hsva.h;

          if (this.cpOutputFormat === 'hex' && this.cpAlphaChannel === 'disabled') {
            this.hsva.a = 1;
          }

          this.updateColorPicker(emit, update);
        }
      };
      /**
       * @return {?}
       */


      ColorPickerComponent.prototype.onResize =
      /**
      * @return {?}
      */
      function () {
        if (this.position === 'fixed') {
          this.setDialogPosition();
        } else if (this.cpDialogDisplay !== 'inline') {
          this.closeColorPicker();
        }
      };
      /**
       * @param {?} slider
       * @return {?}
       */


      ColorPickerComponent.prototype.onDragEnd =
      /**
      * @param {?} slider
      * @return {?}
      */
      function (slider) {
        this.directiveInstance.sliderDragEnd({
          slider: slider,
          color: this.outputColor
        });
      };
      /**
       * @param {?} slider
       * @return {?}
       */


      ColorPickerComponent.prototype.onDragStart =
      /**
      * @param {?} slider
      * @return {?}
      */
      function (slider) {
        this.directiveInstance.sliderDragStart({
          slider: slider,
          color: this.outputColor
        });
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerComponent.prototype.onMouseDown =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        if (this.show && !this.isIE10 && this.cpDialogDisplay === 'popup' && event.target !== this.directiveElementRef.nativeElement && !this.isDescendant(this.elRef.nativeElement, event.target) && !this.isDescendant(this.directiveElementRef.nativeElement, event.target) && this.cpIgnoredElements.filter(
        /**
        * @param {?} item
        * @return {?}
        */
        function (item) {
          return item === event.target;
        }).length === 0) {
          if (this.cpSaveClickOutside) {
            this.directiveInstance.colorSelected(this.outputColor);
          } else {
            this.setColorFromString(this.initialColor, false);

            if (this.cpCmykEnabled) {
              this.directiveInstance.cmykChanged(this.cmykColor);
            }

            this.directiveInstance.colorChanged(this.outputColor);
          }

          if (this.cpCloseClickOutside) {
            this.closeColorPicker();
          }
        }
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerComponent.prototype.onAcceptColor =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        event.stopPropagation();

        if (this.outputColor) {
          this.directiveInstance.colorSelected(this.outputColor);
        }

        if (this.cpDialogDisplay === 'popup') {
          this.closeColorPicker();
        }
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerComponent.prototype.onCancelColor =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        event.stopPropagation();
        this.setColorFromString(this.initialColor, true);

        if (this.cpDialogDisplay === 'popup') {
          if (this.cpCmykEnabled) {
            this.directiveInstance.cmykChanged(this.cmykColor);
          }

          this.directiveInstance.colorChanged(this.outputColor, true);
          this.closeColorPicker();
        }

        this.directiveInstance.colorCanceled();
      };
      /**
       * @param {?} change
       * @return {?}
       */


      ColorPickerComponent.prototype.onFormatToggle =
      /**
      * @param {?} change
      * @return {?}
      */
      function (change) {
        /** @type {?} */
        var availableFormats = this.dialogInputFields.length - (this.cpCmykEnabled ? 0 : 1);
        /** @type {?} */

        var nextFormat = ((this.dialogInputFields.indexOf(this.format) + change) % availableFormats + availableFormats) % availableFormats;
        this.format = this.dialogInputFields[nextFormat];
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onColorChange =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.hsva.s = value.s / value.rgX;
        this.hsva.v = value.v / value.rgY;
        this.updateColorPicker();
        this.directiveInstance.sliderChanged({
          slider: 'lightness',
          value: this.hsva.v,
          color: this.outputColor
        });
        this.directiveInstance.sliderChanged({
          slider: 'saturation',
          value: this.hsva.s,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onHueChange =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.hsva.h = value.v / value.rgX;
        this.sliderH = this.hsva.h;
        this.updateColorPicker();
        this.directiveInstance.sliderChanged({
          slider: 'hue',
          value: this.hsva.h,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onValueChange =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.hsva.v = value.v / value.rgX;
        this.updateColorPicker();
        this.directiveInstance.sliderChanged({
          slider: 'value',
          value: this.hsva.v,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onAlphaChange =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.hsva.a = value.v / value.rgX;
        this.updateColorPicker();
        this.directiveInstance.sliderChanged({
          slider: 'alpha',
          value: this.hsva.a,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onHexInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        if (value === null) {
          this.updateColorPicker();
        } else {
          if (value && value[0] !== '#') {
            value = '#' + value;
          }
          /** @type {?} */


          var validHex = /^#([a-f0-9]{3}|[a-f0-9]{6})$/gi;

          if (this.cpAlphaChannel === 'always') {
            validHex = /^#([a-f0-9]{3}|[a-f0-9]{6}|[a-f0-9]{8})$/gi;
          }
          /** @type {?} */


          var valid = validHex.test(value);

          if (valid) {
            if (value.length < 5) {
              value = '#' + value.substring(1).split('').map(
              /**
              * @param {?} c
              * @return {?}
              */
              function (c) {
                return c + c;
              }).join('');
            }

            if (this.cpAlphaChannel === 'forced') {
              value += Math.round(this.hsva.a * 255).toString(16);
            }

            this.setColorFromString(value, true, false);
          }

          this.directiveInstance.inputChanged({
            input: 'hex',
            valid: valid,
            value: value,
            color: this.outputColor
          });
        }
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onRedInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var rgba = this.service.hsvaToRgba(this.hsva);
        /** @type {?} */

        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          rgba.r = value.v / value.rg;
          this.hsva = this.service.rgbaToHsva(rgba);
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'red',
          valid: valid,
          value: rgba.r,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onBlueInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var rgba = this.service.hsvaToRgba(this.hsva);
        /** @type {?} */

        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          rgba.b = value.v / value.rg;
          this.hsva = this.service.rgbaToHsva(rgba);
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'blue',
          valid: valid,
          value: rgba.b,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onGreenInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var rgba = this.service.hsvaToRgba(this.hsva);
        /** @type {?} */

        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          rgba.g = value.v / value.rg;
          this.hsva = this.service.rgbaToHsva(rgba);
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'green',
          valid: valid,
          value: rgba.g,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onHueInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.hsva.h = value.v / value.rg;
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'hue',
          valid: valid,
          value: this.hsva.h,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onValueInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.hsva.v = value.v / value.rg;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'value',
          valid: valid,
          value: this.hsva.v,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onAlphaInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.hsva.a = value.v / value.rg;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'alpha',
          valid: valid,
          value: this.hsva.a,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onLightnessInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var hsla = this.service.hsva2hsla(this.hsva);
        /** @type {?} */

        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          hsla.l = value.v / value.rg;
          this.hsva = this.service.hsla2hsva(hsla);
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'lightness',
          valid: valid,
          value: hsla.l,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onSaturationInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var hsla = this.service.hsva2hsla(this.hsva);
        /** @type {?} */

        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          hsla.s = value.v / value.rg;
          this.hsva = this.service.hsla2hsva(hsla);
          this.sliderH = this.hsva.h;
          this.updateColorPicker();
        }

        this.directiveInstance.inputChanged({
          input: 'saturation',
          valid: valid,
          value: hsla.s,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onCyanInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.cmyk.c = value.v;
          this.updateColorPicker(false, true, true);
        }

        this.directiveInstance.inputChanged({
          input: 'cyan',
          valid: true,
          value: this.cmyk.c,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onMagentaInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.cmyk.m = value.v;
          this.updateColorPicker(false, true, true);
        }

        this.directiveInstance.inputChanged({
          input: 'magenta',
          valid: true,
          value: this.cmyk.m,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onYellowInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.cmyk.y = value.v;
          this.updateColorPicker(false, true, true);
        }

        this.directiveInstance.inputChanged({
          input: 'yellow',
          valid: true,
          value: this.cmyk.y,
          color: this.outputColor
        });
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onBlackInput =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        /** @type {?} */
        var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

        if (valid) {
          this.cmyk.k = value.v;
          this.updateColorPicker(false, true, true);
        }

        this.directiveInstance.inputChanged({
          input: 'black',
          valid: true,
          value: this.cmyk.k,
          color: this.outputColor
        });
      };
      /**
       * @param {?} event
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onAddPresetColor =
      /**
      * @param {?} event
      * @param {?} value
      * @return {?}
      */
      function (event, value) {
        event.stopPropagation();

        if (!this.cpPresetColors.filter(
        /**
        * @param {?} color
        * @return {?}
        */
        function (color) {
          return color === value;
        }).length) {
          this.cpPresetColors = this.cpPresetColors.concat(value);
          this.directiveInstance.presetColorsChanged(this.cpPresetColors);
        }
      };
      /**
       * @param {?} event
       * @param {?} value
       * @return {?}
       */


      ColorPickerComponent.prototype.onRemovePresetColor =
      /**
      * @param {?} event
      * @param {?} value
      * @return {?}
      */
      function (event, value) {
        event.stopPropagation();
        this.cpPresetColors = this.cpPresetColors.filter(
        /**
        * @param {?} color
        * @return {?}
        */
        function (color) {
          return color !== value;
        });
        this.directiveInstance.presetColorsChanged(this.cpPresetColors);
      }; // Private helper functions for the color picker dialog status
      // Private helper functions for the color picker dialog status

      /**
       * @private
       * @return {?}
       */


      ColorPickerComponent.prototype.openColorPicker = // Private helper functions for the color picker dialog status

      /**
       * @private
       * @return {?}
       */
      function () {
        var _this = this;

        if (!this.show) {
          this.show = true;
          this.hidden = true;
          setTimeout(
          /**
          * @return {?}
          */
          function () {
            _this.hidden = false;

            _this.setDialogPosition();

            _this.cdRef.detectChanges();
          }, 0);
          this.directiveInstance.stateChanged(true);

          if (!this.isIE10) {
            document.addEventListener('mousedown', this.listenerMouseDown);
            document.addEventListener('touchstart', this.listenerMouseDown);
          }

          window.addEventListener('resize', this.listenerResize);
        }
      };
      /**
       * @private
       * @return {?}
       */


      ColorPickerComponent.prototype.closeColorPicker =
      /**
      * @private
      * @return {?}
      */
      function () {
        if (this.show) {
          this.show = false;
          this.directiveInstance.stateChanged(false);

          if (!this.isIE10) {
            document.removeEventListener('mousedown', this.listenerMouseDown);
            document.removeEventListener('touchstart', this.listenerMouseDown);
          }

          window.removeEventListener('resize', this.listenerResize);

          if (!this.cdRef['destroyed']) {
            this.cdRef.detectChanges();
          }
        }
      };
      /**
       * @private
       * @param {?=} emit
       * @param {?=} update
       * @param {?=} cmykInput
       * @return {?}
       */


      ColorPickerComponent.prototype.updateColorPicker =
      /**
      * @private
      * @param {?=} emit
      * @param {?=} update
      * @param {?=} cmykInput
      * @return {?}
      */
      function (emit, update, cmykInput) {
        if (emit === void 0) {
          emit = true;
        }

        if (update === void 0) {
          update = true;
        }

        if (cmykInput === void 0) {
          cmykInput = false;
        }

        if (this.sliderDimMax) {
          if (this.cpColorMode === 2) {
            this.hsva.s = 0;
          }
          /** @type {?} */


          var hue = void 0;
          /** @type {?} */

          var hsla = void 0;
          /** @type {?} */

          var rgba = void 0;
          /** @type {?} */

          var lastOutput = this.outputColor;
          hsla = this.service.hsva2hsla(this.hsva);

          if (!this.cpCmykEnabled) {
            rgba = this.service.denormalizeRGBA(this.service.hsvaToRgba(this.hsva));
          } else {
            if (!cmykInput) {
              rgba = this.service.hsvaToRgba(this.hsva);
              this.cmyk = this.service.denormalizeCMYK(this.service.rgbaToCmyk(rgba));
            } else {
              rgba = this.service.cmykToRgb(this.service.normalizeCMYK(this.cmyk));
              this.hsva = this.service.rgbaToHsva(rgba);
            }

            rgba = this.service.denormalizeRGBA(rgba);
            this.sliderH = this.hsva.h;
          }

          hue = this.service.denormalizeRGBA(this.service.hsvaToRgba(new Hsva(this.sliderH || this.hsva.h, 1, 1, 1)));

          if (update) {
            this.hslaText = new Hsla(Math.round(hsla.h * 360), Math.round(hsla.s * 100), Math.round(hsla.l * 100), Math.round(hsla.a * 100) / 100);
            this.rgbaText = new Rgba(rgba.r, rgba.g, rgba.b, Math.round(rgba.a * 100) / 100);

            if (this.cpCmykEnabled) {
              this.cmykText = new Cmyk(this.cmyk.c, this.cmyk.m, this.cmyk.y, this.cmyk.k, Math.round(this.cmyk.a * 100) / 100);
            }
            /** @type {?} */


            var allowHex8 = this.cpAlphaChannel === 'always';
            this.hexText = this.service.rgbaToHex(rgba, allowHex8);
            this.hexAlpha = this.rgbaText.a;
          }

          if (this.cpOutputFormat === 'auto') {
            if (this.format !== ColorFormats.RGBA && this.format !== ColorFormats.CMYK) {
              if (this.hsva.a < 1) {
                this.format = this.hsva.a < 1 ? ColorFormats.RGBA : ColorFormats.HEX;
              }
            }
          }

          this.hueSliderColor = 'rgb(' + hue.r + ',' + hue.g + ',' + hue.b + ')';
          this.alphaSliderColor = 'rgb(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ')';
          this.outputColor = this.service.outputFormat(this.hsva, this.cpOutputFormat, this.cpAlphaChannel);
          this.selectedColor = this.service.outputFormat(this.hsva, 'rgba', null);

          if (this.format !== ColorFormats.CMYK) {
            this.cmykColor = '';
          } else {
            if (this.cpAlphaChannel === 'always' || this.cpAlphaChannel === 'enabled' || this.cpAlphaChannel === 'forced') {
              /** @type {?} */
              var alpha = Math.round(this.cmyk.a * 100) / 100;
              this.cmykColor = "cmyka(" + this.cmyk.c + "," + this.cmyk.m + "," + this.cmyk.y + "," + this.cmyk.k + "," + alpha + ")";
            } else {
              this.cmykColor = "cmyk(" + this.cmyk.c + "," + this.cmyk.m + "," + this.cmyk.y + "," + this.cmyk.k + ")";
            }
          }

          this.slider = new SliderPosition((this.sliderH || this.hsva.h) * this.sliderDimMax.h - 8, this.hsva.s * this.sliderDimMax.s - 8, (1 - this.hsva.v) * this.sliderDimMax.v - 8, this.hsva.a * this.sliderDimMax.a - 8);

          if (emit && lastOutput !== this.outputColor) {
            if (this.cpCmykEnabled) {
              this.directiveInstance.cmykChanged(this.cmykColor);
            }

            this.directiveInstance.colorChanged(this.outputColor);
          }
        }
      }; // Private helper functions for the color picker dialog positioning
      // Private helper functions for the color picker dialog positioning

      /**
       * @private
       * @return {?}
       */


      ColorPickerComponent.prototype.setDialogPosition = // Private helper functions for the color picker dialog positioning

      /**
       * @private
       * @return {?}
       */
      function () {
        if (this.cpDialogDisplay === 'inline') {
          this.position = 'relative';
        } else {
          /** @type {?} */
          var position = 'static';
          /** @type {?} */

          var transform = '';
          /** @type {?} */

          var style = void 0;
          /** @type {?} */

          var parentNode = null;
          /** @type {?} */

          var transformNode = null;
          /** @type {?} */

          var node = this.directiveElementRef.nativeElement.parentNode;
          /** @type {?} */

          var dialogHeight = this.dialogElement.nativeElement.offsetHeight;

          while (node !== null && node.tagName !== 'HTML') {
            style = window.getComputedStyle(node);
            position = style.getPropertyValue('position');
            transform = style.getPropertyValue('transform');

            if (position !== 'static' && parentNode === null) {
              parentNode = node;
            }

            if (transform && transform !== 'none' && transformNode === null) {
              transformNode = node;
            }

            if (position === 'fixed') {
              parentNode = transformNode;
              break;
            }

            node = node.parentNode;
          }
          /** @type {?} */


          var boxDirective = this.createDialogBox(this.directiveElementRef.nativeElement, position !== 'fixed');

          if (this.useRootViewContainer || position === 'fixed' && (!parentNode || parentNode instanceof HTMLUnknownElement)) {
            this.top = boxDirective.top;
            this.left = boxDirective.left;
          } else {
            if (parentNode === null) {
              parentNode = node;
            }
            /** @type {?} */


            var boxParent = this.createDialogBox(parentNode, position !== 'fixed');
            this.top = boxDirective.top - boxParent.top;
            this.left = boxDirective.left - boxParent.left;
          }

          if (position === 'fixed') {
            this.position = 'fixed';
          }
          /** @type {?} */


          var usePosition = this.cpPosition;

          if (this.cpPosition === 'auto') {
            /** @type {?} */
            var usePositionX = 'right';
            /** @type {?} */

            var usePositionY = 'bottom';
            /** @type {?} */

            var winWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            /** @type {?} */

            var winHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

            if (this.left + this.cpWidth > winWidth) {
              usePositionX = 'left';
            }

            if (this.top + dialogHeight > winHeight) {
              usePositionY = 'top';
            }

            usePosition = usePositionX + '-' + usePositionY;
          }

          this.cpUsePosition = usePosition;

          if (this.cpPosition === 'top') {
            this.arrowTop = dialogHeight - 1;
            this.top -= dialogHeight + this.dialogArrowSize;
            this.left += this.cpPositionOffset / 100 * boxDirective.width - this.dialogArrowOffset;
          } else if (this.cpPosition === 'bottom') {
            this.top += boxDirective.height + this.dialogArrowSize;
            this.left += this.cpPositionOffset / 100 * boxDirective.width - this.dialogArrowOffset;
          } else if (usePosition === 'top-left' || usePosition === 'left-top') {
            this.top -= dialogHeight - boxDirective.height + boxDirective.height * this.cpPositionOffset / 100;
            this.left -= this.cpWidth + this.dialogArrowSize - 2 - this.dialogArrowOffset;
          } else if (usePosition === 'top-right' || usePosition === 'right-top') {
            this.top -= dialogHeight - boxDirective.height + boxDirective.height * this.cpPositionOffset / 100;
            this.left += boxDirective.width + this.dialogArrowSize - 2 - this.dialogArrowOffset;
          } else if (this.cpPosition === 'left' || usePosition === 'bottom-left' || usePosition === 'left-bottom') {
            this.top += boxDirective.height * this.cpPositionOffset / 100 - this.dialogArrowOffset;
            this.left -= this.cpWidth + this.dialogArrowSize - 2;
          } else {
            // this.cpPosition === 'right' || usePosition === 'bottom-right' || usePosition === 'right-bottom'
            this.top += boxDirective.height * this.cpPositionOffset / 100 - this.dialogArrowOffset;
            this.left += boxDirective.width + this.dialogArrowSize - 2;
          }
        }
      }; // Private helper functions for the color picker dialog positioning and opening
      // Private helper functions for the color picker dialog positioning and opening

      /**
       * @private
       * @param {?} parent
       * @param {?} child
       * @return {?}
       */


      ColorPickerComponent.prototype.isDescendant = // Private helper functions for the color picker dialog positioning and opening

      /**
       * @private
       * @param {?} parent
       * @param {?} child
       * @return {?}
       */
      function (parent, child) {
        /** @type {?} */
        var node = child.parentNode;

        while (node !== null) {
          if (node === parent) {
            return true;
          }

          node = node.parentNode;
        }

        return false;
      };
      /**
       * @private
       * @param {?} element
       * @param {?} offset
       * @return {?}
       */


      ColorPickerComponent.prototype.createDialogBox =
      /**
      * @private
      * @param {?} element
      * @param {?} offset
      * @return {?}
      */
      function (element, offset) {
        return {
          top: element.getBoundingClientRect().top + (offset ? window.pageYOffset : 0),
          left: element.getBoundingClientRect().left + (offset ? window.pageXOffset : 0),
          width: element.offsetWidth,
          height: element.offsetHeight
        };
      };

      ColorPickerComponent.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'color-picker',
          template: "<div #dialogPopup class=\"color-picker\" [class.open]=\"show\" [style.display]=\"!show ? 'none' : 'block'\" [style.visibility]=\"hidden ? 'hidden' : 'visible'\" [style.top.px]=\"top\" [style.left.px]=\"left\" [style.position]=\"position\" [style.height.px]=\"cpHeight\" [style.width.px]=\"cpWidth\" (click)=\"$event.stopPropagation()\">\n  <div *ngIf=\"cpDialogDisplay=='popup'\" class=\"arrow arrow-{{cpUsePosition}}\" [style.top.px]=\"arrowTop\"></div>\n\n  <div *ngIf=\"(cpColorMode ||\xA01) === 1\" class=\"saturation-lightness\" [slider] [rgX]=\"1\" [rgY]=\"1\" [style.background-color]=\"hueSliderColor\" (newValue)=\"onColorChange($event)\" (dragStart)=\"onDragStart('saturation-lightness')\" (dragEnd)=\"onDragEnd('saturation-lightness')\">\n    <div class=\"cursor\" [style.top.px]=\"slider?.v\" [style.left.px]=\"slider?.s\"></div>\n  </div>\n\n  <div class=\"hue-alpha box\">\n    <div class=\"left\">\n      <div class=\"selected-color-background\"></div>\n\n      <div class=\"selected-color\" [style.background-color]=\"selectedColor\"></div>\n\n      <button *ngIf=\"cpAddColorButton\" type=\"button\" class=\"{{cpAddColorButtonClass}}\" [disabled]=\"cpPresetColors && cpPresetColors.length >= cpMaxPresetColorsLength\" (click)=\"onAddPresetColor($event, selectedColor)\">\n        {{cpAddColorButtonText}}\n      </button>\n    </div>\n\n    <div class=\"right\">\n      <div *ngIf=\"cpAlphaChannel==='disabled'\" style=\"height: 16px;\"></div>\n\n      <div #hueSlider class=\"hue\" [slider] [rgX]=\"1\" [style.display]=\"(cpColorMode ||\xA01) === 1 ? 'block' : 'none'\" (newValue)=\"onHueChange($event)\" (dragStart)=\"onDragStart('hue')\" (dragEnd)=\"onDragEnd('hue')\">\n        <div class=\"cursor\" [style.left.px]=\"slider?.h\"></div>\n      </div>\n\n      <div #valueSlider class=\"value\" [slider] [rgX]=\"1\" [style.display]=\"(cpColorMode ||\xA01) === 2 ? 'block': 'none'\" (newValue)=\"onValueChange($event)\" (dragStart)=\"onDragStart('value')\" (dragEnd)=\"onDragEnd('value')\">\n        <div class=\"cursor\" [style.right.px]=\"slider?.v\"></div>\n      </div>\n\n      <div #alphaSlider class=\"alpha\" [slider] [rgX]=\"1\" [style.display]=\"cpAlphaChannel === 'disabled' ? 'none' : 'block'\" [style.background-color]=\"alphaSliderColor\" (newValue)=\"onAlphaChange($event)\" (dragStart)=\"onDragStart('alpha')\" (dragEnd)=\"onDragEnd('alpha')\">\n        <div class=\"cursor\" [style.left.px]=\"slider?.a\"></div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"cmyk-text\" [style.display]=\"format !== 3 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.c\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onCyanInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.m\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onMagentaInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.y\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onYellowInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.k\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onBlackInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"cmykText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n     <div class=\"box\">\n      <div>C</div><div>M</div><div>Y</div><div>K</div><div *ngIf=\"cpAlphaChannel!=='disabled'\" >A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1 \" class=\"hsla-text\" [style.display]=\"format !== 2 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"360\" [text] [rg]=\"360\" [value]=\"hslaText?.h\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onHueInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.s\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onSaturationInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.l\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onLightnessInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"hslaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>H</div><div>S</div><div>L</div><div *ngIf=\"cpAlphaChannel!=='disabled'\">A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1 \" [style.display]=\"format !== 1 ? 'none' : 'block'\" class=\"rgba-text\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.r\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onRedInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.g\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onGreenInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.b\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onBlueInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"rgbaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>R</div><div>G</div><div>B</div><div *ngIf=\"cpAlphaChannel!=='disabled'\" >A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"hex-text\" [class.hex-alpha]=\"cpAlphaChannel==='forced'\"\n    [style.display]=\"format !== 0 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input [text] [value]=\"hexText\" (blur)=\"onHexInput(null)\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onHexInput($event)\"/>\n      <input *ngIf=\"cpAlphaChannel==='forced'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"hexAlpha\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\"/>\n    </div>\n\n    <div class=\"box\">\n      <div>Hex</div>\n      <div *ngIf=\"cpAlphaChannel==='forced'\">A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 2\" class=\"value-text\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.l\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onValueInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\"  [text] [rg]=\"1\" [value]=\"hslaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>V</div><div>A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"type-policy\">\n    <span class=\"type-policy-arrow\" (click)=\"onFormatToggle(-1)\"></span>\n    <span class=\"type-policy-arrow\" (click)=\"onFormatToggle(1)\"></span>\n  </div>\n\n  <div *ngIf=\"cpPresetColors?.length || cpAddColorButton\" class=\"preset-area\">\n    <hr>\n\n    <div class=\"preset-label\">{{cpPresetLabel}}</div>\n\n    <div *ngIf=\"cpPresetColors?.length\">\n      <div *ngFor=\"let color of cpPresetColors\" class=\"preset-color\" [style.backgroundColor]=\"color\" (click)=\"setColorFromString(color)\">\n        <span *ngIf=\"cpAddColorButton\" class=\"{{cpRemoveColorButtonClass}}\" (click)=\"onRemovePresetColor($event, color)\"></span>\n      </div>\n    </div>\n\n    <div *ngIf=\"!cpPresetColors?.length && cpAddColorButton\" class=\"{{cpPresetEmptyMessageClass}}\">{{cpPresetEmptyMessage}}</div>\n  </div>\n\n  <div *ngIf=\"cpOKButton || cpCancelButton\" class=\"button-area\">\n    <button *ngIf=\"cpCancelButton\" type=\"button\" class=\"{{cpCancelButtonClass}}\" (click)=\"onCancelColor($event)\">{{cpCancelButtonText}}</button>\n\n    <button *ngIf=\"cpOKButton\" type=\"button\" class=\"{{cpOKButtonClass}}\" (click)=\"onAcceptColor($event)\">{{cpOKButtonText}}</button>\n  </div>\n</div>\n",
          encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
          styles: [".color-picker {\n  position: absolute;\n  z-index: 100000;\n\n  width: 230px;\n  height: auto;\n  border: #777 solid 1px;\n\n  cursor: default;\n\n  -webkit-user-select: none;\n  -khtml-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n\n  user-select: none;\n  background-color: #fff;\n}\n\n.color-picker * {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n\n  box-sizing: border-box;\n  margin: 0;\n\n  font-size: 11px;\n}\n\n.color-picker input {\n  width: 0;\n  height: 26px;\n  min-width: 0;\n\n  font-size: 13px;\n  text-align: center;\n  color: #000;\n}\n\n.color-picker input:invalid,\n.color-picker input:-moz-ui-invalid,\n.color-picker input:-moz-submit-invalid {\n  box-shadow: none;\n}\n\n.color-picker input::-webkit-inner-spin-button,\n.color-picker input::-webkit-outer-spin-button {\n  margin: 0;\n\n  -webkit-appearance: none;\n}\n\n.color-picker .arrow {\n  position: absolute;\n  z-index: 999999;\n\n  width: 0;\n  height: 0;\n  border-style: solid;\n}\n\n.color-picker .arrow.arrow-top {\n  left: 8px;\n\n  border-width: 10px 5px;\n  border-color: #777 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);\n}\n\n.color-picker .arrow.arrow-bottom {\n  top: -20px;\n  left: 8px;\n\n  border-width: 10px 5px;\n  border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #777 rgba(0, 0, 0, 0);\n}\n\n.color-picker .arrow.arrow-top-left,\n.color-picker .arrow.arrow-left-top {\n  right: -21px;\n  bottom: 8px;\n\n  border-width: 5px 10px;\n  border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #777;\n}\n\n.color-picker .arrow.arrow-top-right,\n.color-picker .arrow.arrow-right-top {\n  bottom: 8px;\n  left: -20px;\n\n  border-width: 5px 10px;\n  border-color: rgba(0, 0, 0, 0) #777 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);\n}\n\n.color-picker .arrow.arrow-left,\n.color-picker .arrow.arrow-left-bottom,\n.color-picker .arrow.arrow-bottom-left {\n  top: 8px;\n  right: -21px;\n\n  border-width: 5px 10px;\n  border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #777;\n}\n\n.color-picker .arrow.arrow-right,\n.color-picker .arrow.arrow-right-bottom,\n.color-picker .arrow.arrow-bottom-right {\n  top: 8px;\n  left: -20px;\n\n  border-width: 5px 10px;\n  border-color: rgba(0, 0, 0, 0) #777 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);\n}\n\n.color-picker .cursor {\n  position: relative;\n\n  width: 16px;\n  height: 16px;\n  border: #222 solid 2px;\n  border-radius: 50%;\n\n  cursor: default;\n}\n\n.color-picker .box {\n  display: flex;\n  padding: 4px 8px;\n}\n\n.color-picker .left {\n  position: relative;\n\n  padding: 16px 8px;\n}\n\n.color-picker .right {\n  -webkit-flex: 1 1 auto;\n  -ms-flex: 1 1 auto;\n\n  flex: 1 1 auto;\n\n  padding: 12px 8px;\n}\n\n.color-picker .button-area {\n  padding: 0 16px 16px;\n\n  text-align: right;\n}\n\n.color-picker .preset-area {\n  padding: 4px 15px;\n}\n\n.color-picker .preset-area .preset-label {\n  overflow: hidden;\n  width: 100%;\n  padding: 4px;\n\n  font-size: 11px;\n  white-space: nowrap;\n  text-align: left;\n  text-overflow: ellipsis;\n  color: #555;\n}\n\n.color-picker .preset-area .preset-color {\n  position: relative;\n\n  display: inline-block;\n  width: 18px;\n  height: 18px;\n  margin: 4px 6px 8px;\n  border: #a9a9a9 solid 1px;\n  border-radius: 25%;\n\n  cursor: pointer;\n}\n\n.color-picker .preset-area .preset-empty-message {\n  min-height: 18px;\n  margin-top: 4px;\n  margin-bottom: 8px;\n\n  font-style: italic;\n  text-align: center;\n}\n\n.color-picker .hex-text {\n  width: 100%;\n  padding: 4px 8px;\n\n  font-size: 11px;\n}\n\n.color-picker .hex-text .box {\n  padding: 0 24px 8px 8px;\n}\n\n.color-picker .hex-text .box div {\n  float: left;\n\n  -webkit-flex: 1 1 auto;\n  -ms-flex: 1 1 auto;\n\n  flex: 1 1 auto;\n\n  text-align: center;\n  color: #555;\n  clear: left;\n}\n\n.color-picker .hex-text .box input {\n  -webkit-flex: 1 1 auto;\n  -ms-flex: 1 1 auto;\n\n  flex: 1 1 auto;\n  padding: 1px;\n  border: #a9a9a9 solid 1px;\n}\n\n.color-picker .hex-alpha .box div:first-child,\n.color-picker .hex-alpha .box input:first-child {\n  flex-grow: 3;\n  margin-right: 8px;\n}\n\n.color-picker .cmyk-text,\n.color-picker .hsla-text,\n.color-picker .rgba-text,\n.color-picker .value-text {\n  width: 100%;\n  padding: 4px 8px;\n\n  font-size: 11px;\n}\n\n.color-picker .cmyk-text .box,\n.color-picker .hsla-text .box,\n.color-picker .rgba-text .box {\n  padding: 0 24px 8px 8px;\n}\n\n.color-picker .value-text .box {\n  padding: 0 8px 8px;\n}\n\n.color-picker .cmyk-text .box div,\n.color-picker .hsla-text .box div,\n.color-picker .rgba-text .box div,\n.color-picker .value-text .box div {\n  -webkit-flex: 1 1 auto;\n  -ms-flex: 1 1 auto;\n\n  flex: 1 1 auto;\n  margin-right: 8px;\n\n  text-align: center;\n  color: #555;\n}\n\n.color-picker .cmyk-text .box div:last-child,\n.color-picker .hsla-text .box div:last-child,\n.color-picker .rgba-text .box div:last-child,\n.color-picker .value-text .box div:last-child {\n  margin-right: 0;\n}\n\n.color-picker .cmyk-text .box input,\n.color-picker .hsla-text .box input,\n.color-picker .rgba-text .box input,\n.color-picker .value-text .box input {\n  float: left;\n\n  -webkit-flex: 1;\n  -ms-flex: 1;\n\n  flex: 1;\n  padding: 1px;\n  margin: 0 8px 0 0;\n  border: #a9a9a9 solid 1px;\n}\n\n.color-picker .cmyk-text .box input:last-child,\n.color-picker .hsla-text .box input:last-child,\n.color-picker .rgba-text .box input:last-child,\n.color-picker .value-text .box input:last-child {\n  margin-right: 0;\n}\n\n.color-picker .hue-alpha {\n  align-items: center;\n  margin-bottom: 3px;\n}\n\n.color-picker .hue {\n  direction: ltr;\n\n  width: 100%;\n  height: 16px;\n  margin-bottom: 16px;\n  border: none;\n\n  cursor: pointer;\n  background-size: 100% 100%;\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwkUFWbCCAAAAFxJREFUaN7t0kEKg0AQAME2x83/n2qu5qCgD1iDhCoYdpnbQC9bbY1qVO/jvc6k3ad91s7/7F1/csgPrujuQ17BDYSFsBAWwgJhISyEBcJCWAgLhIWwEBYIi2f7Ar/1TCgFH2X9AAAAAElFTkSuQmCC');\n}\n\n.color-picker .value {\n  direction: rtl;\n\n  width: 100%;\n  height: 16px;\n  margin-bottom: 16px;\n  border: none;\n\n  cursor: pointer;\n  background-size: 100% 100%;\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAACTklEQVR42u3SYUcrABhA4U2SkmRJMmWSJklKJiWZZpKUJJskKUmaTFImKZOUzMySpGRmliRNJilJSpKSJEtmSpIpmWmSdO736/6D+x7OP3gUCoWCv1cqlSQlJZGcnExKSgqpqamkpaWRnp5ORkYGmZmZqFQqsrKyyM7OJicnh9zcXNRqNXl5eeTn56PRaCgoKKCwsJCioiK0Wi3FxcWUlJRQWlpKWVkZ5eXlVFRUUFlZiU6no6qqiurqampqaqitraWurg69Xk99fT0GgwGj0UhDQwONjY00NTXR3NxMS0sLra2ttLW10d7ejslkwmw209HRQWdnJ11dXXR3d9PT00Nvby99fX309/czMDDA4OAgFouFoaEhrFYrw8PDjIyMMDo6ytjYGDabjfHxcSYmJpicnGRqagq73c709DQzMzPMzs4yNzfH/Pw8DocDp9OJy+XC7XazsLDA4uIiS0tLLC8vs7KywurqKmtra3g8HrxeLz6fD7/fz/r6OhsbG2xubrK1tcX29jaBQICdnR2CwSC7u7vs7e2xv7/PwcEBh4eHHB0dcXx8zMnJCaenp5ydnXF+fs7FxQWXl5dcXV1xfX3Nzc0Nt7e33N3dEQqFuL+/5+HhgXA4TCQS4fHxkaenJ56fn3l5eeH19ZVoNMrb2xvv7+98fHwQi8WIx+N8fn6SSCT4+vri+/ubn58ffn9/+VcKgSWwBJbAElgCS2AJLIElsASWwBJYAktgCSyBJbAElsASWAJLYAksgSWwBJbAElgCS2AJLIElsP4/WH8AmJ5Z6jHS4h8AAAAASUVORK5CYII=');\n}\n\n.color-picker .alpha {\n  direction: ltr;\n\n  width: 100%;\n  height: 16px;\n  border: none;\n\n  cursor: pointer;\n  background-size: 100% 100%;\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwYQlZMa3gAAAWVJREFUaN7tmEGO6jAQRCsOArHgBpyAJYGjcGocxAm4A2IHpmoWE0eBH+ezmFlNvU06shJ3W6VEelWMUQAIIF9f6qZpimsA1LYtS2uF51/u27YVAFZVRUkEoGHdPV/sIcbIEIIkUdI/9Xa7neyv61+SWFUVAVCSct00TWn2fv6u3+Ecfd3tXzy/0+nEUu+SPjo/kqzrmiQpScN6v98XewfA8/lMkiLJ2WxGSUopcT6fM6U0NX9/frfbjev1WtfrlZfLhYfDQQHG/AIOlnGwjINlHCxjHCzjYJm/TJWdCwquJXseFFzGwDNNeiKMOJTO8xQdDQaeB29+K9efeLaBo9J7vdvtJj1RjFFjfiv7qv95tjx/7leSQgh93e1ffMeIp6O+YQjho/N791t1XVOSSI7N//K+4/GoxWLBx+PB5/Op5XLJ+/3OlJJWqxU3m83ovv5iGf8KjYNlHCxjHCzjYBkHy5gf5gusvQU7U37jTAAAAABJRU5ErkJggg==');\n}\n\n.color-picker .type-policy {\n  position: absolute;\n  top: 218px;\n  right: 12px;\n\n  width: 16px;\n  height: 24px;\n\n  background-size: 8px 16px;\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAgCAYAAAAffCjxAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACewAAAnsB01CO3AAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIASURBVEiJ7ZY9axRRFIafsxMStrLQJpAgpBFhi+C9w1YSo00I6RZ/g9vZpBf/QOr4GyRgkSKNSrAadsZqQGwCkuAWyRZJsySwvhZ7N/vhzrgbLH3Ld8597jlzz50zJokyxXH8DqDVar0qi6v8BbItqSGpEcfxdlmsFWXkvX8AfAVWg3UKPEnT9GKujMzsAFgZsVaCN1VTQd77XUnrgE1kv+6935268WRpzrnHZvYRWC7YvC3pRZZl3wozqtVqiyH9IgjAspkd1Gq1xUJQtVrdB9ZKIAOthdg/Qc65LUk7wNIMoCVJO865rYFhkqjX6/d7vV4GPJwBMqofURS5JEk6FYBer/eeYb/Mo9WwFnPOvQbeAvfuAAK4BN4sAJtAG/gJIElmNuiJyba3EGNmZiPeZuEVmVell/Y/6N+CzDn3AXhEOOo7Hv/3BeAz8IzQkMPnJbuPx1wC+yYJ7/0nYIP5S/0FHKdp+rwCEEXRS/rf5Hl1Gtb2M0iSpCOpCZzPATmX1EySpHMLAsiy7MjMDoHrGSDXZnaYZdnRwBh7J91utwmczAA6CbG3GgPleX4jqUH/a1CktqRGnuc3hSCAMB32gKspkCtgb3KCQMmkjeP4WNJThrNNZval1WptTIsv7JtQ4tmIdRa8qSoEpWl6YWZNoAN0zKxZNPehpLSBZv2t+Q0CJ9lLnARQLAAAAABJRU5ErkJggg==');\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n.color-picker .type-policy .type-policy-arrow {\n  display: block;\n\n  width: 100%;\n  height: 50%;\n}\n\n.color-picker .selected-color {\n  position: absolute;\n  top: 16px;\n  left: 8px;\n\n  width: 40px;\n  height: 40px;\n  border: 1px solid #a9a9a9;\n  border-radius: 50%;\n}\n\n.color-picker .selected-color-background {\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAh0lEQVRYR+2W0QlAMQgD60zdfwOdqa8TmI/wQMr5K0I5bZLIzLOa2nt37VVVbd+dDx5obgCC3KBLwJ2ff4PnVidkf+ucIhw80HQaCLo3DMH3CRK3iFsmAWVl6hPNDwt8EvNE5q+YuEXcMgkonVM6SdyCoEvAnZ8v1Hjx817MilmxSUB5rdLJDycZgUAZUch/AAAAAElFTkSuQmCC');\n}\n\n.color-picker .saturation-lightness {\n  direction: ltr;\n\n  width: 100%;\n  height: 130px;\n  border: none;\n\n  cursor: pointer;\n  touch-action: manipulation;\n  background-size: 100% 100%;\n  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAACCCAYAAABSD7T3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwksPWR6lgAAIABJREFUeNrtnVuT47gRrAHN+P//Or/61Y5wONZ7mZ1u3XAeLMjJZGZVgdKsfc5xR3S0RIIUW+CHzCpc2McYo7XGv3ex7UiZd57rjyzzv+v+33X/R/+3r/f7vR386Y+TvKNcf/wdhTLPcv9qU2wZd74uth0t1821jkIZLPcsI/6nWa4XvutquU0Z85mnx80S/ZzgpnLnOtHNt7/ofx1TKXcSNzN/7qbMQ3ju7rNQmMYYd/4s2j9aa+P+gGaMcZrb1M/tdrvf7/d2v99P9/t93O/3cbvdxu12G9frdVwul3E+n8c///nP+2+//Xb66aefxl//+tfx5z//2YK5Al2rgvf4UsbpdGrB52bAvArXpuzjmiqAVSGz5eDmGYXzhbAZmCrnmzddpUU+8Y1dAOYeXCtDUwVwV7YCGH6uAmyMcZ9l5vkUaBPGMUZ7/J5w/792/fvv9Xq93263dr/fTxPECeME8nK5jM/Pz/HTTz/dv337dvrll1/GP/7xj/G3v/1t/OUvfwkVswongjdOp9PzH3U3D3zmWGnZVXn4jCqs7wC2BKP4/8tAzkZsoWx6XrqeHZymvp4ABCBJhTQwKfDT8gzrZCIqi5AhiACjBfEB2rP8/X63MM7f6/V6v9/v7Xa7bYC83W7jcrlsVHIq5ffv30+//fbb+OWXX8ZPP/00/v73v4+ff/75JSvbeu+bL2WMMaFbAlpBNM85QX+ct6qoSqkPAwuQlBVKqGNFSUOAA3Bmu7gC5hNOd15nSwvAOUW7C4giUCV8Sgn5L9hNFIqTsp0GxI0ysioyjAjkY/tGJVEpz+fz+OWXX+7fv38//f777+Pbt2/j119/HT///PP49ddfx8fHRwrmTjV779EXu2px2xhjwtdJZQcAWQIPLPISsMJaSwiD8gzIKrwSyATE5j5nAbR5c1dBUwBlsEWW0h6LqiYsqFPAQxCyRZ3wOSARxmlXMX5k64pQfvv27f75+dk+Pj5OHx8f4/v37+Pbt2/jt99+G9++fRsfHx/jcrmUFLO31gYDWblxRIs/TqfT7ousxJsAxXA2Gc7TA9XdgfdoHbFsj76X2+1WArgI1ageGwA3qupqoHsmcbI6Fu93quggFa9d7LeDtgKfAFHBJ+NEByIkcJ5KervdTmhhGcgJJSZ5vn//fj+fz+18Pp8+Pz/H5+fnmGD+/vvv4/v37+Pj42N8fn6O2+1Ws7JjjP6wraMI5E4RZ8x2vV5TSwkquotV7/d7Tz6HFWsD/qNcdw0CQ3q/321c686TwDVIdbuy73zNldhSHb8I2klZznm+InBS4U6n0302aBFsLhHDAKJVJVglfI9jhvu53W53sLANYNxAiDA6MCeUHx8f9+v12i6XS7tcLqcZW57P5yeY8/fz83Ocz+fnsSmYUyknWEG85WBst9stzSLyMdfr9Qi08iY15UZ0LlDGLhR3o5zK2j7OPUTD0E+nU3tk7Xb/16NFbhloAMuY1zjLUOO3BKeIDe+Z8s3/J4gFo4TM5jPmuRg28foUKKVSwo16TgA5npywcWLHgYl/Pz8/73/605/ab7/91m63W7tcLie0sZj4mao5gTyfz88E0f1+j8EcYzwTPEG2cqjyfHNF0M8fuqEiaOVnRzZZQNh5fwQyHg/HDGfJo89Q1zb/quu5XC6773I2XKfTqd/v9+d3wuqWva/YTdUdEV3fhIv/Viyps6YE3x3r43K5bJQS66zaxVGFsvd+//j4aF+/fm3fv39vt9utff36tf3+++/tdrudvn37ZuNLBaaCMgUzC+rZRiFowxUuJI8YMqcCp9Opq5vagaYU6lGJA1XQqejchw6Cj0Gw5nYBrGw01A2O206n04BGouNNyTfp/FwElhUey6nXrIKw7QQWddxuN2ldL5fL839gSPF8ahu/JvBO48CPSuqMf8Vp9/P53L58+dLu93s7n8/tfr8/39/v9/b5+TkhPJ3P56mQ436/j+/fv+/iSgbzer0+AZx/5+88bv6OMda6S5z6kd21fYC9dxv7cIJJ2d9AOS30fPMzyHiTM8B4DF6XUlYHp4KQW3W+1t77MNB1vGHxWq7Xa7vf78+y5/N5A+H1et29xuP5dbYtyaRu4AksbPq6936fjRzXRxBbPr/b+b18+fKljTHaBBBfn8/n0/1+H1++fBnn8zm0sB8fH5u4cr5GuBhMVk0EEn9RsctgVhM+ixlJtMA23R8B6yysAstBOgFXIKKCMIgToMqNEu2fYMH7ztc732dQKkCj1ytAZtY0Kx8pIr8GGJ+AT3V+2Hirhl++fBmXy2Wz73w+b17P8p+fn8/tUwGVleVkTyUb68DkfayWY4zxNRihU4EpLJPZVrK+u7J4/mgfKqeLW9X2REWlItL1diynbDDb3+jXgYjQqn0rrxWc+NkILP7F7xIbMvx7vV53x40xnlbWJF12ZSag/N0pW6t+ZzmOMzHjajKwDfond78zYTdfq18up97zr2q8v3IioBprRtBl0EZ9og5WBRGOdOHjIjXF7UotFbgOWnXzIJyzYvjG5IYgsmMOxHkz8OsMSrVNWeq5T8DaOcbEv1Od5rbs9aO7YvMet63EkF++fMExq+MRl4/L5bLZN/+ez+fnZ6KazuMqXSQVO5spJXflHAIzes/xJseckRJiDMog9d6VfRrqXMr6KpVV27jRwJacGovOAM1zMdQMnwK1AubK63kdCChvI1C7g0z9nf/D+Xze2Vj8H7Gx4P9duQlsYCrqyN8XqG3Hm/10Oj3jw/n+crlstuM+jPmmxT2dTuPz83Pzt2pn1XsEHX/bnPaVqVmh0xwOt0o6XLLAHePUU203wHfcrspCwmV3TryB5s0Mseeg97x/BwzCjBlbB+pRAPla0BVQuT6V6QHdBlj3d0KG147b+DqxQeUymDO43W4dQar+TIjwmAd0z8/h65vf0/yLv3Pb5XLpru/ydDo9s7ET0I+Pj6dKK9VUEIeKWQWPAOrJ8LKd4vE+t91Y3e7UFlWatg2VwJnb+HPmtvm/sfK59/OaWF3x/eP1UPHvA5DDYDpYXfb0drv1V2DkBkxtw/tEWVVlXWdC9pFYs5/jfh9dS/16vW7s6lTG+TfqsxSJHxkXXq/Xdr1eu4LsfD6P3vsT3N77DkL+zPm5jSdKL4zR3AxQd6rHkLkYlSowsrq7znzu6wSwdsMJOXmA5fBcjxtgMGBYHlr5zokhtsMCTgXLQOW4XC6dEyEMprL8mAQzXRgduix2yZzorxkYsDn3hB1VeMLGsXsVtgl2pW8S3svk0vw7R4hNaHvv4cACl5HFzwIH0Kc6zu4XjDPR/jpAVxWzO1Xk2DDb3vTcxeGU1iWZHkmIDWziWKvirCJ4Dravs6IJ/GG6cTqWdXDy+fArQDVVkLqkVjAoZIITdmmIqXwqa95N3+MGYoZQdRVNO53Y1xRkhO16vY7eu507Ca9lJnbGpxOemQhSw/AQsmmp5zU9BiU8G6wvX76M6/U6Pj4+do0Bz4CpgiknTUeDqwlKBmg3u4OVjrZ1A+rAcgaejWq6eJCvCYFDONSwOgHX4EQRw8lxbzDOdEK6gZ3Hk1b+8g2o1JFtKXyv/fEdTXuWjWXdAZiBp6ADeDrCFiim7B6ZFneeI7Gvm/PMkUDX67W7xI8b0D7/v8dA9qfN5oaCf74WZjH0mf1cmfY1Y0JUFmVrTWu8uzkNcLtEj7u5FXBTkfC6GOA5q8YMxO8KVvF6sAVGdcrUbsKODcQKkLMOMdmlxum642YrPm26AlhZW1YB1R+rrGswE8TaYAWeUMxdf+WjwSvZ2Ef3ytOyfn5+PpVPAaqOn43MtNBqvmjjxbjM4lZjZY4gqNMI5ktaW/sYKNwS+9lFQzGihmMCKPa7+Z0V6Eb0GRmobtpX8JljWu5FMLN5ja6hG9kwQgZqf5+1NH5UxzkFReCdWhJ8XdlGUkxO7HRlYRm4mVO43W7ter12TPJEw/rmEN3L5SKHIWZg9mz+pUoKOYq5bJTJdX2gme1UcxMZQFaEQIlHct32M+Y1BzGkGuzfiyAN9z+ugplZ1symCrDCYYkGxDTpI9RzBy0rHyeDUC1nWaeUaD9n4xkNyYMBDZtzZ3B++fJlY21XFDOcARJlabOyiS3uCpLI9jrZjCDkaVvcCCjwognKShWdzXZWlZMvVTgD8LpqlCLrqgbcB+qYwrgKYpT0ccCqbKyCValkEabn/FynogCrPKfqf51xJ7sGB2ZXcZmxoSOztjx300DZi7a0/2AIR0UlBag9SuDw6KcAzlaB7vHZvWpjK90dyrq6bKyDUZQbR0B05biLQkHIcSUmgIK+SwuqgHCnoio2RQU1yj+BnBy9pphVKLGyC7ZzFK1pxWK+E8IhVCWLN/uLtnUU4ayoYLoaANz8FdtaSvY4pV0BEW2ls61czqllBKpTyKgMAhrZ1cdc1RROtPmvWNkdcKZ7ZKxaWjiPLJMpp7OZKxA+rqG/oJLjxf0pnJlqLoDZo3gyU0mKGys2taKecj/d1C+rJSplBqlTyAqgR+D8KjKlmRL2gtUcAdCtsL+ijCNT1oqqqkH2OHEbG5sDFnUg5Aa+yLou2VU1ptj1S2ZQqv1ORZN9IWzRfgaRBxKoBE8UWyqlJFtrIc0AxNjSjed99CTY/XDfSzCz5M0IZoVEsWnPFNTsl8ooVC1TzbGgqFZNDSgVwKK+1sGDMKqxZCWGVMDysiEr1jVSQJUYwj5iHOlThdHt44SQg9CN+nl8D90NMIgAdgr46JqRiR9I8vRdFvbr17m/yxUMKjNLMiVUADwu2CWGhhi+F55TWM9M9cogzms1dnM4uOF/LAEYWdcqnM7yFmyq3IfwmOROd7Y1iFWtOjoY8To41mTV5IysgFFuRzsbWFGbNIIJCDv1dOo4lZG7jWBwRFtVTKuWyeCByJKOan8oZ3ep9XddNl0tDuaywLz9cXPYeDAA0SpkBO9sbVcTOVWldPv4uyzEkzxHtjvonHoSkFEWNoo1d8DhcQputd2ppNon4BzoAiJ1hBFQg0dVtdbGHHDQWushmNEQukLM2QO1G2Y8bgTXqFhcBJj7EjPgcPts8US8qPpPB/dXznOh5Z438tzH5ec6QgrOKrRRfKmysBmUDB+PhYabMlVPER+GCSITTzr7am2tArH3bgcEzPJm+cr5jJ4NnHNFDVrFXcI5Le9k5Jnw+bedbV+FfRzZIHaOOaOsLY0/7UGs58DjrGwKMIMFIGzOEW1/jGsdAtCN6hEAI4hBe9YXeRROBSVPAVPAqvIM5bx5hVKWAMP6zBRy3iescridVdFBinBxXDnG2GRY2XbCvp1lhvGtO9Bxu5h908XQu42lnSArMFdizMim8uwRCxPGnnOS8lwpnbOiDqTAjsrRN/PcoAScCbaACqVM40ylnjjTBs+bwWlAG23/UKbdkiwKWIQPGzWaczpoSlxPEj822cNWkpS7FyzsDrqpfgpG3jahw2vgbaSQAxuLWZYt7JzyNe8JoZpNAcvDFOdw0wqYT9AK1rZz/DdbSlLPp0ryIxgQJlK9AZlEq7IOXpohg9PIhrCng88JsOxiV4ZWAYfg4sikx/8ky2Z9l862uqwrfscIH8+ugTmVGyiddeVYUgEMn4GZzg14EwIsh9sx2cKKiWXReuOE5gzGOQgdlRKVVdlevqb279Xq0Qnsts2VDaBO0coezsruWtHApu6sKG4IBhN0aGU2kLrMKGRTN3HmbCDwKV14zvkMEDG4QfZVspVlaNU2mhc5TEZ3N1h/zqTheuLpW05ZWTGVjb3dbnNmxKZBnN8JqidaVLKAOyARNLS+MB54Z2+VaqoMLKroVBlngefnTPAcoHNWCSvlfA8CI0HEmBNBnBlXyMrzU7A7WVm94PPqQ2gmqKx+WDGsnvilmcSOBJqOK1nYyAIzuAyesq3UdSK3KfWcYKD95HmfYOU3qser2CtYEUA+FpfqdNvgPBZUBhDrGONRVlQsh8rLcaUCykHG0OOUwTlLBrsh5soEMGezi1E4HRVt1icp5wZEFXdibCkG8Y8vX75sbO4E0iom9z+hjSiOfy3DhpXItpVhE+UGQdvoWjtChmrGHf4YAzKgBNnGtuJxFCeGdhUAfQLLK8kBYAP6gvFJZajMG3Xkycy8KuC0q4Eyymwtwdxdv2M0mIBtK0LKnf640j00Auq4gUkdWGlhs22qJc6dZCsL19oxnlTJG4SYVRIGpD8TPFBuM6OElbS1pldid4mGAyN6ZIupbC5bXJN9fdpbThSxLUaI8IG1XIYBxW3Tjs6KQosKcxfxcQmdnwRGM10GnFcCy2XYunLMyAkdgk4mePiczsLygthcBut6goOqS7YVFXADLjaosB6s6ofcZWAZSIRYqSUkizYwttYab3vUOQ9w2HRxIIg8WwRVeE68xi4UtL3zRphxplzwuZrcqYCq1I3jPI5dnJIygEohMbPqVJSzrwzxBJTs5zN+ReUSgxikPQVF3JVBeNQxbHENrEMNvEdFZVV9lH9+ORGEsNZQpyTNc4C3AG7XF4ngzq+DrO2zbuaaOXgdaFcdkEotoSFBVX2qJ0C8OWZeG4KGlpghA0XfTOPCqV2qqwQ26QWfF2PMLhI2w1lVAa2aPsYd0za25MQRwgcZN6uQDCi+ZxiD4XEM2kZxOT41FnZnaRlcpZouzlRqqdbQVWopQoSB58RV50lBNrHi/AwXS5LrwDVlpY3Fc3ByiYGc52Trist6kOXdwInAQtJpp5QchyaquYOV7Su+fxVMaV3dc0RE2S6mUY0gLt2pMcYqrKIQ9w2l1gpQUMtQYcmmbt5DTNxdhnUCjQqtbK9SUSzvrC0mmhhE1e2FS2+oxypy/ZASutkmtjx3vcBC24PX65nbqkBCRhfjS9kIYPnee8cMagVOhI/3T1fAmdtAWZsCswTJCkQVNa0qWKSKPOpHAUhD9DrbVcyoYkwqhvh17vYAayXLQyKGYdxlUDFp494rBXRjYgO17DDYetNIUj/ezp6S0lnlpEwsWmJMkOwsKXeZKEAjIHn0EQJISaRBcO6UMINz7p/bEjjnw4ft+xmDvksxX4G2rIris7qaeKwAFMP2Oi7n4criuZwtpSUwpfLxSnORSrIqusc5ZFaXysqRWjiZ2DyAWEIL35tVSoQElFACjOeGGSE7AHEQgdo/LSvCOgGBvkxsmDbvlS3Fp5vhaB2TAGqRKrKKMrhLVpaGzEVjZ0OQxDhaCTA+QyRR1d15aQzrJntL3RibsipjG6jlgL4yqbS0sNYg1e84vhbBVrElK64CUcWYXDfKxhpIuxiVJZUxsbMy/uRBKTNRQ4kQ3LdRYLS0rJjRPlTPqY6gdJsEDc+aQXAn+HgsNUCbRuF0Oj0zwnA7bWDkbhO5Ens00qeQhS1laBMl5M/cAaxsLF8rKyql+Tf7ELLEGu/ixiimdCvo0TjfpjKwaggen4eh5v7LokLKbLuyvHhcZG8dhGrEDx7Hg93ZppJF7qBqO3iVveXEDQNInzeoe8Yq6ePaZBZ2JviM3W2UAGotekRCAGq4EkF1X3DOnR11yRsBL1tRa0PVcZiNFXZ2c34FskvomInQQ6lzpJoZbJxk43NwKJFBquJSsrByHydxKOnTxQASBmS3j+JMnsHSla3Ec6K9VWoJVn9zfjwOM7hqYAAqJQwE2a3nA48J2QGegRkpZNivSY+ys3EkKd4oJIwsvIHl3cWgLt5k4NH6OmtLWdpurOkwEMupYc7eMtDRhOcI2ui5JhVIzXzLyto/GAPuZoyo8wkoduVgJglCt7OhGbgID4Mq4si+63zUS1FuFFXFlqyaj2emHlLMcBqYu0FMuR28BbB7lOxRMSiCQXFhCKuwkhZ+pYDiGSgbsKKV8MiSRsuHSIWM9rklRiIlZZuqXjsQK8ooYJMgq3JKWVkhHbhsVxFUzthOWPkYijcbx54IKsSdT+uLr3crGKyoYgFiGR9iBk4kfloUX+JIlQRQqabmpgnhqtpQpb6RVQ1WH5DnrS4hEoGZqaerQ2dhFbz8XePxShmDbo70eISjoorO2vK8SJXI4SUmEU4zWKDzUDtWTYw7xXlbSTEj4FRg7zKnKoGRALv0Gs9Tgc1BpCywGZRQAtqVz2xrBcAMzEpfZwFSa2G5W0QBFjSMapWAEFa3HcGN7CxDzECyIkJ97qwrqWNTWVo876PPsjPkj2wvgroM5lLZKMETKVql/CvnWVFiFa/SzJUQwkoZsr67Y6vlSRV3/2tmNTOY3vnaxYwMuoPKqdzR1w7IqHymlPxaAThfU7Ko2ZXYj4AYJHL+kNdKwRQYESTRa5fsUZ/rVC1TMTyWVyYoqNtuzaHsMyv2tvoarxdfqwYgU1axFo/cnql1FGsqK+uAROV8BX4GU8WcZTATi2q7Qcyi0O0V+GhWBMNRUkn8H1SsWVE5By3Gi0ECqUeJoBfAtDa4amkdXG37AGP5Ggeb84p7UazpoKRzdFzeQ8HkoHGxprKy/Hpm5t12p47J6xTYDEz7uINEXSuxYXvFskYAc+ySxH9sf5ftKzU6IbwVBcUGg5e5FMCEXSErZR0wGayV19woM9guPjTqJdVTqR4uE4nJnLldWVkECCZLd2VLF+xtamex7IpiriSDUpvrpn9lrwGMCHyppMH+ps6LILsuFGUj1XEOXiqbqSHPUKnClpWV68kqtURVNDY4TNaocykoYeTU5ngGEQa/S1DnnE4AeXMcKjHPAmFVjCBENaeyLVNHfr3px8xUstJ94hIpfH4HKE/eDaArK6lSyVVFbdt1gxTIVk3pppVlFXi4pEhVBTObquohU85MLXn1iahvUkHJjSCMc01tLFveVVBx0DodM6jftCu7DOtIzYxrc0qp1JGP2ayYFz2Gb6HvMrO8cnGtV6Gjm3uImSfD2GpWK6uowbZGMxFKQCo1pOMtcMXFpRst+hXGoAomF3sSTBGgTglbBKWwsQ3tZqaYSp0Z1CimRDWFcCJUPYJ00BI5FkKYNoifuQxmN88SWVXWLMaUqqqgC0BmQJR6sk3u9NCf6jYLXxAfqsYEgVLAhRY2AtgtflZNFmFyhxdrLkAdWlk4D88M2ixHyepIdhMHrG/iR1ZGtq0MGpbDbRPYOXeSY1M6Ny4ZstvGSktK+XbFPATj2D371saPEsAMXhXrsZ0km/XStkhhMyBfsa6uXFZe2VCe+YMr1+GKgwrQyNYq1VRrB+EizAow6NsdNKcyVEkYeM73ys6q4kAHp6BiFklTkIrVC5oYV7uzwOGCz4UJ0Stq2lWMJy4wtb+RetL6tZFicnJmBw5UjCvXXMZVJX2MQkbf+XN5EWd78Vz8/JEsMZTBiKNzsm1inLRUQ74H4NidaqI68j5sAFgxcRveC7ieLJXfQYxjZZ2CsiWFewZXJmBIlZ1tdtrX4hSuateKso/RZOtOKW2nmq1oTzeK6dRWAWu2NRVb4hq0SXm1GvtugHrbr5IXqmSktg5CuDE2MSlPwsY5kNE2Wp3AqiZbWVLAxiBF+2iBZbuNj6MB6rsMLC7FyasaYDyo7KkoPyEtw3pEMXfPvxAJi2jAQQgjrz0rLIZSWZlIoNhwd5xK4AR9mYNjWAaLrnuImJeBVN9zBORObVvbr+mTTfFSEJLSRnHo7hEJoIi8MFqjxmvgmF5URZz4zLFgZZ8Ctu2X7ggVccKm9gVxIsOHqxXgNMKnFWZYnf1dBnOhayXq17QwFlWW09eNKyVJFmXqaONGA5aCegMbJ3UUkGY1ic3nKWgjq8qfVYGQG1gRt6rs62a6HiqqUOqdesK5NmX4nGofJoiE1d0dF9lVVkvT1/kEEaaCoYOwFpcVcoLM+7669PxC9rWqktH0sWUYld0VCpuBZ/stVRcGgy9WX2+U1Qthi9SzAqSxzZsy+OiFzBYnySGV6Gku44rD8BCOZBV3BvD5+AKRHNwMEsB6EzHnJpkTAeiUlEGkcECeB6GDZTp5YEJTlvdrknxYjTllMkfNtXwDjM7uVjK5JXUUn43rrqpK2jytaxHW0M5G8DC8rtHMYs7KSgduVQMGTYFqFvVS6rkD3sDJ46afdYFwoq11AOKCBLhvwoUgc8IGANycR6knZrdJPdsuxnyjfd3FovTlRMdEdtOl5CMV5EHsXQBis7TOwvIDZaGj2Vnpbh7cpK63VwYEMLwqbjzyl699sawFFkF1yqjUU31HfC6sW1ZFVFuXVXVgz9keEaw0ys1lWfm+azQAQSWA+hKYVfsZjPncAcUB9oIayy/UZXRNckDGji77GsWbvBo6tPrWPqOyVkBUq+INeqpzNdYs/u0ifh5qmpqIW+33JVSUcwY70KL4U9lYdU6ljtSls7lmfi9g3YzeQfVkaGFaV3ODCnaD2N8wsEDFklE3RzM3ZghdYkWHsszq70FIecnKkVkt8ezMzRq9bkGuKojRLBVSod3Y1yPqKgYW7JRQTPVyy5xIYLjOgxgT52RKJUY1dOrIiRd4futQx/A5AcSmEjz0vFWrkLzvbWAu9HOWbGgxFk1VNTpnBKk6TgwisI/HcxYXP1uAWO72ULFlBTq+aSu2VTUs6hrxM2CF+hEor1VIA9ZmFUaab1lSSgZsVs4sxzHlVLoJHr9H4DhONTkI1XC0/wiY2NoWAG5RlnHFnq6oLccpQddMuJ/O17JVA5OHLi0BqCztq7Y1++ucCd98qLI8MIHBV/cKjxQTme3hFBS3MyCqnDsuym2o80HjvFFTtrURmNaGJsmVahImjTsUXKtQZTAVs7Mvv8/+fzUrZAXcLJ6M4koe6XP0b6SmWWNDzyUpQ8bl+LtWx4tuqZ36cRYV3yuVxPNwvIiqiQCSmu7srgTzR6nkyhpCarXwFy1vGd5iP2cY06lFr5Njhhg1Y6+NB28ftbK83s8rf7kLJbKwDFPbLg25a0AdZJEiqr5phixKMDlRUtcssq1hriLqGoH+zeNgVm9OemjsETV8JdF0NHnkIFxWY1OB4Yrp7rtWJ7NgAAAPXklEQVQ3oNs5nplyVf8u2FoLu1JrHveaZWQjqAkshtFa2gzsSG3Zpkbvg3HafF9slPPlldjFlK80Gysm8Mr4MPhneNWENPGjAIpmilTPATdTRTXlCBYHYAQuPwA36xIpWtGN4q3Y2MhiGsUpuSSnlEJRD8PorC7CFYVw+F51qThgabxsTxWzCGY0ZSsb3lfqAy0OPNjNy8xiQQKsHYFQ2HBZVvVbBuq3m1oWKajqaonsM6uZUr6CjXWNZ0l5E3h3jURma6kP3MJIiy1Lm+kahQq41N2iZja5sjtlLYNZHZrH6qUGm4vMbDp6Rw2CFmvuyFkrBcCyMtFqBaECmsHoK9BZ2LA/lJcRqSaDqnaWbrZdGaz3DLgIvBln4woGztbyJGqslwxkhhHrTjTYFXCtOoKS8uLdofVdAbOylGU6nlYpXWZts4nXBq6WxJitMNokHUJnbnJplQm+aGpY2a5GMV2QD1hRubBPFKdumf5OHkLHz0F9luE5kjBjRa0nFE5CUGqHw32MmjZ6xkgINVnSnZ1VZStK2qKlRaLlQgK7uTq7JFXJwM+3SOEKyhZNI+tJ0I5qMYy9k2qJD7dVWdqKXa0CKNR0Ccjg+B2IYu2fcBZJZkMFgM11r0X92wilghFGgzVnexlqB7xL9mS29SiYUVY2nXOZjNBRsyDsQPRWW5hrZ4XcdC4HVWRbjgJr4sFofK5SzjQ7rhI1UebdPdEbj6sqIvTZQZ5va08rABsAW0UxeWytAk7A2KJ9ZpxzCioB24XFtYAeXYxr6anSqhLgppEqWbGwLunTgrV+IjWlL29ljaAl4EQMGsErp4apeZiquwRXLXAqOCeru32mmydc6oWTSWpFAGdzeTB8RTHVMEtlM90CbbQCYhPjq3egYr1FGdYIQjiuDGZ5zZ/AzobKGOyLxti6c4Rwtv2anyWlLICnlLhxJRXt6A5ebDBWFNONbxWZ2d02mnu4S9YECpeppV1zSWRBWxHYzVIv1CXSouwqqX3jBBBDZdYQbpTQW4ZQlS8r5kH4suSRmg2++3JN10x1PaAmEkmtYlEdeGpJEM6kOuCqCR22oSujj5IV2HdT0zj5prLKTjXFAPjdQlyq7xIBxAQP5yMczG4VxAKw0n6ilZ2QBce2pLulkuxxqnoIzFfgqyqjil9S1VNwBrFmeyeops8yOjZUybZdfS8CuaTIJumzs5tODaNtLpFDQ/PcJGweLhmeL1nB0KqiUDScsiUVD89Di3HtrKtSULw3RLiygZD+7sF8JTObgYsrGvDNUFRGl1iy0Ll1YkUc2aJYMog920I8qW6YDCg1Mqk0JHJFKXkbgbRreI+qpYNOZHrVcDUba7pjsphSJNtK6upgRNAVoOS0mugBeN4bIZgHhuPZ/s1ENaX6KsVr+YNrh1Nb7ipR0PE5zbNRegCbrHRUw6Yf07dLBJl1f8KB9as2V1nNqAsl62LBBhehwalerkHmB1JFIEZKSEusdl5JQj1nJlHXSCF342gJ9CYGrXelknJIXqVP8sD+qtplCR3XH2qfKq0ygMp+KnVkKxNlZ8m2YkIlVMiCnXUwl7qznBKSvQz3m3Pt6oQbXO5b5FixCh/fHxUQW/AEcK6zCNqKQnL9sywqmKuwvqSYzT/aPVNNpVyhvRW21aqciCsjdWvBwILUvh5VyCzbWoC1pJjJ680CWsl+udKB6T5RwG1mlohnlpbg47iz5U9ha0FGtmRLFYBtO99y97Ap0z+ZDTAog6kSLZsMHg/IFkkgp6CpvU2U0cYVSdnmkjwBdOmXbxTWNWzuIbipMioVxEckZEoahSOiy2M3K0jcC1LhVDwaqG0ZvkcWqCnrG4GIxykrqlbWdw6LQyBaZR8HmLRIhQWsHswD42ZXVLNkf9l+FlW0HVQ2lwFsC/Z1FdzlQR0KaPfo+Fdfu+/dwVRICu1CGR7AEIiAhc+AZUF0kOBaPxmUqg4i64vQnU4nFDYJ9Nz+1fVXveH9qmr+kPILx8oKcRV/BFbxbE0JMT0kSD4w6L/lNY8ocsqagVdU3A3MjxhxcGuqzsPH4irpaow1q6OyrVjvp9Npc59E91LldboYVzJWdimWfAW2SNEKcDaX2FmBLLA/uKxlmhh613Is1URQApbKfttwxL02q6Onx5pQxSbPojAg+v5hAnN6LHVRDXIsvKtRjiS0qJUyZTAXVbAK82ElFJWaQdVoqUC1Unt7BVaTQudM6SuqexjQJN4+0icaxv/utbKv83ETbT8H8gjcOKxOJmbUa6OOVXht3dFY6rHv9XoNzFLceEA1o8+pKm0LAHPHZ2rYKjFq0hfZFixsqHJgD3eD5n+U0kb1mFjXkn2lvMSSOsNE/CdIAKF0Sytq6urOHUN5gwg4GZosgbmggM5ucra2qrS2Ig1cbiBBcxYzgzUDNLCvL8GbZXNp6ORy3LmS+Kk83zRIAK6A1ioKa2I9NapIuiUFdfC9766PFZUtqUr6KbWk+zZU1a/ZrIXEztrjTOfz7hwKziCeXIaraHtbZIMz+2pGgazCmw4qWAFvEdhodYp0Xq0pV7G1YWYWbO4qhGq42+Z8BYtrLWvluNPpZAeaFFS1vubPgbgxsqcpnAaszBovKaFoDQ8BGtjfUOl4NAG2nmQV04feJgumvX2fsrQEWZghL0JnVdYkn3DOZIeRN86RqPWCmsvGVqEMRnwxQAxwS8EMYo3IzmY2+BCcLp4MKiuyuhImamlbZFcNoNl7tp+RHd18ZjQIRKyXdFRhN98/hyKqwXWNo7O1wiaXoHN108REZZWEq6grnIfjzeg8jdRf1XEL4kkXa5bBjKxoKaljBjeHlVxQ4GaycpW4lDOAKtnTxHAtOfzOtZwHAM7sqVXkV6yu6kap1nHkXKqWF/4XHqjenNKqBjpR3l1ch3Ejg1+EsgdQhsdG0B4FM9sWAVWpuAyiwTPleZxt9VyZVS2qXfReWqTAilpr9ApoWTjxymit7NwV4JTriZyOA9B0k7HFfULourmKYHVnRQvqGL5HMHdqFcR2qWpmcK6eTwx2dipWrviDilr+fKWq3OWRWdHKwA4eu8wjchbeRzFilqjjZN3ufCpfkJ0/scVpnYk6L0PI77lxdWCZ87WiWm7B/AGquQSnujGKsB8CJmiJq8q1pKIVWyqOiTK66r18BN8r74/AE71fdC3yPS2MxdOpnE1tlVxD9JmVOoggN+r4PjAXVFPa3Eg5jVJGFVUGNolH20GVrUB7BOySWq6WqYQdWR92pcFMYMwckbSgCKCqD67DiiWu1g8MQC9ByfcFqW1L+jL714qNCuznoSxt0da2gtWN1G8F0BK0NN0nuimelUF9dIdAfjO44UT3CjQLoUeLHJFTO3gmpRuIIOvwBQCbqNeo3qtZ9iF6xVK13GRlo4zqimq+CGdTiR1uRY8oqgE02hZBa79kZXPMquxRHKla2saZWN4mRqZUj0vLCKhkjKnqOQHNuSZVJoKvAqS1wpEquvWDC1B2ypwrCPsRMEPVTODMLJMDv6qeKXwi2JYV5Sq4qKyvgGsHCLiuj2jR59V8gMqSJ2FJZRXEHVRHj3sFPrct6OpqlW1GpatQdt0GvwfM6n63InsGVFhJGaBqgqqIV6IsXllZgySPq4R3bnt3wi5cv+cN2yqQLW1T95KYVsWWtKk4cB9W53WQQflQYR6Wl4HaJZjvVE0D5yvq+RKgZCs5qdBEP5sD94cAvQLlSgNaSMAtHx88BuNQ41zdFsX30zKbcs0MLD/ihkpQzl0wiTqKLTfbKmCmyYICnK0IbaieC4CG9iSyLQ7cIMGQwau6TKoq60Apl3WN40LZpca1CKKK9VQyyIEn8w0F8F6CL2h8o3ixGwC7s7EWzCOqmcApYxYD4jsAzVS0sl2t98pA7vrKophCVSonbYpgH6mvSn24pTBV4sdtV3BtMq5k82y+IADvUJ0uAlkCVTxIaPm+UNu/qkV4F1TzHXCGrXIAqItBKypqK99VtAOVs64O4ObX7pHLVCpYHcRmwvLR7TvYAKBBN58LGVzDuFz+hQbWgncQyCZAk+VbsPSouf93261iZgmfCpwRbAvqmSqriU2PwhjaoOyYqtIegVXViTsmyta6bGySpY3gyRrpIyAeaWDDxtpsXwKyalMDKNP7YBXMqEskUsi2uC8FNAPxAKTVfT1o6VzM0E0jF+1rWcUuHvdyg7vgoFplX8HpvHpMCOMRUPHzZkInsqlFKNX/EIO52E0SxSzOwob2VmRLW5D1XIU0rbgM1AzWgyC7fe8G7xUAK/taEBat7luqtyP7EmsaJQOj5F+mrnZfCuYCfBUAWwShyd6pMY/vAHG1UqOYpbI/gy5T0CMKm+UO3gFuC85dgfDVeguPDfITrIBLsLrcgdh3CFgFZjaKJ4Iv3F8ANEqvuxR1tVKOgLoCa1jxboBAkj6v7j/icFbA7f4rfRnQDLRViG13i0vqBQrYVqBbADZT0ZpiHoSzvQpopKIFS3sE1HfBWlHXd0H7LnArqvougMtljHBgZnh3Eoz/BKjLML4Z2Aq0+hEJr9jaVUBbvNzCIUiroC7AWmmFw4o5AK3MtB5VypZMSFgs05JyGVwlwBqsEGAAa2ZU1CjUexXGsE4rKriilBvFzOKKo3AuAroE6QFQU3u8YpNXwS5k+1TZt5UrwouN4KiUEw+k3ZWDp1RXHNRqXb21Ts39945yZSg3VnZFNQ9CF3XeZyr5DgBXKiwCMa2MxeTDYXgP1Fsf9QNKZc0k81RJk3r6EQ3rCmBVyLL75EjZ1pIVDHoFtiOAHoB0BdTVylqBsKKKS+AeBXJVLY+CXASuGvO/Auq7GuEjDfGKg1oKa1z/dmmi9I9SUGNhl0AtfulHAawoYrnSkmNXAVuGEhrEVXvUF+A5Ct2PqNOjDetyna4CmeUolmeXLN4Aq7C5Sj10Q7yjgl+t6CNxSRHmI5X+CpwreYB3Qfdqna4q21KdBuc4GoZsn49ZOOiVinwHqK9WzjvgeweEh2AU5+vtxZ9Cd9Wqkh49V18E5oj6vVyn0RStAyGIO5edXRKd5B0VGVXq2yr3xYp+5Ut+C4QJ4P1N339pQMjRejj4vb/Dcr6rQc3O/0rjmtZpeYCBiCHfCemRbNhbK/pNUPc3wfKy5f2D7OlL3/uPhve/oU4T0F8f+VNM2vyoiv0jK+KHQfdHq+0bncz4oz73/+Y6LbKw1o/5B7eOf1Rl/0du9B9tn/9bvrf/j+v0h6ttn2tp/r/4819y4/zv5391uvzzfwDifz6phT1MPgAAAABJRU5ErkJggg==');\n}\n\n.color-picker .cp-add-color-button-class {\n  position: absolute;\n\n  display: inline;\n  padding: 0;\n  margin: 3px -3px;\n  border: 0;\n\n  cursor: pointer;\n  background: transparent;\n}\n\n.color-picker .cp-add-color-button-class:hover {\n  text-decoration: underline;\n}\n\n.color-picker .cp-add-color-button-class:disabled {\n  cursor: not-allowed;\n  color: #999;\n}\n\n.color-picker .cp-add-color-button-class:disabled:hover {\n  text-decoration: none;\n}\n\n.color-picker .cp-remove-color-button-class {\n  position: absolute;\n  top: -5px;\n  right: -5px;\n\n  display: block;\n  width: 10px;\n  height: 10px;\n  border-radius: 50%;\n\n  cursor: pointer;\n  text-align: center;\n  background: #fff;\n\n  box-shadow: 1px 1px 5px #333;\n}\n\n.color-picker .cp-remove-color-button-class::before {\n  content: 'x';\n\n  position: relative;\n  bottom: 3.5px;\n\n  display: inline-block;\n\n  font-size: 10px;\n}\n"]
        }]
      }];
      /** @nocollapse */

      ColorPickerComponent.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ColorPickerService
        }];
      };

      ColorPickerComponent.propDecorators = {
        dialogElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['dialogPopup', {
            "static": true
          }]
        }],
        hueSlider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['hueSlider', {
            "static": true
          }]
        }],
        alphaSlider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['alphaSlider', {
            "static": true
          }]
        }],
        handleEsc: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['document:keyup.esc', ['$event']]
        }],
        handleEnter: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['document:keyup.enter', ['$event']]
        }]
      };
      return ColorPickerComponent;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var ColorPickerDirective =
    /** @class */
    function () {
      function ColorPickerDirective(injector, cfr, appRef, vcRef, elRef, _service) {
        this.injector = injector;
        this.cfr = cfr;
        this.appRef = appRef;
        this.vcRef = vcRef;
        this.elRef = elRef;
        this._service = _service;
        this.dialogCreated = false;
        this.ignoreChanges = false;
        this.cpWidth = '230px';
        this.cpHeight = 'auto';
        this.cpToggle = false;
        this.cpDisabled = false;
        this.cpIgnoredElements = [];
        this.cpFallbackColor = '';
        this.cpColorMode = 'color';
        this.cpCmykEnabled = false;
        this.cpOutputFormat = 'auto';
        this.cpAlphaChannel = 'enabled';
        this.cpDisableInput = false;
        this.cpDialogDisplay = 'popup';
        this.cpSaveClickOutside = true;
        this.cpCloseClickOutside = true;
        this.cpUseRootViewContainer = false;
        this.cpPosition = 'auto';
        this.cpPositionOffset = '0%';
        this.cpPositionRelativeToArrow = false;
        this.cpOKButton = false;
        this.cpOKButtonText = 'OK';
        this.cpOKButtonClass = 'cp-ok-button-class';
        this.cpCancelButton = false;
        this.cpCancelButtonText = 'Cancel';
        this.cpCancelButtonClass = 'cp-cancel-button-class';
        this.cpPresetLabel = 'Preset colors';
        this.cpMaxPresetColorsLength = 6;
        this.cpPresetEmptyMessage = 'No colors added';
        this.cpPresetEmptyMessageClass = 'preset-empty-message';
        this.cpAddColorButton = false;
        this.cpAddColorButtonText = 'Add color';
        this.cpAddColorButtonClass = 'cp-add-color-button-class';
        this.cpRemoveColorButtonClass = 'cp-remove-color-button-class';
        this.cpInputChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.cpToggleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.cpSliderChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.cpSliderDragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.cpSliderDragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.colorPickerOpen = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.colorPickerClose = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.colorPickerCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.colorPickerSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.colorPickerChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](false);
        this.cpCmykColorChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        this.cpPresetColorsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
      }
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.handleClick =
      /**
      * @return {?}
      */
      function () {
        this.inputFocus();
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.handleFocus =
      /**
      * @return {?}
      */
      function () {
        this.inputFocus();
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.handleInput =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.inputChange(event);
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.ngOnDestroy =
      /**
      * @return {?}
      */
      function () {
        if (this.cmpRef !== undefined) {
          this.cmpRef.destroy();
        }
      };
      /**
       * @param {?} changes
       * @return {?}
       */


      ColorPickerDirective.prototype.ngOnChanges =
      /**
      * @param {?} changes
      * @return {?}
      */
      function (changes) {
        if (changes.cpToggle && !this.cpDisabled) {
          if (changes.cpToggle.currentValue) {
            this.openDialog();
          } else if (!changes.cpToggle.currentValue) {
            this.closeDialog();
          }
        }

        if (changes.colorPicker) {
          if (this.dialog && !this.ignoreChanges) {
            if (this.cpDialogDisplay === 'inline') {
              this.dialog.setInitialColor(changes.colorPicker.currentValue);
            }

            this.dialog.setColorFromString(changes.colorPicker.currentValue, false);

            if (this.cpUseRootViewContainer && this.cpDialogDisplay !== 'inline') {
              this.cmpRef.changeDetectorRef.detectChanges();
            }
          }

          this.ignoreChanges = false;
        }

        if (changes.cpPresetLabel || changes.cpPresetColors) {
          if (this.dialog) {
            this.dialog.setPresetConfig(this.cpPresetLabel, this.cpPresetColors);
          }
        }
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.openDialog =
      /**
      * @return {?}
      */
      function () {
        if (!this.dialogCreated) {
          /** @type {?} */
          var vcRef = this.vcRef;
          this.dialogCreated = true;

          if (this.cpUseRootViewContainer && this.cpDialogDisplay !== 'inline') {
            /** @type {?} */
            var classOfRootComponent = this.appRef.componentTypes[0];
            /** @type {?} */

            var appInstance = this.injector.get(classOfRootComponent);
            vcRef = appInstance.vcRef || appInstance.viewContainerRef || this.vcRef;

            if (vcRef === this.vcRef) {
              console.warn('You are using cpUseRootViewContainer, ' + 'but the root component is not exposing viewContainerRef!' + 'Please expose it by adding \'public vcRef: ViewContainerRef\' to the constructor.');
            }
          }
          /** @type {?} */


          var compFactory = this.cfr.resolveComponentFactory(ColorPickerComponent);
          /** @type {?} */

          var injector = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ReflectiveInjector"].fromResolvedProviders([], vcRef.parentInjector);

          this.cmpRef = vcRef.createComponent(compFactory, 0, injector, []);
          this.cmpRef.instance.setupDialog(this, this.elRef, this.colorPicker, this.cpWidth, this.cpHeight, this.cpDialogDisplay, this.cpFallbackColor, this.cpColorMode, this.cpCmykEnabled, this.cpAlphaChannel, this.cpOutputFormat, this.cpDisableInput, this.cpIgnoredElements, this.cpSaveClickOutside, this.cpCloseClickOutside, this.cpUseRootViewContainer, this.cpPosition, this.cpPositionOffset, this.cpPositionRelativeToArrow, this.cpPresetLabel, this.cpPresetColors, this.cpMaxPresetColorsLength, this.cpPresetEmptyMessage, this.cpPresetEmptyMessageClass, this.cpOKButton, this.cpOKButtonClass, this.cpOKButtonText, this.cpCancelButton, this.cpCancelButtonClass, this.cpCancelButtonText, this.cpAddColorButton, this.cpAddColorButtonClass, this.cpAddColorButtonText, this.cpRemoveColorButtonClass);
          this.dialog = this.cmpRef.instance;

          if (this.vcRef !== vcRef) {
            this.cmpRef.changeDetectorRef.detectChanges();
          }
        } else if (this.dialog) {
          this.dialog.openDialog(this.colorPicker);
        }
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.closeDialog =
      /**
      * @return {?}
      */
      function () {
        if (this.dialog && this.cpDialogDisplay === 'popup') {
          this.dialog.closeDialog();
        }
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerDirective.prototype.cmykChanged =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.cpCmykColorChange.emit(value);
      };
      /**
       * @param {?} state
       * @return {?}
       */


      ColorPickerDirective.prototype.stateChanged =
      /**
      * @param {?} state
      * @return {?}
      */
      function (state) {
        this.cpToggleChange.emit(state);

        if (state) {
          this.colorPickerOpen.emit(this.colorPicker);
        } else {
          this.colorPickerClose.emit(this.colorPicker);
        }
      };
      /**
       * @param {?} value
       * @param {?=} ignore
       * @return {?}
       */


      ColorPickerDirective.prototype.colorChanged =
      /**
      * @param {?} value
      * @param {?=} ignore
      * @return {?}
      */
      function (value, ignore) {
        if (ignore === void 0) {
          ignore = true;
        }

        this.ignoreChanges = ignore;
        this.colorPickerChange.emit(value);
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerDirective.prototype.colorSelected =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.colorPickerSelect.emit(value);
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.colorCanceled =
      /**
      * @return {?}
      */
      function () {
        this.colorPickerCancel.emit();
      };
      /**
       * @return {?}
       */


      ColorPickerDirective.prototype.inputFocus =
      /**
      * @return {?}
      */
      function () {
        /** @type {?} */
        var element = this.elRef.nativeElement;
        /** @type {?} */

        var ignored = this.cpIgnoredElements.filter(
        /**
        * @param {?} item
        * @return {?}
        */
        function (item) {
          return item === element;
        });

        if (!this.cpDisabled && !ignored.length) {
          if (typeof document !== 'undefined' && element === document.activeElement) {
            this.openDialog();
          } else if (!this.dialog || !this.dialog.show) {
            this.openDialog();
          } else {
            this.closeDialog();
          }
        }
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.inputChange =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        if (this.dialog) {
          this.dialog.setColorFromString(event.target.value, true);
        } else {
          this.colorPicker = event.target.value;
          this.colorPickerChange.emit(this.colorPicker);
        }
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.inputChanged =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.cpInputChange.emit(event);
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.sliderChanged =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.cpSliderChange.emit(event);
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.sliderDragEnd =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.cpSliderDragEnd.emit(event);
      };
      /**
       * @param {?} event
       * @return {?}
       */


      ColorPickerDirective.prototype.sliderDragStart =
      /**
      * @param {?} event
      * @return {?}
      */
      function (event) {
        this.cpSliderDragStart.emit(event);
      };
      /**
       * @param {?} value
       * @return {?}
       */


      ColorPickerDirective.prototype.presetColorsChanged =
      /**
      * @param {?} value
      * @return {?}
      */
      function (value) {
        this.cpPresetColorsChange.emit(value);
      };

      ColorPickerDirective.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
        args: [{
          selector: '[colorPicker]',
          exportAs: 'ngxColorPicker'
        }]
      }];
      /** @nocollapse */

      ColorPickerDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: ColorPickerService
        }];
      };

      ColorPickerDirective.propDecorators = {
        colorPicker: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpWidth: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpHeight: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpToggle: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDisabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpIgnoredElements: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpFallbackColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpColorMode: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCmykEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOutputFormat: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAlphaChannel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDisableInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDialogDisplay: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpSaveClickOutside: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCloseClickOutside: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpUseRootViewContainer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPosition: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPositionOffset: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPositionRelativeToArrow: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetLabel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetColors: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpMaxPresetColorsLength: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetEmptyMessage: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetEmptyMessageClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpRemoveColorButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpInputChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpToggleChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderDragEnd: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderDragStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerOpen: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerClose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerCancel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerSelect: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpCmykColorChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpPresetColorsChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        handleClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['click']
        }],
        handleFocus: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['focus']
        }],
        handleInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['input', ['$event']]
        }]
      };
      return ColorPickerDirective;
    }();
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var ColorPickerModule =
    /** @class */
    function () {
      function ColorPickerModule() {}

      ColorPickerModule.decorators = [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
          exports: [ColorPickerDirective],
          providers: [ColorPickerService],
          declarations: [ColorPickerComponent, ColorPickerDirective, TextDirective, SliderDirective],
          entryComponents: [ColorPickerComponent]
        }]
      }];
      return ColorPickerModule;
    }(); //# sourceMappingURL=ngx-color-picker.es5.js.map

    /***/

  },

  /***/
  "./node_modules/positioning/dist/positioning.js":
  /*!******************************************************!*\
    !*** ./node_modules/positioning/dist/positioning.js ***!
    \******************************************************/

  /*! exports provided: Positioning, positionElements */

  /***/
  function node_modulesPositioningDistPositioningJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Positioning", function () {
      return Positioning;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "positionElements", function () {
      return positionElements;
    }); // previous version:
    // https://github.com/angular-ui/bootstrap/blob/07c31d0731f7cb068a1932b8e01d2312b796b4ec/src/position/position.js


    var Positioning = function () {
      function Positioning() {}

      Positioning.prototype.getAllStyles = function (element) {
        return window.getComputedStyle(element);
      };

      Positioning.prototype.getStyle = function (element, prop) {
        return this.getAllStyles(element)[prop];
      };

      Positioning.prototype.isStaticPositioned = function (element) {
        return (this.getStyle(element, 'position') || 'static') === 'static';
      };

      Positioning.prototype.offsetParent = function (element) {
        var offsetParentEl = element.offsetParent || document.documentElement;

        while (offsetParentEl && offsetParentEl !== document.documentElement && this.isStaticPositioned(offsetParentEl)) {
          offsetParentEl = offsetParentEl.offsetParent;
        }

        return offsetParentEl || document.documentElement;
      };

      Positioning.prototype.position = function (element, round) {
        if (round === void 0) {
          round = true;
        }

        var elPosition;
        var parentOffset = {
          width: 0,
          height: 0,
          top: 0,
          bottom: 0,
          left: 0,
          right: 0
        };

        if (this.getStyle(element, 'position') === 'fixed') {
          elPosition = element.getBoundingClientRect();
        } else {
          var offsetParentEl = this.offsetParent(element);
          elPosition = this.offset(element, false);

          if (offsetParentEl !== document.documentElement) {
            parentOffset = this.offset(offsetParentEl, false);
          }

          parentOffset.top += offsetParentEl.clientTop;
          parentOffset.left += offsetParentEl.clientLeft;
        }

        elPosition.top -= parentOffset.top;
        elPosition.bottom -= parentOffset.top;
        elPosition.left -= parentOffset.left;
        elPosition.right -= parentOffset.left;

        if (round) {
          elPosition.top = Math.round(elPosition.top);
          elPosition.bottom = Math.round(elPosition.bottom);
          elPosition.left = Math.round(elPosition.left);
          elPosition.right = Math.round(elPosition.right);
        }

        return elPosition;
      };

      Positioning.prototype.offset = function (element, round) {
        if (round === void 0) {
          round = true;
        }

        var elBcr = element.getBoundingClientRect();
        var viewportOffset = {
          top: window.pageYOffset - document.documentElement.clientTop,
          left: window.pageXOffset - document.documentElement.clientLeft
        };
        var elOffset = {
          height: elBcr.height || element.offsetHeight,
          width: elBcr.width || element.offsetWidth,
          top: elBcr.top + viewportOffset.top,
          bottom: elBcr.bottom + viewportOffset.top,
          left: elBcr.left + viewportOffset.left,
          right: elBcr.right + viewportOffset.left
        };

        if (round) {
          elOffset.height = Math.round(elOffset.height);
          elOffset.width = Math.round(elOffset.width);
          elOffset.top = Math.round(elOffset.top);
          elOffset.bottom = Math.round(elOffset.bottom);
          elOffset.left = Math.round(elOffset.left);
          elOffset.right = Math.round(elOffset.right);
        }

        return elOffset;
      };

      Positioning.prototype.positionElements = function (hostElement, targetElement, placement, appendToBody) {
        var hostElPosition = appendToBody ? this.offset(hostElement, false) : this.position(hostElement, false);
        var targetElStyles = this.getAllStyles(targetElement);
        var targetElBCR = targetElement.getBoundingClientRect();
        var placementPrimary = placement.split('-')[0] || 'top';
        var placementSecondary = placement.split('-')[1] || 'center';
        var targetElPosition = {
          'height': targetElBCR.height || targetElement.offsetHeight,
          'width': targetElBCR.width || targetElement.offsetWidth,
          'top': 0,
          'bottom': targetElBCR.height || targetElement.offsetHeight,
          'left': 0,
          'right': targetElBCR.width || targetElement.offsetWidth
        };

        switch (placementPrimary) {
          case 'top':
            targetElPosition.top = hostElPosition.top - (targetElement.offsetHeight + parseFloat(targetElStyles.marginBottom));
            break;

          case 'bottom':
            targetElPosition.top = hostElPosition.top + hostElPosition.height;
            break;

          case 'left':
            targetElPosition.left = hostElPosition.left - (targetElement.offsetWidth + parseFloat(targetElStyles.marginRight));
            break;

          case 'right':
            targetElPosition.left = hostElPosition.left + hostElPosition.width;
            break;
        }

        switch (placementSecondary) {
          case 'top':
            targetElPosition.top = hostElPosition.top;
            break;

          case 'bottom':
            targetElPosition.top = hostElPosition.top + hostElPosition.height - targetElement.offsetHeight;
            break;

          case 'left':
            targetElPosition.left = hostElPosition.left;
            break;

          case 'right':
            targetElPosition.left = hostElPosition.left + hostElPosition.width - targetElement.offsetWidth;
            break;

          case 'center':
            if (placementPrimary === 'top' || placementPrimary === 'bottom') {
              targetElPosition.left = hostElPosition.left + hostElPosition.width / 2 - targetElement.offsetWidth / 2;
            } else {
              targetElPosition.top = hostElPosition.top + hostElPosition.height / 2 - targetElement.offsetHeight / 2;
            }

            break;
        }

        targetElPosition.top = Math.round(targetElPosition.top);
        targetElPosition.bottom = Math.round(targetElPosition.bottom);
        targetElPosition.left = Math.round(targetElPosition.left);
        targetElPosition.right = Math.round(targetElPosition.right);
        return targetElPosition;
      }; // get the availble placements of the target element in the viewport dependeing on the host element


      Positioning.prototype.getAvailablePlacements = function (hostElement, targetElement) {
        var availablePlacements = [];
        var hostElemClientRect = hostElement.getBoundingClientRect();
        var targetElemClientRect = targetElement.getBoundingClientRect();
        var html = document.documentElement;
        var windowHeight = window.innerHeight || html.clientHeight;
        var windowWidth = window.innerWidth || html.clientWidth;
        var hostElemClientRectHorCenter = hostElemClientRect.left + hostElemClientRect.width / 2;
        var hostElemClientRectVerCenter = hostElemClientRect.top + hostElemClientRect.height / 2; // left: check if target width can be placed between host left and viewport start and also height of target is
        // inside viewport

        if (targetElemClientRect.width < hostElemClientRect.left) {
          // check for left only
          if (hostElemClientRectVerCenter > targetElemClientRect.height / 2 && windowHeight - hostElemClientRectVerCenter > targetElemClientRect.height / 2) {
            availablePlacements.splice(availablePlacements.length, 1, 'left');
          } // check for left-top and left-bottom


          this.setSecondaryPlacementForLeftRight(hostElemClientRect, targetElemClientRect, 'left', availablePlacements);
        } // top: target height is less than host top


        if (targetElemClientRect.height < hostElemClientRect.top) {
          if (hostElemClientRectHorCenter > targetElemClientRect.width / 2 && windowWidth - hostElemClientRectHorCenter > targetElemClientRect.width / 2) {
            availablePlacements.splice(availablePlacements.length, 1, 'top');
          }

          this.setSecondaryPlacementForTopBottom(hostElemClientRect, targetElemClientRect, 'top', availablePlacements);
        } // right: check if target width can be placed between host right and viewport end and also height of target is
        // inside viewport


        if (windowWidth - hostElemClientRect.right > targetElemClientRect.width) {
          // check for right only
          if (hostElemClientRectVerCenter > targetElemClientRect.height / 2 && windowHeight - hostElemClientRectVerCenter > targetElemClientRect.height / 2) {
            availablePlacements.splice(availablePlacements.length, 1, 'right');
          } // check for right-top and right-bottom


          this.setSecondaryPlacementForLeftRight(hostElemClientRect, targetElemClientRect, 'right', availablePlacements);
        } // bottom: check if there is enough space between host bottom and viewport end for target height


        if (windowHeight - hostElemClientRect.bottom > targetElemClientRect.height) {
          if (hostElemClientRectHorCenter > targetElemClientRect.width / 2 && windowWidth - hostElemClientRectHorCenter > targetElemClientRect.width / 2) {
            availablePlacements.splice(availablePlacements.length, 1, 'bottom');
          }

          this.setSecondaryPlacementForTopBottom(hostElemClientRect, targetElemClientRect, 'bottom', availablePlacements);
        }

        return availablePlacements;
      };
      /**
       * check if secondary placement for left and right are available i.e. left-top, left-bottom, right-top, right-bottom
       * primaryplacement: left|right
       * availablePlacementArr: array in which available placemets to be set
       */


      Positioning.prototype.setSecondaryPlacementForLeftRight = function (hostElemClientRect, targetElemClientRect, primaryPlacement, availablePlacementArr) {
        var html = document.documentElement; // check for left-bottom

        if (targetElemClientRect.height <= hostElemClientRect.bottom) {
          availablePlacementArr.splice(availablePlacementArr.length, 1, primaryPlacement + '-bottom');
        }

        if ((window.innerHeight || html.clientHeight) - hostElemClientRect.top >= targetElemClientRect.height) {
          availablePlacementArr.splice(availablePlacementArr.length, 1, primaryPlacement + '-top');
        }
      };
      /**
       * check if secondary placement for top and bottom are available i.e. top-left, top-right, bottom-left, bottom-right
       * primaryplacement: top|bottom
       * availablePlacementArr: array in which available placemets to be set
       */


      Positioning.prototype.setSecondaryPlacementForTopBottom = function (hostElemClientRect, targetElemClientRect, primaryPlacement, availablePlacementArr) {
        var html = document.documentElement; // check for left-bottom

        if ((window.innerWidth || html.clientWidth) - hostElemClientRect.left >= targetElemClientRect.width) {
          availablePlacementArr.splice(availablePlacementArr.length, 1, primaryPlacement + '-left');
        }

        if (targetElemClientRect.width <= hostElemClientRect.right) {
          availablePlacementArr.splice(availablePlacementArr.length, 1, primaryPlacement + '-right');
        }
      };

      return Positioning;
    }();

    var positionService = new Positioning();
    /*
     * Accept the placement array and applies the appropriate placement dependent on the viewport.
     * Returns the applied placement.
     * In case of auto placement, placements are selected in order
     *   'top', 'bottom', 'left', 'right',
     *   'top-left', 'top-right',
     *   'bottom-left', 'bottom-right',
     *   'left-top', 'left-bottom',
     *   'right-top', 'right-bottom'.
     * */

    function positionElements(hostElement, targetElement, placement, appendToBody) {
      var placementVals = Array.isArray(placement) ? placement : [placement]; // replace auto placement with other placements

      var hasAuto = placementVals.findIndex(function (val) {
        return val === 'auto';
      });

      if (hasAuto >= 0) {
        ['top', 'bottom', 'left', 'right', 'top-left', 'top-right', 'bottom-left', 'bottom-right', 'left-top', 'left-bottom', 'right-top', 'right-bottom'].forEach(function (obj) {
          if (placementVals.find(function (val) {
            return val.search('^' + obj) !== -1;
          }) == null) {
            placementVals.splice(hasAuto++, 1, obj);
          }
        });
      } // coordinates where to position


      var topVal = 0,
          leftVal = 0;
      var appliedPlacement; // get available placements

      var availablePlacements = positionService.getAvailablePlacements(hostElement, targetElement);

      var _loop_1 = function _loop_1(item, index) {
        // check if passed placement is present in the available placement or otherwise apply the last placement in the
        // passed placement list
        if (availablePlacements.find(function (val) {
          return val === item;
        }) != null || placementVals.length === index + 1) {
          appliedPlacement = item;
          var pos = positionService.positionElements(hostElement, targetElement, item, appendToBody);
          topVal = pos.top;
          leftVal = pos.left;
          return "break";
        }
      }; // iterate over all the passed placements


      for (var _i = 0, _a = toItemIndexes(placementVals); _i < _a.length; _i++) {
        var _b = _a[_i],
            item = _b.item,
            index = _b.index;

        var state_1 = _loop_1(item, index);

        if (state_1 === "break") break;
      }

      targetElement.style.top = topVal + "px";
      targetElement.style.left = leftVal + "px";
      return appliedPlacement;
    } // function to get index and item of an array


    function toItemIndexes(a) {
      return a.map(function (item, index) {
        return {
          item: item,
          index: index
        };
      });
    } //# sourceMappingURL=positioning.js.map

    /***/

  },

  /***/
  "./node_modules/type-func/dist/bundle.js":
  /*!***********************************************!*\
    !*** ./node_modules/type-func/dist/bundle.js ***!
    \***********************************************/

  /*! no static exports found */

  /***/
  function node_modulesTypeFuncDistBundleJs(module, exports, __webpack_require__) {
    "use strict";

    Object.defineProperty(exports, '__esModule', {
      value: true
    });

    function getDef(f, d) {
      if (typeof f === 'undefined') {
        return typeof d === 'undefined' ? f : d;
      }

      return f;
    }

    function _boolean(func, def) {
      func = getDef(func, def);

      if (typeof func === 'function') {
        return function f() {
          for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          return !!func.apply(this, args);
        };
      }

      return !!func ? function () {
        return true;
      } : function () {
        return false;
      };
    }

    function integer(func, def) {
      func = getDef(func, def);

      if (typeof func === 'function') {
        return function f() {
          for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
          }

          var n = parseInt(func.apply(this, args), 10);
          return n != n ? 0 : n;
        };
      }

      func = parseInt(func, 10);
      return func != func ? function () {
        return 0;
      } : function () {
        return func;
      };
    }

    function string(func, def) {
      func = getDef(func, def);

      if (typeof func === 'function') {
        return function f() {
          for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            args[_key3] = arguments[_key3];
          }

          return '' + func.apply(this, args);
        };
      }

      func = '' + func;
      return function () {
        return func;
      };
    }

    exports["boolean"] = _boolean;
    exports.integer = integer;
    exports.string = string; //# sourceMappingURL=bundle.js.map

    /***/
  },

  /***/
  "./src/app/main/apps/calendar/calendar.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/main/apps/calendar/calendar.service.ts ***!
    \********************************************************/

  /*! exports provided: CalendarService */

  /***/
  function srcAppMainAppsCalendarCalendarServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarService", function () {
      return CalendarService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var CalendarService = /*#__PURE__*/function () {
      /**
       * Constructor
       *
       * @param {HttpClient} _httpClient
       */
      function CalendarService(_httpClient) {
        _classCallCheck(this, CalendarService);

        this._httpClient = _httpClient; // Set the defaults

        this.onEventsUpdated = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
      } // -----------------------------------------------------------------------------------------------------
      // @ Public methods
      // -----------------------------------------------------------------------------------------------------

      /**
       * Resolver
       *
       * @param {ActivatedRouteSnapshot} route
       * @param {RouterStateSnapshot} state
       * @returns {Observable<any> | Promise<any> | any}
       */


      _createClass(CalendarService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _this27 = this;

          return new Promise(function (resolve, reject) {
            Promise.all([_this27.getEvents()]).then(function (_ref65) {
              var _ref66 = _slicedToArray(_ref65, 1),
                  events = _ref66[0];

              resolve();
            }, reject);
          });
        }
        /**
         * Get events
         *
         * @returns {Promise<any>}
         */

      }, {
        key: "getEvents",
        value: function getEvents() {
          var _this28 = this;

          return new Promise(function (resolve, reject) {
            _this28._httpClient.get('api/calendar/events').subscribe(function (response) {
              _this28.events = response.data;

              _this28.onEventsUpdated.next(_this28.events);

              resolve(_this28.events);
            }, reject);
          });
        }
        /**
         * Update events
         *
         * @param events
         * @returns {Promise<any>}
         */

      }, {
        key: "updateEvents",
        value: function updateEvents(events) {
          var _this29 = this;

          return new Promise(function (resolve, reject) {
            _this29._httpClient.post('api/calendar/events', {
              id: 'events',
              data: _toConsumableArray(events)
            }).subscribe(function (response) {
              _this29.getEvents();
            }, reject);
          });
        }
      }]);

      return CalendarService;
    }();

    CalendarService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    CalendarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], CalendarService);
    /***/
  },

  /***/
  "./src/app/main/apps/calendar/event.model.ts":
  /*!***************************************************!*\
    !*** ./src/app/main/apps/calendar/event.model.ts ***!
    \***************************************************/

  /*! exports provided: CalendarEventModel */

  /***/
  function srcAppMainAppsCalendarEventModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarEventModel", function () {
      return CalendarEventModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var date_fns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! date-fns */
    "./node_modules/date-fns/index.js");
    /* harmony import */


    var date_fns__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_1__);

    var CalendarEventModel =
    /**
     * Constructor
     *
     * @param data
     */
    function CalendarEventModel(data) {
      _classCallCheck(this, CalendarEventModel);

      data = data || {};
      this.start = new Date(data.start) || Object(date_fns__WEBPACK_IMPORTED_MODULE_1__["startOfDay"])(new Date());
      this.end = new Date(data.end) || Object(date_fns__WEBPACK_IMPORTED_MODULE_1__["endOfDay"])(new Date());
      this.title = data.title || '';
      this.color = {
        primary: data.color && data.color.primary || '#1e90ff',
        secondary: data.color && data.color.secondary || '#D1E8FF'
      };
      this.draggable = data.draggable;
      this.resizable = {
        beforeStart: data.resizable && data.resizable.beforeStart || true,
        afterEnd: data.resizable && data.resizable.afterEnd || true
      };
      this.actions = data.actions || [];
      this.allDay = data.allDay || false;
      this.cssClass = data.cssClass || '';
      this.meta = {
        location: data.meta && data.meta.location || '',
        notes: data.meta && data.meta.notes || ''
      };
    };
    /***/

  }
}]);
//# sourceMappingURL=default~calendar-calendar-module~report-assistanceReport-assistanceReport-module-es5.js.map