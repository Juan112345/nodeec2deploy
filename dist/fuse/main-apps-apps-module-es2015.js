(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main-apps-apps-module"],{

/***/ "./src/app/main/apps/apps.module.ts":
/*!******************************************!*\
  !*** ./src/app/main/apps/apps.module.ts ***!
  \******************************************/
/*! exports provided: AppsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppsModule", function() { return AppsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _fuse_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fuse/shared.module */ "./src/@fuse/shared.module.ts");




const routes = [
    {
        path: 'colaborador/buscar',
        loadChildren: './colaborador/buscar/buscar.module#SearchEmployeModule'
    },
    {
        path: 'colaborador/nuevo',
        loadChildren: './colaborador/nuevo/nuevo.module#NewEmployeModule'
    },
    {
        path: 'horasExtras/buscar',
        loadChildren: './horasExtras/buscar/buscar.module#BuscarModule'
    },
    {
        path: 'horasExtras/registrar',
        loadChildren: './horasExtras/registrar/registrar.module#RegistrarModule'
    },
    {
        path: 'assistance/manageAssistance',
        loadChildren: './assistance/manageAssistance/manage.module#ManageAssistanceModule'
    },
    {
        path: 'assistance/searchAssistance',
        loadChildren: './assistance/searchAssistance/search.module#SearchAssistanceModule'
    },
    {
        path: 'requerimientos/buscar',
        loadChildren: './requerimientos/buscar/searchRequest.module#SearchRequestModule'
    },
    {
        path: 'requerimientos/regis-masivo',
        loadChildren: './requerimientos/regis-masivo/regis-masivo.module#RegisMasivoModule'
    },
    {
        path: 'requerimientos/registrar',
        loadChildren: './requerimientos/registrar/registry.module#RegistryRequestModule'
    },
    {
        path: 'requerimientos/reporte',
        loadChildren: './requerimientos/reporte/reporte.module#ReporteModule'
    },
    {
        path: 'permits/managePermits',
        loadChildren: './permits/managePermits/managePermits.module#ManagePermitsModule'
    },
    {
        path: 'permits/searchPermits',
        loadChildren: './permits/searchPermits/searchPermits.module#SearchPermitsModule'
    },
    {
        path: 'dashboards/analytics',
        loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
    },
    {
        path: 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectDashboardModule'
    },
    {
        path: 'mail',
        loadChildren: './mail/mail.module#MailModule'
    },
    {
        path: 'mail-ngrx',
        loadChildren: './mail-ngrx/mail.module#MailNgrxModule'
    },
    {
        path: 'chat',
        loadChildren: './chat/chat.module#ChatModule'
    },
    {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    },
    {
        path: 'e-commerce',
        loadChildren: './e-commerce/e-commerce.module#EcommerceModule'
    },
    {
        path: 'academy',
        loadChildren: './academy/academy.module#AcademyModule'
    },
    {
        path: 'todo',
        loadChildren: './todo/todo.module#TodoModule'
    },
    {
        path: 'file-manager',
        loadChildren: './file-manager/file-manager.module#FileManagerModule'
    },
    {
        path: 'contacts',
        loadChildren: './contacts/contacts.module#ContactsModule'
    },
    {
        path: 'report/assistanceReport',
        loadChildren: './report/assistanceReport/assistanceReport.module#AssistanceReportModule'
    },
    {
        path: 'report/reports',
        loadChildren: './report/reports/reports.module#ReportsModule'
    },
    {
        path: 'scrumboard',
        loadChildren: './scrumboard/scrumboard.module#ScrumboardModule'
    },
    {
        path: 'vacation/manageVacation',
        loadChildren: './vacation/manageVacation/manageVacation.module#ManageVacationModule'
    },
    {
        path: 'vacation/searchVacation',
        loadChildren: './vacation/searchVacation/searchVacation.module#SearchVacationModule'
    },
    {
        path: 'vacation/controlVacation',
        loadChildren: './vacation/controlVacation/controlVacation.module#ControlVacationModule'
    },
];
let AppsModule = class AppsModule {
};
AppsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes),
            _fuse_shared_module__WEBPACK_IMPORTED_MODULE_3__["FuseSharedModule"]
        ],
        declarations: []
    })
], AppsModule);



/***/ })

}]);
//# sourceMappingURL=main-apps-apps-module-es2015.js.map