function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["permits-managePermits-managePermits-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.html":
  /*!************************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.html ***!
    \************************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMainAppsPermitsManagePermitsDialogPersonalizePermitsDialogPersonalizePermitsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div>\n    <div class=\"agregar\" *ngIf=\"operation.name == 'agregar'\">\n        <div id=\"titulo\" mat-dialog-title>\n            <h1>Personalizar</h1>\n            <button mat-icon-button class=\"close\" (click)=\"onNoClick()\"><mat-icon class=\"secondary-text\">close</mat-icon></button>\n        </div>\n        <mat-form-field class=\"add-input\" appearance=\"outline\">\n            <mat-label>Se repite el:</mat-label>\n            <mat-select [formControl]=\"toppingsControl\" multiple>\n          \n              <mat-select-trigger>\n                <mat-chip-list>\n                  <mat-chip *ngFor=\"let topping of toppingsControl.value\"\n                    [removable]=\"true\" (removed)=\"onToppingRemoved(topping)\">\n                    {{ topping }}\n                    <mat-icon matChipRemove>cancel</mat-icon>\n                  </mat-chip>\n                </mat-chip-list>\n              </mat-select-trigger>\n          \n              <mat-option *ngFor=\"let topping of toppingList\" [value]=\"topping\">{{topping}}</mat-option>\n          \n            </mat-select>\n        </mat-form-field>\n        <div class=\"add-input\">\n            <div class=\"agregar\">\n                <h1 id=\"titulo\">Termina:</h1>\n            </div>\n            <mat-radio-group\n            aria-labelledby=\"example-radio-group-label\"\n            class=\"example-radio-group\"\n            [(ngModel)]=\"favoriteSeason\">\n            <mat-radio-button class=\"example-radio-button\" [value]=\"1\">\n                Dentro 6 meses.\n            </mat-radio-button>\n            <mat-radio-button class=\"example-radio-button\" [value]=\"2\">\n                El:\n                <mat-form-field appearance=\"outline\" style=\"width: 243px; margin-left: 250px;\">\n                    <mat-label>Termina el:</mat-label>\n                    <input matInput [matDatepicker]=\"assistanceDatePickerFinal1\" [(ngModel)]=\"fechaTermino\">\n                    <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePickerFinal1\"></mat-datepicker-toggle>\n                    <mat-datepicker #assistanceDatePickerFinal1></mat-datepicker>\n                </mat-form-field>\n            </mat-radio-button>\n            <mat-radio-button class=\"example-radio-button\" [value]=\"3\">\n                Después de:\n                <mat-form-field appearance=\"outline\" style=\"width: 243px; margin-left: 176px;padding-bottom: 0px;\">\n                    <mat-label>Iteraciones</mat-label>\n                    <input type=\"number\" min=\"0\" matInput [(ngModel)]=\"numeroIteraciones\">\n                </mat-form-field>\n            </mat-radio-button>\n            </mat-radio-group>\n        </div>\n        <div>\n            <button mat-raised-button color=\"accent\" aria-label=\"LOG IN\" (click)=\"onSaveForm()\">\n                Guardar\n            </button>\n        </div>  \n    </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/managePermits.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/managePermits.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMainAppsPermitsManagePermitsManagePermitsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"mat-elevation-z0 width-card\">\n<div id=\"managePermits\" class=\"page-layout blank\" >\n        \n            <div fxLayout=\"row\">\n                <div fxLayoutAlign=\"left start\" id=\"titulo\">\n                    <h1>Registrar Permisos</h1>\n                </div>\n                <div id=\"back\">\n                    <button id=\"buttonColor\" [routerLink]=\"'/apps/permits/searchPermits'\"  mat-button  mat-raised-button color=\"primary\" aria-label=\"Nuevo\">\n                        <mat-icon>refresh</mat-icon>\n                    </button>\n                </div>\n            </div>\n            <mat-card id=\"datosPersonales\">\n            <form [formGroup]=\"formGroup2\">\n                <div  fxLayout=\"row\">\n                    <div  fxLayoutAlign=\"left start\" id=\"selectOptions\">\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Colaborador</mat-label>\n                            <input type=\"text\" name=\"empleado\" formControlName=\"empleado\" placeholder=\"Seleccione\" aria-label=\"Number\" matInput [matAutocomplete]=\"auto\">\n                            <mat-autocomplete #auto=\"matAutocomplete\">\n                              <mat-option *ngFor=\"let option of filteredOptions | async\" [value]=\"option.employeeTo.employeeCompleteName\" (click)=\"onSelectionChange(option.employeeTo.employeePk)\">\n                                {{option.employeeTo.employeeCompleteName}}\n                              </mat-option>\n                            </mat-autocomplete>\n                        </mat-form-field>\n                    </div>\n                \n                    <div  fxLayoutAlign=\"left start\" id=\"check\">\n                        <mat-checkbox name=\"checked\" formControlName=\"checked\">Afecta Horas Acumuladas</mat-checkbox>\n                    </div>\n                    \n                </div>\n                <div  fxLayout=\"row\">\n                    <div  fxLayoutAlign=\"left start\" id=\"selectMotivo\" >\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Motivo</mat-label>\n                            <mat-select name=\"document\" required formControlName=\"motivo\">\n                                <mat-option value=\"\">-Seleccione-</mat-option>\n                                <mat-option *ngFor=\"let option of motivos\" [value]=\"option\">\n                                    {{option}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                    </div>\n                    <div  fxLayoutAlign=\"left start\" id=\"tiprPermist\">\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Tipo Permiso</mat-label>\n                            <mat-select name=\"tipoPermisoSelect\"  required formControlName=\"tipoPermisoSelect\"\n                            name=\"tipoPermisoSelect\">\n                                <mat-option value=\"\">-Seleccione-</mat-option>\n                                <mat-option *ngFor=\"let option of tipoPermisos\" [value]=\"option\" (click)=\"tipoPermiso(option)\">\n                                    {{option}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                    </div>\n                    <div  id=\"frecuencia\">\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Frecuencia</mat-label>\n                            <mat-select name=\"tipoFrecuenciaSelected\"  required [disabled]=\"tipoPermisoFlag\" formControlName=\"tipoFrecuenciaSelected\"\n                            name=\"tipoFrecuenciaSelected\">\n                                <mat-option value=\"\">-Seleccione-</mat-option>\n                                <mat-option *ngFor=\"let option of frecuencia\" [value]=\"option\">\n                                    {{option}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div  fxLayout=\"row\">\n                    <div  id=\"fechaInicial\">\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Fecha Permiso Inicial</mat-label>\n                            <input matInput [matDatepicker]=\"assistanceDatePicker\" formControlName=\"diaAsistencia\"\n                                name=\"diaDeAsistencia\" placeholder=\"{{today}}\">\n                            <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePicker\"></mat-datepicker-toggle>\n                            <mat-datepicker #assistanceDatePicker></mat-datepicker>\n                        </mat-form-field>\n                    </div>\n                    <div  id=\"fechaFinal\">\n                        <mat-form-field appearance=\"outline\" fxFlex>\n                            <mat-label>Fecha Permiso Final</mat-label>\n                            <input matInput [matDatepicker]=\"assistanceDatePickerFinal\" formControlName=\"diaAsistenciaFinal\"\n                                name=\"diaAsistenciaFinal\" placeholder=\"{{today}}\">\n                            <mat-datepicker-toggle matSuffix [for]=\"assistanceDatePickerFinal\"></mat-datepicker-toggle>\n                            <mat-datepicker #assistanceDatePickerFinal></mat-datepicker>\n                        </mat-form-field>\n                    </div>\n                    <div>\n                        <mat-form-field appearance=\"outline\" style=\"width: 150px; margin-right: 20px\">\n                            <mat-label>Hora Inicial</mat-label>\n                            <input matInput type=\"time\" fxFlex \n                                name=\"horaInicial\" matInput formControlName=\"horaInicial\">\n                        </mat-form-field>\n                    </div>\n                    <div>\n                        <mat-form-field appearance=\"outline\" style=\"width: 150px;\">\n                            <mat-label>Hora Final</mat-label>\n                            <input matInput type=\"time\" fxFlex \n                                name=\"horaFinal\" matInput formControlName=\"horaFinal\">\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div fxLayout=\"row\">\n                    <div id=\"text-area-width\">\n                        <mat-form-field appearance=\"outline\" id=\"text-area-width\">\n                            <mat-label>Descripción</mat-label>\n                            <textarea matInput cdkTextareaAutosize\n                            #autosize=\"cdkTextareaAutosize\"\n                            cdkAutosizeMinRows=\"1\"\n                            cdkAutosizeMaxRows=\"6\" name=\"comentario\" formControlName=\"comentario\" type=\"text\"></textarea>\n                        </mat-form-field>\n                    </div>\n                </div>\n            </form>\n        </mat-card>\n        <br>\n        \n            <div fxLayout=\"row\">\n                <div>\n                    <button id=\"buttonColor\"  mat-button  mat-raised-button color=\"primary\" aria-label=\"regist\" (click)=\"getDates()\">\n                        Agregar Permiso\n                    </button>\n                </div>\n            </div>\n            \n            <div id=\"permisos\">\n                <div>\n                <mat-paginator style=\"float: left;\" [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n                <table class=\"mat-elevation-z8\" mat-table [dataSource]=\"datos\" class=\"mat-elevation-z8\">\n        \n                    <ng-container matColumnDef=\"check\">\n                        <th mat-header-cell *matHeaderCellDef>\n                            <mat-checkbox [disabled]=\"false\"\n                                          (change)=\"$event ? masterToggle() : null\"\n                                          [checked]=\"selection.hasValue() && isAllSelected()\"\n                                          [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\n                                          [aria-label]=\"checkboxLabel()\">\n                            </mat-checkbox>\n                          </th>\n                          <td mat-cell *matCellDef=\"let row; let i = index;\">\n                            <mat-checkbox (click)=\"$event.stopPropagation();\"\n                                          (change)=\"$event ? selection.toggle(row) : null\"\n                                          [checked]=\"selection.isSelected(row)\"\n                                          [aria-label]=\"checkboxLabel(row)\">\n                            </mat-checkbox>\n                          </td>\n                    </ng-container>\n        \n                    <ng-container matColumnDef=\"descripcion\">\n                        <th mat-header-cell *matHeaderCellDef> Descripcion </th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.descripcion}} </td>\n                    </ng-container>\n        \n                    <ng-container matColumnDef=\"fecha\">\n                        <th mat-header-cell *matHeaderCellDef> Fecha </th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.fecha | date: 'dd/MM/yyyy'}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"horaInicio\">\n                        <th mat-header-cell *matHeaderCellDef> Hora Inicio </th>\n                        <td mat-cell *matCellDef=\"let element, let i = index\">\n                            <mat-form-field appearance=\"outline\" style=\"width: 150px;padding-right: 10px;\">\n                                <input matInput type=\"time\" fxFlex value=\"{{element.horaInicio}}\"\n                                    name=\"element.horaInicio\" matInput [(ngModel)]=\"horaInicioPermitDay[i]\">\n                            </mat-form-field>\n                        </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"horaFinal\">\n                        <th mat-header-cell *matHeaderCellDef> Hora Final </th>\n                        <td mat-cell *matCellDef=\"let element; let i = index\">\n                            <mat-form-field appearance=\"outline\" style=\"width: 150px;\">\n                                <input matInput type=\"time\" fxFlex value=\"{{element.horaFin}}\"\n                                    name=\"element.horaFin\" matInput [(ngModel)]=\"horaFinalPermitDay[i]\">\n                            </mat-form-field>\n                        </td>\n                    </ng-container>\n                    <!--<mat-checkbox>Check me!</mat-checkbox>-->\n                    <tr mat-header-row *matHeaderRowDef=\"columnas\"></tr>\n                    <tr mat-row *matRowDef=\"let row; columns: columnas;\"></tr>\n                </table>\n            </div>\n        </div>\n            <div fxLayout=\"row\" >\n                <div id=\"dowmload\" fxLayoutAlign=\"left start\">\n                <label style=\"padding-top: 8px; padding-right: 20px;\">{{fileToUpload}}</label>\n                    <button mat-icon-button color=\"primary\">\n                        <button mat-fab>\n                            <mat-icon>save</mat-icon>\n                            <input type=\"file\" multiple [(ngModel)]=\"fileToUpload\"\n                (change)=\"handleFileInput($event.target.files)\"  \n                 accept=\".docx,.pdf,.xlsx,.xls\"\n                style=\"opacity: 0; position: absolute; z-index: 2;\n                top: 0; left: 0; height: 100%; width: 100%;\">\n                        </button>\n                    </button>\n\n                </div>\n            </div>\n            <div id=\"buscar\">\n                <button class=\"search-button\" mat-raised-button color=\"primary\" aria-label=\"BUSCAR\" (click)=\"registrar()\">\n                    Registrar\n                </button>\n            </div>\n            <div id=\"limpiar\">\n                <button class=\"search-button\" mat-raised-button color=\"primary\" aria-label=\"LIMPIAR\">\n                    Limpiar\n                </button>\n            </div>\n</div>\n</mat-card>";
    /***/
  },

  /***/
  "./src/app/common/rest/services/PermitsDayController.ts":
  /*!**************************************************************!*\
    !*** ./src/app/common/rest/services/PermitsDayController.ts ***!
    \**************************************************************/

  /*! exports provided: PermitsDayController */

  /***/
  function srcAppCommonRestServicesPermitsDayControllerTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PermitsDayController", function () {
      return PermitsDayController;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _oauth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../oauth.service */
    "./src/app/common/oauth.service.ts");

    var PermitsDayController = /*#__PURE__*/function () {
      function PermitsDayController(httpClient, oauth) {
        _classCallCheck(this, PermitsDayController);

        this.httpClient = httpClient;
        this.oauth = oauth;
        this.url = oauth.servidor + '/permitsDayResource';
        this.header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
          'Content-Type': 'application/json'
        }); //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
      }

      _createClass(PermitsDayController, [{
        key: "getListPermitsDay",
        value: function getListPermitsDay(arg0) {
          var metodo = this.url + '/getListPermitsDay'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "createPermitsDay",
        value: function createPermitsDay(arg0) {
          var metodo = this.url + '/createPermitsDay'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }, {
        key: "updatePermitsDay",
        value: function updatePermitsDay(arg0) {
          var metodo = this.url + '/updatePermitsDay'; // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }

          console.log(JSON.stringify(arg0));
          return this.httpClient.post(metodo, JSON.stringify(arg0), {
            headers: this.header
          });
        }
      }]);

      return PermitsDayController;
    }();

    PermitsDayController.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _oauth_service__WEBPACK_IMPORTED_MODULE_3__["OAuthService"]
      }];
    };

    PermitsDayController = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
      providedIn: 'root'
    })], PermitsDayController);
    /***/
  },

  /***/
  "./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.scss":
  /*!**********************************************************************************************************************!*\
    !*** ./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.scss ***!
    \**********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMainAppsPermitsManagePermitsDialogPersonalizePermitsDialogPersonalizePermitsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".agregar {\n  display: block;\n  text-align: center;\n}\n\n#titulo {\n  align-items: flex-start;\n  text-align: left;\n  display: flex;\n}\n\n.labelI {\n  text-align: left;\n  display: inherit;\n}\n\n.add-input {\n  display: block;\n  width: 100%;\n  text-align: none;\n}\n\n.agregar button {\n  align-content: center;\n}\n\n.editar {\n  display: block;\n  text-align: center;\n}\n\n.edit-input {\n  display: block;\n  width: 100%;\n  text-align: none;\n}\n\n.edit-button {\n  margin: 8px;\n  align-content: center;\n}\n\n.example-radio-group {\n  display: flex;\n  flex-direction: column;\n  margin: 15px 0;\n}\n\n.example-radio-button {\n  margin: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9hcHBzL3Blcm1pdHMvbWFuYWdlUGVybWl0cy9kaWFsb2ctcGVyc29uYWxpemUtcGVybWl0cy9DOlxcVXNlcnNcXG11ZXJ0XFxnaXRcXGF3cy1iaXRidWNrZXQtZnJvbnQtcGFuZG9yYS9zcmNcXGFwcFxcbWFpblxcYXBwc1xccGVybWl0c1xcbWFuYWdlUGVybWl0c1xcZGlhbG9nLXBlcnNvbmFsaXplLXBlcm1pdHNcXGRpYWxvZy1wZXJzb25hbGl6ZS1wZXJtaXRzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tYWluL2FwcHMvcGVybWl0cy9tYW5hZ2VQZXJtaXRzL2RpYWxvZy1wZXJzb25hbGl6ZS1wZXJtaXRzL2RpYWxvZy1wZXJzb25hbGl6ZS1wZXJtaXRzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREVBO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0kscUJBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0FDQ0o7O0FERUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FDQ0o7O0FERUE7RUFDSSxXQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9tYWluL2FwcHMvcGVybWl0cy9tYW5hZ2VQZXJtaXRzL2RpYWxvZy1wZXJzb25hbGl6ZS1wZXJtaXRzL2RpYWxvZy1wZXJzb25hbGl6ZS1wZXJtaXRzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFncmVnYXIge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiN0aXR1bG8ge1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmxhYmVsSSB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgZGlzcGxheTogaW5oZXJpdDtcclxufVxyXG5cclxuLmFkZC1pbnB1dCB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogbm9uZTtcclxufVxyXG5cclxuLmFncmVnYXIgYnV0dG9uIHtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmVkaXRhciB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmVkaXQtaW5wdXQge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IG5vbmU7XHJcbn1cclxuXHJcbi5lZGl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDhweDtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmV4YW1wbGUtcmFkaW8tZ3JvdXAge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxufVxyXG4gIFxyXG4uZXhhbXBsZS1yYWRpby1idXR0b24ge1xyXG4gICAgbWFyZ2luOiA1cHg7XHJcbn0iLCIuYWdyZWdhciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiN0aXR1bG8ge1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmxhYmVsSSB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGRpc3BsYXk6IGluaGVyaXQ7XG59XG5cbi5hZGQtaW5wdXQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IG5vbmU7XG59XG5cbi5hZ3JlZ2FyIGJ1dHRvbiB7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmVkaXRhciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5lZGl0LWlucHV0IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBub25lO1xufVxuXG4uZWRpdC1idXR0b24ge1xuICBtYXJnaW46IDhweDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xufVxuXG4uZXhhbXBsZS1yYWRpby1ncm91cCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIG1hcmdpbjogMTVweCAwO1xufVxuXG4uZXhhbXBsZS1yYWRpby1idXR0b24ge1xuICBtYXJnaW46IDVweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.ts":
  /*!********************************************************************************************************************!*\
    !*** ./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.ts ***!
    \********************************************************************************************************************/

  /*! exports provided: DialogPersonalizePermitsComponent */

  /***/
  function srcAppMainAppsPermitsManagePermitsDialogPersonalizePermitsDialogPersonalizePermitsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DialogPersonalizePermitsComponent", function () {
      return DialogPersonalizePermitsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/esm2015/dialog.js");

    var DialogPersonalizePermitsComponent = /*#__PURE__*/function () {
      function DialogPersonalizePermitsComponent(dialogRef, data) {
        _classCallCheck(this, DialogPersonalizePermitsComponent);

        this.dialogRef = dialogRef;
        this.data = data;
        this.toppingsControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]([]);
        this.toppingList = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        this.operation = this.data;
      }

      _createClass(DialogPersonalizePermitsComponent, [{
        key: "onToppingRemoved",
        value: function onToppingRemoved(topping) {
          var toppings = this.toppingsControl.value;
          this.removeFirst(toppings, topping);
          this.toppingsControl.setValue(toppings); // To trigger change detection
        }
      }, {
        key: "removeFirst",
        value: function removeFirst(array, toRemove) {
          var index = array.indexOf(toRemove);

          if (index !== -1) {
            array.splice(index, 1);
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          console.log(this.operation);
        }
      }, {
        key: "onSaveForm",
        value: function onSaveForm() {
          var diasRepeticion = this.toppingsControl.value;

          for (var index = 0; index < diasRepeticion.length; index++) {
            if (diasRepeticion[index] == 'Domingo') {
              diasRepeticion[index] = 0;
            }

            if (diasRepeticion[index] == 'Lunes') {
              diasRepeticion[index] = 1;
            }

            if (diasRepeticion[index] == 'Martes') {
              diasRepeticion[index] = 2;
            }

            if (diasRepeticion[index] == 'Miercoles') {
              diasRepeticion[index] = 3;
            }

            if (diasRepeticion[index] == 'Jueves') {
              diasRepeticion[index] = 4;
            }

            if (diasRepeticion[index] == 'Viernes') {
              diasRepeticion[index] = 5;
            }

            if (diasRepeticion[index] == 'Sabado') {
              diasRepeticion[index] = 6;
            }
          }

          var opcionTermino = this.favoriteSeason;
          var fechaTermino = this.fechaTermino;
          var numeroIteraciones = this.numeroIteraciones;
          var object = {
            diasRepeticion: diasRepeticion,
            opcionTermino: opcionTermino,
            fechaTermino: fechaTermino,
            numeroIteraciones: numeroIteraciones
          };
          this.dialogRef.close({
            data: object
          });
        }
      }, {
        key: "onNoClick",
        value: function onNoClick() {
          this.dialogRef.close();
        }
      }]);

      return DialogPersonalizePermitsComponent;
    }();

    DialogPersonalizePermitsComponent.ctorParameters = function () {
      return [{
        type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
          args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]]
        }]
      }];
    };

    DialogPersonalizePermitsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-dialog-personalize-permits',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dialog-personalize-permits.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dialog-personalize-permits.component.scss */
      "./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]))], DialogPersonalizePermitsComponent);
    /***/
  },

  /***/
  "./src/app/main/apps/permits/managePermits/managePermits.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/main/apps/permits/managePermits/managePermits.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMainAppsPermitsManagePermitsManagePermitsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".width-card {\n  margin: 20px;\n  width: 100%;\n}\n\n#managePermits #titulo {\n  width: 80%;\n  float: left;\n  padding-right: 20%;\n}\n\n#managePermits #back {\n  float: right;\n  padding-top: 15px;\n  padding-left: 12%;\n}\n\n#managePermits #selectOptions {\n  width: 80%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #search {\n  width: 10%;\n  float: left;\n  padding-right: 2%;\n}\n\n#managePermits #inputSearch {\n  width: 40%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #selectMotivo {\n  width: 35%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #check {\n  width: 35%;\n  float: left;\n  padding-top: 2%;\n}\n\n#managePermits #horaEntrada {\n  width: 40%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #horaSalida {\n  width: 25%;\n  float: left;\n}\n\n#managePermits #tiprPermist {\n  width: 30%;\n  float: left;\n  margin-right: 5%;\n}\n\n#managePermits #fechaInicial {\n  width: 35%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #fechaFinal {\n  width: 35%;\n  float: left;\n  padding-right: 5%;\n}\n\n#managePermits #frecuencia {\n  width: 30%;\n  float: left;\n}\n\n#managePermits #descripcion {\n  width: 30%;\n  float: left;\n}\n\n#managePermits #dowmload {\n  width: 30%;\n  float: left;\n  padding-left: 5%;\n  padding-top: 5%;\n}\n\n#managePermits #check2 {\n  width: 35%;\n  float: left;\n  padding-right: 5%;\n  padding-top: 5%;\n}\n\n#managePermits #buttonRegist {\n  width: 30%;\n  float: right;\n  padding-left: 30%;\n  padding-top: 5%;\n}\n\n#managePermits #buttonClean {\n  padding-left: 15%;\n  width: 30%;\n  float: right;\n  padding-top: 5%;\n}\n\n#managePermits table {\n  width: 100%;\n}\n\n#managePermits #buttonColor {\n  background-color: #00a2ce;\n}\n\n#managePermits #text-area-width {\n  width: 100%;\n}\n\n.mat-header-cell, .mat-sort-header {\n  background-color: #00a2ce;\n  font-size: 16px;\n  color: white !important;\n}\n\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  color: #00a2ce !important;\n}\n\n#datosPersonales {\n  border-style: solid;\n  border-color: #00a2ce;\n  border-width: unset;\n}\n\n#buscar {\n  width: 50%;\n  float: left;\n  padding-left: 39.8%;\n  padding-right: 2.5%;\n}\n\n.search-button {\n  background: #00a2ce !important;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9hcHBzL3Blcm1pdHMvbWFuYWdlUGVybWl0cy9DOlxcVXNlcnNcXG11ZXJ0XFxnaXRcXGF3cy1iaXRidWNrZXQtZnJvbnQtcGFuZG9yYS9zcmNcXGFwcFxcbWFpblxcYXBwc1xccGVybWl0c1xcbWFuYWdlUGVybWl0c1xcbWFuYWdlUGVybWl0cy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWFpbi9hcHBzL3Blcm1pdHMvbWFuYWdlUGVybWl0cy9tYW5hZ2VQZXJtaXRzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNDSjs7QURFSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNDUjs7QURDSTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNDUjs7QURDSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNDUjs7QURDSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtBQ0NSOztBREVJO0VBQ0ksVUFBQTtFQUNBLFdBQUE7QUNBUjs7QURHSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDRFI7O0FER0k7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0RSOztBREdJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNEUjs7QURHSTtFQUNJLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDRFI7O0FER0k7RUFDSSxXQUFBO0FDRFI7O0FER0k7RUFDSSx5QkFBQTtBQ0RSOztBREdJO0VBQ0ksV0FBQTtBQ0RSOztBRElBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7QUNESjs7QURJQTtFQUNJLHlCQUFBO0FDREo7O0FESUM7RUFDRyxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNESjs7QURJQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0RKOztBRElBO0VBQ0ksOEJBQUE7RUFDQSxZQUFBO0FDREoiLCJmaWxlIjoic3JjL2FwcC9tYWluL2FwcHMvcGVybWl0cy9tYW5hZ2VQZXJtaXRzL21hbmFnZVBlcm1pdHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2lkdGgtY2FyZCB7XG4gICAgbWFyZ2luOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuI21hbmFnZVBlcm1pdHN7XG4gICAgI3RpdHVsb3tcbiAgICAgICAgd2lkdGg6IDgwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwJTtcbiAgICB9XG4gICAgI2JhY2t7XG4gICAgICAgIGZsb2F0OiByaWdodDsgICAgIFxuICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMiU7XG4gICAgfVxuICAgICNzZWxlY3RPcHRpb25ze1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgfVxuICAgICNzZWFyY2h7XG4gICAgICAgIHdpZHRoOiAxMCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0OyAgXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIlO1xuICAgIH1cbiAgICAjaW5wdXRTZWFyY2h7XG4gICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0OyAgXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIH1cbiAgICAjc2VsZWN0TW90aXZve1xuICAgICAgICB3aWR0aDozNSU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICB9XG4gICAgI2NoZWNre1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy10b3A6IDIlO1xuICAgIH1cbiAgICAjaG9yYUVudHJhZGF7XG4gICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICB9XG4gICAgI2hvcmFTYWxpZGF7XG4gICAgICAgIHdpZHRoOiAyNSU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgIH1cbiAgICAjdGlwclBlcm1pc3R7XG4gICAgICAgIHdpZHRoOiAzMCU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDUlO1xuICAgIH1cbiAgICAjZmVjaGFJbmljaWFse1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgfVxuICAgICNmZWNoYUZpbmFse1xuICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgfVxuICAgICNmcmVjdWVuY2lhe1xuICAgICAgICB3aWR0aDogMzAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICBcbiAgICB9XG4gICAgI2Rlc2NyaXBjaW9ue1xuICAgICAgICB3aWR0aDogMzAlO1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgXG4gICAgfVxuICAgICNkb3dtbG9hZHtcbiAgICAgICAgd2lkdGg6IDMwJTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB9XG4gICAgI2NoZWNrMntcbiAgICAgICAgd2lkdGg6IDM1JTtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgICAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgfVxuICAgICNidXR0b25SZWdpc3R7XG4gICAgICAgIHdpZHRoOiAzMCU7XG4gICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAzMCU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICB9XG4gICAgI2J1dHRvbkNsZWFue1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1JTtcbiAgICAgICAgd2lkdGg6IDMwJTtcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgfVxuICAgIHRhYmxle1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG4gICAgI2J1dHRvbkNvbG9ye1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwgMTYyLCAyMDYpO1xuICAgIH1cbiAgICAjdGV4dC1hcmVhLXdpZHRoIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxufVxuLm1hdC1oZWFkZXItY2VsbCwgLm1hdC1zb3J0LWhlYWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDE2MiwgMjA2KTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59IFxuXG4ubWF0LWZvcm0tZmllbGQtYXBwZWFyYW5jZS1vdXRsaW5lIC5tYXQtZm9ybS1maWVsZC1vdXRsaW5lIHtcbiAgICBjb2xvcjogcmdiKDAsIDE2MiwgMjA2KSAhaW1wb3J0YW50O1xuIH1cblxuICNkYXRvc1BlcnNvbmFsZXMge1xuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMCwgMTYyLCAyMDYpO1xuICAgIGJvcmRlci13aWR0aDogdW5zZXQ7XG59XG5cbiNidXNjYXIge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgcGFkZGluZy1sZWZ0OiAzOS44JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyLjUlO1xufVxuXG4uc2VhcmNoLWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZDogcmdiKDAsIDE2MiwgMjA2KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOndoaXRlO1xufSIsIi53aWR0aC1jYXJkIHtcbiAgbWFyZ2luOiAyMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuI21hbmFnZVBlcm1pdHMgI3RpdHVsbyB7XG4gIHdpZHRoOiA4MCU7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nLXJpZ2h0OiAyMCU7XG59XG4jbWFuYWdlUGVybWl0cyAjYmFjayB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctbGVmdDogMTIlO1xufVxuI21hbmFnZVBlcm1pdHMgI3NlbGVjdE9wdGlvbnMge1xuICB3aWR0aDogODAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG4jbWFuYWdlUGVybWl0cyAjc2VhcmNoIHtcbiAgd2lkdGg6IDEwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDIlO1xufVxuI21hbmFnZVBlcm1pdHMgI2lucHV0U2VhcmNoIHtcbiAgd2lkdGg6IDQwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuI21hbmFnZVBlcm1pdHMgI3NlbGVjdE1vdGl2byB7XG4gIHdpZHRoOiAzNSU7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nLXJpZ2h0OiA1JTtcbn1cbiNtYW5hZ2VQZXJtaXRzICNjaGVjayB7XG4gIHdpZHRoOiAzNSU7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nLXRvcDogMiU7XG59XG4jbWFuYWdlUGVybWl0cyAjaG9yYUVudHJhZGEge1xuICB3aWR0aDogNDAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG4jbWFuYWdlUGVybWl0cyAjaG9yYVNhbGlkYSB7XG4gIHdpZHRoOiAyNSU7XG4gIGZsb2F0OiBsZWZ0O1xufVxuI21hbmFnZVBlcm1pdHMgI3RpcHJQZXJtaXN0IHtcbiAgd2lkdGg6IDMwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1yaWdodDogNSU7XG59XG4jbWFuYWdlUGVybWl0cyAjZmVjaGFJbmljaWFsIHtcbiAgd2lkdGg6IDM1JTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuI21hbmFnZVBlcm1pdHMgI2ZlY2hhRmluYWwge1xuICB3aWR0aDogMzUlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1yaWdodDogNSU7XG59XG4jbWFuYWdlUGVybWl0cyAjZnJlY3VlbmNpYSB7XG4gIHdpZHRoOiAzMCU7XG4gIGZsb2F0OiBsZWZ0O1xufVxuI21hbmFnZVBlcm1pdHMgI2Rlc2NyaXBjaW9uIHtcbiAgd2lkdGg6IDMwJTtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4jbWFuYWdlUGVybWl0cyAjZG93bWxvYWQge1xuICB3aWR0aDogMzAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuI21hbmFnZVBlcm1pdHMgI2NoZWNrMiB7XG4gIHdpZHRoOiAzNSU7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuI21hbmFnZVBlcm1pdHMgI2J1dHRvblJlZ2lzdCB7XG4gIHdpZHRoOiAzMCU7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy1sZWZ0OiAzMCU7XG4gIHBhZGRpbmctdG9wOiA1JTtcbn1cbiNtYW5hZ2VQZXJtaXRzICNidXR0b25DbGVhbiB7XG4gIHBhZGRpbmctbGVmdDogMTUlO1xuICB3aWR0aDogMzAlO1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmctdG9wOiA1JTtcbn1cbiNtYW5hZ2VQZXJtaXRzIHRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4jbWFuYWdlUGVybWl0cyAjYnV0dG9uQ29sb3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDBhMmNlO1xufVxuI21hbmFnZVBlcm1pdHMgI3RleHQtYXJlYS13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWhlYWRlci1jZWxsLCAubWF0LXNvcnQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwYTJjZTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2Utb3V0bGluZSAubWF0LWZvcm0tZmllbGQtb3V0bGluZSB7XG4gIGNvbG9yOiAjMDBhMmNlICFpbXBvcnRhbnQ7XG59XG5cbiNkYXRvc1BlcnNvbmFsZXMge1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItY29sb3I6ICMwMGEyY2U7XG4gIGJvcmRlci13aWR0aDogdW5zZXQ7XG59XG5cbiNidXNjYXIge1xuICB3aWR0aDogNTAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZy1sZWZ0OiAzOS44JTtcbiAgcGFkZGluZy1yaWdodDogMi41JTtcbn1cblxuLnNlYXJjaC1idXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjMDBhMmNlICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/main/apps/permits/managePermits/managePermits.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/main/apps/permits/managePermits/managePermits.component.ts ***!
    \****************************************************************************/

  /*! exports provided: ManagePermitsComponent */

  /***/
  function srcAppMainAppsPermitsManagePermitsManagePermitsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ManagePermitsComponent", function () {
      return ManagePermitsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _fuse_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @fuse/animations */
    "./src/@fuse/animations/index.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/cdk/collections */
    "./node_modules/@angular/cdk/esm2015/collections.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/paginator */
    "./node_modules/@angular/material/esm2015/paginator.js");
    /* harmony import */


    var _dialog_personalize_permits_dialog_personalize_permits_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./dialog-personalize-permits/dialog-personalize-permits.component */
    "./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.ts");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/esm2015/dialog.js");
    /* harmony import */


    var app_common_rest_services_PermitsController__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! app/common/rest/services/PermitsController */
    "./src/app/common/rest/services/PermitsController.ts");
    /* harmony import */


    var app_common_rest_services_PermitsDayController__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! app/common/rest/services/PermitsDayController */
    "./src/app/common/rest/services/PermitsDayController.ts");
    /* harmony import */


    var app_common_rest_services_EmployeeController__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! app/common/rest/services/EmployeeController */
    "./src/app/common/rest/services/EmployeeController.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ELEMENT_DATA = [{
      descripcion: 'juan',
      fecha: '11/07/2020',
      horaInicio: '09:34:00',
      horaFin: '09:34'
    }, {
      descripcion: 'kihara',
      fecha: '11/07/2020',
      horaInicio: '09:34',
      horaFin: '09:34'
    }, {
      descripcion: 'yamil',
      fecha: '11/07/2020',
      horaInicio: '09:34',
      horaFin: '09:34'
    }, {
      descripcion: 'clinsman',
      fecha: '11/07/2020',
      horaInicio: '09:34',
      horaFin: '09:34'
    }];

    var ManagePermitsComponent = /*#__PURE__*/function () {
      function ManagePermitsComponent(formBuilder, dialog, _permitsController, _permitsDayController, _employeeController, router) {
        var _this = this;

        _classCallCheck(this, ManagePermitsComponent);

        this.formBuilder = formBuilder;
        this.dialog = dialog;
        this._permitsController = _permitsController;
        this._permitsDayController = _permitsDayController;
        this._employeeController = _employeeController;
        this.router = router;
        this.uri = "";
        this.fileToUpload = "Ningún archivo seleccionado";
        this.datoMomentaneo = [];
        this.tipoPermisoFlag = false;
        this.objectSelect = [];
        this.columnas = ['check', 'descripcion', 'fecha', 'horaInicio', 'horaFinal'];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.motivos = ['Laborales', 'Personales', 'Lactancia', 'Cumpleaños', 'Estudios', 'Maternidad', 'Luto (Familiares directos)', 'Salud'];
        this.frecuencia = ['Cada semana (El dia que inicia)', 'Una vez al mes (Dia que inicia)', 'Personalizar'];
        this.tipoPermisos = ['Diario', 'Periodica'];
        this.options = [{
          employeeTo: {
            name: 'Clinsman',
            lastName: 'Campos Cruz',
            employeePk: 2,
            employeeCompleteName: 'Clinsman Campos Cruz'
          }
        }, {
          employeeTo: {
            name: 'Yamil',
            lastName: 'Rios Riquelme',
            employeePk: 3,
            employeeCompleteName: 'Yamil Rios Riquelme'
          }
        }];
        this.today = new Date().toISOString().substring(0, 10);
        this.estadoV = "";
        this.motivoV = "";
        this.horaFinalPermitDay = [];
        this.horaInicioPermitDay = [];

        this.onLoadCallback = function (event) {
          _this.uri = btoa(event.target.result.toString());
        }; // (Inicio) -- Obtener query params data


        var snapshot = router.routerState.snapshot;
        console.log(snapshot.root.queryParams.empleado); // <-- hope it helps
        // (Fin) -- Obtener query params data

        var dato = ELEMENT_DATA;
        this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](dato);
      }

      _createClass(ManagePermitsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          /*const prueba: RequestCanonical = {permitsTo: {permitsPK: 2}};
          this._permitsController.getListPermitsWithPermitsPk(prueba).subscribe(
            data => {
              console.log(data);
              
            },
            error => console.log(JSON.stringify(error))
          );*/
          // VERIFICAR SI ES SMART O ADMINISTRADOR
          var currentSession = JSON.parse(localStorage.getItem('currentSession'));
          var nombre = currentSession.username;

          this._employeeController.findByUsername(nombre).subscribe(function (data) {
            console.log(data);
            _this2.employeePk = data.employeePk;
          }, function (error) {
            return console.log(JSON.stringify(error));
          });

          var requestCanonical = {
            employeeTo: {}
          };

          this._employeeController.getListEmployee(requestCanonical).subscribe(function (data) {
            console.log(data);
            var lstEmployee = [];

            for (var index = 0; index < data.lstEmployeeTo.length; index++) {
              var element = data.lstEmployeeTo[index];
              element.employeeCompleteName = "".concat(element.name, " ").concat(element.lastName);
              var momentaneo = {};
              momentaneo.employeeTo = element;
              lstEmployee.push(momentaneo);
            }

            _this2.options = lstEmployee;
            _this2.filteredOptions = _this2.formGroup2.get('empleado').valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (value) {
              return _this2._filter(value);
            }));
          }, function (error) {
            return console.log(JSON.stringify(error));
          });

          this.datos.paginator = this.paginator;
          this.createForm();
        }
      }, {
        key: "_filter",
        value: function _filter(value) {
          var filterValue = value.toLowerCase();
          return this.options.filter(function (option) {
            return option.employeeTo.employeeCompleteName.toLowerCase().includes(filterValue);
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {}
      }, {
        key: "createForm",
        value: function createForm() {
          var tipoPermisoSelect = '';
          var tipoFrecuenciaSelected = '';
          var motivo = '';
          var empleado = '';
          this.formGroup2 = this.formBuilder.group({
            // name:this.name,
            tipoPermisoSelect: tipoPermisoSelect,
            tipoFrecuenciaSelected: tipoFrecuenciaSelected,
            motivo: motivo,
            empleado: empleado,
            diaAsistencia: this.today,
            diaAsistenciaFinal: this.today,
            comentario: '',
            horaInicial: '09:00',
            horaFinal: '09:00',
            checked: false
          });
        }
      }, {
        key: "onSelectionChange",
        value: function onSelectionChange(event) {
          console.log("Cambio el select" + event);
          this.empleadoSeleccionado = event;
        }
      }, {
        key: "isAllSelected",
        value: function isAllSelected() {
          var numSelected = this.selection.selected.length;
          var numRows = this.datos.data.length;
          return numSelected === numRows;
        }
      }, {
        key: "deselectObject",
        value: function deselectObject() {
          this.objectSelect = this.selection.selected;
          var object = [];
          this.objectSelect = object;
          this.selection.clear();
        }
      }, {
        key: "masterToggle",
        value: function masterToggle() {
          var _this3 = this;

          this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(function (row) {
            return _this3.selection.select(row);
          });
        }
      }, {
        key: "checkboxLabel",
        value: function checkboxLabel(row) {
          if (!row) {
            return "".concat(this.isAllSelected() ? 'select' : 'deselect', " all");
          }

          return "".concat(this.selection.isSelected(row) ? 'deselect' : 'select', " row ").concat(row.horaInicio + 1);
        }
      }, {
        key: "tipoPermiso",
        value: function tipoPermiso(option) {
          if (option == 'Diario') {
            this.tipoPermisoFlag = true;
          } else if (option == 'Periodica') {
            this.tipoPermisoFlag = false;
          }
        }
      }, {
        key: "getDates",
        value: function getDates() {
          if (this.tipoPermisoSelect == 'Diario') {
            var today = this.diaAsistencia;
            var endDate = this.diaAsistenciaFinal; // const today = new Date();
            // const endDate = new Date(today);
            // endDate.setDate( endDate.getDate() + 3);
            // ========================================

            var dates = []; // to avoid modifying the original date

            var theDate = new Date(today);

            while (theDate < endDate) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
              theDate.setDate(theDate.getDate() + 1);
            }

            dates = [].concat(_toConsumableArray(dates), [endDate]);
            console.log(dates);
            this.datoMomentaneo = [];

            for (var index = 0; index < dates.length; index++) {
              var element = {
                descripcion: '',
                fecha: '',
                horaInicio: '',
                horaFin: ''
              };
              element.descripcion = this.comentario;
              element.fecha = dates[index];
              element.horaInicio = this.horaInicial;
              element.horaFin = this.horaFinal;
              this.horaFinalPermitDay[index] = this.horaFinal;
              this.horaInicioPermitDay[index] = this.horaInicial;
              console.log(element);
              this.datoMomentaneo.push(element);
            }

            this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
            this.datos.paginator = this.paginator;
          } else if (this.tipoPermisoSelect == 'Periodica') {
            this.tipoFrecuencia(this.tipoFrecuenciaSelected);
          } // return dates;

        }
      }, {
        key: "tipoFrecuencia",
        value: function tipoFrecuencia(frecuencia) {
          if (frecuencia == 'Cada semana (El dia que inicia)') {
            // ==============================
            var today = this.diaAsistencia;
            var endDate = this.diaAsistenciaFinal;
            this.datoMomentaneo = [];
            this.tipoFrecuenciaSemana(today, endDate);
          }

          if (frecuencia == 'Una vez al mes (Dia que inicia)') {
            this.datoMomentaneo = [];
            this.tipoFrecuenciaMes(this.diaAsistencia, this.diaAsistenciaFinal);
          }

          if (frecuencia == 'Personalizar') {
            // Dialogo
            this.openDialog('agregar', 'prueba');
          }
        }
      }, {
        key: "tipoFrecuenciaSemana",
        value: function tipoFrecuenciaSemana(diaPermisoInicial, diaPermisoFinal) {
          var fechaFinalPeriodica = new Date(diaPermisoInicial);
          console.log(fechaFinalPeriodica.getMonth());
          fechaFinalPeriodica = new Date(fechaFinalPeriodica.setMonth(fechaFinalPeriodica.getMonth() + 2));

          while (diaPermisoInicial < fechaFinalPeriodica) {
            var dates = []; // to avoid modifying the original date

            var theDate = new Date(diaPermisoInicial);

            while (theDate < diaPermisoFinal) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
              theDate.setDate(theDate.getDate() + 1);
            }

            dates = [].concat(_toConsumableArray(dates), [diaPermisoFinal]);
            console.log(dates);

            for (var index = 0; index < dates.length; index++) {
              var element = {
                descripcion: '',
                fecha: '',
                horaInicio: '',
                horaFin: ''
              };
              element.descripcion = this.comentario;
              element.fecha = dates[index];
              element.horaInicio = this.horaInicial;
              element.horaFin = this.horaFinal;
              this.horaFinalPermitDay[index] = this.horaFinal;
              this.horaInicioPermitDay[index] = this.horaInicial;
              console.log(element);
              this.datoMomentaneo.push(element);
            }

            var fechaNueva = new Date(diaPermisoInicial);
            fechaNueva.setDate(fechaNueva.getDate() + (7 - fechaNueva.getDay()) % 7 + fechaNueva.getDay());
            console.log(fechaNueva); // const fechaTransitoria = new Date(fechaNueva);

            var fechaNuevaFinal = new Date(fechaNueva);
            fechaNuevaFinal.setDate(fechaNueva.getDate() - 1 + dates.length);
            console.log(fechaNuevaFinal);
            diaPermisoInicial = new Date(fechaNueva);
            diaPermisoFinal = new Date(fechaNuevaFinal); // this.tipoFrecuenciaSemana(fechaNueva, fechaNuevaFinal);
          }

          this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
          this.datos.paginator = this.paginator;
        }
      }, {
        key: "tipoFrecuenciaMes",
        value: function tipoFrecuenciaMes(diaPermisoInicial, diaPermisoFinal) {
          var fechaFinalPeriodica = new Date(diaPermisoInicial);
          var arrayFechas = this.posicionDiaPorMes(fechaFinalPeriodica.getDay(), fechaFinalPeriodica);
          var posicionDiaEnMes = this.numeroDeDiaEnMes(arrayFechas, fechaFinalPeriodica);
          var diaSemana = fechaFinalPeriodica.getDay();
          fechaFinalPeriodica = new Date(fechaFinalPeriodica.setMonth(fechaFinalPeriodica.getMonth() + 6));

          while (diaPermisoInicial < fechaFinalPeriodica) {
            // Obtener el intervalo
            var dates = []; // to avoid modifying the original date

            var theDate = new Date(diaPermisoInicial);

            while (theDate < diaPermisoFinal) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
              theDate.setDate(theDate.getDate() + 1);
            }

            dates = [].concat(_toConsumableArray(dates), [diaPermisoFinal]); // Guarda datos momentaneos para la tabla

            for (var index = 0; index < dates.length; index++) {
              var element = {
                descripcion: '',
                fecha: '',
                horaInicio: '',
                horaFin: ''
              };
              element.descripcion = this.comentario;
              element.fecha = dates[index];
              element.horaInicio = this.horaInicial;
              element.horaFin = this.horaFinal;
              this.horaFinalPermitDay[index] = this.horaFinal;
              this.horaInicioPermitDay[index] = this.horaInicial;
              console.log(element);
              this.datoMomentaneo.push(element);
            } // Definir nuevas fechas para siguiente iteracion


            var fechaNueva = new Date(diaPermisoInicial);
            fechaNueva.setMonth(fechaNueva.getMonth() + 1);
            fechaNueva.setDate(1);
            var arrayFechasDos = this.posicionDiaPorMes(diaSemana, fechaNueva);
            fechaNueva = new Date(arrayFechasDos[posicionDiaEnMes]);
            var fechaNuevaFinal = new Date(fechaNueva);
            fechaNuevaFinal.setDate(fechaNueva.getDate() - 1 + dates.length);
            console.log(fechaNuevaFinal);
            diaPermisoInicial = new Date(fechaNueva);
            diaPermisoFinal = new Date(fechaNuevaFinal);
          }

          this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
          this.datos.paginator = this.paginator;
        }
      }, {
        key: "posicionDiaPorMes",
        value: function posicionDiaPorMes(numberDay, fechaEvaluar) {
          var d = new Date(fechaEvaluar),
              month = d.getMonth(),
              mondays = [];
          d.setDate(1); // Get the first Monday in the month

          while (d.getDay() !== numberDay) {
            d.setDate(d.getDate() + 1);
          } // Get all the other Mondays in the month


          while (d.getMonth() === month) {
            mondays.push(new Date(d.getTime()));
            d.setDate(d.getDate() + 7);
          }

          return mondays;
        }
      }, {
        key: "numeroDeDiaEnMes",
        value: function numeroDeDiaEnMes(arrayFechas, fechaBusqueda) {
          var numero = 0;

          for (var index = 0; index < arrayFechas.length; index++) {
            var element = arrayFechas[index];

            if (fechaBusqueda.getTime() === element.getTime()) {
              numero = index;
            }
          }

          return numero;
        }
      }, {
        key: "registrarPermisos",
        value: function registrarPermisos() {
          this.datoMomentaneo = [];
        }
      }, {
        key: "openDialog",
        value: function openDialog(operation, description_name) {
          var _this4 = this;

          // =====================================
          var dialogRef = this.dialog.open(_dialog_personalize_permits_dialog_personalize_permits_component__WEBPACK_IMPORTED_MODULE_8__["DialogPersonalizePermitsComponent"], {
            // width: '250px',
            data: {
              name: operation,
              animal: description_name
            },
            height: '550px',
            width: '600px',
            panelClass: 'my-panel',
            disableClose: true
          });
          dialogRef.afterClosed().subscribe(function (result) {
            if (result.data.opcionTermino == 1) {
              _this4.datoMomentaneo = [];

              _this4.normal(result.data);
            } else if (result.data.opcionTermino == 2) {
              _this4.datoMomentaneo = [];

              _this4.fechaLimite(result.data);
            } else if (result.data.opcionTermino == 3) {
              _this4.datoMomentaneo = [];

              _this4.iteraciones(result.data);
            }
          });
        }
      }, {
        key: "normal",
        value: function normal(data) {
          var diasRepeticion = data.diasRepeticion;
          var diaAsistenciaFinal = new Date(this.diaAsistencia);
          diaAsistenciaFinal.setMonth(diaAsistenciaFinal.getMonth() + 6);
          var today = this.diaAsistencia;
          var endDate = diaAsistenciaFinal;
          var dates = []; // to avoid modifying the original date

          var theDate = new Date(today);

          while (theDate < endDate) {
            if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
            }

            theDate.setDate(theDate.getDate() + 1);
          }

          dates = [].concat(_toConsumableArray(dates), [endDate]);
          console.log(dates);
          this.datoMomentaneo = [];

          for (var index = 0; index < dates.length; index++) {
            var element = {
              descripcion: '',
              fecha: '',
              horaInicio: '',
              horaFin: ''
            };
            element.descripcion = this.comentario;
            element.fecha = dates[index];
            element.horaInicio = this.horaInicial;
            element.horaFin = this.horaFinal;
            this.horaFinalPermitDay[index] = this.horaFinal;
            this.horaInicioPermitDay[index] = this.horaInicial;
            console.log(element);
            this.datoMomentaneo.push(element);
          }

          this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
          this.datos.paginator = this.paginator;
        }
      }, {
        key: "fechaLimite",
        value: function fechaLimite(data) {
          var diasRepeticion = data.diasRepeticion;
          var diaAsistenciaFinal = new Date(data.fechaTermino);
          var today = this.diaAsistencia;
          var endDate = diaAsistenciaFinal;
          var dates = []; // to avoid modifying the original date

          var theDate = new Date(today);

          while (theDate < endDate) {
            if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
            }

            theDate.setDate(theDate.getDate() + 1);
          }

          dates = [].concat(_toConsumableArray(dates), [endDate]);
          console.log(dates);
          this.datoMomentaneo = [];

          for (var index = 0; index < dates.length; index++) {
            var element = {
              descripcion: '',
              fecha: '',
              horaInicio: '',
              horaFin: ''
            };
            element.descripcion = this.comentario;
            element.fecha = dates[index];
            element.horaInicio = this.horaInicial;
            element.horaFin = this.horaFinal;
            this.horaFinalPermitDay[index] = this.horaFinal;
            this.horaInicioPermitDay[index] = this.horaInicial;
            console.log(element);
            this.datoMomentaneo.push(element);
          }

          this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
          this.datos.paginator = this.paginator;
        }
      }, {
        key: "iteraciones",
        value: function iteraciones(data) {
          var iteraciones = data.numeroIteraciones;
          var diasRepeticion = data.diasRepeticion;
          var diaAsistenciaFinal = new Date(this.diaAsistencia);
          diaAsistenciaFinal.setDate(diaAsistenciaFinal.getDate() + 7 * iteraciones);
          var today = this.diaAsistencia;
          var endDate = diaAsistenciaFinal;
          var dates = []; // to avoid modifying the original date

          var theDate = new Date(today);

          while (theDate < endDate) {
            if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
              dates = [].concat(_toConsumableArray(dates), [new Date(theDate)]);
            }

            theDate.setDate(theDate.getDate() + 1);
          }

          dates = [].concat(_toConsumableArray(dates), [endDate]);
          console.log(dates);
          this.datoMomentaneo = [];

          for (var index = 0; index < dates.length; index++) {
            var element = {
              descripcion: '',
              fecha: '',
              horaInicio: '',
              horaFin: ''
            };
            element.descripcion = this.comentario;
            element.fecha = dates[index];
            element.horaInicio = this.horaInicial;
            element.horaFin = this.horaFinal;
            this.horaFinalPermitDay[index] = this.horaFinal;
            this.horaInicioPermitDay[index] = this.horaInicial;
            console.log(element);
            this.datoMomentaneo.push(element);
          }

          this.datos = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.datoMomentaneo);
          this.datos.paginator = this.paginator;
        }
      }, {
        key: "registrar",
        value: function registrar() {
          var _this5 = this;

          var requestCanonical = {
            permitsTo: {}
          };
          requestCanonical.permitsTo.employeeFk = this.empleadoSeleccionado; // 4 antes ahora probando el get de colaborador

          requestCanonical.permitsTo.affectsHours = this.checked; // afecta horas

          requestCanonical.permitsTo.name = this.empleado;
          requestCanonical.permitsTo.description = this.comentario;
          requestCanonical.permitsTo.reason = this.motivo;
          requestCanonical.permitsTo.startDate = this.diaAsistencia;
          requestCanonical.permitsTo.endDate = this.diaAsistenciaFinal;
          requestCanonical.permitsTo.support = this.uri;
          console.log(requestCanonical);

          this._permitsController.createPermits(requestCanonical).subscribe(function (data) {
            console.log(data); // Registro de dias

            var requestCanonicalDays = {
              permitsDayTo: {},
              lstPermitsDay: []
            };

            for (var index = 0; index < _this5.datos.data.length; index++) {
              var element = _this5.datos.data[index];
              requestCanonicalDays.lstPermitsDay.push(_this5.llenadoPermitsDay(element, data.id, index));
            }

            console.log(requestCanonicalDays); // For para llenado de permisos a request

            _this5._permitsDayController.createPermitsDay(requestCanonicalDays).subscribe(function (dataDos) {
              console.log(dataDos);
            }, function (error) {
              return console.log(JSON.stringify(error));
            });
          }, function (error) {
            return console.log(JSON.stringify(error));
          });
        }
      }, {
        key: "llenadoPermitsDay",
        value: function llenadoPermitsDay(element, id, index) {
          var permitsDayToPrueba;
          var requestCanonicalDays = {
            permitsDayTo: {},
            lstPermitsDay: []
          };
          requestCanonicalDays.permitsDayTo.description = element.descripcion;
          var fechaMomentanea = new Date(element.fecha);
          requestCanonicalDays.permitsDayTo.permitsDayDate = fechaMomentanea;
          requestCanonicalDays.permitsDayTo.permitsDayEntryTime = new Date(this.permitsDayEntryTime(fechaMomentanea, index));
          requestCanonicalDays.permitsDayTo.permitsDayDepatureTime = new Date(this.permitsDayDepatureTime(fechaMomentanea, index));
          requestCanonicalDays.permitsDayTo.permitsPk = id;
          permitsDayToPrueba = requestCanonicalDays.permitsDayTo;
          return permitsDayToPrueba;
        }
      }, {
        key: "permitsDayDepatureTime",
        value: function permitsDayDepatureTime(fechaMomentanea, index) {
          var requestCanonicalDays = {
            permitsDayTo: {},
            lstPermitsDay: []
          };
          var horasminutos = this.horaFinalPermitDay[index].split(':');
          var departureTime;
          requestCanonicalDays.permitsDayTo.permitsDayDepatureTime = new Date(fechaMomentanea);
          requestCanonicalDays.permitsDayTo.permitsDayDepatureTime.setHours(Number(horasminutos[0]));
          requestCanonicalDays.permitsDayTo.permitsDayDepatureTime.setMinutes(Number(horasminutos[1]));
          departureTime = new Date(requestCanonicalDays.permitsDayTo.permitsDayDepatureTime);
          return departureTime;
        }
      }, {
        key: "permitsDayEntryTime",
        value: function permitsDayEntryTime(fechaMomentanea, index) {
          var requestCanonicalDays = {
            permitsDayTo: {},
            lstPermitsDay: []
          };
          var horasminutos = this.horaInicioPermitDay[index].split(':');
          var entryTime;
          requestCanonicalDays.permitsDayTo.permitsDayEntryTime = new Date(fechaMomentanea);
          requestCanonicalDays.permitsDayTo.permitsDayEntryTime.setHours(Number(horasminutos[0]));
          requestCanonicalDays.permitsDayTo.permitsDayEntryTime.setMinutes(Number(horasminutos[1]));
          entryTime = new Date(requestCanonicalDays.permitsDayTo.permitsDayEntryTime);
          return entryTime;
        }
      }, {
        key: "handleFileInput",
        value: function handleFileInput(files) {
          if (files != null) {
            var reader = new FileReader();
            var evt = files.item(0);
            this.file = files.item(0).name;
            reader.onload = this.onLoadCallback.bind(this);
            reader.readAsBinaryString(evt);
          }
        }
      }, {
        key: "diaAsistencia",
        get: function get() {
          return this.formGroup2.get('diaAsistencia').value;
        }
      }, {
        key: "diaAsistenciaFinal",
        get: function get() {
          return this.formGroup2.get('diaAsistenciaFinal').value;
        }
      }, {
        key: "empleado",
        get: function get() {
          return this.formGroup2.get('empleado').value;
        }
      }, {
        key: "tipoPermisoSelect",
        get: function get() {
          return this.formGroup2.get('tipoPermisoSelect').value;
        }
      }, {
        key: "motivo",
        get: function get() {
          return this.formGroup2.get('motivo').value;
        }
      }, {
        key: "comentario",
        get: function get() {
          return this.formGroup2.get('comentario').value;
        }
      }, {
        key: "tipoFrecuenciaSelected",
        get: function get() {
          return this.formGroup2.get('tipoFrecuenciaSelected').value;
        }
      }, {
        key: "horaInicial",
        get: function get() {
          return this.formGroup2.get('horaInicial').value;
        }
      }, {
        key: "horaFinal",
        get: function get() {
          return this.formGroup2.get('horaFinal').value;
        }
      }, {
        key: "checked",
        get: function get() {
          return this.formGroup2.get('checked').value;
        }
      }]);

      return ManagePermitsComponent;
    }();

    ManagePermitsComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
      }, {
        type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialog"]
      }, {
        type: app_common_rest_services_PermitsController__WEBPACK_IMPORTED_MODULE_10__["PermitsController"]
      }, {
        type: app_common_rest_services_PermitsDayController__WEBPACK_IMPORTED_MODULE_11__["PermitsDayController"]
      }, {
        type: app_common_rest_services_EmployeeController__WEBPACK_IMPORTED_MODULE_12__["EmployeeController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__["MatPaginator"], {
      "static": true
    })], ManagePermitsComponent.prototype, "paginator", void 0);
    ManagePermitsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'managePermits',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./managePermits.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/main/apps/permits/managePermits/managePermits.component.html"))["default"],
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      animations: _fuse_animations__WEBPACK_IMPORTED_MODULE_2__["fuseAnimations"],
      providers: [{
        provide: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MAT_CHECKBOX_CLICK_ACTION"],
        useValue: 'check'
      }],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./managePermits.component.scss */
      "./src/app/main/apps/permits/managePermits/managePermits.component.scss"))["default"]]
    })], ManagePermitsComponent);
    /***/
  },

  /***/
  "./src/app/main/apps/permits/managePermits/managePermits.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/main/apps/permits/managePermits/managePermits.module.ts ***!
    \*************************************************************************/

  /*! exports provided: ManagePermitsModule */

  /***/
  function srcAppMainAppsPermitsManagePermitsManagePermitsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ManagePermitsModule", function () {
      return ManagePermitsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/esm2015/button.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/esm2015/checkbox.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/esm2015/form-field.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/esm2015/icon.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/esm2015/input.js");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/esm2015/card.js");
    /* harmony import */


    var _angular_material_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/select */
    "./node_modules/@angular/material/esm2015/select.js");
    /* harmony import */


    var _fuse_shared_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @fuse/shared.module */
    "./src/@fuse/shared.module.ts");
    /* harmony import */


    var _fuse_components__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @fuse/components */
    "./src/@fuse/components/index.ts");
    /* harmony import */


    var ng2_charts__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ng2-charts */
    "./node_modules/ng2-charts/fesm2015/ng2-charts.js");
    /* harmony import */


    var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @swimlane/ngx-charts */
    "./node_modules/@swimlane/ngx-charts/release/esm.js");
    /* harmony import */


    var _agm_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @agm/core */
    "./node_modules/@agm/core/index.js");
    /* harmony import */


    var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/tabs */
    "./node_modules/@angular/material/esm2015/tabs.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/material/menu */
    "./node_modules/@angular/material/esm2015/menu.js");
    /* harmony import */


    var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! @angular/material/datepicker */
    "./node_modules/@angular/material/esm2015/datepicker.js");
    /* harmony import */


    var _angular_material_table__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/material/table */
    "./node_modules/@angular/material/esm2015/table.js");
    /* harmony import */


    var _managePermits_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./managePermits.component */
    "./src/app/main/apps/permits/managePermits/managePermits.component.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! @angular/material/autocomplete */
    "./node_modules/@angular/material/esm2015/autocomplete.js");
    /* harmony import */


    var _dialog_personalize_permits_dialog_personalize_permits_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ./dialog-personalize-permits/dialog-personalize-permits.component */
    "./src/app/main/apps/permits/managePermits/dialog-personalize-permits/dialog-personalize-permits.component.ts");
    /* harmony import */


    var _angular_material_chips__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! @angular/material/chips */
    "./node_modules/@angular/material/esm2015/chips.js");
    /* harmony import */


    var _angular_material_radio__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! @angular/material/radio */
    "./node_modules/@angular/material/esm2015/radio.js");

    var routes = [{
      path: '**',
      component: _managePermits_component__WEBPACK_IMPORTED_MODULE_20__["ManagePermitsComponent"]
    }];

    var ManagePermitsModule = function ManagePermitsModule() {
      _classCallCheck(this, ManagePermitsModule);
    };

    ManagePermitsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_managePermits_component__WEBPACK_IMPORTED_MODULE_20__["ManagePermitsComponent"], _dialog_personalize_permits_dialog_personalize_permits_component__WEBPACK_IMPORTED_MODULE_23__["DialogPersonalizePermitsComponent"]],
      entryComponents: [_dialog_personalize_permits_dialog_personalize_permits_component__WEBPACK_IMPORTED_MODULE_23__["DialogPersonalizePermitsComponent"]],
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes), _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_9__["MatSelectModule"], _fuse_shared_module__WEBPACK_IMPORTED_MODULE_10__["FuseSharedModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__["MatMenuModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_9__["MatSelectModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_15__["MatTabsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_19__["MatTableModule"], _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({
        apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
      }), ng2_charts__WEBPACK_IMPORTED_MODULE_12__["ChartsModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_13__["NgxChartsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_21__["MatPaginatorModule"], _fuse_shared_module__WEBPACK_IMPORTED_MODULE_10__["FuseSharedModule"], _fuse_components__WEBPACK_IMPORTED_MODULE_11__["FuseWidgetModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_22__["MatAutocompleteModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_24__["MatChipsModule"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_25__["MatRadioModule"]]
    })], ManagePermitsModule);
    /***/
  }
}]);
//# sourceMappingURL=permits-managePermits-managePermits-module-es5.js.map