import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, timer, Subject } from 'rxjs';
import { TokenBean } from './token-bean';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OAuthService {

  private _servidor: string;
  private controller: string;
  public tokenBean: TokenBean;
  private _logeado: boolean;
  private timerClock: Observable<number>;
  userB: any = {};

  constructor(private httpClient: HttpClient, private router: Router) {
    this._servidor = 'http://localhost:8080';
    this.tokenBean = {} as TokenBean;
    this.controller = '/oauth';
    this.tokenBean = JSON.parse(localStorage.getItem('user-id'));
   // this._logeado= false;
  }

  get logueado() { return this._logeado }
  expira: number;
  eventTimer(e: number) {
    let token;
        if(localStorage.getItem('x-access-token')!=null){
            token = localStorage.getItem('x-access-token').replace(/\\/g,'\\');
          }
    if (this._logeado == true && token != null && token != undefined) {
      this.expira--;
      //console.log("Expira en " + this.expira);
      if (this.expira < 20) {
        this.expira = 40;
        this.refreshToken();
      }
    }
  }


  login(usuario: string, clave: string, dataBase: string): Observable<boolean> {
    let loginSub: Subject<boolean> = new Subject();
    let metodo = this.servidor + this.controller;
    metodo += '/token?grant_type=password&username=' + usuario + '@@' + clave + '@@369@@' + dataBase + '&password=' + clave;
    const body = new HttpParams();

    this.httpClient.post(metodo, body, {
      headers:
        new HttpHeaders({
          'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
        }), withCredentials: true
    }).subscribe(
      res => {
       // console.log(res,'TOKEN ORIGINAL');
        this.tokenBean = res as TokenBean;
        if (this.tokenBean.access_token.length === 36) {
         // console.log('Access Token ', this.tokenBean.access_token);
         // console.log('Refresh Token ', this.tokenBean.refresh_token);
          localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
          localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
          localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
         // console.log(this.tokenBean.access_token);
          this.expira = this.tokenBean.expires_in;
          this._logeado = true;
         // console.log(this._logeado);
          loginSub.next(true);
        } else {
          this._logeado = false;
          loginSub.next(false);
        }
      },
      error => {
        console.log('Error', error);
        this._logeado = false;
        loginSub.next(false);
      });

    return loginSub.asObservable();
  }
  public get servidor(): string {
    return this._servidor;
  }

  public get token(): string {
    return this.tokenBean.access_token;
  }

  getRefreshToken() {
    return localStorage.getItem('x-refresh-token').replace(/\\/g,'\\');
  }

  getAccessToken() {
    return localStorage.getItem('x-access-token').replace(/\\/g,'\\');
  }

  public refreshToken() {
    let token;
          if(localStorage.getItem('x-access-token')!=null){
            token = localStorage.getItem('x-access-token').replace(/\\/g,'\\');
          }

    if (token != null && token != undefined) {
      console.log('ejecuta proceso.. refreesh',this._logeado);
      let metodo = this.servidor + this.controller;
      metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
      const body = new HttpParams();
      //console.log(metodo);
      this.httpClient.post(metodo, body, {
        headers:
          new HttpHeaders({
            'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
          }), withCredentials: true
      })
        .subscribe(
          res => {
           // console.log('ACA ESTA EL RESTTTTT', res);
            this.tokenBean = res as TokenBean;
            if (this.tokenBean.access_token != null && this.tokenBean.access_token.length === 36) {
             // console.log('Refresh => Access Token ', this.tokenBean.access_token);
             // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
              this._logeado = true;
              localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
              localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
              localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
              this.setAccessToken(this.tokenBean.access_token);

              //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
              //  console.log(new Date(this.tokenBean.expires_in));
              this.expira = this.tokenBean.expires_in;
            }
          }, error => {
            console.log('Error', error);
            this._logeado = false;
          });
    }
  }

  public getNewAccessToken(): Observable<boolean>  {
    this.expira = 40;
    let refreshSub: Subject<boolean> = new Subject();
   // console.log('EJECUTANDO REFRESH',this._logeado);
    let metodo = this.servidor + this.controller;
    metodo += '/token?grant_type=refresh_token&refresh_token=' + this.tokenBean.refresh_token;
    const body = new HttpParams();
    this.httpClient.post(metodo, body, {
      headers:
        new HttpHeaders({
          'Authorization': 'Basic ' + btoa('my-trusted-client:250123')
        }), withCredentials: true
    })
      .subscribe(
        res => {
        //  console.log('ACA ESTA EL RESTTTTT', res);
          this.tokenBean = res as TokenBean;
          if (this.tokenBean.access_token != null && this.tokenBean.access_token.length === 36) {
           // console.log('Refresh => Access Token ', this.tokenBean.access_token);
           // console.log('Refresh => Refresh Token ', this.tokenBean.refresh_token);
            this._logeado = true;
            localStorage.setItem('x-access-token', JSON.stringify(this.tokenBean.access_token));
            localStorage.setItem('x-refresh-token', JSON.stringify(this.tokenBean.refresh_token));
            localStorage.setItem('user-id', JSON.stringify(this.tokenBean));
            this.setAccessToken(this.tokenBean.access_token);
            refreshSub.next(true);
            //   this.tokenBean = JSON.parse(localStorage.getItem('tokenAccess'));
            //  console.log(new Date(this.tokenBean.expires_in));
            this.expira = this.tokenBean.expires_in;
          } else {
            refreshSub.next(false);
          }
        }, error => {
          console.log('Error', error);
          this._logeado = false;
          refreshSub.next(false);
        });

    return refreshSub.asObservable();

  }

  setAccessToken(accessToken: string) {
    localStorage.setItem('x-access-token', accessToken)
  }


  logout() {
    this.removeSession();
    this._logeado = false;
    this.router.navigateByUrl('/pages/auth/login');
  }

  private removeSession() {
    localStorage.removeItem('user-id');
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-refresh-token');
    this.tokenBean = {} as TokenBean;
  }

  userValid(): boolean {
    if(!(this.tokenBean.expires_in<1)){
      return true;
    }else{
      return false;
    }
  }

}