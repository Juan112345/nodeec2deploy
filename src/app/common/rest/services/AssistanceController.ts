import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class AssistanceController   {
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/assistanceResource';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    // Date.prototype.toJSON = function () { return moment(this).format(); };
  }

    getListAssistance(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListAssistance';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    createAssistance(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/createAssistance';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    updateAssistance(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/updateAssistance';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }
  
    getLastAssistance(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/getLastAssistance';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }

  getLastRegisteredAssistance(arg0: beans.RequestCanonical): Observable<any> {
    const metodo = this.url + '/getLastRegisteredAssistance';
    // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
    console.log(JSON.stringify(arg0));
    
    return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }

  }