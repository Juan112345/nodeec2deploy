import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
    providedIn: 'root'
})
export class FilesController {
    private url: string;
    private header: HttpHeaders;
    constructor(private httpClient: HttpClient, private oauth: OAuthService) {
        this.url = oauth.servidor + '/filesResource';
        this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
        //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
    }

    getListFiles(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListFiles';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));

        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }

    createFiles(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/createFiles';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));

        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }

    updateFiles(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/updateFiles';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));

        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }

    getListFilesEmployee(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListFilesEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));

        return this.httpClient.post(metodo, JSON.stringify(arg0), { headers: this.header });
    }

}