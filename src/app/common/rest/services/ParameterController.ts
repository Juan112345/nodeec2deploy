import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class ParameterController   {
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/parametersResource';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
  }

    getListMaster(arg0: beans.RequestCanonical): Observable<any> {
    const metodo = this.url + '/getListMaster';
    // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
    console.log(JSON.stringify(arg0));
    
    return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }

    getListParameters(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/getListParameters';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
   }
    createParameter(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/createParameter';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }
    updateParameter(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/updateParameter';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }


  }
  