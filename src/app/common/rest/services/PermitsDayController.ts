import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class PermitsDayController   {
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/permitsDayResource';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    //Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
  }

  getListPermitsDay(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListPermitsDay';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    createPermitsDay(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/createPermitsDay';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    updatePermitsDay(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/updatePermitsDay';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }

}