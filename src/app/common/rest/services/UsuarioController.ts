import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class UsuarioController   {
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
  }

    /*login(arg0: beans.UserBean): Observable<any> {
    const metodo = this.url + '/';
    return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      */
  }