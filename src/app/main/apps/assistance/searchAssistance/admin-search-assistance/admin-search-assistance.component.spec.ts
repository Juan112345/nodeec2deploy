import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSearchAssistanceComponent } from './admin-search-assistance.component';

describe('AdminSearchAssistanceComponent', () => {
  let component: AdminSearchAssistanceComponent;
  let fixture: ComponentFixture<AdminSearchAssistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSearchAssistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSearchAssistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
