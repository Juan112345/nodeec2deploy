import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, AfterViewInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { SelectionModel } from '@angular/cdk/collections';

import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { AssistanceController } from 'app/common/rest/services/AssistanceController';
import { AssistanceTo, RequestCanonical } from 'app/common/rest/services/Beans';

export interface PeriodicElement {
  position: string;
  dia: Date;
  empleado: string;
  hora_ingreso: string;
  hora_salida: string;
  horas_trabajadas: any;
  disabled: any;
}
  

const ELEMENT_DATA: PeriodicElement[] = [
    {position:'1', dia: new Date('10/27/2020'), empleado:'Ada Lovelace', hora_ingreso: '09:34',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
    {position:'2', dia: new Date('10/30/2020'), empleado:'Grace Hopper', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
    {position:'3', dia: new Date('10/26/2020'), empleado:'Margaret Hamilton', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
      {position:'4', dia: new Date('10/18/2020'), empleado:'Joan Clarke', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
      {position:'5', dia: new Date('10/7/2020'), empleado:'Ada Lovelace', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
      {position:'6', dia: new Date('10/12/2020'), empleado:'Juan Balboa', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
      {position:'7', dia: new Date('10/12/2020'), empleado:'Margaret Hamilton', hora_ingreso: '21:36',hora_salida: '21:36',  horas_trabajadas: '8h 45min', disabled: true},
      {position:'8', dia: new Date('10/12/2020'), empleado:'Joan Clarke', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'9', dia: new Date('10/12/2020'), empleado:'Ada Lovelace', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'10', dia: new Date('10/14/2020'), empleado:'Grace Hopper', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'11', dia: new Date('10/13/2020'), empleado:'Margaret Hamilton', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'12', dia: new Date('10/11/2020'), empleado:'Juan Balboa', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'13', dia: new Date('10/18/2020'), empleado:'Ada Lovelace', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'14', dia: new Date('10/12/2020'), empleado:'Grace Hopper', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'15', dia: new Date('10/7/2020'), empleado:'Margaret Hamilton', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true},
      {position:'16', dia: new Date('10/2/2020'), empleado:'Joan Clarke', hora_ingreso: '21:36',hora_salida: '21:36', horas_trabajadas: '8h 45min', disabled: true}
  ];

@Component({
    selector     : 'contacts-contact-list',
    templateUrl  : './search.component.html',
    styleUrls    : ['./search.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SearchAssistanceComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void{
  }

  lstAsistencias: AssistanceTo[] = [];
    // TipoEmpleado puede ser admin o empleado para la vista correspondiente
  tipoEmpleado = 'admin';
  selection = new SelectionModel<PeriodicElement>(true, []);

  today = new Date().toISOString().substring(0, 10);

  // Filas reabiertas
  filasReabiertas: any = [];


  formGroup = new FormGroup({
    empleado: new FormControl(),
    diaAsistencia:  new FormControl(),
    diaAsistenciaFinal:  new FormControl(),
  });

  options: string[] = ['Clinsman Campos', 'Juan Balboa', 'Cris Martinez', 'Clinsman Campos Cruz', 'Juan Cruz Nomberto', 'Reynaldo', 'Silvia Jurado'];
  filteredOptions: Observable<string[]>;

  get diaAsistencia() { return this.formGroup.get('diaAsistencia').value; }
  get diaAsistenciaFinal() { return this.formGroup.get('diaAsistenciaFinal').value; }
  get empleado() { return this.formGroup.get('empleado').value; }

  columnas: string[] = ['position', 'dia', 'empleado', 'hora_ingreso', 'hora_salida', 'horas_trabajadas', 'select'];
  datos = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  input_data: any;
  input_data_final: any;
  pipe: DatePipe;

  flagModificar = false;
  flagReabrir = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  
  @ViewChild('searchInput', {static: false}) searchInputField;

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.datos.data.length;
      return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.datos.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
      if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  constructor(private formBuilder: FormBuilder, private _assistanceController: AssistanceController) { 
    this.pipe = new DatePipe('en');
    this.datos.filterPredicate = (data, filter) =>{
      if (this.diaAsistencia && this.diaAsistenciaFinal && this.empleado){
        return data.dia >= this.diaAsistencia && data.dia <= this.diaAsistenciaFinal && data.empleado == this.empleado;
      }else if (this.diaAsistencia && this.diaAsistenciaFinal) {
        return data.dia >= this.diaAsistencia && data.dia <= this.diaAsistenciaFinal;
      }else if (this.empleado){
        return data.empleado == this.empleado;
      }
      return true;
    };
  }

  ngOnInit(): void {
    const LISTA_ASISTENCIAS: PeriodicElement[] = [];
    const requestCanonical: RequestCanonical = {};
    this._assistanceController.getListAssistance(requestCanonical).subscribe(
      data => {
          console.log(data.lstAssistance);
          this.lstAsistencias = data.lstAssistance;
          for (let index = 0; index < this.lstAsistencias.length; index++) {
            let asistenciaElement: any = {};
            const element = this.lstAsistencias[index];
            element.entryTime = new Date(element.entryTime);
            element.depatureTime = new Date(element.depatureTime);
            element.assistanceDay = new Date(element.assistanceDay);
            asistenciaElement.position = `${element.assistancePK}`; 
            asistenciaElement.dia = element.assistanceDay;
            asistenciaElement.empleado = `${element.employeeName} ${element.employeeLastName}`; 
            let horaEntrada = '';
            let minutosEntrada = '';
            // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
            if (element.entryTime.getHours() < 10){
              horaEntrada = `0${element.entryTime.getHours()}`;
            }else {
              horaEntrada = `${element.entryTime.getHours()}`;
            }
            // Minutos y Segundos
            if (element.entryTime.getMinutes() < 10){
              minutosEntrada = `0${element.entryTime.getMinutes()}`; 
            }else {
              minutosEntrada = `${element.entryTime.getMinutes()}`;
            }
            
            asistenciaElement.hora_ingreso = `${horaEntrada}:${minutosEntrada}`;
            
            let horaSalida = '';
            let minutosSalida = '';
            // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
            if (element.depatureTime.getHours() < 10){
              horaSalida = `0${element.depatureTime.getHours()}`;
            }else {
              horaSalida = `${element.depatureTime.getHours()}`;
            }
            // Minutos y Segundos
            if (element.depatureTime.getMinutes() < 10){
              minutosSalida = `0${element.depatureTime.getMinutes()}`; 
            }else {
              minutosSalida = `${element.depatureTime.getMinutes()}`;
            }
            
            asistenciaElement.hora_salida = `${horaSalida}:${minutosSalida}`;
            
            asistenciaElement.horas_trabajadas = '8h 45min'; 
            if (element.state == 0) {
              asistenciaElement.disabled = true;
            }
            LISTA_ASISTENCIAS.push(asistenciaElement);
          }

          this.datos.data =  LISTA_ASISTENCIAS;
      },
      error => console.log(JSON.stringify(error))
    );
    this.datos.paginator = this.paginator;
    // this.buildForm();
    this.filteredOptions = this.formGroup.get('empleado').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  // Nuevo paginacion y filtrado
  applyFilter(filterValue: any, campo: string) {    

    this.datos.filter = '' + Math.random();
  }

  // Reabrir
  reabrir(): void{
    if (this.selection.isEmpty()){
      console.log('vacio');
    } else{
      let lista = this.selection.selected;
      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        element.disabled = false;
        console.log(element);
      }
      this.flagModificar = true;
      this.flagReabrir = false;
    }
  }

  // Modificar
  modificar(): void{
    if (this.selection.isEmpty()){
      console.log('vacio');
    } else{
      let lista = this.selection.selected;
      var requestCanonical: RequestCanonical = {lstAssistanceTo: []};
      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        if (element.disabled == false) {
          element.disabled = true;
          console.log(element);

          let assistanceTo: AssistanceTo = this.lstAsistencias.find(assistance => assistance.assistancePK === Number(element.position));;
          assistanceTo.assistanceDay = element.dia;
          const horasYminutosEntrada = element.hora_ingreso.split(':');
          // tslint:disable-next-line: max-line-length
          assistanceTo.entryTime = new Date( element.dia.getFullYear(), element.dia.getMonth(), element.dia.getDate(), Number(horasYminutosEntrada[0]), Number(horasYminutosEntrada[1]), 0);
          const horasYminutosSalida = element.hora_salida.split(':');
          // tslint:disable-next-line: max-line-length
          assistanceTo.depatureTime = new Date( element.dia.getFullYear(), element.dia.getMonth(), element.dia.getDate(), Number(horasYminutosSalida[0]), Number(horasYminutosSalida[1]), 0);
          requestCanonical.lstAssistanceTo.push(assistanceTo);
        }
      }
      console.log(requestCanonical);
      
      this._assistanceController.updateAssistance(requestCanonical).subscribe(
        data => {
          console.log(data);
        },
        error => console.log(JSON.stringify(error))
      );

      this.flagModificar = false;
      this.flagReabrir = true;
    }
  }

}