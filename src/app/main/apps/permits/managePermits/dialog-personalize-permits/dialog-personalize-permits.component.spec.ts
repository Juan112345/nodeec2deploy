import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPersonalizePermitsComponent } from './dialog-personalize-permits.component';

describe('DialogPersonalizePermitsComponent', () => {
  let component: DialogPersonalizePermitsComponent;
  let fixture: ComponentFixture<DialogPersonalizePermitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPersonalizePermitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPersonalizePermitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
