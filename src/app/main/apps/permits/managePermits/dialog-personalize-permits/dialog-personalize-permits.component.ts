import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog-personalize-permits',
  templateUrl: './dialog-personalize-permits.component.html',
  styleUrls: ['./dialog-personalize-permits.component.scss']
})
export class DialogPersonalizePermitsComponent implements OnInit {
  favoriteSeason: any;
  operation: any;
  fechaTermino: Date;
  numeroIteraciones: number;

  toppingsControl = new FormControl([]);
  toppingList: string[] = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

  onToppingRemoved(topping: string) {
    const toppings = this.toppingsControl.value as string[];
    this.removeFirst(toppings, topping);
    this.toppingsControl.setValue(toppings); // To trigger change detection
  }

  private removeFirst<T>(array: T[], toRemove: T): void {
    const index = array.indexOf(toRemove);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }
  
  constructor(
    public dialogRef: MatDialogRef<DialogPersonalizePermitsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.operation = this.data;
  }

  ngOnInit() {
    console.log(this.operation);
  }

  onSaveForm(): void{
    const diasRepeticion = this.toppingsControl.value;

    for (let index = 0; index < diasRepeticion.length; index++) {
      if (diasRepeticion[index] == 'Domingo') {
        diasRepeticion[index] = 0;
      }
      if (diasRepeticion[index] == 'Lunes') {
        diasRepeticion[index] = 1;
      }
      if (diasRepeticion[index] == 'Martes') {
        diasRepeticion[index] = 2;
      }
      if (diasRepeticion[index] == 'Miercoles') {
        diasRepeticion[index] = 3;
      }
      if (diasRepeticion[index] == 'Jueves') {
        diasRepeticion[index] = 4;
      }
      if (diasRepeticion[index] == 'Viernes') {
        diasRepeticion[index] = 5;
      }
      if (diasRepeticion[index] == 'Sabado') {
        diasRepeticion[index] = 6;
      }
    }
    
    const opcionTermino = this.favoriteSeason;
    const fechaTermino = this.fechaTermino;
    const numeroIteraciones = this.numeroIteraciones;

    const object = {
      diasRepeticion: diasRepeticion,
      opcionTermino: opcionTermino,
      fechaTermino: fechaTermino,
      numeroIteraciones: numeroIteraciones
    };
    
    this.dialogRef.close({data: object});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
