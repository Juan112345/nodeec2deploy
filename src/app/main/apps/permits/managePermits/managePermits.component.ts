import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { DialogPersonalizePermitsComponent } from './dialog-personalize-permits/dialog-personalize-permits.component';

import {MatDialog} from '@angular/material/dialog';

import { PermitsController } from 'app/common/rest/services/PermitsController';
import { PermitsDayTo, PermitsTo, RequestCanonical } from 'app/common/rest/services/Beans';
import { PermitsDayController } from 'app/common/rest/services/PermitsDayController';
import { indexOf, split } from 'lodash';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { Router, RouterStateSnapshot } from '@angular/router';

interface registroPermist {
    descripcion: string;
    fecha: string;
    horaInicio: string;
    horaFin: string;
  }
const ELEMENT_DATA: registroPermist[] = [
    {descripcion: 'juan', fecha: '11/07/2020', horaInicio: '09:34:00',horaFin:'09:34'},
    {descripcion: 'kihara', fecha: '11/07/2020', horaInicio: '09:34',horaFin:'09:34'},
    {descripcion: 'yamil', fecha: '11/07/2020', horaInicio: '09:34',horaFin:'09:34'},
    {descripcion: 'clinsman', fecha: '11/07/2020', horaInicio: '09:34',horaFin:'09:34'},
    
  ];
@Component({
    selector     : 'managePermits',
    templateUrl  : './managePermits.component.html',
    styleUrls    : ['./managePermits.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    providers: [
      {provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check'}
    ]
})
export class ManagePermitsComponent implements OnInit, OnDestroy
{
  file: string;
  uri: string = "";
  fileToUpload: string = "Ningún archivo seleccionado";
    datoMomentaneo: any = [];
    tipoPermisoFlag = false;
    objectSelect: any[] = [];
    public formGroup2: FormGroup;
    columnas: string[] = ['check', 'descripcion', 'fecha', 'horaInicio', 'horaFinal'];
   // datos = ELEMENT_DATA;
    datos: MatTableDataSource<registroPermist>;
    selection = new SelectionModel<registroPermist>(true, []);

    motivos = ['Laborales', 'Personales', 'Lactancia', 'Cumpleaños', 'Estudios', 'Maternidad', 'Luto (Familiares directos)',
    'Salud'];

    frecuencia = ['Cada semana (El dia que inicia)', 'Una vez al mes (Dia que inicia)', 'Personalizar'];
    tipoPermisos = ['Diario', 'Periodica']

    options: RequestCanonical[] = [{employeeTo: {name: 'Clinsman', lastName: 'Campos Cruz', employeePk: 2, employeeCompleteName: 'Clinsman Campos Cruz'}},
    {employeeTo: {name: 'Yamil', lastName: 'Rios Riquelme', employeePk: 3, employeeCompleteName: 'Yamil Rios Riquelme'}}];
    filteredOptions: Observable<RequestCanonical[]>;

    today = new Date().toISOString().substring(0, 10);
    get diaAsistencia() { return this.formGroup2.get('diaAsistencia').value; }
    get diaAsistenciaFinal() { return this.formGroup2.get('diaAsistenciaFinal').value; }
    get empleado() { return this.formGroup2.get('empleado').value; }
    get tipoPermisoSelect() { return this.formGroup2.get('tipoPermisoSelect').value; }
    get motivo() { return this.formGroup2.get('motivo').value; }
    get comentario() { return this.formGroup2.get('comentario').value; }
    get tipoFrecuenciaSelected() { return this.formGroup2.get('tipoFrecuenciaSelected').value; }
    get horaInicial() { return this.formGroup2.get('horaInicial').value; }
    get horaFinal() { return this.formGroup2.get('horaFinal').value; }
    get checked() { return this.formGroup2.get('checked').value; }

  estadoV: string = "";
  motivoV: string = "";

  horaFinalPermitDay: string[] = [];
  horaInicioPermitDay: string[] = [];

  employeePk: number;
  empleadoSeleccionado: number;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    constructor(
      private formBuilder: FormBuilder, 
      public dialog: MatDialog, 
      private _permitsController: PermitsController,
      private _permitsDayController: PermitsDayController,
      private _employeeController: EmployeeController,
      private router: Router)
    {
      // (Inicio) -- Obtener query params data
      const snapshot: RouterStateSnapshot = router.routerState.snapshot;
      console.log(snapshot.root.queryParams.empleado);  // <-- hope it helps
      // (Fin) -- Obtener query params data
      const dato = ELEMENT_DATA;
      this.datos = new MatTableDataSource(dato);
    }
    ngOnInit(): void
    {
      /*const prueba: RequestCanonical = {permitsTo: {permitsPK: 2}};
      this._permitsController.getListPermitsWithPermitsPk(prueba).subscribe(
        data => {
          console.log(data);
          
        },
        error => console.log(JSON.stringify(error))
      );*/
      // VERIFICAR SI ES SMART O ADMINISTRADOR
      const currentSession = JSON.parse(localStorage.getItem('currentSession'));
      const nombre: string = currentSession.username;
      this._employeeController.findByUsername(nombre).subscribe(
              data => {
                  console.log(data);
                  this.employeePk = data.employeePk;
              },
              error => console.log(JSON.stringify(error))
      );
      const requestCanonical: RequestCanonical = {
        employeeTo: {}
      };
      this._employeeController.getListEmployee(requestCanonical).subscribe(
              data => {
                  console.log(data);
                  const lstEmployee: RequestCanonical[] = [];
                  for (let index = 0; index < data.lstEmployeeTo.length; index++) {
                    const element = data.lstEmployeeTo[index];
                    element.employeeCompleteName = `${element.name} ${element.lastName}`
                    const momentaneo: RequestCanonical = {};
                    momentaneo.employeeTo = element;
                    lstEmployee.push(momentaneo)
                  }
                  this.options = lstEmployee;
                  this.filteredOptions = this.formGroup2.get('empleado').valueChanges.pipe(
                    startWith(''),
                    map(value => this._filter(value))
                    );
              },
              error => console.log(JSON.stringify(error))
      );
      this.datos.paginator = this.paginator;
      this.createForm();
      
    }
    private _filter(value: string): RequestCanonical[] {
      const filterValue = value.toLowerCase();

      return this.options.filter(option => (option.employeeTo.employeeCompleteName.toLowerCase().includes(filterValue)));
    }
    ngOnDestroy(): void
    {
        
    }
    private createForm(){
      const tipoPermisoSelect = '';
      const tipoFrecuenciaSelected = '';
      const motivo = '';
      const empleado = '';
      this.formGroup2 = this.formBuilder.group({
       // name:this.name,
        tipoPermisoSelect: tipoPermisoSelect,
        tipoFrecuenciaSelected: tipoFrecuenciaSelected,
        motivo: motivo,
        empleado: empleado,
        diaAsistencia: this.today,
        diaAsistenciaFinal: this.today,
        comentario: '',
        horaInicial: '09:00',
        horaFinal: '09:00',
        checked: false
      })
    }

    onSelectionChange(event){
      console.log("Cambio el select" + event);  
      this.empleadoSeleccionado = event;    
    }

    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.datos.data.length;
      return numSelected === numRows;
    }
    deselectObject(){
     this.objectSelect=this.selection.selected;
     var object: any[] = [];
     this.objectSelect= object;
     this.selection.clear();
    }  
    masterToggle() {
      this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
     }
    checkboxLabel(row?: registroPermist): string {
      if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
     }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.horaInicio + 1}`;
    }

    tipoPermiso(option: any): void{
      if (option == 'Diario') {
        this.tipoPermisoFlag = true;
      } else if (option == 'Periodica'){
        this.tipoPermisoFlag = false;
      }
    }

    getDates() {
      if (this.tipoPermisoSelect == 'Diario') {
        const today = this.diaAsistencia;
        const endDate = this.diaAsistenciaFinal;
        // const today = new Date();
        // const endDate = new Date(today);
        // endDate.setDate( endDate.getDate() + 3);
        // ========================================
        let dates = [];
        // to avoid modifying the original date
        const theDate = new Date(today);
        while (theDate < endDate) {
          dates = [...dates, new Date(theDate)];
          theDate.setDate(theDate.getDate() + 1);
        }
        dates = [...dates, endDate];
        console.log(dates);
        this.datoMomentaneo = [];
        for (let index = 0; index < dates.length; index++) {
          let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
          element.descripcion = this.comentario;
          element.fecha = dates[index];
          element.horaInicio = this.horaInicial;
          element.horaFin = this.horaFinal;
          this.horaFinalPermitDay[index] = this.horaFinal;
          this.horaInicioPermitDay[index] = this.horaInicial;
          console.log(element);
          
          this.datoMomentaneo.push(element);
        }
        this.datos = new MatTableDataSource(this.datoMomentaneo);
        this.datos.paginator = this.paginator;
      }else if (this.tipoPermisoSelect == 'Periodica') {
        this.tipoFrecuencia(this.tipoFrecuenciaSelected);
      }
      

      
      // return dates;
    }

    tipoFrecuencia(frecuencia) {
      if (frecuencia == 'Cada semana (El dia que inicia)') {
        // ==============================
        const today = this.diaAsistencia;
        const endDate = this.diaAsistenciaFinal;
        this.datoMomentaneo = [];
        this.tipoFrecuenciaSemana(today, endDate);
        
      }
      if (frecuencia == 'Una vez al mes (Dia que inicia)') {
        this.datoMomentaneo = [];
        this.tipoFrecuenciaMes(this.diaAsistencia, this.diaAsistenciaFinal);
      }
      if (frecuencia == 'Personalizar'){
        // Dialogo
        this.openDialog('agregar', 'prueba');
      }

    }

    tipoFrecuenciaSemana(diaPermisoInicial, diaPermisoFinal) {
      let fechaFinalPeriodica = new Date(diaPermisoInicial);
      console.log(fechaFinalPeriodica.getMonth());
      fechaFinalPeriodica = new Date(fechaFinalPeriodica.setMonth(fechaFinalPeriodica.getMonth()+2));
      
      while (diaPermisoInicial < fechaFinalPeriodica) {
        let dates = [];
        // to avoid modifying the original date
        const theDate = new Date(diaPermisoInicial);
        while (theDate < diaPermisoFinal) {
          dates = [...dates, new Date(theDate)];
          theDate.setDate(theDate.getDate() + 1);
        }
        dates = [...dates, diaPermisoFinal];
        console.log(dates);
        for (let index = 0; index < dates.length; index++) {
          let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
          element.descripcion = this.comentario;
          element.fecha = dates[index];
          element.horaInicio = this.horaInicial;
          element.horaFin = this.horaFinal;
          this.horaFinalPermitDay[index] = this.horaFinal;
          this.horaInicioPermitDay[index] = this.horaInicial;
          console.log(element);
            
          this.datoMomentaneo.push(element);
        }

        var fechaNueva = new Date(diaPermisoInicial);
        fechaNueva.setDate(fechaNueva.getDate() + (7 - fechaNueva.getDay()) % 7 + fechaNueva.getDay());
        console.log(fechaNueva);
        // const fechaTransitoria = new Date(fechaNueva);
        var fechaNuevaFinal = new Date(fechaNueva);
        fechaNuevaFinal.setDate(fechaNueva.getDate() - 1 + dates.length);
        console.log(fechaNuevaFinal);

        diaPermisoInicial = new Date(fechaNueva);
        diaPermisoFinal = new Date(fechaNuevaFinal);
        // this.tipoFrecuenciaSemana(fechaNueva, fechaNuevaFinal);

      } 
      this.datos = new MatTableDataSource(this.datoMomentaneo);
      this.datos.paginator = this.paginator;
    }
  
  tipoFrecuenciaMes(diaPermisoInicial, diaPermisoFinal){
    let fechaFinalPeriodica = new Date(diaPermisoInicial);
    const arrayFechas = this.posicionDiaPorMes(fechaFinalPeriodica.getDay(), fechaFinalPeriodica);
    const posicionDiaEnMes = this.numeroDeDiaEnMes(arrayFechas, fechaFinalPeriodica);
    const diaSemana = fechaFinalPeriodica.getDay();

    fechaFinalPeriodica = new Date(fechaFinalPeriodica.setMonth(fechaFinalPeriodica.getMonth()+6));
      
    while (diaPermisoInicial < fechaFinalPeriodica) {
      // Obtener el intervalo
      let dates = [];
      // to avoid modifying the original date
      const theDate = new Date(diaPermisoInicial);
      while (theDate < diaPermisoFinal) {
        dates = [...dates, new Date(theDate)];
        theDate.setDate(theDate.getDate() + 1);
      }
      dates = [...dates, diaPermisoFinal];

      // Guarda datos momentaneos para la tabla
      for (let index = 0; index < dates.length; index++) {
        let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
        element.descripcion = this.comentario;
        element.fecha = dates[index];
        element.horaInicio = this.horaInicial;
        element.horaFin = this.horaFinal;
        this.horaFinalPermitDay[index] = this.horaFinal;
        this.horaInicioPermitDay[index] = this.horaInicial;
        console.log(element);
            
        this.datoMomentaneo.push(element);
      }

      // Definir nuevas fechas para siguiente iteracion
      var fechaNueva = new Date(diaPermisoInicial);
      fechaNueva.setMonth(fechaNueva.getMonth() + 1);
      fechaNueva.setDate(1);
      const arrayFechasDos = this.posicionDiaPorMes(diaSemana, fechaNueva);
      fechaNueva = new Date(arrayFechasDos[posicionDiaEnMes]);

      var fechaNuevaFinal = new Date(fechaNueva);
      fechaNuevaFinal.setDate(fechaNueva.getDate() - 1 + dates.length);
      console.log(fechaNuevaFinal);

      diaPermisoInicial = new Date(fechaNueva);
      diaPermisoFinal = new Date(fechaNuevaFinal);

    } 
    this.datos = new MatTableDataSource(this.datoMomentaneo);
    this.datos.paginator = this.paginator;
  }

  posicionDiaPorMes(numberDay: number, fechaEvaluar: Date){
      var d = new Date(fechaEvaluar), month = d.getMonth(), mondays = [];
    
      d.setDate(1);
    
      // Get the first Monday in the month
      while (d.getDay() !== numberDay) {
        d.setDate(d.getDate() + 1);
      }
    
      // Get all the other Mondays in the month
      while (d.getMonth() === month) {
        mondays.push(new Date(d.getTime()));
        d.setDate(d.getDate() + 7);
      }
    
      return mondays;
  }

  numeroDeDiaEnMes(arrayFechas: Array<Date>, fechaBusqueda: Date) {
    let numero = 0;
    for (let index = 0; index < arrayFechas.length; index++) {
      const element = arrayFechas[index];
      if (fechaBusqueda.getTime() === element.getTime()) {
        numero = index;
      }
    }
    return numero;
  }

  registrarPermisos() {
    this.datoMomentaneo = [];
  }

  openDialog(operation: string, description_name: string) {
    // =====================================
    const dialogRef = this.dialog.open(DialogPersonalizePermitsComponent, {
      // width: '250px',
      data: {name: operation, animal: description_name},
      height: '550px',
      width: '600px',
      panelClass: 'my-panel',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.data.opcionTermino == 1){
        this.datoMomentaneo = [];
        this.normal(result.data);
      }else if(result.data.opcionTermino == 2){
        this.datoMomentaneo = [];
        this.fechaLimite(result.data);
      }else if(result.data.opcionTermino == 3){
        this.datoMomentaneo = [];
        this.iteraciones(result.data);
      }
    });
  }

  normal(data){
    const diasRepeticion = data.diasRepeticion;
    const diaAsistenciaFinal = new Date(this.diaAsistencia);
    diaAsistenciaFinal.setMonth(diaAsistenciaFinal.getMonth() + 6);
    const today = this.diaAsistencia;
    const endDate = diaAsistenciaFinal;
    
    let dates = [];
        // to avoid modifying the original date
    const theDate = new Date(today);
    while (theDate < endDate) {
      if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
        dates = [...dates, new Date(theDate)];
      }
      theDate.setDate(theDate.getDate() + 1);
    }
    dates = [...dates, endDate];
    console.log(dates);
    this.datoMomentaneo = [];
    for (let index = 0; index < dates.length; index++) {
          let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
          element.descripcion = this.comentario;
          element.fecha = dates[index];
          element.horaInicio = this.horaInicial;
          element.horaFin = this.horaFinal;
          this.horaFinalPermitDay[index] = this.horaFinal;
          this.horaInicioPermitDay[index] = this.horaInicial;
          console.log(element);
          
          this.datoMomentaneo.push(element);
    }
    this.datos = new MatTableDataSource(this.datoMomentaneo);
    this.datos.paginator = this.paginator;
  }

  fechaLimite(data){
    const diasRepeticion = data.diasRepeticion;
    const diaAsistenciaFinal = new Date(data.fechaTermino);
    const today = this.diaAsistencia;
    const endDate = diaAsistenciaFinal;
    
    let dates = [];
        // to avoid modifying the original date
    const theDate = new Date(today);
    while (theDate < endDate) {
      if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
        dates = [...dates, new Date(theDate)];
      }
      theDate.setDate(theDate.getDate() + 1);
    }
    dates = [...dates, endDate];
    console.log(dates);
    this.datoMomentaneo = [];
    for (let index = 0; index < dates.length; index++) {
          let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
          element.descripcion = this.comentario;
          element.fecha = dates[index];
          element.horaInicio = this.horaInicial;
          element.horaFin = this.horaFinal;
          this.horaFinalPermitDay[index] = this.horaFinal;
          this.horaInicioPermitDay[index] = this.horaInicial;
          console.log(element);
          
          this.datoMomentaneo.push(element);
    }
    this.datos = new MatTableDataSource(this.datoMomentaneo);
    this.datos.paginator = this.paginator;
  }
  iteraciones(data){
    const iteraciones = data.numeroIteraciones;
    const diasRepeticion = data.diasRepeticion;
    const diaAsistenciaFinal = new Date(this.diaAsistencia);
    diaAsistenciaFinal.setDate(diaAsistenciaFinal.getDate() + 7 * iteraciones);
    const today = this.diaAsistencia;
    const endDate = diaAsistenciaFinal;
    
    let dates = [];
        // to avoid modifying the original date
    const theDate = new Date(today);
    while (theDate < endDate) {
      if (diasRepeticion.indexOf(theDate.getDay()) != -1) {
        dates = [...dates, new Date(theDate)];
      }
      theDate.setDate(theDate.getDate() + 1);
    }
    dates = [...dates, endDate];
    console.log(dates);
    this.datoMomentaneo = [];
    for (let index = 0; index < dates.length; index++) {
          let element = {descripcion: '', fecha: '', horaInicio: '', horaFin: ''};
          element.descripcion = this.comentario;
          element.fecha = dates[index];
          element.horaInicio = this.horaInicial;
          element.horaFin = this.horaFinal;
          this.horaFinalPermitDay[index] = this.horaFinal;
          this.horaInicioPermitDay[index] = this.horaInicial;
          console.log(element);
          
          this.datoMomentaneo.push(element);
    }
    this.datos = new MatTableDataSource(this.datoMomentaneo);
    this.datos.paginator = this.paginator;
  }

  registrar(): void {
    let requestCanonical: RequestCanonical = {permitsTo:{}};
    requestCanonical.permitsTo.employeeFk = this.empleadoSeleccionado; // 4 antes ahora probando el get de colaborador
    requestCanonical.permitsTo.affectsHours = this.checked; // afecta horas
    requestCanonical.permitsTo.name = this.empleado;
    requestCanonical.permitsTo.description = this.comentario;
    requestCanonical.permitsTo.reason = this.motivo; 
    requestCanonical.permitsTo.startDate = this.diaAsistencia;
    requestCanonical.permitsTo.endDate = this.diaAsistenciaFinal;
    requestCanonical.permitsTo.support = this.uri;
    
    console.log(requestCanonical);
    
    this._permitsController.createPermits(requestCanonical).subscribe(
      data => {
        console.log(data);
        // Registro de dias
      var requestCanonicalDays: RequestCanonical = {permitsDayTo:{},lstPermitsDay:[]};
      for (let index = 0; index < this.datos.data.length; index++) {
        const element = this.datos.data[index];
        requestCanonicalDays.lstPermitsDay.push(this.llenadoPermitsDay(element, data.id, index));
      }
    console.log(requestCanonicalDays);
        
        // For para llenado de permisos a request
        this._permitsDayController.createPermitsDay(requestCanonicalDays).subscribe(
          dataDos => {
            console.log(dataDos);
          },
          error => console.log(JSON.stringify(error))
        );
      },
      error => console.log(JSON.stringify(error))
    );

  }

  llenadoPermitsDay(element: any, id: number, index: number): PermitsDayTo {
    var permitsDayToPrueba: PermitsDayTo;
    var requestCanonicalDays: RequestCanonical = {permitsDayTo:{},lstPermitsDay:[]};
    requestCanonicalDays.permitsDayTo.description = element.descripcion;
    var fechaMomentanea = new Date(element.fecha);
    requestCanonicalDays.permitsDayTo.permitsDayDate = fechaMomentanea;
    requestCanonicalDays.permitsDayTo.permitsDayEntryTime = new Date(this.permitsDayEntryTime(fechaMomentanea, index));
    requestCanonicalDays.permitsDayTo.permitsDayDepatureTime = new Date(this.permitsDayDepatureTime(fechaMomentanea, index));
    requestCanonicalDays.permitsDayTo.permitsPk = id;
    permitsDayToPrueba = requestCanonicalDays.permitsDayTo;
    return permitsDayToPrueba;
  }

  permitsDayDepatureTime(fechaMomentanea: Date, index: number): Date{
    var requestCanonicalDays: RequestCanonical = {permitsDayTo:{},lstPermitsDay:[]};
    var horasminutos = this.horaFinalPermitDay[index].split(':');
    var departureTime: Date;
    requestCanonicalDays.permitsDayTo.permitsDayDepatureTime = new Date(fechaMomentanea);
    requestCanonicalDays.permitsDayTo.permitsDayDepatureTime.setHours(Number(horasminutos[0]));
    requestCanonicalDays.permitsDayTo.permitsDayDepatureTime.setMinutes(Number(horasminutos[1]));
    departureTime = new Date(requestCanonicalDays.permitsDayTo.permitsDayDepatureTime);
    return departureTime;
  }

  permitsDayEntryTime(fechaMomentanea: Date, index: number): Date{
    var requestCanonicalDays: RequestCanonical = {permitsDayTo:{},lstPermitsDay:[]};
    var horasminutos = this.horaInicioPermitDay[index].split(':');
    var entryTime: Date;
    requestCanonicalDays.permitsDayTo.permitsDayEntryTime = new Date(fechaMomentanea);
    requestCanonicalDays.permitsDayTo.permitsDayEntryTime.setHours(Number(horasminutos[0]));
    requestCanonicalDays.permitsDayTo.permitsDayEntryTime.setMinutes(Number(horasminutos[1]));
    entryTime = new Date(requestCanonicalDays.permitsDayTo.permitsDayEntryTime);
    return entryTime;
  }

  handleFileInput(files: FileList) {
    if(files!=null){
        var reader = new FileReader();
        var evt = files.item(0);
        this.file = files.item(0).name;
        reader.onload = this.onLoadCallback.bind(this);
        reader.readAsBinaryString(evt);
    }
  }

  onLoadCallback = (event) => {
    this.uri = (btoa((event.target as FileReader).result.toString()));
  }
}
