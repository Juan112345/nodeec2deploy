import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DialogElementsExampleDialog } from 'assets/angular-material-examples/dialog-elements/dialog-elements-example';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { PermitsTo, RequestCanonical } from 'app/common/rest/services/Beans';
import { PermitsController } from 'app/common/rest/services/PermitsController';
import { Router } from '@angular/router';

interface infoEmploye {
  dni: number;
  employe: string;
  motivo: string;
  tipo: string;
  estado: string;
  dia_registro: Date;
  dia_fin_registro?: Date;
}

const ELEMENT_DATA: infoEmploye[] = [
  { dni: 1, employe: 'juan', motivo: 'LABORALES', tipo: 'periodica', estado: 'Pendiente', dia_registro: new Date('10/10/2020') },
  { dni: 2, employe: 'kihara', motivo: 'LABORALES', tipo: 'periodica', estado: 'Confirmado', dia_registro: new Date('10/17/2020') },
  { dni: 3, employe: 'yamil', motivo: 'PERSONALES', tipo: 'periodica', estado: 'Rechazado', dia_registro: new Date('10/27/2020') },
  { dni: 4, employe: 'clinsman', motivo: 'CAPACITACIÓN', tipo: 'periodica', estado: 'Confirmado', dia_registro: new Date('11/07/2020') },

];
@Component({
  selector: 'searchPermits',
  templateUrl: './searchPermits.component.html',
  styleUrls: ['./searchPermits.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class SearchPermitsComponent implements OnInit, OnDestroy {
  keyword = 'name';
  objectSelect: any[] = [];

  public formGroup: FormGroup;
  name: string = "";
  // varName: string = "";
  employe: string = "";
  employes = [
    {
      name: 'juan',

    },
    {
      name: 'yamil',
    },
    {
      name: 'clinsman',
    }
  ];
  estados = [
    {estado: 'Pendiente'},
    {estado: 'Confirmado'},
    {estado: 'Rechazado'}
];
  motivos = [
    {motivo: 'LABORALES'},
    {motivo: 'PERSONALES'},
    {motivo: 'PERMISOS ESPECIALES'},
    {motivo: 'CUMPLEAÑOS'},
    {motivo: 'LICENCIA POR PATERNIDAD'},
    {motivo: 'PROBLEMAS DE SALUD'},
    {motivo: 'HORARIO DE LACTANCIA'},
    {motivo: 'DESCANSO MÉDICO'},
  ];

  options: RequestCanonical[] = [{employeeTo: {name: 'Clinsman', lastName: 'Campos Cruz', employeePk: 2, employeeCompleteName: 'Clinsman Campos Cruz'}},
    {employeeTo: {name: 'Yamil', lastName: 'Rios Riquelme', employeePk: 3, employeeCompleteName: 'Yamil Rios Riquelme'}}];
  filteredOptions: Observable<RequestCanonical[]>;
 
  columnas: string[] = ['dni', 'employe', 'motivo', 'tipo', 'estado', 'actions'];
  datos: MatTableDataSource<infoEmploye>;
  selection = new SelectionModel<infoEmploye>(true, []);
  //datos = ELEMENT_DATA;
  blnModificar = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  today = new Date().toISOString().substring(0, 10);
  get diaAsistencia() { return this.formGroup.get('diaAsistencia').value; }
  get diaAsistenciaFinal() { return this.formGroup.get('diaAsistenciaFinal').value; }
  get empleado() { return this.formGroup.get('empleado').value; }
  get estado() { return this.formGroup.get('estado').value; }
  get motivo() { return this.formGroup.get('motivo').value; }

  estadoV: string = "";
  motivoV: string = "";

  employeePk: number;
  lstPermits: PermitsTo[] = []

  dato: infoEmploye[] = [];

  constructor(private router: Router, private formBuilder: FormBuilder, private _employeeController: EmployeeController, private _permitsController: PermitsController) {
    this.dato = ELEMENT_DATA;
    const requestCanonical: RequestCanonical = {permitsTo: {}};
    _permitsController.getListPermits(requestCanonical).subscribe(
      data => {
          console.log(data);
          this.lstPermits = data.lstPermits;
          const lstpermisoMomentaneo: infoEmploye[] = [];
          for (let index = 0; index < this.lstPermits.length; index++) {
            const element = this.lstPermits[index];
            const permisoMomentaneo: infoEmploye = {dni: null, employe: null, motivo: null, tipo: null, estado: null, dia_registro: null};
            permisoMomentaneo.dni = element.permitsPK;
            permisoMomentaneo.employe = `${element.name} ${element.lastName}`;
            permisoMomentaneo.motivo = element.reason;
            permisoMomentaneo.tipo = "";
            if (element.permitStatus == 1) {
              permisoMomentaneo.estado = 'Pendiente';
            } else if (element.permitStatus == 2){
              permisoMomentaneo.estado = 'Confirmado';
            } else if (element.permitStatus == 3){
              permisoMomentaneo.estado = 'Rechazado';
            }
            permisoMomentaneo.dia_registro = element.startDate;
            permisoMomentaneo.dia_fin_registro = element.endDate;
            lstpermisoMomentaneo.push(permisoMomentaneo);
          }
          this.dato = lstpermisoMomentaneo;
          this.datos = new MatTableDataSource(this.dato);
          this.datos.paginator = this.paginator;
          
      },
      error => console.log(JSON.stringify(error))
  );
  
    this.datos = new MatTableDataSource(this.dato);
    
    
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.datos.data.length;
    return numSelected === numRows;
  }
  deselectObject() {
    this.objectSelect = this.selection.selected;
    var object: any[] = [];
    this.objectSelect = object;
    this.selection.clear();
  }
  masterToggle() {
    this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: infoEmploye): string {
    if(this.selection.selected.length == 1){
      this.blnModificar = false;
    }else {
      this.blnModificar = true;
    }
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.dni + 1}`;
  }
  ngOnInit(): void {
    // VERIFICAR SI ES SMART O ADMINISTRADOR
    const currentSession = JSON.parse(localStorage.getItem('currentSession'));
    const nombre: string = currentSession.username;
    this._employeeController.findByUsername(nombre).subscribe(
            data => {
                console.log(data);
                this.employeePk = data.employeePk;
            },
            error => console.log(JSON.stringify(error))
    );
    const requestCanonical: RequestCanonical = {
      employeeTo: {}
    };
    this._employeeController.getListEmployee(requestCanonical).subscribe(
            data => {
                console.log(data);
                const lstEmployee: RequestCanonical[] = [];
                for (let index = 0; index < data.lstEmployeeTo.length; index++) {
                  const element = data.lstEmployeeTo[index];
                  element.employeeCompleteName = `${element.name}, ${element.lastName}`;
                  const momentaneo: RequestCanonical = {};
                  momentaneo.employeeTo = element;
                  lstEmployee.push(momentaneo)
                }
                this.options = lstEmployee;
                this.filteredOptions = this.formGroup.get('empleado').valueChanges
                .pipe(
                  startWith(''),
                  map(value => this._filter(value))
                );
            },
            error => console.log(JSON.stringify(error))
    );

    this.createForm();
    this.datos.paginator = this.paginator;
    this.datos.sort = this.sort;
    // this.datos.filterPredicate = this.getFilterPredicate();
    
  }
  private _filter(value: string): RequestCanonical[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => (option.employeeTo.employeeCompleteName.toLowerCase().includes(filterValue)));
  }

  ngOnDestroy(): void {

  }

  private createForm(){
    const estado ='';
    const motivo ='';
    const empleado ='';
    this.formGroup = this.formBuilder.group({
     //name:this.name,
      estado:estado,
      motivo:motivo,
      empleado: empleado,
      diaAsistencia: this.today,
      diaAsistenciaFinal: this.today,
    })
  }

  clickEmpleadoSeleccionado(empleadoPk: number){
    this.employeePk = empleadoPk;
  }

  limpiar(){
    const requestCanonical: RequestCanonical = {
      permitsTo: {
        employeeFk: null,
        permitStatus: null,
        reason: null,
        startDate: null,
        endDate: null
      }
    };
    this._permitsController.getListPermits(requestCanonical).subscribe(
      data => {
          console.log(data);
          this.lstPermits = data.lstPermits;
          const lstpermisoMomentaneo: infoEmploye[] = [];
          for (let index = 0; index < this.lstPermits.length; index++) {
            const element = this.lstPermits[index];
            const permisoMomentaneo: infoEmploye = {dni: null, employe: null, motivo: null, tipo: null, estado: null, dia_registro: null};
            permisoMomentaneo.dni = element.permitsPK;
            permisoMomentaneo.employe = `${element.name} ${element.lastName}`;
            permisoMomentaneo.motivo = element.reason;
            permisoMomentaneo.tipo = "";
            if (element.permitStatus == 1) {
              permisoMomentaneo.estado = 'Pendiente';
            } else if (element.permitStatus == 2){
              permisoMomentaneo.estado = 'Confirmado';
            } else if (element.permitStatus == 3){
              permisoMomentaneo.estado = 'Rechazado';
            }
            permisoMomentaneo.dia_registro = element.startDate;
            permisoMomentaneo.dia_fin_registro = element.endDate;
            lstpermisoMomentaneo.push(permisoMomentaneo);
          }
          this.dato = lstpermisoMomentaneo;
          this.datos = new MatTableDataSource(this.dato);
          this.datos.paginator = this.paginator;
          
      },
      error => console.log(JSON.stringify(error))
  );
  
    this.datos = new MatTableDataSource(this.dato);
  }

  applyFilter(){
    const requestCanonical: RequestCanonical = {
      permitsTo: {
        employeeFk: this.employeePk,
        permitStatus: this.estado,
        reason: this.motivo,
        startDate: this.diaAsistencia,
        endDate: this.diaAsistenciaFinal
      }
    };
    this._permitsController.getListPermits(requestCanonical).subscribe(
      data => {
          console.log(data);
          this.lstPermits = data.lstPermits;
          const lstpermisoMomentaneo: infoEmploye[] = [];
          for (let index = 0; index < this.lstPermits.length; index++) {
            const element = this.lstPermits[index];
            const permisoMomentaneo: infoEmploye = {dni: null, employe: null, motivo: null, tipo: null, estado: null, dia_registro: null};
            permisoMomentaneo.dni = element.permitsPK;
            permisoMomentaneo.employe = `${element.name} ${element.lastName}`;
            permisoMomentaneo.motivo = element.reason;
            permisoMomentaneo.tipo = "";
            if (element.permitStatus == 1) {
              permisoMomentaneo.estado = 'Pendiente';
            } else if (element.permitStatus == 2){
              permisoMomentaneo.estado = 'Confirmado';
            } else if (element.permitStatus == 3){
              permisoMomentaneo.estado = 'Rechazado';
            }
            permisoMomentaneo.dia_registro = element.startDate;
            permisoMomentaneo.dia_fin_registro = element.endDate;
            lstpermisoMomentaneo.push(permisoMomentaneo);
          }
          this.dato = lstpermisoMomentaneo;
          this.datos = new MatTableDataSource(this.dato);
          this.datos.paginator = this.paginator;
          
      },
      error => console.log(JSON.stringify(error))
  );
  
    this.datos = new MatTableDataSource(this.dato);
    
    
  }
  
  selectEvent(item) {
    console.log(item.name);
    this.employe=item.name;
  }

  modificarPermiso() {
    console.log("Entro a modificarEmpleado()");
    console.log(this.selection[0]);
    this.router.navigate(['/apps/permits/managePermits'], {
      queryParams: {
        // empleado: JSON.stringify(this.selection.selected[0]),
        modo: 'Actualizar',
        permisoPk: `${this.selection.selected[0].dni}`
      }
    });
  }

  confirmarPermiso(){
    const lstPermitsUpdate: PermitsTo[] = [];
    for (let indexSelected = 0; indexSelected < this.selection.selected.length; indexSelected++) {
      for (let indexPermits = 0; indexPermits < this.lstPermits.length; indexPermits++) {
        const permits = this.lstPermits[indexPermits];
        const permitsSelected = this.selection.selected[indexSelected];
        if (permitsSelected.dni == permits.permitsPK && permitsSelected.estado == 'Pendiente'){
          // Se puede filtrar desde el front no es seguro? revisar*
          permits.permitStatus = 2;
          lstPermitsUpdate.push(permits);
        }
      }
    }

    const requestCanonical: RequestCanonical = {
      lstPermits: lstPermitsUpdate
    }
    console.log(requestCanonical);
    
    /*this._permitsController.updatePermits(requestCanonical).subscribe(
      data => {
          console.log(data);
      },
      error => console.log(JSON.stringify(error))
  );*/
  }

  rechazarPermiso(){

  }
}
