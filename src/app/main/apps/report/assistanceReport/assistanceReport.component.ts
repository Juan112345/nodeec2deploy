import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { CalendarService } from '../../calendar/calendar.service';
import { isSameDay, isSameMonth, startOfDay } from 'date-fns';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay } from 'angular-calendar';
import { CalendarEventModel } from '../../calendar/event.model';



  
@Component({
    selector     : 'assistanceReport',
    templateUrl  : './assistanceReport.component.html',
    styleUrls    : ['./assistanceReport.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AssistanceReportComponent implements OnInit, OnDestroy
{
    actions: CalendarEventAction[];
    events: CalendarEvent[];
    refresh: Subject<any> = new Subject();
    activeDayIsOpen: boolean;
    selectedDay: any;
    view: string;
    viewDate: Date; 
    
    myControl = new FormControl();
    options: string[] = ['clinsman', 'yamil', 'juan', 'Clinsman Campos', 'Juan Balboa', 'Cris Martinez', 'Clinsman Campos Cruz', 'Juan Cruz Nomberto', 'Reynaldo', 'Silvia Jurado'];
  filteredOptions: Observable<string[]>;
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
    constructor(private formBuilder: FormBuilder,
        private _calendarService: CalendarService)
    {

        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = {date: startOfDay(new Date())};

        this.setEvents();
    }
    ngOnInit(): void
    {
        //this.createForm;
        this.filteredOptions = this.myControl.valueChanges
        .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
        this.refresh.subscribe(updateDB => {
            if ( updateDB )
            {
                this._calendarService.updateEvents(this.events);
            }
        });

        this._calendarService.onEventsUpdated.subscribe(events => {
            this.setEvents();
            this.refresh.next();
        });
        
    }
   
    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
    
        return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
      }
    ngOnDestroy(): void
    {
        
    }
   /* private createForm(){
        const empleado ='';
        this.formGroup3 = this.formBuilder.group({
         //name:this.name,
          empleado: empleado,
        })
    }*/
    
    setEvents(): void
    {
        this.events = this._calendarService.events.map(item => {
            item.actions = this.actions;
            return new CalendarEventModel(item);
        });
    }

    dayClicked(day: CalendarMonthViewDay): void
    {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if ( isSameMonth(date, this.viewDate) )
        {
            if ( (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0 )
            {
                this.activeDayIsOpen = false;
            }
            else
            {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    beforeMonthViewRender({header, body}): void
    {
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if ( _selectedDay )
        {
            /**
             * Set selected day style
             * @type {string}
             */
            _selectedDay.cssClass = 'cal-selected';
        }

    }
    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void
    {
        event.start = newStart;
        event.end = newEnd;
        // console.warn('Dropped or resized', event);
        this.refresh.next(true);
    }

   
}
