import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card'
import { MatSelectModule } from '@angular/material/select'
import { FuseSharedModule } from '@fuse/shared.module';

import { Register2Component } from 'app/main/pages/authentication/register-2/register-2.component';
import { FuseWidgetModule } from '@fuse/components';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import { ColorPickerModule } from 'ngx-color-picker';
import { CalendarModule as AngularCalendarModule, DateAdapter } from 'angular-calendar';
import { MatChipsModule, MatPaginatorModule, MatRadioModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AssistanceReportComponent } from './assistanceReport.component';

import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarService } from 'app/main/apps/calendar/calendar.service';




const routes = [
    {
        path     : '**',
        component: AssistanceReportComponent,
        
        resolve  : {
            chat: CalendarService
        }
    }
];

@NgModule({
    declarations: [
        AssistanceReportComponent,
        
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatCardModule,
        MatSelectModule,
        FuseSharedModule,
        MatButtonModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatTabsModule,
        MatTableModule,
        AngularCalendarModule.forRoot({
            provide   : DateAdapter,
            useFactory: adapterFactory
        }),
        ColorPickerModule,
        ChartsModule,
        NgxChartsModule,
        MatPaginatorModule,
        FuseSharedModule,
        FuseWidgetModule,
        MatAutocompleteModule,
       
      
    ],
    providers      : [
        CalendarService
    ],
})
export class AssistanceReportModule
{
}
