import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'regis-masivo',
  templateUrl: './regis-masivo.component.html',
  styleUrls: ['./regis-masivo.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RegisMasivoComponent implements OnInit {

  public formGroup: FormGroup;
  fileToUpload1: string = "";
  fileToUpload2: string = "";
  fileToUpload3: string = "";
  fileToUpload4: string = "";
  isHidden1: boolean = true;
  isHidden2: boolean = false;
  isHidden3: boolean = false;
  
  constructor(private formBuilder: FormBuilder, private router: Router) { }

  private buildForm() {
    const fileToUpload1 = '';
    const fileToUpload2 = '';
    const fileToUpload3 = '';
    const fileToUpload4 = '';
    this.formGroup = this.formBuilder.group({
      fileToUpload1: fileToUpload1,
      fileToUpload2: fileToUpload2,
      fileToUpload3: fileToUpload3,
      fileToUpload4: fileToUpload4
    });
  }

  ngOnInit() {
    this.buildForm();
  }

  handleFileInput(i:number,event: any) {
    switch(i){
      case 1: {
        if(event.target.files.item(0).name!=null){
          this.fileToUpload1 = event.target.files.item(0).name;
        } 
        break;
      }
      case 2: {
        break;
      }
      case 3: {
        break;
      }
      case 4: {
        if(event.target.files.item(0).name!=null){
          this.fileToUpload4 = event.target.files.item(0).name;
        } 
        break;
      }
      default: {
        break;
      }
    }
  }

  accionRegis(i:number){
    switch(i){
      case 1: {
        this.isHidden1 = false;
        this.isHidden2 = true;
        break;
      }
      case 2: {
        this.isHidden2 = false;
        this.isHidden3 = true;
        break;
      }
      case 3: {
        this.router.navigate(['/apps/requerimientos/buscar']);
        break;
      }
      default: {
        break;
      }
    }
  }

}
