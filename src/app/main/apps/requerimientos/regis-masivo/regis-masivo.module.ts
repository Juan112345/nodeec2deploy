import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisMasivoComponent } from './regis-masivo.component';
import { MatFormFieldModule, MatDividerModule, MatIconModule, MatInputModule, MatButtonModule, MatCardModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
      path     : '**',
      component: RegisMasivoComponent
  }
];


@NgModule({
  declarations: [
    RegisMasivoComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatFormFieldModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
  }),
  ]
})
export class RegisMasivoModule { }
