import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter, MatDateFormats, MatTableDataSource, MatPaginator, MatSort, MatCheckboxChange } from '@angular/material';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';

export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      let day: string = date.getDate().toString();
      day = +day < 10 ? '0' + day : day;
      let month: string = (date.getMonth() + 1).toString();
      month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${day}-${month}-${year}`;
    }
    return date.toDateString();
  }
}
export const APP_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'numeric' },
    dateA11yLabel: {
      year: 'numeric', month: 'long', day: 'numeric'
    },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};

@Component({
  selector: 'registry-request',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})

export class RegistryRequestComponent {
  nuevoRequerimiento: PeriodicElement;
  codigo: string = "";
  cliente: string = "";
  estado: string = "";
  descripcion: string = "";
  fechaCreacion: Date = new Date();
  indicador: string = "";
  centro: string = "";
  sistema: string = "";
  componente: string = "";
  detalle: string = "";
  desarrollo: number = 0;
  qa: number = 0;
  devops: number = 0;

  dataHoras:any[] = [];

  clientes = [
    { cliente: 'BVL' },
    { cliente: 'CAVALI' }
  ];

  estados = [
    { estado: 'Abierto' },
    { estado: 'Cerrado' }
  ];

  indicadores = [
    { indicador: 'Inversión' },
    { indicador: 'Gasto' },
    { indicador: 'Garantía' }
  ];

  sistemas = [
    { sistema: 'Wari' },
    { sistema: 'Factrack' },
    { sistema: 'Pagarés' },
    { sistema: 'Jira' }
  ];

  selectedTabIndex: number = 0;

  consumidas: number = 0;
  restantes: number = 0;

  labelDetalleHoras: Label[] = ['Consumidas', 'Restantes'];
  datosDetalleHoras: MultiDataSet = [[15,20]];
  tipoDetalleHoras: ChartType = 'doughnut';

  columnas: string[] = ['dia', 'detalle', 'hora', 'tipo', 'act'];

  datos: MatTableDataSource<PartElement>;

  @ViewChild('horasPorAprobarMatPaginator', { static: true }) paginator: MatPaginator;

  public formGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router) {
    //const dato = ELEMENT_DATA;
    this.datos = new MatTableDataSource();
  }

  ngOnInit(): void {
    this.buildForm();
    this.datos.paginator = this.paginator;
  }

  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if(index==1){
      this.restantes = this.desarrollo + this.qa + this.devops;
      var a: any[] = [[this.consumidas,this.restantes]];
      this.datosDetalleHoras = a;
    }
  }

  regresar() {
    this.router.navigate(['/apps/requerimientos/buscar']);
  }

  private buildForm() {
    const codigo = '';
    const cliente = '';
    const estado = '';
    const descripcion = '';
    const fechaCreacion = new Date();
    const indicador = '';
    const centro = '';
    const sistema = '';
    const componente = '';
    const detalle = '';
    const desarrollo = 0;
    const qa = 0;
    const devops = 0;
    const consumidas = 0;
    const restantes = 0;
    this.formGroup = this.formBuilder.group({
      codigo: codigo,
      cliente: cliente,
      estado: estado,
      descripcion: descripcion,
      fechaCreacion: fechaCreacion,
      indicador: indicador,
      centro: centro,
      sistema: sistema,
      componente: componente,
      detalle: detalle,
      desarrollo: desarrollo,
      qa: qa,
      devops: devops,
      consumidas: consumidas,
      restantes: restantes
    });
  }

  guardar() {
    if (this.codigo != "" && this.codigo != null) {
      this.nuevoRequerimiento = {
        codigo: this.codigo, cliente: this.cliente, estado: this.estado,
        descripcion: this.descripcion, fechaCreacion: formatDate(this.fechaCreacion, "dd-MM-yyyy", "en-ES"), indicador: this.indicador, centro: this.centro, sistema: this.sistema,
        componente: this.componente, detalle: this.detalle, desarrollo: this.desarrollo, qa: this.qa, devops: this.devops, lstDetHoras: this.datos.data
      };
      this.router.navigate(['/apps/requerimientos/buscar'], {
        queryParams: {
          requerimiento: this.codigo, cliente: this.cliente, estado: this.estado,
          descripcion: this.descripcion, fechaCreacion: formatDate(this.fechaCreacion, "dd-MM-yyyy", "en-ES"), indicador: this.indicador, centro: this.centro, sistema: this.sistema,
          componente: this.componente, detalle: this.detalle, desarrollo: this.desarrollo, qa: this.qa, devops: this.devops, total: this.desarrollo + this.qa + this.devops, lstDetHoras: this.datos.data
        }
      });
    }
  }

  limpiarInputs() {
    this.buildForm();
  }

  ngOnDestroy(): void {

  }

  registRow(element: PartElement, i:number): void {
    let a:any={dia: element.dia, detalleH: element.detalleH, hora: element.hora, tipo: element.tipo};
    this.consumidas+=element.hora;
    var a1: any[] = [[this.consumidas,this.restantes]];
    this.datosDetalleHoras = a1;
    this.dataHoras.push(a);
    this.datos.data.splice(i,1);
    this.datos.data.push(a);
    this.datos._updateChangeSubscription();
  }

  addRow(){
    let a:any={dia: new Date(), detalleH: "", hora: Number(""), tipo: ""};
    this.datos.data.push(a);
    this.datos._updateChangeSubscription();
  }

  deleteRow(element: PartElement, i: number) {
    if (i > -1) {
      this.datos.data.splice(i, 1);
      this.dataHoras.splice(i,1);
      this.consumidas-=element.hora;
      var a1: any[] = [[this.consumidas,this.restantes]];
    }
    this.datos._updateChangeSubscription();
  }

  limpiar1(){
    if(this.datos.data.length>this.dataHoras.length){
      for (let i = 0; i < this.datos.data.length; i++) {
        this.datos.data.splice(i,1);
      }
      this.datos._updateChangeSubscription();
    }
  }
}

export interface PeriodicElement {
  codigo: string;
  cliente: string;
  estado: string;
  descripcion: string;
  fechaCreacion: string;
  indicador: string;
  centro: string;
  sistema: string;
  componente: string;
  detalle: string;
  desarrollo: number;
  qa: number;
  devops: number;
  lstDetHoras: Array<PartElement>;
}

export interface PartElement{
  dia: string;
  detalleH: string;
  hora: number;
  tipo: string;
}

/*const ELEMENT_DATA: PartElement[] = [
  { dia: formatDate(new Date(), "dd-MM-yyyy", "en-ES"), detalleH: "", hora: 0, tipo: "" }
];*/

/*const ELEMENT_DATAA: PartElement[] = [
  { dia: "21-08-20", detalle: "Desarrollo Hallazgo Mejora", hora: 2, tipo: "QA" },
  { dia: "28-08-20", detalle: "Desarrollo bdd", hora: 110, tipo: "Gest conf" }
];*/