import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

interface controlVacation {
    id:string;
    empleado:string;
    fechaContra:string;
    timeWork:string;
    accumulateVacation:string;
    enjoyedVacation:string;
    enjoyedForVacation:string;
  }
  const ELEMENT_DATA: controlVacation[] = [
    {id:'1' , empleado: 'Desarrollador', fechaContra: '12/04/2020', timeWork:'3 años 3 meses',accumulateVacation:'10',enjoyedVacation:'5',enjoyedForVacation:'3'},
    {id: '2', empleado: 'Desarrollador', fechaContra: '12/04/2020', timeWork:'3 años 3 meses',accumulateVacation:'10',enjoyedVacation:'5',enjoyedForVacation:'3'},
    {id: '2', empleado: 'Desarroladdor', fechaContra: '12/04/2020', timeWork:'3 años 3 meses',accumulateVacation:'10',enjoyedVacation:'5',enjoyedForVacation:'3'},
    {id: '3', empleado: 'Desarrollador', fechaContra: '12/04/2020', timeWork:'3 años 3 meses',accumulateVacation:'10',enjoyedVacation:'5',enjoyedForVacation:'3'},
    
  ];
@Component({
    selector     : 'controlVacation',
    templateUrl  : './controlVacation.component.html',
    styleUrls    : ['./controlVacation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ControlVacationComponent implements OnInit, OnDestroy
{
    objectSelect:any[]=[];
    formGroup2: FormGroup;
    columnas: string[] = ['id', 'empleado','fechaContratacion', 'tiempoTrabajo', 'vacacionesAcumuladas', 'vacacionesGozadas', 'vacacionesPorGozar', 'actions'];
    datos: MatTableDataSource<controlVacation>;
    selection = new SelectionModel<controlVacation>(true, []);
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    private _unsubscribeAll: Subject<any>;
    dateInicial:Date;
   
    constructor(
        private _formBuilder: FormBuilder
    )
    {
      const dato = ELEMENT_DATA;
      this.datos = new MatTableDataSource(dato);
    }

   
    ngOnInit(): void
    {
      this.datos.paginator = this.paginator;
      this.datos.sort = this.sort;
    }

  
    ngOnDestroy(): void
    {

    }
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.datos.data.length;
      return numSelected === numRows;
    }
    deselectObject(){
     this.objectSelect=this.selection.selected;
     var object: any[] = [];
     this.objectSelect= object;
     this.selection.clear();
    }  
    masterToggle() {
      this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
     }
    checkboxLabel(row?: controlVacation): string {
      if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
     }
     return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }
}
